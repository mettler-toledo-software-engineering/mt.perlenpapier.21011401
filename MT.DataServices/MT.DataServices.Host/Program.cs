﻿using System;
using MT.DataServices.Host.Infrastructure;

namespace MT.DataServices.Host
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            new Bootstrapper(args).Run();
        }
    }
}
