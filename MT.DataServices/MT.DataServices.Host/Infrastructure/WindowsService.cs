﻿using MT.DataServices.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace MT.DataServices.Host.Infrastructure
{
    partial class WindowsService : ServiceBase
    {
        private PluginManagerService pluginManagerService;

        public WindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var options = HostOptions.Parse(args);

            var eventLogSink = new EventLogSink();
            var filteredLogSink = new LogFilter(eventLogSink, LogLevel.Warning);

            pluginManagerService = new PluginManagerService(options, filteredLogSink);

            pluginManagerService.ActivatePlugins();
            pluginManagerService.Start();
        }

        protected override void OnStop()
        {
            pluginManagerService.Stop();
            pluginManagerService.Dispose();
        }
    }
}
