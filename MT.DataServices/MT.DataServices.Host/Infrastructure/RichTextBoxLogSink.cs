﻿using MT.DataServices.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.DataServices.Host.Infrastructure
{
    /// <summary>
    /// Displays the log messages in a <see cref="RichTextBox"/> control.
    /// </summary>
    internal class RichTextBoxLogSink : ILogSink
    {
        private readonly RichTextBox textBox;

        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextBoxLogSink"/> class.
        /// </summary>
        public RichTextBoxLogSink(RichTextBox textBox)
        {
            if (textBox == null)
                throw new ArgumentNullException("textBox");

            this.textBox = textBox;
        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />.
        /// </summary>
        /// <param name="message">Message to handle.</param>
        public void Log(LogMessage message)
        {
            if (message == null)
                return;

            if (textBox.InvokeRequired)
                textBox.Invoke(new Action<LogMessage>(Log), message);
            else
            {
                textBox.SelectionStart = textBox.TextLength;
                textBox.SelectionColor = LogLevelToColor(message.Level);
                textBox.AppendText(message.ToString());
            }
        }

        private static Color LogLevelToColor(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Action:
                    return Color.Black;
                case LogLevel.Success:
                    return Color.DarkGreen;
                case LogLevel.Hint:
                    return Color.DarkCyan;
                case LogLevel.Warning:
                    return Color.DarkOrange;
                case LogLevel.Error:
                    return Color.DarkRed;
                case LogLevel.Fatal:
                    return Color.Red;
                default:
                    throw new ArgumentOutOfRangeException("level");
            }
        }
    }
}
