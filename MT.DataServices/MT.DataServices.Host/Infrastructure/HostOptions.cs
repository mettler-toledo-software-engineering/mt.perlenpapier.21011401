﻿using MT.DataServices.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Host.Infrastructure
{
    internal class HostOptions 
    {
        private HostOptions()
        {
        }

        /// <summary>
        /// Gets or sets if the host is running in production mode.
        /// </summary>
        public bool ProductionMode { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to attach a debugger when the process starts.
        /// </summary>
        public bool BreakOnStart { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to automatically start the services.
        /// </summary>
        public bool AutoStart { get; private set; }

        /// <summary>
        /// Gets or sets the path of the configuration file
        /// </summary>
        public string ConfigurationFileName { get; private set; }

        /// <summary>
        /// Parses command-line arguments and creates a <see cref="HostOptions"/> instance from this information.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static HostOptions Parse(string[] args)
        {
            var options = new HostOptions();

            foreach (var arg in args)
            {
                if (arg.StartsWith("/production", StringComparison.OrdinalIgnoreCase))
                    options.ProductionMode = true;
                else if (arg.StartsWith("/autoStart", StringComparison.OrdinalIgnoreCase))
                    options.AutoStart = true;
                else if (arg.StartsWith("/bos", StringComparison.OrdinalIgnoreCase))
                    options.BreakOnStart = true;
                else if (arg.StartsWith("/config:"))
                    options.ConfigurationFileName = GetCommandLineArgParameter(arg);
            }    

            return options;
        }

        /// <summary>
        /// Gets the parameter of a command line argument.
        /// </summary>
        /// <param name="arg">The argument to parse.</param>
        /// <returns>The parameter or null if no parameter was specified</returns>
        private static string GetCommandLineArgParameter(string arg)
        {
            if (arg == null)
                throw new ArgumentNullException("arg");

            int separator = arg.IndexOf(':');

            if (separator == -1)
                return null;
            else
                return arg.Substring(separator + 1).Trim();
        }
    }
}
