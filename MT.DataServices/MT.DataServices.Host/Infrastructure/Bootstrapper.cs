﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Windows.Forms;

namespace MT.DataServices.Host.Infrastructure
{
    internal class Bootstrapper
    {
        private readonly string[] args;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        internal Bootstrapper(string[] args)
        {
            this.args = args;
        }

        /// <summary>
        /// Runs the application logic.
        /// </summary>
        internal void Run()
        {
            if (Environment.UserInteractive)
            {
                // We are running as a normal windows application, show the main window.
                var runner = new InteractiveApplicationController(args);
                runner.Run();
            }
            else
            {
                // We are running as a windows service.
                ServiceBase.Run(new ServiceBase[] { new WindowsService() });
            }
        }

        /// <summary>
        /// Prints the specified message to a suitble target.
        /// </summary>
        /// <param name="message">The message to write.</param>
        private static void Echo(string message)
        {
            if (Environment.UserInteractive)
            {
                if (ConsoleUtils.IsConsoleVisible())
                    Console.WriteLine(message);
                else
                    MessageBox.Show(message, ProductInformation.FullName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                EventLog.WriteEntry(ProductInformation.ApplicationName, message, EventLogEntryType.Error);
        }
    }
}
