﻿using MT.DataServices.Host.Views;
using MT.DataServices.Logging;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MT.DataServices.Host.Infrastructure
{
    internal class InteractiveApplicationController
    {
        private readonly LogDistributor bootlog;
        private readonly HostOptions hostOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="InteractiveApplicationController"/> class.
        /// </summary>
        internal InteractiveApplicationController(string[] args)
        {
            hostOptions = HostOptions.Parse(args);
            bootlog = new LogDistributor();
            bootlog.LockDuringLogging = true;

            if (ConsoleUtils.IsConsoleVisible())
                bootlog.Add(new ConsoleLogSink());
                
        }

        /// <summary>
        /// Runs the application logic.
        /// </summary>
        internal void Run()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow(hostOptions, bootlog));
        }      
    }
}
