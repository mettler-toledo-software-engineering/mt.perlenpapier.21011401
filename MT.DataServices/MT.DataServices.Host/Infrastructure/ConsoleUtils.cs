﻿using System;
using System.IO;
using System.Linq;

namespace MT.DataServices.Host.Infrastructure
{
    internal static class ConsoleUtils
    {
        /// <summary>
        /// Determines whether a console window is visible.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if a console window is visible; otherwise, <c>false</c>.
        /// </returns>
        internal static bool IsConsoleVisible()
        {
            try
            {
                if (Console.WindowWidth > 0)
                    return true;
            }
            catch (IOException)
            {
            }

            return false;
        }
    }
}
