﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using MT.DataServices.Configuration;
using MT.DataServices.Hosting;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;

namespace MT.DataServices.Host.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    internal class PluginManagerService : IDisposable
    {
        private bool isDisposed;
        private readonly ILogSink bootlog;
        private readonly HostOptions options;
        private readonly ManualResetEvent stateChangedEvent = new ManualResetEvent(false);

        private PluginManager pluginManager;
        private HostConfiguration hostConfiguration;
        private RuntimeConfiguration runtimeConfiguration;

        private string configurationFileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManagerService"/> class.
        /// </summary>
        public PluginManagerService(HostOptions options, ILogSink bootlog)
        {
            if (options == null)
                throw new ArgumentNullException("options");

            this.options = options;
            this.bootlog = bootlog ?? DummyLogSink.Instance;

            LoadConfiguration();
            CheckEnvironment();
            CreatePluginManager();
        }

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        public RuntimeConfiguration RuntimeConfiguration
        {
            get { return runtimeConfiguration; }
        }

        /// <summary>
        /// Gets the host configuration.
        /// </summary>
        public HostConfiguration HostConfiguration
        {
            get { return hostConfiguration; }
        }

        /// <summary>
        /// Occures when the state of the plugin manager changes
        /// </summary>
        public event EventHandler StateChanged;

        /// <summary>
        /// Occures when the busy state of the plugin manager changes
        /// </summary>
        public event EventHandler IsBusyChanged;

        /// <summary>
        /// Gets the current state of the plugin manager service.
        /// </summary>
        public PluginManagerState State
        {
            get { return pluginManager.State; }
        }

        /// <summary>
        /// Gets a value indicating whether the plugin manager service is currenty busy.
        /// </summary>
        public bool IsBusy
        {
            get { return pluginManager.IsBusy; }
        }

        /// <summary>
        /// Gets the plugins.
        /// </summary>
        public IEnumerable<IPlugin> Plugins
        {
            get { return pluginManager.Plugins; }
        }

        /// <summary>
        /// Gets the list of active service containers.
        /// </summary>
        public ObservableCollection<IServiceContainer> ServiceContainers
        {
            get { return pluginManager.ServiceContainers; }
        }

        /// <summary>
        /// Activates the plugins asynchronously.
        /// </summary>
        public void ActivatePluginsAsync()
        {
            CheckNotDisposed();
            pluginManager.ActivatePluginsAsync();
        }

        /// <summary>
        /// Activates the plugins synchronously.
        /// </summary>
        public void ActivatePlugins()
        {
            CheckNotDisposed();
            SynchronousCall(pluginManager.ActivatePluginsAsync);
        }

        /// <summary>
        /// Starts the services asynchronously.
        /// </summary>
        public void StartAsync()
        {
            CheckNotDisposed();
            pluginManager.StartAsync();
        }

        /// <summary>
        /// Starts the services synchronously.
        /// </summary>
        public void Start()
        {
            CheckNotDisposed();
            SynchronousCall(pluginManager.StartAsync);
        }

        /// <summary>
        /// Stops the services asynchronously.
        /// </summary>
        public void StopAsync()
        {
            CheckNotDisposed();
            pluginManager.StopAsync();
        }

        /// <summary>
        /// Stops the services synchronously.
        /// </summary>
        public void Stop()
        {
            CheckNotDisposed();
            SynchronousCall(pluginManager.StopAsync);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (isDisposed)
                return;

            pluginManager.Dispose();
            isDisposed = true;
        }

        /// <summary>
        /// Saves the configuration.
        /// </summary>
        public void SaveConfiguration()
        {
            try
            {
                bootlog.Action("Saving configuration...");
                ConfigurationServices.SaveConfiguration(configurationFileName, hostConfiguration);
                bootlog.Success("Configuration saved.");
            }
            catch (UnauthorizedAccessException ex)
            {
                bootlog.Error("No access to file \"" + configurationFileName + "\". Do you need administrative privileges?", ex);
            }
            catch (IOException ex)
            {
                bootlog.Error("Cannot write file \"" + configurationFileName + "\".", ex);
            }
            catch (Exception ex)
            {
                bootlog.Fatal("Unexpected exception while writing configuration file \"" + configurationFileName + "\".", ex);
            }
        }

        #region Private Utility Methods

        private void OnIsBusyChanged(object sender, EventArgs e)
        {
            var isBusyChanged = IsBusyChanged;
            if (isBusyChanged != null)
                isBusyChanged(this, EventArgs.Empty);            
        }

        private void OnStateChanged(object sender, EventArgs e)
        {
            var stateChanged = StateChanged;
            if (stateChanged != null)
                stateChanged(this, EventArgs.Empty);

            if (!pluginManager.IsBusy)
                stateChangedEvent.Set();

            if (pluginManager.State == PluginManagerState.Starting)
                hostConfiguration.Freeze();

            if (pluginManager.State == PluginManagerState.Stopped)
                hostConfiguration.Unfreeze();
        }

        private void CheckNotDisposed()
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().Name);
        }

        private void SynchronousCall(Action action)
        {
            stateChangedEvent.Reset();

            action();
            stateChangedEvent.WaitOne();
        }

        #endregion

        #region Initialization Utilities

        private void LoadConfiguration()
        {
            bootlog.Action("Locating configuration file...");

            configurationFileName = options.ConfigurationFileName;
            if (string.IsNullOrEmpty(configurationFileName))
            {
                bootlog.Hint("No configuration file was provided, falling back to default location.");
                var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
                configurationFileName = Path.Combine(Path.GetDirectoryName(assembly.Location), "Configuration.xml");
            }

            if (!File.Exists(configurationFileName))
                bootlog.Warning("Could not find configuration file at \"" + configurationFileName + "\".");
            else
            {
                try
                {
                    bootlog.Action("Loading configuration from \"" + configurationFileName + "\".");
                    hostConfiguration = ConfigurationServices.LoadConfiguration(configurationFileName);
                }
                catch (Exception ex)
                {
                    bootlog.Error("Could not parse the configuration file.", ex);
                }
            }

            if (hostConfiguration == null)
            {
                bootlog.Hint("No configuration loaded, autogenerating...");
                hostConfiguration = new HostConfiguration()
                {
                    PluginFolders = Path.Combine("%HOME%", "Plugins"),
                    DataFolder = Path.Combine("%HOME%", "Data"),
                    LogFolder = Path.Combine("%HOME%", "Logs")
                };
            }

            runtimeConfiguration = new RuntimeConfiguration(hostConfiguration);
            bootlog.Success("Configuration loaded.");
        }

        private void CheckEnvironment()
        {
            bootlog.Action("Checking environment...");

            runtimeConfiguration.DataFolder = TryCreateDirectoryWithFallback(runtimeConfiguration.DataFolder, Path.Combine(Path.GetTempPath(), "MT.DataServices", "Data"));
            runtimeConfiguration.LogFolder = TryCreateDirectoryWithFallback(runtimeConfiguration.LogFolder, Path.Combine(Path.GetTempPath(), "MT.DataServices", "Logs"));
        }

        private string TryCreateDirectoryWithFallback(string directoryName, string alternativeName)
        {
            if (TryCreateDirectory(directoryName))
                return directoryName;

            bootlog.Warning("Required directory could not be created, falling back to \"" + alternativeName + "\".");
            if (TryCreateDirectory(alternativeName))
                return alternativeName;

            throw new InitializationException("Essential directory could not be created.");
        }

        private bool TryCreateDirectory(string directoryName)
        {
            if (!Directory.Exists(directoryName))
            {
                bootlog.Hint("Directory not found, creating: \"" + directoryName + "\".");
                try
                {
                    Directory.CreateDirectory(directoryName);
                    return true;
                }
                catch (UnauthorizedAccessException ex)
                {
                    bootlog.Error("No access to directory \"" + directoryName + "\". Do you need administrative privileges?", ex);
                    return false;
                }
                catch (IOException ex)
                {
                    bootlog.Error("Could not create directory \"" + directoryName + "\".", ex);
                    return false;
                }
            }
            else
                return true;
        }

        private void CreatePluginManager()
        {
            var sourceFolders = GetValidPluginFolders();
            ComposablePartCatalog catalog;

            if (sourceFolders.Length == 0)
                catalog = new TypeCatalog();
            else
                catalog = new AggregateCatalog(sourceFolders.Select(f => PluginManager.CreateCatalogFromDirectory(f)));

            pluginManager = new PluginManager(runtimeConfiguration, catalog);

            pluginManager.AttachLogSink(bootlog);
            pluginManager.IsBusyChanged +=OnIsBusyChanged;
            pluginManager.StateChanged +=OnStateChanged;
        }

        private string[] GetValidPluginFolders()
        {
            var pluginFolders = new List<string>();

            foreach (var folder in runtimeConfiguration.PluginFolders.Split(';'))
            {
                if (!string.IsNullOrWhiteSpace(folder))
                {
                    var fullFolderPath = Path.GetFullPath(folder);

                    // The HomeFolder is implicitly used a plugin folder, therefore we don't have to include it here
                    // (in fact we can't, otherwise the plugins in the home folder would be loaded twice.)
                    if (Directory.Exists(fullFolderPath))
                    {
                        if (!runtimeConfiguration.HomeFolder.Equals(fullFolderPath, StringComparison.OrdinalIgnoreCase))
                            pluginFolders.Add(fullFolderPath);
                    }
                    else
                        bootlog.Warning("Plugin folder not found: \"" + fullFolderPath + "\".");
                }
            }

            return pluginFolders.ToArray();
        }

        #endregion
    }
}
