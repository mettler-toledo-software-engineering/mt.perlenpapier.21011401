﻿using System;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Host.Infrastructure;
using MT.DataServices.Logging;
using System.Threading.Tasks;
using MT.DataServices.Hosting;
using System.Collections.Specialized;
using MT.DataServices.Plugins;
using System.Diagnostics;

namespace MT.DataServices.Host.Views
{
    /// <summary>
    /// Contains the logic for the main window
    /// </summary>
    internal partial class MainWindow : Form
    {
        private readonly HostOptions hostOptions;
        private readonly LogDistributor bootlog;

        private PluginManagerService pluginManagerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        internal MainWindow(HostOptions hostOptions, LogDistributor bootlog)
        {
            if (hostOptions == null)
                throw new ArgumentNullException("hostOptions");

            if (bootlog == null)
                throw new ArgumentNullException("bootlog");
            
            this.bootlog = bootlog;
            this.hostOptions = hostOptions;

            InitializeComponent();

            // Allow to attach a debugger when the application starts
            if (hostOptions.BreakOnStart)
            {
                if (!Debugger.IsAttached)
                    Debugger.Launch();

                Debugger.Break();
            }

            bootlog.Add(new RichTextBoxLogSink(systemLogTextBox));
        }

        #region UI Element Event Handlers

        private void OnMainWindowLoad(object sender, EventArgs e)
        {
            BeginInvoke(new Action(LateInitialization));
        }

        private void OnMainWindowClosing(object sender, FormClosingEventArgs e)
        {
            if (pluginManagerService != null)
            {
                if (hostOptions.ProductionMode && pluginManagerService.State != PluginManagerState.Disposed)
                {
                    var prompt = "Sind sie sicher, dass sie die Anwendung schliessen möchten?";
                    if (MessageBox.Show(prompt, ProductInformation.FullName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                if (pluginManagerService.State != PluginManagerState.Disposed)
                {
                    Task.Factory.StartNew(pluginManagerService.Dispose);
                    e.Cancel = true;
                }
            }
        }

        private void OnMainWindowClosed(object sender, FormClosedEventArgs e)
        {
            // Hide the notify icon before closing the window
            // to prevent a bug which causes the icon to stay visible
            // even though the application is not running anymore.
            notifyIcon.Visible = false;
        }

        private void OnConfigureMenuItemClick(object sender, EventArgs e)
        {
            OpenConfigurationWindow();            
        }
  
        private void OnStartServicesMenuItemClick(object sender, EventArgs e)
        {
            StartServices();
        }
  
        private void OnStopServicesMenuItemClick(object sender, EventArgs e)
        {
            StopServices();
        }

        private void OnQuitMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnConfigureNotifyMenuItemClick(object sender, EventArgs e)
        {
            OpenConfigurationWindow();
        }

        private void OnStartServicesNotifyMenuItemClick(object sender, EventArgs e)
        {
            StartServices();
        }

        private void OnStopServicesNotifyMenuItemClick(object sender, EventArgs e)
        {
            StopServices();
        }

        private void OnQuitNotifyMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnMainWindowResize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Visible = false;
                notifyIcon.Visible = true;
            }
            else
                notifyIcon.Visible = false;
        }

        private void OnNotifyIconMouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visible = true;
            WindowState = FormWindowState.Normal;
            Activate();
        }

        #endregion

        /// <summary>
        /// Invokes the late initializtion tasks when the window has been completely loaded and rendered.
        /// </summary>
        private void LateInitialization()
        {
            // If we are in production mode, start minimized
            if (hostOptions.ProductionMode)
                WindowState = FormWindowState.Minimized;

            Task.Factory.StartNew(() =>
            {               
                bootlog.Action("Initializing plugin hosting services...");
                try
                {
                    pluginManagerService = new PluginManagerService(hostOptions, bootlog);
                }
                catch (Exception ex)
                {
                    bootlog.Fatal("An unexpected exception occured during the initialization of the plugin manager.", ex);

                    configureNotifyMenuItem.Enabled = configureMenuItem.Enabled = false;
                    startServicesNotifyMenuItem.Enabled = startServicesMenuItem.Enabled = false;
                    stopServicesNotifyMenuItem.Enabled = stopServicesMenuItem.Enabled = false;
                }

                if (!hostOptions.ProductionMode)
                    pluginManagerService.RuntimeConfiguration.DebugMode = true;

                pluginManagerService.StateChanged += OnPluginManagerServiceStateChanged;

                pluginManagerService.ServiceContainers.CollectionChanged += OnServiceContainerCollectionChanged;

                pluginManagerService.ActivatePluginsAsync();
            });
        }

        /// <summary>
        /// Called when a <see cref="IServiceContainer"/> has been created or disposed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
        private void OnServiceContainerCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;

            if (InvokeRequired)
                BeginInvoke(new NotifyCollectionChangedEventHandler(OnServiceContainerCollectionChanged), sender, e);
            else
            {
                foreach (IServiceContainer serviceContainer in e.NewItems)
                {
                    serviceLogsTabControl.TabPages.Add(new ServiceContainerLogTabPage(serviceContainer));
                }
            }
        }

        /// <summary>
        /// Called when the plugin manager service state changed and updates the UI state accordingly.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnPluginManagerServiceStateChanged(object sender, EventArgs e)
        {
            if (InvokeRequired)
                BeginInvoke(new EventHandler(OnPluginManagerServiceStateChanged), sender, e);
            else
            {
                var statusString = PluginManagerStateToString(pluginManagerService.State);
                statusLabel.Text = statusString;
                notifyIcon.Text = "MT.DataServices - " + statusString;
                Text = "MT.DataServices - " + statusString;

                switch (pluginManagerService.State)
                {
                    case PluginManagerState.Running:
                        configureNotifyMenuItem.Enabled = configureMenuItem.Enabled = false;
                        startServicesNotifyMenuItem.Enabled = startServicesMenuItem.Enabled = false;
                        stopServicesNotifyMenuItem.Enabled = stopServicesMenuItem.Enabled = true;
                        break;
                    case PluginManagerState.Activated:
                    case PluginManagerState.Stopped:
                        configureNotifyMenuItem.Enabled = configureMenuItem.Enabled = true;
                        startServicesNotifyMenuItem.Enabled = startServicesMenuItem.Enabled = true;
                        stopServicesNotifyMenuItem.Enabled = stopServicesMenuItem.Enabled = false;
                        break;
                    default:
                        configureNotifyMenuItem.Enabled = configureMenuItem.Enabled = false;
                        startServicesNotifyMenuItem.Enabled = startServicesMenuItem.Enabled = false;
                        stopServicesNotifyMenuItem.Enabled = stopServicesMenuItem.Enabled = false;
                        break;
                }

                // Auto-start the services in production mode
                if (pluginManagerService.State == PluginManagerState.Activated)
                {
                    var plugins = pluginManagerService.Plugins.ToList();
                    foreach (var plugin in plugins)
                    {
                        bootlog.Success(string.Format("Activated plugin {0} (v{1}).", plugin.Name, plugin.Version));
                    }

                    if (plugins.Count == 0)
                        bootlog.Warning("No plugins found.");

                    if (hostOptions.ProductionMode || hostOptions.AutoStart)
                        pluginManagerService.StartAsync();
                }

                // If the plugin manager was disposed, we can close the window now.
                if (pluginManagerService.State == PluginManagerState.Disposed)
                {
                    Close();
                }
            }
        }

        private string PluginManagerStateToString(PluginManagerState state)
        {
            switch (state)
            {
                case PluginManagerState.Idle:
                    return "Bereit";
                case PluginManagerState.Activating:
                    return "Aktiviere Plugins...";
                case PluginManagerState.Activated:
                    return "Bereit";
                case PluginManagerState.Starting:
                    return "Dienste werden gestartet...";
                case PluginManagerState.Running:
                    return "Dienste gestartet";
                case PluginManagerState.Stopping:
                    return "Dienste werden gestoppt...";
                case PluginManagerState.Stopped:
                    return "Dienste gestoppt";
                case PluginManagerState.Disposing:
                    return "Plugins werden beendet...";
                case PluginManagerState.Disposed:
                    return "Plugins beendet.";
                case PluginManagerState.Faulted:
                    return "Ein unerwarteter Fehler ist aufgetreten.";
                default:
                    return "Status unbekannt.";
            }
        }

        private void OpenConfigurationWindow()
        {
            new ConfigurationWindow(pluginManagerService).ShowDialog();
        }

        private void StartServices()
        {
            if (pluginManagerService.State == PluginManagerState.Activated ||
                pluginManagerService.State == PluginManagerState.Stopped)
            {
                for (; ; )
                {
                    var serviceContainerPage = serviceLogsTabControl.TabPages.OfType<ServiceContainerLogTabPage>().FirstOrDefault();
                    if (serviceContainerPage == null)
                        break;

                    serviceLogsTabControl.TabPages.Remove(serviceContainerPage);
                }

                pluginManagerService.StartAsync();
            }
        }

        private void StopServices()
        {
            if (pluginManagerService.State == PluginManagerState.Running)
            {
                pluginManagerService.StopAsync();
            }
        }       
    }
}
    