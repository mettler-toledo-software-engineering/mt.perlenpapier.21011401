﻿using System;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Host.Infrastructure;
using MT.DataServices.Plugins;

namespace MT.DataServices.Host.Views
{
    internal partial class ConfigurationWindow : Form, IPluginConfigurationHost
    {
        private PluginManagerService pluginManagerService;
        private EventHandler<PluginConfigurationHostClosingEventArgs> formClosing;
        private EventHandler<PluginConfigurationHostClosingEventArgs> formClosed;

        internal ConfigurationWindow(PluginManagerService pluginManagerService)
        {
            if (pluginManagerService == null)
                throw new ArgumentNullException("pluginManagerService");

            this.pluginManagerService = pluginManagerService;

            InitializeComponent();

            var configurationDialogs = pluginManagerService.Plugins.SelectMany(d => d.CreateConfigurationControl(this));
            foreach (var configurationDialog in configurationDialogs)
            {
                var tabPage = new TabPage(configurationDialog.Title);
                var control = configurationDialog.Control;
                tabPage.Controls.Add(control);
                control.Dock = DockStyle.Fill;
                tabPage.Tag = configurationDialog;

                configurationTabControl.TabPages.Add(tabPage);
            }
        }

        event EventHandler<PluginConfigurationHostClosingEventArgs> IPluginConfigurationHost.FormClosing
        {
            add { formClosing += value; }
            remove { formClosing -= value; }
        }

        event EventHandler<PluginConfigurationHostClosingEventArgs> IPluginConfigurationHost.FormClosed
        {
            add { formClosed += value; }
            remove { formClosed -= value; }
        }

        private void OnConfigurationWindowFormClosing(object sender, FormClosingEventArgs e)
        {
            if (formClosing != null)
            {
                PluginConfigurationHostClosingEventArgs innerEventArgs = new PluginConfigurationHostClosingEventArgs(DialogResult == DialogResult.OK);
                formClosing(sender, innerEventArgs);
                e.Cancel = innerEventArgs.Cancel;
            }
        }

        private void OnConfigurationWindowFormClosed(object sender, FormClosedEventArgs e)
        {
            if (formClosed != null)
            {
                var innerEventArgs = new PluginConfigurationHostClosingEventArgs(DialogResult == DialogResult.OK);
                formClosed(sender, innerEventArgs);

                if (innerEventArgs.Cancel)
                    throw new InvalidOperationException("Canceling in the FormClosed event is not supported. Use the FormClosing event to validate and cancel in case the new configuration is invalid.");
            }

            if (DialogResult == DialogResult.OK)
                pluginManagerService.SaveConfiguration();
        }

        public void EnsureConfigurationVisible(IPluginConfigurationPane pane)
        {
            if (pane == null)
                throw new ArgumentNullException("pane");

            var selectedTab = configurationTabControl.TabPages.Cast<TabPage>().FirstOrDefault(d => d.Tag == pane);
            if (selectedTab != null)
                configurationTabControl.SelectedTab = selectedTab;
        }
    }
}
