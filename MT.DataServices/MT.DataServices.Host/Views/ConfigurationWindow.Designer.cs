﻿namespace MT.DataServices.Host.Views
{
    partial class ConfigurationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationWindow));
            this.SubmitDialogButton = new System.Windows.Forms.Button();
            this.CancelDialogButton = new System.Windows.Forms.Button();
            this.configurationTabControl = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // SubmitDialogButton
            // 
            this.SubmitDialogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SubmitDialogButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SubmitDialogButton.Location = new System.Drawing.Point(680, 409);
            this.SubmitDialogButton.Name = "SubmitDialogButton";
            this.SubmitDialogButton.Size = new System.Drawing.Size(75, 23);
            this.SubmitDialogButton.TabIndex = 0;
            this.SubmitDialogButton.Text = "&OK";
            this.SubmitDialogButton.UseVisualStyleBackColor = true;
            // 
            // CancelDialogButton
            // 
            this.CancelDialogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelDialogButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelDialogButton.Location = new System.Drawing.Point(761, 409);
            this.CancelDialogButton.Name = "CancelDialogButton";
            this.CancelDialogButton.Size = new System.Drawing.Size(75, 23);
            this.CancelDialogButton.TabIndex = 0;
            this.CancelDialogButton.Text = "&Abbrechen";
            this.CancelDialogButton.UseVisualStyleBackColor = true;
            // 
            // configurationTabControl
            // 
            this.configurationTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.configurationTabControl.Location = new System.Drawing.Point(12, 12);
            this.configurationTabControl.Name = "configurationTabControl";
            this.configurationTabControl.SelectedIndex = 0;
            this.configurationTabControl.Size = new System.Drawing.Size(824, 391);
            this.configurationTabControl.TabIndex = 1;
            // 
            // ConfigurationWindow
            // 
            this.AcceptButton = this.SubmitDialogButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 444);
            this.Controls.Add(this.configurationTabControl);
            this.Controls.Add(this.SubmitDialogButton);
            this.Controls.Add(this.CancelDialogButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigurationWindow";
            this.Text = "Konfiguration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnConfigurationWindowFormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnConfigurationWindowFormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SubmitDialogButton;
        private System.Windows.Forms.Button CancelDialogButton;
        private System.Windows.Forms.TabControl configurationTabControl;
    }
}