﻿using MT.DataServices.Host.Infrastructure;
using MT.DataServices.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.DataServices.Host.Views
{
    internal class ServiceContainerLogTabPage : TabPage
    {
        private RichTextBox logRichTextBox;
        private RichTextBoxLogSink logSink;
        private readonly IServiceContainer serviceContainer;

        internal ServiceContainerLogTabPage(IServiceContainer serviceContainer)
        {
            this.serviceContainer = serviceContainer;

            InitializeComponent();

            Text = serviceContainer.Name;

            Controls.Add(logRichTextBox);

            logSink = new RichTextBoxLogSink(logRichTextBox);
            serviceContainer.AttachLogSink(logSink);
            
            serviceContainer.StateChanged += OnStateChanged;
        }

        void OnStateChanged(object sender, EventArgs e)
        {
            if (InvokeRequired)
                BeginInvoke(new EventHandler(OnStateChanged), sender, e);
            else
            {
                switch (serviceContainer.State)
                {
                    case ServiceState.Idle:
                        Text = serviceContainer.Name + " [Idle]";
                        break;
                    case ServiceState.Running:
                        Text = serviceContainer.Name + " [Running]";
                        break;
                    case ServiceState.Stopping:
                        Text = serviceContainer.Name + " [Stopping]";
                        break;
                    case ServiceState.Faulted:
                        Text = serviceContainer.Name + " [Faulted]";
                        break;
                }
            }
        }

        #region Auto-Generated Designer Code

        private void InitializeComponent()
        {
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logRichTextBox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.Size = new System.Drawing.Size(100, 96);
            this.logRichTextBox.TabIndex = 0;
            this.logRichTextBox.Text = "";
            this.ResumeLayout(false);

        }

        #endregion
    }
}
