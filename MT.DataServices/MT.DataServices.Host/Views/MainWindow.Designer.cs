﻿namespace MT.DataServices.Host.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.startServicesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServicesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.serviceLogsTabControl = new System.Windows.Forms.TabControl();
            this.systemTabPage = new System.Windows.Forms.TabPage();
            this.systemLogTextBox = new System.Windows.Forms.RichTextBox();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.notifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.configureNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.startServicesNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopServicesNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.quitNotifyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.serviceLogsTabControl.SuspendLayout();
            this.systemTabPage.SuspendLayout();
            this.notifyIconContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.servicesToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(656, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureMenuItem,
            this.toolStripMenuItem1,
            this.startServicesMenuItem,
            this.stopServicesMenuItem,
            this.toolStripMenuItem2,
            this.quitMenuItem});
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.servicesToolStripMenuItem.Text = "Services";
            // 
            // configureMenuItem
            // 
            this.configureMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Options;
            this.configureMenuItem.Name = "configureMenuItem";
            this.configureMenuItem.Size = new System.Drawing.Size(182, 22);
            this.configureMenuItem.Text = "Konfigurieren";
            this.configureMenuItem.Click += new System.EventHandler(this.OnConfigureMenuItemClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(179, 6);
            // 
            // startServicesMenuItem
            // 
            this.startServicesMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Play;
            this.startServicesMenuItem.Name = "startServicesMenuItem";
            this.startServicesMenuItem.Size = new System.Drawing.Size(182, 22);
            this.startServicesMenuItem.Text = "Alle Dienste starten";
            this.startServicesMenuItem.Click += new System.EventHandler(this.OnStartServicesMenuItemClick);
            // 
            // stopServicesMenuItem
            // 
            this.stopServicesMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Stop;
            this.stopServicesMenuItem.Name = "stopServicesMenuItem";
            this.stopServicesMenuItem.Size = new System.Drawing.Size(182, 22);
            this.stopServicesMenuItem.Text = "Alle Dienste stoppen";
            this.stopServicesMenuItem.Click += new System.EventHandler(this.OnStopServicesMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(179, 6);
            // 
            // quitMenuItem
            // 
            this.quitMenuItem.Name = "quitMenuItem";
            this.quitMenuItem.Size = new System.Drawing.Size(182, 22);
            this.quitMenuItem.Text = "Beenden";
            this.quitMenuItem.Click += new System.EventHandler(this.OnQuitMenuItemClick);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 345);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(656, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(79, 17);
            this.statusLabel.Text = "Bitte warten...";
            // 
            // serviceLogsTabControl
            // 
            this.serviceLogsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serviceLogsTabControl.Controls.Add(this.systemTabPage);
            this.serviceLogsTabControl.Location = new System.Drawing.Point(12, 27);
            this.serviceLogsTabControl.Name = "serviceLogsTabControl";
            this.serviceLogsTabControl.SelectedIndex = 0;
            this.serviceLogsTabControl.Size = new System.Drawing.Size(632, 315);
            this.serviceLogsTabControl.TabIndex = 2;
            // 
            // systemTabPage
            // 
            this.systemTabPage.Controls.Add(this.systemLogTextBox);
            this.systemTabPage.Location = new System.Drawing.Point(4, 22);
            this.systemTabPage.Name = "systemTabPage";
            this.systemTabPage.Size = new System.Drawing.Size(624, 289);
            this.systemTabPage.TabIndex = 0;
            this.systemTabPage.Text = "System";
            this.systemTabPage.UseVisualStyleBackColor = true;
            // 
            // systemLogTextBox
            // 
            this.systemLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemLogTextBox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemLogTextBox.Location = new System.Drawing.Point(0, 0);
            this.systemLogTextBox.Name = "systemLogTextBox";
            this.systemLogTextBox.Size = new System.Drawing.Size(624, 289);
            this.systemLogTextBox.TabIndex = 0;
            this.systemLogTextBox.Text = "";
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.notifyIconContextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "MT.DataServices";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnNotifyIconMouseDoubleClick);
            // 
            // notifyIconContextMenuStrip
            // 
            this.notifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureNotifyMenuItem,
            this.toolStripMenuItem3,
            this.startServicesNotifyMenuItem,
            this.stopServicesNotifyMenuItem,
            this.toolStripMenuItem4,
            this.quitNotifyMenuItem});
            this.notifyIconContextMenuStrip.Name = "notifyIconContextMenuStrip";
            this.notifyIconContextMenuStrip.Size = new System.Drawing.Size(183, 126);
            // 
            // configureNotifyMenuItem
            // 
            this.configureNotifyMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Options;
            this.configureNotifyMenuItem.Name = "configureNotifyMenuItem";
            this.configureNotifyMenuItem.Size = new System.Drawing.Size(182, 22);
            this.configureNotifyMenuItem.Text = "Konfigurieren";
            this.configureNotifyMenuItem.Click += new System.EventHandler(this.OnConfigureNotifyMenuItemClick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(179, 6);
            // 
            // startServicesNotifyMenuItem
            // 
            this.startServicesNotifyMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Play;
            this.startServicesNotifyMenuItem.Name = "startServicesNotifyMenuItem";
            this.startServicesNotifyMenuItem.Size = new System.Drawing.Size(182, 22);
            this.startServicesNotifyMenuItem.Text = "Alle Dienste starten";
            this.startServicesNotifyMenuItem.Click += new System.EventHandler(this.OnStartServicesNotifyMenuItemClick);
            // 
            // stopServicesNotifyMenuItem
            // 
            this.stopServicesNotifyMenuItem.Image = global::MT.DataServices.Host.Properties.Resources.Stop;
            this.stopServicesNotifyMenuItem.Name = "stopServicesNotifyMenuItem";
            this.stopServicesNotifyMenuItem.Size = new System.Drawing.Size(182, 22);
            this.stopServicesNotifyMenuItem.Text = "Alle Dienste stoppen";
            this.stopServicesNotifyMenuItem.Click += new System.EventHandler(this.OnStopServicesNotifyMenuItemClick);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(179, 6);
            // 
            // quitNotifyMenuItem
            // 
            this.quitNotifyMenuItem.Name = "quitNotifyMenuItem";
            this.quitNotifyMenuItem.Size = new System.Drawing.Size(182, 22);
            this.quitNotifyMenuItem.Text = "Beenden";
            this.quitNotifyMenuItem.Click += new System.EventHandler(this.OnQuitNotifyMenuItemClick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 367);
            this.Controls.Add(this.serviceLogsTabControl);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainWindow";
            this.Text = "MT.DataServices";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnMainWindowClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnMainWindowClosed);
            this.Load += new System.EventHandler(this.OnMainWindowLoad);
            this.Resize += new System.EventHandler(this.OnMainWindowResize);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.serviceLogsTabControl.ResumeLayout(false);
            this.systemTabPage.ResumeLayout(false);
            this.notifyIconContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem startServicesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopServicesMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.TabControl serviceLogsTabControl;
        private System.Windows.Forms.TabPage systemTabPage;
        private System.Windows.Forms.RichTextBox systemLogTextBox;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip notifyIconContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem configureNotifyMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem startServicesNotifyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopServicesNotifyMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem quitNotifyMenuItem;
    }
}