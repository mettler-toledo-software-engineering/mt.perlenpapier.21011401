﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using MT.DataServices;

// This file contains the global versioning and product information

namespace MT.DataServices
{
    /// <summary>
    /// Contains product-wide version information
    /// </summary>
    static class ProductInformation
    {
        /// <summary>
        /// Gets the application name
        /// </summary>
        public const string ApplicationName = "MT.DataServices";

        /// <summary>
        /// Gets the manufacturer
        /// </summary>
        public const string Manufacturer = "Mettler Toledo (Schweiz) GmbH";

        /// <summary>
        /// Gets the trademark
        /// </summary>
        public const string Trademark = "Mettler Toledo (Schweiz) GmbH";
        
        /// <summary>
        /// Gets the copyright
        /// </summary>
        public const string Copyright = "Copyright © Mettler Toledo (Schweiz) GmbH 2009-2021";

        /// <summary>
        /// Gets the version of the application 
        /// </summary>
        public const string Version = "5";

        /// <summary>
        /// Gets the full application name
        /// </summary>
        public const string FullName = ApplicationName + " " + Version;

#if DEBUG

        /// <summary>
        /// Gets the configuration name
        /// </summary>
        public const string Configuration = "Debug";
#else

        /// <summary>
        /// Gets the configuration name
        /// </summary>
        public const string Configuration = "Release";
#endif

        public const string PublicKey = "00240000048000009400000006020000002400005253413100040000010001002b37ee12841f512ac723ca2f04f3ba4232f9c7b174f40b0d35b50e7179514d381ea3d903dee949a1a38138302a7b9401c192f71527385fde0a6e0cff8a2360e35ad50ff41717b226de1748997eaa117fa5e61595487e394c8b2028972887a2a1c74541273168921e22c8f6a0c47fdc0e30777c510a3244c9cec5278ee6dbd1a9";
    }
}

