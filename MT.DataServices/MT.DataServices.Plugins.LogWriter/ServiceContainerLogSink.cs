﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MT.DataServices.Logging;

namespace MT.DataServices.Plugins.LogWriter
{
    /// <summary>
    /// Listens for the log messages of a single service container and delegates them to their configured target.
    /// </summary>
    internal sealed class ServiceContainerLogSink : ILogSink, IDisposable
    {
        private static readonly Regex removeInvalidChars = new Regex(string.Format("[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()))), RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.CultureInvariant);
        private readonly Plugin plugin;
        private readonly string pluginFileName;
        private readonly IServiceContainer source;

        private bool isDisposed;
        private RotatingLogFile rotatingLogFile;

        public ServiceContainerLogSink(Plugin plugin, IServiceContainer source)
        {
            this.plugin = plugin;
            this.source = source;

            if (plugin.Configuration.LogFilePerService)
            {
                var sourceName = source.Name;
                if (string.IsNullOrWhiteSpace(sourceName))
                    sourceName = "Unnamed";

                var pluginFileNameBase = Path.Combine(plugin.RuntimeConfiguration.LogFolder, removeInvalidChars.Replace(sourceName, "_"));
                pluginFileName = pluginFileNameBase + ".log";
                int counter = 0;
                lock (plugin.ProcessLogFileNames)
                {
                    while (!plugin.ProcessLogFileNames.Add(pluginFileName))
                    {
                        pluginFileName = pluginFileNameBase + (++counter).ToString() + ".log";
                    }
                }

                rotatingLogFile = new RotatingLogFile(pluginFileName, plugin.Configuration.MaximumLogFileSize, plugin.Configuration.MaximumLogFileCount);
            }

            source.AttachLogSink(this);
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                source.DetachLogSink(this);

                if (rotatingLogFile != null)
                {
                    rotatingLogFile.Dispose();
                    rotatingLogFile = null;
                }

                lock (plugin.ProcessLogFileNames)
                {
                    plugin.ProcessLogFileNames.Remove(pluginFileName);
                }

                isDisposed = true;
            }
        }

        void ILogSink.Log(LogMessage message)
        {
            if (message.Level < plugin.Configuration.MinimumLevel)
                return;

            if (plugin.Configuration.LogFilePerService)
            {
                if (rotatingLogFile != null)
                    rotatingLogFile.Log(message);
            }
            else
                plugin.LogSink.Log(message);
        }
    }
}
