﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using MT.DataServices.Configuration;
using MT.DataServices.Logging;

namespace MT.DataServices.Plugins.LogWriter
{
    /// <summary>
    /// Plugin for writing the log messages to disk and managing the log files.
    /// </summary>
    [Export(typeof(IPlugin))]
    public class Plugin : IPlugin
    {
        private LogWriterConfiguration configuration;
        private IPluginHost host;
        private readonly HashSet<string> processLogFileNames = new HashSet<string>();
        private readonly Dictionary<IServiceContainer, ServiceContainerLogSink> serviceContainerMapping = new Dictionary<IServiceContainer, ServiceContainerLogSink>();
        private RotatingLogFile rotatingLogFile;
        private LogFilter logFilter;

        [Import]
        internal ILogSink LogSink { get; private set; }

        /// <summary>
        /// Activates the plugin.
        /// </summary>
        /// <param name="host">The plugin host environment.</param>
        public void Activate(IPluginHost host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            this.host = host;

            LoadConfiguration();

            var systemLogFileName = Path.Combine(RuntimeConfiguration.LogFolder, "System.log");
            ProcessLogFileNames.Add(systemLogFileName);            
            rotatingLogFile = new RotatingLogFile(systemLogFileName, configuration.MaximumLogFileSize, configuration.MaximumLogFileCount);
            UpdateLogFilter();

            host.ServiceContainerCreated += OnServiceContainerCreated;
            host.ServiceContainerDisposed += OnServiceContainerDisposed;
        }

        /// <summary>
        /// Creates a user control that provides the UI for configuring this plugin.
        /// </summary>
        /// <param name="configurationHost">The configuration dialog host object.</param>
        /// <returns>
        /// A usercontrol or <c>null</c> if this plugin does not support configuration.
        /// </returns>
        public IEnumerable<IPluginConfigurationPane> CreateConfigurationControl(IPluginConfigurationHost configurationHost)
        {
            configurationHost.FormClosed += OnConfigurationFormClosed;
            yield return new ConfigurationDialog(this, configurationHost);   
        }

        /// <summary>
        /// Gets the name of this plugin.
        /// </summary>
        public string Name
        {
            get { return "LogWriter"; }
        }

        /// <summary>
        /// Gets the manufacturer of the plugin.
        /// </summary>
        public string Manufacturer
        {
            get { return ProductInformation.Manufacturer; }
        }

        /// <summary>
        /// Gets the version of the plugin.
        /// </summary>
        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (rotatingLogFile != null)
                rotatingLogFile.Dispose();
        }

        internal LogWriterConfiguration Configuration
        {
            get { return configuration; }
        }

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        internal RuntimeConfiguration RuntimeConfiguration
        {
            get { return host.RuntimeConfiguration; }
        }

        internal HashSet<string> ProcessLogFileNames
        {
            get { return processLogFileNames; }
        }

        internal void SaveConfiguration()
        {
            host.SaveConfigurationObject(configuration);
        }

        internal void LoadConfiguration()
        {
            configuration = host.LoadConfigurationObject() as LogWriterConfiguration ?? new LogWriterConfiguration();
        }

        private void OnConfigurationFormClosed(object sender, PluginConfigurationHostClosingEventArgs e)
        {
            if (e.SaveChanges)
                UpdateLogFilter();
        }

        private void OnServiceContainerCreated(object sender, ServiceContainerEventArgs e)
        {
            lock (serviceContainerMapping)
            {
                if (!serviceContainerMapping.ContainsKey(e.ServiceContainer))
                    serviceContainerMapping.Add(e.ServiceContainer, new ServiceContainerLogSink(this, e.ServiceContainer));
            }
        }

        private void OnServiceContainerDisposed(object sender, ServiceContainerEventArgs e)
        {
            lock (serviceContainerMapping)
            {
                ServiceContainerLogSink serviceContainerLogSink;
                if (serviceContainerMapping.TryGetValue(e.ServiceContainer, out serviceContainerLogSink))
                {
                    serviceContainerLogSink.Dispose();
                    serviceContainerMapping.Remove(e.ServiceContainer);
                }
            }
        }

        private void UpdateLogFilter()
        {
            if (logFilter != null)
            {
                host.DetachLogSink(logFilter);
                logFilter = null;
            }

            if (rotatingLogFile != null)
            {
                logFilter = new LogFilter(rotatingLogFile, configuration.MinimumLevel);
                host.AttachLogSink(logFilter);
            }
        }
    }
}
