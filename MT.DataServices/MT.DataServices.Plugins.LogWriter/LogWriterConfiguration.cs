﻿using MT.DataServices.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.LogWriter
{
    /// <summary>
    /// Contains the configuration of the log writer plugin.
    /// </summary>
    public class LogWriterConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriterConfiguration"/> class.
        /// </summary>
        public LogWriterConfiguration()
        {
            MaximumLogFileSize = 1024 * 1024;
            MaximumLogFileCount = 5;
            LogFilePerService = true;
            MinimumLevel = LogLevel.Warning;
        }

        /// <summary>
        /// Gets or sets the maximum size of a log file in bytes.
        /// </summary>
        public int MaximumLogFileSize { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of log files before the oldest logfiles are removed.
        /// </summary>
        public int MaximumLogFileCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a log file per service should be created or all services write to the same shared logfile.
        /// </summary>
        public bool LogFilePerService { get; set; }

        /// <summary>
        /// Gets the lowest message level that is required for a message to be logged.
        /// </summary>
        public LogLevel MinimumLevel { get; set; }
    }
}
