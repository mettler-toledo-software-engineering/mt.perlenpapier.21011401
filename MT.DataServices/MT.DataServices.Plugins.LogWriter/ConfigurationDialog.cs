﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MT.DataServices.Logging;

namespace MT.DataServices.Plugins.LogWriter
{
    /// <summary>
    /// Provides the configuration dialog for the log writer plugin.
    /// </summary>
    internal partial class ConfigurationDialog : UserControl, IPluginConfigurationPane
    {
        private readonly LogWriterConfiguration configuration;
        private readonly Plugin plugin;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationDialog"/> class.
        /// </summary>
        internal ConfigurationDialog(Plugin plugin, IPluginConfigurationHost configurationHost)
        {
            InitializeComponent();

            this.plugin = plugin;
            this.configuration = plugin.Configuration;
           
            MaximumFileSizeNumericUpDown.Value = configuration.MaximumLogFileSize / 1024;
            MaximumLogFilesNumericUpDown.Value = configuration.MaximumLogFileCount;
            MinimumLevelComboBox.SelectedIndex = (int)configuration.MinimumLevel;
            FilePerServiceCheckBox.Checked = configuration.LogFilePerService;

            configurationHost.FormClosed += OnConfigurationClosed;
        }

        private void OnConfigurationClosed(object sender, PluginConfigurationHostClosingEventArgs e)
        {
            if (e.SaveChanges)
                plugin.SaveConfiguration();
            else
            {
                // Reload the configuration how it was last saved, thus removing all changes
                plugin.LoadConfiguration();
            }
        }

        /// <summary>
        /// Gets the title that should be displayed in the tab panel.
        /// </summary>
        string IPluginConfigurationPane.Title
        {
            get { return "Logging"; }
        }

        /// <summary>
        /// Gets the control that contains the configuration user interface.
        /// </summary>
        Control IPluginConfigurationPane.Control
        {
            get { return this; }
        }

        private void OnMaximumFileSizeNumericUpDownValueChanged(object sender, EventArgs e)
        {
            configuration.MaximumLogFileSize = (int)MaximumFileSizeNumericUpDown.Value * 1024;
        }

        private void OnMaximumLogFilesNumericUpDownValueChanged(object sender, EventArgs e)
        {
            configuration.MaximumLogFileCount = (int)MaximumLogFilesNumericUpDown.Value;
        }

        private void OnMinimumLevelComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            configuration.MinimumLevel = (LogLevel)MinimumLevelComboBox.SelectedIndex;
        }

        private void OnFilePerServiceCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            configuration.LogFilePerService = FilePerServiceCheckBox.Checked;
        }
    }
}
