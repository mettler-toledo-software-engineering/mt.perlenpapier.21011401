﻿namespace MT.DataServices.Plugins.LogWriter
{
    partial class ConfigurationDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.MaximumFileSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MaximumLogFilesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.MinimumLevelComboBox = new System.Windows.Forms.ComboBox();
            this.FilePerServiceCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumFileSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumLogFilesNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Maximale Logfilegrösse:";
            // 
            // MaximumFileSizeNumericUpDown
            // 
            this.MaximumFileSizeNumericUpDown.Location = new System.Drawing.Point(214, 13);
            this.MaximumFileSizeNumericUpDown.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
            this.MaximumFileSizeNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaximumFileSizeNumericUpDown.Name = "MaximumFileSizeNumericUpDown";
            this.MaximumFileSizeNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.MaximumFileSizeNumericUpDown.TabIndex = 1;
            this.MaximumFileSizeNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaximumFileSizeNumericUpDown.ValueChanged += new System.EventHandler(this.OnMaximumFileSizeNumericUpDownValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "kb";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Maximale Anzahl Logdateien:";
            // 
            // MaximumLogFilesNumericUpDown
            // 
            this.MaximumLogFilesNumericUpDown.Location = new System.Drawing.Point(214, 38);
            this.MaximumLogFilesNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaximumLogFilesNumericUpDown.Name = "MaximumLogFilesNumericUpDown";
            this.MaximumLogFilesNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.MaximumLogFilesNumericUpDown.TabIndex = 1;
            this.MaximumLogFilesNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaximumLogFilesNumericUpDown.ValueChanged += new System.EventHandler(this.OnMaximumLogFilesNumericUpDownValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Minimaler Schweregrad:";
            // 
            // MinimumLevelComboBox
            // 
            this.MinimumLevelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MinimumLevelComboBox.FormattingEnabled = true;
            this.MinimumLevelComboBox.Items.AddRange(new object[] {
            "Aktion",
            "Erfolg",
            "Hinweis",
            "Warnung",
            "Fehler",
            "Schwerwiegender Fehler"});
            this.MinimumLevelComboBox.Location = new System.Drawing.Point(214, 64);
            this.MinimumLevelComboBox.Name = "MinimumLevelComboBox";
            this.MinimumLevelComboBox.Size = new System.Drawing.Size(192, 21);
            this.MinimumLevelComboBox.TabIndex = 2;
            this.MinimumLevelComboBox.SelectedIndexChanged += new System.EventHandler(this.OnMinimumLevelComboBoxSelectedIndexChanged);
            // 
            // FilePerServiceCheckBox
            // 
            this.FilePerServiceCheckBox.AutoSize = true;
            this.FilePerServiceCheckBox.Location = new System.Drawing.Point(20, 96);
            this.FilePerServiceCheckBox.Name = "FilePerServiceCheckBox";
            this.FilePerServiceCheckBox.Size = new System.Drawing.Size(197, 17);
            this.FilePerServiceCheckBox.TabIndex = 3;
            this.FilePerServiceCheckBox.Text = "Eigene Datei pro Dienst verwenden.";
            this.FilePerServiceCheckBox.UseVisualStyleBackColor = true;
            this.FilePerServiceCheckBox.CheckedChanged += new System.EventHandler(this.OnFilePerServiceCheckBoxCheckedChanged);
            // 
            // ConfigurationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FilePerServiceCheckBox);
            this.Controls.Add(this.MinimumLevelComboBox);
            this.Controls.Add(this.MaximumLogFilesNumericUpDown);
            this.Controls.Add(this.MaximumFileSizeNumericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "ConfigurationDialog";
            this.Size = new System.Drawing.Size(440, 266);
            ((System.ComponentModel.ISupportInitialize)(this.MaximumFileSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaximumLogFilesNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown MaximumFileSizeNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown MaximumLogFilesNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox MinimumLevelComboBox;
        private System.Windows.Forms.CheckBox FilePerServiceCheckBox;
    }
}
