﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Declares constants specifying different states that a service (container) can be in.
    /// </summary>
    public enum ServiceState
    {
        /// <summary>
        /// The service is currently idle.
        /// </summary>
        Idle,

        /// <summary>
        /// The service is running.
        /// </summary>
        Running,

        /// <summary>
        /// The service is currently being stopped.
        /// </summary>
        Stopping,

        /// <summary>
        /// The service implementation threw an unexpected exception and was terminated.
        /// </summary>
        Faulted
    }
}
