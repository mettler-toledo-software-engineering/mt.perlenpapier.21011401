﻿using MT.DataServices.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Contains the event data for a service create/dispose event
    /// </summary>
    public class ServiceContainerEventArgs : EventArgs
    {
        private readonly IServiceContainer serviceContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceContainerEventArgs"/> class with a <see cref="IServiceContainer"/> instance.
        /// </summary>
        /// <param name="serviceContainer">The service container.</param>
        public ServiceContainerEventArgs(IServiceContainer serviceContainer)
        {
            this.serviceContainer = serviceContainer;
        }

        /// <summary>
        /// Gets the service container.
        /// </summary>
        public IServiceContainer ServiceContainer
        {
            get { return serviceContainer; }
        }
    }
}
