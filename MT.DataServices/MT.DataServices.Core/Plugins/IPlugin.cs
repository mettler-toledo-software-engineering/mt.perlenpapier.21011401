﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Exposes the methods that are required for implementing a generic plugin.
    /// </summary>
    public interface IPlugin : IDisposable
    {
        /// <summary>
        /// Activates the plugin.
        /// </summary>
        /// <param name="host">The plugin host environment.</param>
        void Activate(IPluginHost host);

        /// <summary>
        /// Creates a user control that provides the UI for configuring this plugin.
        /// </summary>
        /// <param name="configurationHost">The configuration dialog host object.</param>
        /// <returns>A usercontrol or <c>null</c> if this plugin does not support configuration.</returns>
        IEnumerable<IPluginConfigurationPane> CreateConfigurationControl(IPluginConfigurationHost configurationHost);

        /// <summary>
        /// Gets the name of this plugin.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the manufacturer of the plugin.
        /// </summary>
        string Manufacturer { get; }

        /// <summary>
        /// Gets the version of the plugin.
        /// </summary>
        Version Version { get; }
    }
}
