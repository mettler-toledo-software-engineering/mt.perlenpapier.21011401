using System;
using System.ComponentModel;
using System.Linq;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Exposes members that allow to interact with the configuration dialog host window.
    /// </summary>
    public interface IPluginConfigurationHost
    {
        /// <summary>
        /// Occurs before the configuration window closes.
        /// </summary>
        event EventHandler<PluginConfigurationHostClosingEventArgs> FormClosing;

        /// <summary>
        /// Occurs after the configuration window closed.
        /// </summary>
        event EventHandler<PluginConfigurationHostClosingEventArgs> FormClosed;

        /// <summary>
        /// Ensures that the specified configuration pane is visible.
        /// </summary>
        /// <param name="pane">The pane to ensure the visibility of.</param>
        void EnsureConfigurationVisible(IPluginConfigurationPane pane);
    }
}