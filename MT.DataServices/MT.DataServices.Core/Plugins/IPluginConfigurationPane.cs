﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Describes a single tab in the configuration window.
    /// </summary>
    public interface IPluginConfigurationPane
    {
        /// <summary>
        /// Gets the title that should be displayed in the tab panel.
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Gets the control that contains the configuration user interface.
        /// </summary>
        Control Control { get; }
    }
}
