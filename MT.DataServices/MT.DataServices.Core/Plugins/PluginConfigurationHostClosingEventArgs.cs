﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Contains the event data for the <see cref="IPluginConfigurationHost.FormClosing"/> event
    /// </summary>
    public class PluginConfigurationHostClosingEventArgs : CancelEventArgs
    {
        /// <summary>
        /// Gets a value indicating whether the user wants to save the changes were made to the configuration.
        /// </summary>
        public bool SaveChanges { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginConfigurationHostClosingEventArgs" /> class.
        /// </summary>
        /// <param name="saveChanges">if set to <c>true</c> the configuration changes should be saved.</param>
        public PluginConfigurationHostClosingEventArgs(bool saveChanges)
        {
            SaveChanges = saveChanges;
        }
    }
}
