﻿using System;
using System.Linq;
using MT.DataServices.Logging;
using System.Threading;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Exposes members that allow a service to access its host container.
    /// </summary>
    public interface IServiceHost
    {
        /// <summary>
        /// Gets the log sink for this service.
        /// </summary>
        ILogSink Log { get; }

        /// <summary>
        /// Gets the cancellation token for stopping the service.
        /// </summary>
        CancellationToken CancellationToken { get; }
    }
}
