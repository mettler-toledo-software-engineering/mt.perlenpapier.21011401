﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Exposes a method that runs the functionality of this service.
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Gets the name of this service for log files and displaying debug information.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Runs functionality that this service provides.
        /// </summary>
        /// <param name="serviceHost">The service host that allows the interaction with the host system.</param>
        void Run(IServiceHost serviceHost);
    }
}
