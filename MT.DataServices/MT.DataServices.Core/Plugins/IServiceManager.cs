﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Exposes methods that allow to create and manage service containers.
    /// </summary>
    public interface IServiceManager
    {
        /// <summary>
        /// Creates a new service container for the given service implementation.
        /// </summary>
        /// <param name="service">The service to wrap in a container.</param>
        /// <param name="completionGracePeriod">The time period between signaling a service to shutdown and aborting the thread.</param>
        /// <returns>
        /// The newly created service container.
        /// </returns>
        IServiceContainer CreateServiceContainer(IService service, TimeSpan completionGracePeriod);
    }
}
