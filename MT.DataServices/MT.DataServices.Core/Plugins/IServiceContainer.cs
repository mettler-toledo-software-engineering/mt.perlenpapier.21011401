﻿using MT.DataServices.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Abstraction for running a service implementation on a background thread.
    /// </summary>
    public interface IServiceContainer : IDisposable
    {
        /// <summary>
        /// Gets the current state of the service.
        /// </summary>
        ServiceState State { get; }

        /// <summary>
        /// Occurs when state of this service container has changed.
        /// </summary>
        event EventHandler StateChanged;

        /// <summary>
        /// Gets the last exception that is responsible for transitioning the service into the <see cref="ServiceState.Faulted"/> state.
        /// </summary>
        Exception FaultingException { get; }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Adds the log sink as a listener for log messages of this service.
        /// </summary>
        /// <param name="logSink">The log sink to attach.</param>
        void AttachLogSink(ILogSink logSink);

        /// <summary>
        /// Removes the log sink as a listener for log messages of this service.
        /// </summary>
        /// <param name="logSink">The log sink to detach.</param>
        void DetachLogSink(ILogSink logSink);

        /// <summary>
        /// Starts the service implemention on a separate thread.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the service implementation.
        /// </summary>
        void Stop();
    }
}
