using System;
using System.Linq;
using MT.DataServices.Configuration;
using System.Collections.ObjectModel;
using MT.DataServices.Logging;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Provides the interface between a plugin and the host system.
    /// </summary>
    public interface IPluginHost
    {
        /// <summary>
        /// Occures when the plugin start phase executes
        /// </summary>
        event EventHandler<ServicesEventArgs> StartServices;

        /// <summary>
        /// Occures when the plugin stop phase executes
        /// </summary>
        event EventHandler<ServicesEventArgs> StopServices;

        /// <summary>
        /// Occurs when a service container is created.
        /// </summary>
        event EventHandler<ServiceContainerEventArgs> ServiceContainerCreated;

        /// <summary>
        /// Occurs when a service container is disposed.
        /// </summary>  
        event EventHandler<ServiceContainerEventArgs> ServiceContainerDisposed;

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        RuntimeConfiguration RuntimeConfiguration { get; }

        /// <summary>
        /// Loads the configuration object of this plugin.
        /// </summary>
        object LoadConfigurationObject();

        /// <summary>
        /// Sets the XAML serializable configuration object of this plugin.
        /// </summary>
        void SaveConfigurationObject(object value);

        /// <summary>
        /// Gets the collection of active service containers.
        /// </summary>
        Collection<IServiceContainer> ServiceContainers { get; }

        /// <summary>
        /// Adds the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to attach.</param>
        void AttachLogSink(ILogSink logSink);

        /// <summary>
        /// Removes the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to detach.</param>
        void DetachLogSink(ILogSink logSink);
    }
}