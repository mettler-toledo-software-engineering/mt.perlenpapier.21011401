﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MT.DataServices.Hosting;

namespace MT.DataServices.Plugins
{
    /// <summary>
    /// Contains all information related to a service start / stop events
    /// </summary>
    public class ServicesEventArgs : EventArgs
    {
        private readonly IServiceManager serviceManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServicesEventArgs"/> class.
        /// </summary>
        /// <param name="serviceManager">The service manager.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="serviceManager"/> is <c>null</c>.</exception>
        public ServicesEventArgs(IServiceManager serviceManager)
        {
            if (serviceManager == null)
                throw new ArgumentNullException("serviceManager");

            this.serviceManager = serviceManager;
        }

        /// <summary>
        /// Gets the service manager.
        /// </summary>
        public IServiceManager ServiceManager
        {
            get { return serviceManager; }
        }
    }
}
