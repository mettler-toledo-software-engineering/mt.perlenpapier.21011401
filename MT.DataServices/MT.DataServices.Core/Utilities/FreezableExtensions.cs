﻿using System;
using System.Linq;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Contains extension methods for the <see cref="IFreezable"/> interface.
    /// </summary>
    public static class FreezableExtensions
    {
        /// <summary>
        /// Verifies that the properties of a freezable object can be modified
        /// </summary>
        /// <param name="self">The freezable to check.</param>
        /// <param name="propertyName">The name of the property to access.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="self"/> parameter is null.</exception>
        /// <exception cref="FrozenValueModifyException">The object is currently frozen and cannot be modified.</exception>
        public static void VerifyWriteAccess(this IFreezable self, string propertyName)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            if (self.IsFrozen)
                throw new FrozenValueModifyException(propertyName);
        }

        /// <summary>
        /// Verifies that the freezable object can be modified
        /// </summary>
        /// <param name="self">The freezable to check.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="self"/> parameter is null.</exception>
        /// <exception cref="FrozenValueModifyException">The object is currently frozen and cannot be modified.</exception>
        public static void VerifyWriteAccess(this IFreezable self)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            if (self.IsFrozen)
                throw new FrozenValueModifyException("", "Cannot execute the write operation on this frozen instance.");
        }
    }
}