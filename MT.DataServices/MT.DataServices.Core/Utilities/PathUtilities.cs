﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MT.DataServices.Utilities
{
    internal static class PathUtilities
    {
        /// <summary>
        /// Gets the folder of the entry assembly.
        /// </summary>
        internal static string GetHomeFolder()
        {
            var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            return Path.GetDirectoryName(assembly.Location);
        }

        /// <summary>
        /// Check if the specified directory exists and creates it if not.
        /// </summary>
        internal static void EnsureDirectoryExists(string directoryName)
        {
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
        }
    }
}
