﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace MT.DataServices.Utilities
{
    internal class XCollection<T> : IList<T>, IFreezable
        where T : IXCollectionItem, new()
    {
        private readonly List<T> items = new List<T>();
        private readonly XElement parent;

        public XCollection(XElement parent)
        {
            this.parent = parent;

            items.AddRange(parent.Elements().Select(d => new T() { Element = d }));
        }

        public int IndexOf(T item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            items.Insert(index, item);
            parent.Elements().ElementAt(index).AddBeforeSelf(item.Element);
        }

        public void RemoveAt(int index)
        {
            items[index].Element.Remove();
            items.RemoveAt(index);
        }

        public T this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                items[index] = value;
                items[index].Element.ReplaceWith(value.Element);
            }
        }

        public void Add(T item)
        {
            items.Add(item);
            parent.Add(item.Element);
        }

        public void Clear()
        {
            items.Clear();
            parent.RemoveAll();
        }

        public bool Contains(T item)
        {
            return items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return items.Count; }
        }

        public bool IsReadOnly
        {
            get { return isFrozen; }
        }

        public bool Remove(T item)
        {
            if (items.Remove(item))
            {
                item.Element.Remove();
                return true;
            }
            else
                return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #region IFreezable Implementation

        /// <summary>
        /// Gets a value indicating whether this instance is frozen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is frozen; otherwise, <c>false</c>.
        /// </value>
        public bool IsFrozen
        {
            get { return isFrozen; }
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Freeze()
        {
            this.OfType<IFreezable>().ForEach(d => d.Freeze());
            isFrozen = true;
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Unfreeze()
        {
            this.OfType<IFreezable>().ForEach(d => d.Unfreeze());
            isFrozen = false;
        }

        private bool isFrozen;

        #endregion 
    }
}
