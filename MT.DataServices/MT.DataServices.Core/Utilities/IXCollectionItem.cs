﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace MT.DataServices.Utilities
{
    internal interface IXCollectionItem
    {
        XElement Element { get; set; }
    }
}
