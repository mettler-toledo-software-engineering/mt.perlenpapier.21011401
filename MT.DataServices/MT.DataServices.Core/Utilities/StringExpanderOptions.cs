﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Declares constants specifying different operation modes of the <see cref="StringExpander" /> class.
    /// </summary>
    [Flags]
    public enum StringExpanderOptions
    {
        /// <summary>
        /// Use no additional expanding options
        /// </summary>
        None = 0x00,

        /// <summary>
        /// Include environment variables when expanding the string.
        /// </summary>
        ExpandEnvironmentVariables = 0x01,

        /// <summary>
        /// Expand the environment variables recurively, until no more environment variable names can be found in the string.
        /// </summary>
        Recursive = 0x02
    }
}
