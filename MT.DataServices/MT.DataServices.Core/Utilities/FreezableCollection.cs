﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Implements a collection that can be frozen.
    /// </summary>
    /// <typeparam name="T">The type of the collection elements.</typeparam>
    public class FreezableCollection<T> : Collection<T>, IFreezable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FreezableCollection{T}"/> class.
        /// </summary>
        public FreezableCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FreezableCollection{T}" /> class wrapping the given inner list.
        /// </summary>
        /// <param name="list">The inner list to wrap as a freezable collection.</param>
        public FreezableCollection(IList<T> list)
            : base(list)
        {
        }

        /// <summary>
        /// Clears the list after the access right has been verified.
        /// </summary>
        protected override void ClearItems()
        {
            this.VerifyWriteAccess();
            base.ClearItems();
        }

        /// <summary>
        /// Inserts an item into the list after the access right has been verified.
        /// </summary>
        protected override void InsertItem(int index, T item)
        {
            this.VerifyWriteAccess();
            base.InsertItem(index, item);
        }

        /// <summary>
        /// Removes an item from the list after the access right has been verified.
        /// </summary>
        protected override void RemoveItem(int index)
        {
            this.VerifyWriteAccess();
            base.RemoveItem(index);
        }

        /// <summary>
        /// Replaces the value of an item in the list after the access right has been verified.
        /// </summary>
        protected override void SetItem(int index, T item)
        {
            this.VerifyWriteAccess();
            base.SetItem(index, item);
        }

        #region IUnfreezable Implementation

        /// <summary>
        /// Gets a value indicating whether this instance is frozen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is frozen; otherwise, <c>false</c>.
        /// </value>
        public bool IsFrozen
        {
            get { return isFrozen; }
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Freeze()
        {
            isFrozen = true;
        }

        /// <summary>
        /// Restores this instance into mutable state.
        /// </summary>
        public void Unfreeze()
        {
            isFrozen = false;
        }

        private bool isFrozen;
        
        #endregion     
    }
}