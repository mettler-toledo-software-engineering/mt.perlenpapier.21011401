﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Utilities
{
    internal static class EnumerableExtensions
    {
        /// <summary>
        /// Calles the provided method for each element in the enumerable.
        /// </summary>
        /// <typeparam name="T">Type of the elements</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="action">The action.</param>
        internal static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable == null)
                throw new ArgumentNullException("enumerable");

            if (action == null)
                throw new ArgumentNullException("action");

            foreach (var item in enumerable)
            {
                action(item);
            }
        }
    }
}
