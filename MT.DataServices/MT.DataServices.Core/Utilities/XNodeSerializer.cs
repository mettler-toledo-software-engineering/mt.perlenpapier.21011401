﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xaml;
using System.Xml.Linq;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Provides utility methods for using the XAML serializer to store plugin configuration data.
    /// </summary>
    public static class XNodeSerializer
    {
        /// <summary>
        /// Serializes the specified object into a XAML document and converts it into an XElement.
        /// </summary>
        public static XElement Serialize(object graph)
        {
            if (graph != null)
            {
                return XElement.Parse(XamlServices.Save(graph));
            }
            else
                return null;
        }

        /// <summary>
        /// Deserializes the specified XElement using the XAML serializer and converts it into an object of the specified type.
        /// </summary>
        public static T Deserialize<T>(XElement element)
            where T : class
        {
            if (element != null)
                return XamlServices.Load(element.CreateReader()) as T;
            else
                return null;
        }        
    }
}
