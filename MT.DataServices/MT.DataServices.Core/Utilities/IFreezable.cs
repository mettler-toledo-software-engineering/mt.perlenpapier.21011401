﻿using System;
using System.Linq;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Exposes members for freezing the state of an object and thus prevents any modification
    /// </summary>
    public interface IFreezable
    {
        /// <summary>
        /// Gets a value indicating whether this instance is frozen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is frozen; otherwise, <c>false</c>.
        /// </value>
        bool IsFrozen { get; }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        void Freeze();

        /// <summary>
        /// Unfreezes this instance.
        /// </summary>
        void Unfreeze();
    }
}