﻿using System;
using System.Linq;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Occurs when an attempt is made to write to a property of a frozen object.
    /// </summary>
    public class FrozenValueModifyException : Exception
    {
        /// <summary>
        /// Gets the name of the property that was accessed.
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrozenValueModifyException"/> class.
        /// </summary>
        /// <param name="propertyName">Name of the property that was accessed.</param>
        public FrozenValueModifyException(string propertyName)
            : base(CreateMessage(propertyName, null))
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrozenValueModifyException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="propertyName">Name of the property that was accessed.</param>
        public FrozenValueModifyException(string propertyName, string message)
            : base(CreateMessage(propertyName, message))
        { 
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrozenValueModifyException"/> class.
        /// </summary>
        /// <param name="propertyName">Name of the property that was accessed.</param>
        /// <param name="message">The error message.</param>
        /// <param name="inner">The inner exception.</param>
        public FrozenValueModifyException(string propertyName, string message, Exception inner) 
            : base(CreateMessage(propertyName, message), inner)
        { 
        }

        /// <summary>
        /// Creates the error message from the different parts.
        /// </summary>
        /// <param name="propertyName">Name of the property that was accessed.</param>
        /// <param name="message">The user-supplied message.</param>
        /// <returns>The complete error message</returns>
        private static string CreateMessage(string propertyName, string message)
        {
            if (propertyName == null)
                throw new ArgumentNullException("propertyName");

            if (string.IsNullOrEmpty(message))
                message = "Attempted to modify a property of a frozen object";

            return string.Format("{0} Property Name: {2}", message, propertyName);
        }
    }
}