﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MT.DataServices.Utilities
{
    /// <summary>
    /// Provides a configurable method that expands environment variables (%HOME%) in strings.
    /// </summary>
    public class StringExpander
    {
        private readonly Regex environmentVariablePattern = new Regex("%([a-zA-Z0-9()_]+)%");
        private readonly Dictionary<string, string> expansions = new Dictionary<string, string>();
        private readonly StringExpanderOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringExpander"/> class that does not use environment variables and does not do recursive evaluation.
        /// </summary>
        public StringExpander()
            : this(StringExpanderOptions.None)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringExpander" /> class with the given options.
        /// </summary>
        /// <param name="options">The expander options.</param>
        public StringExpander(StringExpanderOptions options)
        {           
            this.options = options;
        }

        /// <summary>
        /// Gets the dictionary containing the mapping between the variable names and their expansion content.
        /// </summary>
        public IDictionary<string, string> Expansions
        {
            get { return expansions; }
        }

        /// <summary>
        /// Expands the variables in the given string.
        /// </summary>
        /// <param name="str">The string to expand.</param>
        /// <returns>The expanded string.</returns>
        public string Expand(string str)
        {
            bool successfulExpansion;

            do
            {
                successfulExpansion = false;
                str = environmentVariablePattern.Replace(str, match =>
                {
                    var variableName = match.Groups[1].Value;
                    string replacement;

                    if (!expansions.TryGetValue(variableName, out replacement))
                    {
                        if ((options & StringExpanderOptions.ExpandEnvironmentVariables) != 0)
                            replacement = Environment.GetEnvironmentVariable(variableName);
                        else
                            replacement = null;
                    }

                    if (replacement != null)
                    {
                        successfulExpansion = true;
                        return replacement;
                    }
                    else
                        return match.Value;
                });
            } while (successfulExpansion && (options & StringExpanderOptions.Recursive) != 0);

            return str;
        }
    }
}
