﻿using System;
using System.Linq;
using System.Threading;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;

namespace MT.DataServices.Hosting
{
    internal sealed class ServiceContainer : IServiceContainer, IServiceHost, IProvideLogSource
    {
        private readonly LogDistributor distributor = new LogDistributor() { LockDuringLogging = true };
        private readonly IService service;
        private readonly TimeSpan completionGracePeriod;
        private readonly CommonPluginHost pluginHost;

        private ServiceState state;
        private Exception faultingException;
        private CancellationTokenSource cancellationTokenSource;
        private Thread thread;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceContainer"/> class.
        /// </summary>
        internal ServiceContainer(CommonPluginHost pluginHost, IService service, TimeSpan completionGracePeriod)
        {
            this.pluginHost = pluginHost;
            this.completionGracePeriod = completionGracePeriod;
            this.service = service;
        }

        #region IServiceContainer

        /// <summary>
        /// Gets the current state of the service.
        /// </summary>
        public ServiceState State
        {
            get { return state; }
            private set
            {
                if (state != value)
                {
                    state = value;
                    var stateChanged = StateChanged;
                    if (stateChanged != null)
                        stateChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Occurs when state of this service container has changed.
        /// </summary>
        public event EventHandler StateChanged;

        /// <summary>
        /// Gets the last exception that is responsible for transitioning the service into the <see cref="ServiceState.Faulted" /> state.
        /// </summary>
        public Exception FaultingException
        {
            get { return faultingException; }
        }

        /// <summary>
        /// Adds the log sink as a listener for log messages of this service.
        /// </summary>
        /// <param name="logSink">The log sink to attach.</param>
        public void AttachLogSink(ILogSink logSink)
        {
            lock (distributor)
            {
                distributor.Add(logSink); 
            }
        }

        /// <summary>
        /// Removes the log sink as a listener for log messages of this service.
        /// </summary>
        /// <param name="logSink">The log sink to detach.</param>
        public void DetachLogSink(ILogSink logSink)
        {
            lock (distributor)
            {
                distributor.Add(logSink); 
            }
        }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        public string Name
        {
            get { return service.Name; }
        }

        /// <summary>
        /// Starts the service implemention on a separate thread.
        /// </summary>
        public void Start()
        {
            if (state == ServiceState.Stopping)
                throw new InvalidOperationException("The service cannot be started when is being stopped.");

            if (state == ServiceState.Running)
                return;

            cancellationTokenSource = new CancellationTokenSource();
            thread = new Thread(ServiceRunner);
            thread.Name = "[Service] " + (service.Name ?? "Unnamed");
            thread.Start();

            State = ServiceState.Running;
        }

        /// <summary>
        /// Stops the service implementation.
        /// </summary>
        public void Stop()
        {
            if (state == ServiceState.Running)
            {
                State = ServiceState.Stopping;
                cancellationTokenSource.Cancel();

                thread.Join(completionGracePeriod);

                thread = null;
                cancellationTokenSource = null;
            }
        }

        #endregion

        #region IServiceHost

        /// <summary>
        /// Gets the log sink for this service.
        /// </summary>
        public ILogSink Log
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the cancellation token for stopping the service.
        /// </summary>
        public CancellationToken CancellationToken
        {
            get { return cancellationTokenSource != null ? cancellationTokenSource.Token : CancellationToken.None; }
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets the source name of the log message
        /// </summary>
        string IProvideLogSource.Source
        {
            get { return service.Name; }
        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />
        /// </summary>
        /// <param name="message">Message to handle.</param>
        void ILogSink.Log(LogMessage message)
        {
            distributor.Log(message);
        }

        private void ServiceRunner()
        {
            try
            {
                service.Run(this);
                State = ServiceState.Idle;
                this.Action("The service stopped normally.");
            }
            catch (OperationCanceledException)
            {
                State = ServiceState.Idle;
                this.Action("The service has been canceled.");
            }
            catch (Exception ex)
            {
                this.Error("The service threw an unexpected exception.", ex);
                faultingException = ex;
                State = ServiceState.Faulted;
            }            
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
                pluginHost.NotifyServiceContainerDisposed(this);
            }
        }
    }
}
