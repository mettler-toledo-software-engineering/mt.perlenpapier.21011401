﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;

namespace MT.DataServices.Hosting
{
    /// <summary>
    /// Contains the members of the <see cref="IPluginHost"/> interface that are shared between all plugins.
    /// </summary>
    [Export]
    internal class CommonPluginHost : IServiceManager
    {       
        private readonly HostConfiguration hostConfiguration;
        private readonly RuntimeConfiguration runtimeConfiguration;
        private readonly Collection<IServiceContainer> serviceContainers = new Collection<IServiceContainer>();

        private LogDistributor systemLog;

        [ImportingConstructor]
        internal CommonPluginHost(HostConfiguration hostConfiguration, RuntimeConfiguration runtimeConfiguration)
        {
            this.runtimeConfiguration = runtimeConfiguration;
            this.hostConfiguration = hostConfiguration;
        }

        internal LogDistributor SystemLog
        {
            get { return systemLog; }
            set { systemLog = value; }
        }

        internal event EventHandler<ServiceContainerEventArgs> ServiceContainerCreated;

        internal event EventHandler<ServiceContainerEventArgs> ServiceContainerDisposed;

        internal event EventHandler<ServicesEventArgs> StartServices;

        internal event EventHandler<ServicesEventArgs> StopServices;

        internal HostConfiguration HostConfiguration
        {
            get { return hostConfiguration; }
        }

        internal RuntimeConfiguration RuntimeConfiguration
        {
            get { return runtimeConfiguration; }
        }

        internal Collection<IServiceContainer> ServiceContainers
        {
            get { return serviceContainers; }
        }

        /// <summary>
        /// Creates the service container.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="completionGracePeriod">The completion grace period.</param>
        /// <returns></returns>
        public IServiceContainer CreateServiceContainer(IService service, TimeSpan completionGracePeriod)
        {
            var serviceContainer = new ServiceContainer(this, service, completionGracePeriod);

            var serviceContainerCreated = ServiceContainerCreated;
            if (serviceContainerCreated != null)
                serviceContainerCreated(this, new ServiceContainerEventArgs(serviceContainer));

            serviceContainers.Add(serviceContainer);

            return serviceContainer;
        }

        internal void RaiseStartServicesEvent()
        {
            Debug.Assert(serviceContainers.Count == 0);

            var startServices = StartServices;
            if (startServices != null)
                startServices(this, new ServicesEventArgs(this));
        }

        internal void RaiseStopServicesEvent()
        {
            var stopServices = StopServices;
            if (stopServices != null)
                stopServices(this, new ServicesEventArgs(this));

            while (serviceContainers.Count > 0)
            {
                // The Dispose method will call the NotifyServiceContainerDisposed method which in turn 
                // removes the ServiceContainer from the list, so we don't need to remove the container
                // explicitly here.

                serviceContainers[0].Dispose(); 
            }
        }

        internal void NotifyServiceContainerDisposed(IServiceContainer serviceContainer)
        {
            var serviceContainerDisposed = ServiceContainerDisposed;
            if (serviceContainerDisposed != null)
                serviceContainerDisposed(this, new ServiceContainerEventArgs(serviceContainer));

            serviceContainers.Remove(serviceContainer);
        }

        internal PluginHost CreatePluginHost(Type pluginType)
        {
            return new PluginHost(this, pluginType);
        }

        internal void AttachLogSink(ILogSink logSink)
        {
            lock (systemLog)
            {
                systemLog.Add(logSink);
            }            
        }

        internal void DetachLogSink(ILogSink logSink)
        {
            lock (systemLog)
            {
                systemLog.Remove(logSink);
            }
        }
    }
}
