﻿using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MT.DataServices.Hosting
{
    /// <summary>
    /// Provides a highlevel interface for managing the life-cycle and composition of plugins.
    /// </summary>
    public class PluginManager : IDisposable
    {
        private readonly ObservableCollection<IServiceContainer> serviceContainers = new ObservableCollection<IServiceContainer>();
        private PluginManagerState state = PluginManagerState.Idle;
        private readonly ComposablePartCatalog catalog;

        [Export]
        private readonly HostConfiguration hostConfiguration;

        [Export]
        private readonly RuntimeConfiguration runtimeConfiguration;

        [Export(typeof(ILogSink))]
        private LogDistributor log;

        [Import]
        private CommonPluginHost pluginHost = null;

        [ImportMany]
        private IEnumerable<IPlugin> plugins = null;

        // <summary>
        // Initializes a new instance of the<see cref="PluginManager" /> class from a plugin folder.
        // </summary>
        // <param name = "runtimeConfiguration" > The runtime configuration object.</param>
        // <param name = "pluginFolderPath" > The path to the plugin folder.</param>
        public PluginManager(RuntimeConfiguration runtimeConfiguration, string pluginFolderPath)
            : this(runtimeConfiguration, CreateCatalogFromDirectory(pluginFolderPath))
        {
        }

        public static ComposablePartCatalog CreateCatalogFromDirectory(string directory)
        {
            var assemblies = new List<Assembly>();
            foreach (var plugin in Directory.GetFiles(directory, "*.dll"))
            {
                try
                {
                    if (plugin.Contains("Foundation"))
                        Assembly.LoadFrom(plugin);
                    else
                        assemblies.Add(AssemblyLoadContext.Default.LoadFromAssemblyPath(plugin));
                }
                catch (BadImageFormatException ex)
                {

                }
            }
            return new AggregateCatalog(assemblies.Select(a => new AssemblyCatalog(a)));
        }
              
        
        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager" /> class.
        /// </summary>
        /// <param name="runtimeConfiguration">The runtime configuration object.</param>
        /// <param name="catalog">The catalog to compose the plugins from.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="catalog"/> is <c>null</c>.</exception>
        public PluginManager(RuntimeConfiguration runtimeConfiguration, ComposablePartCatalog catalog)
        {
            if (catalog == null)
                throw new ArgumentNullException("catalog");

            if (runtimeConfiguration == null)
                throw new ArgumentNullException("runtimeConfiguration");

            this.catalog = catalog;
            this.runtimeConfiguration = runtimeConfiguration;
            this.hostConfiguration = runtimeConfiguration.HostConfiguration;
            log = new LogDistributor();
            log.LockDuringLogging = true;
        }

        /// <summary>
        /// Gets the host configuration.
        /// </summary>
        public HostConfiguration HostConfiguration
        {
            get { return hostConfiguration; }
        }

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        public RuntimeConfiguration RuntimeConfiguration
        {
            get { return runtimeConfiguration; }
        }

        /// <summary>
        /// Adds the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to attach.</param>
        public void AttachLogSink(ILogSink logSink)
        {
            lock (log)
            {
                log.Add(logSink);
            }            
        }

        /// <summary>
        /// Removes the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to detach.</param>
        public void DetachLogSink(ILogSink logSink)
        {
            lock (log)
            {
                log.Remove(logSink);    
            }            
        }

        /// <summary>
        /// Gets the activated plugins.
        /// </summary>
        public IEnumerable<IPlugin> Plugins
        {
            get 
            {
                if (state == PluginManagerState.Idle || state == PluginManagerState.Activating)
                    return Enumerable.Empty<IPlugin>();
                else
                    return plugins;
            }
        }

        /// <summary>
        /// Gets the list of active service containers.
        /// </summary>
        public ObservableCollection<IServiceContainer> ServiceContainers
        {
            get { return serviceContainers; }
        }

        /// <summary>
        /// Occures when the plugin manager state changed
        /// </summary>
        public event EventHandler StateChanged;

        /// <summary>
        /// Occures when the busy state of the plugin manager changed
        /// </summary>
        public event EventHandler IsBusyChanged;

        /// <summary>
        /// Activates the loaded plugins asynchronously.
        /// </summary>
        public void ActivatePluginsAsync()
        {
            if (state == PluginManagerState.Disposing || state == PluginManagerState.Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (state != PluginManagerState.Idle)
                throw new InvalidOperationException("The plugins can only be activated once, when the manager is idle.");

            state = PluginManagerState.Activating;
            Task.Factory.StartNew(() =>
            {
                var homeFolderDirectory = new DirectoryCatalog(runtimeConfiguration.HomeFolder);
                var compositionManager = new CompositionContainer(new AggregateCatalog(catalog, homeFolderDirectory));
                log.Action("Composing plugins...");
                try
                {
                    compositionManager.ComposeParts(this);
                }
                catch (Exception ex)
                {
                    log.Error("An exception occured during plugin composition.", ex);
                    State = PluginManagerState.Faulted;
                    return;
                }

                pluginHost.SystemLog = log;
                pluginHost.ServiceContainerCreated += OnServiceContainerCreated;
                pluginHost.ServiceContainerDisposed += OnServiceContainerDisposed;

                log.Action("Activating plugins...");                
                foreach (var plugin in plugins)
                {
                    try
                    {
                        plugin.Activate(pluginHost.CreatePluginHost(plugin.GetType()));
                    }
                    catch (Exception ex)
                    {
                        log.Error(string.Format("The plugin {0} cannot be activated.", plugin.Name), ex);
                    }
                }

                log.Success("Plugins activated...");
                State = PluginManagerState.Activated;
            });
        }

        /// <summary>
        /// Starts the plugins asynchronously.
        /// </summary>
        public void StartAsync()
        {
            if (state == PluginManagerState.Disposing || state == PluginManagerState.Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (state != PluginManagerState.Activated && state != PluginManagerState.Stopped)
                throw new InvalidOperationException("This operation is only possible when the plugin are activated and not running.");

            state = PluginManagerState.Starting;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    log.Action("Starting services...");
                    pluginHost.RaiseStartServicesEvent();
                    State = PluginManagerState.Running;
                    log.Success("Services running...");
                }
                catch (Exception ex)
                {
                    log.Error("The starting phase failed with an exception.", ex);
                    State = PluginManagerState.Faulted;
                }
            });
        }

        /// <summary>
        /// Stops the plugins asynchronously.
        /// </summary>
        public void StopAsync()
        {
            if (state == PluginManagerState.Disposing || state == PluginManagerState.Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (state != PluginManagerState.Running)
                throw new InvalidOperationException("This operation is only possible when the plugin are running.");

            state = PluginManagerState.Stopping;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    log.Action("Stopping services...");
                    pluginHost.RaiseStopServicesEvent();                    
                    State = PluginManagerState.Stopped;
                    log.Success("Services stopped...");
                }
                catch (Exception ex)
                {
                    log.Error("The stopping phase failed with an exception.", ex);
                    State = PluginManagerState.Faulted;
                }
            });
        }  

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <remarks>
        /// If a long-running background operation is active, this call blocks until the operation completes.
        /// </remarks>
        public void Dispose()
        {
            if (State == PluginManagerState.Disposed || State == PluginManagerState.Disposing)
                return;

            while (IsBusy)
            {
                Thread.Sleep(5);
            }

            if (State == PluginManagerState.Running)
                StopAsync();

            while (IsBusy)
            {
                Thread.Sleep(5);
            }
            
            State = PluginManagerState.Disposing;

            if (plugins != null)
            {
                foreach (var plugin in plugins)
                {
                    try
                    {
                        plugin.Dispose();
                    }
                    catch (Exception ex)
                    {
                        log.Error("Unexpected exception thrown by Dispose method.", ex);
                    }
                }
            }

            State = PluginManagerState.Disposed;
        }

        /// <summary>
        /// Gets the current state of the plugin manager.
        /// </summary>
        public PluginManagerState State
        {
            get { return state; }
            private set
            {
                if (state != value)
                {
                    state = value;
                    OnStateChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the plugin manager is currently is busy.
        /// </summary>
        public bool IsBusy
        {
            get { return ((int)state & 1) != 0; }
        }

        /// <summary>
        /// Raises the <see cref="StateChanged"/> event
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnStateChanged(EventArgs e)
        {
            if (StateChanged != null)
                StateChanged(this, e);
        }

        /// <summary>
        /// Raises the <see cref="IsBusyChanged"/> event
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnIsBusyChanged(EventArgs e)
        {
            if (IsBusyChanged != null)
                IsBusyChanged(this, e);
        }

        private void OnServiceContainerCreated(object sender, ServiceContainerEventArgs e)
        {
            serviceContainers.Add(e.ServiceContainer);
        }
  
        private void OnServiceContainerDisposed(object sender, ServiceContainerEventArgs e)
        {
            serviceContainers.Remove(e.ServiceContainer);
        }
    }
}
