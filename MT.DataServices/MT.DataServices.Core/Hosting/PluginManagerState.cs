﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Hosting
{
    /// <summary>
    /// Declares constants specifying different state of a <see cref="PluginManager"/>
    /// </summary>
    public enum PluginManagerState
    {
        /// <summary>
        /// The plugin manager has been instantiated and the plugins have not been activated.
        /// </summary>
        Idle = 0x00,

        /// <summary>
        /// The plugin manager is currently activating the plugins
        /// </summary>
        Activating = 0x01,

        /// <summary>
        /// The plugins have been activated and are currently in an idle state.
        /// </summary>
        Activated = 0x02,

        /// <summary>
        /// The plugins are being started 
        /// </summary>
        Starting = 0x03,

        /// <summary>
        /// The plugins are currently running
        /// </summary>
        Running = 0x04,

        /// <summary>
        /// The plugins are being stopped
        /// </summary>
        Stopping = 0x05,

        /// <summary>
        /// All plugins have been stopped.
        /// </summary>
        Stopped = 0x06,

        /// <summary>
        /// The plugins are being disposed.
        /// </summary>
        Disposing = 0x07,

        /// <summary>
        /// The plugins have been disposed.
        /// </summary>
        Disposed = 0x08,

        /// <summary>
        /// An exception occured that prevents the plugin manager from working.
        /// </summary>
        Faulted = 0x0A
    }
}
