﻿using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.DataServices.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MT.DataServices.Hosting
{
    internal class PluginHost : IPluginHost
    {
        private readonly CommonPluginHost commonPluginHost;
        private readonly Type pluginType;

        private PluginConfiguration pluginConfiguration;
        private object configurationObject;

        internal PluginHost(CommonPluginHost commonPluginHost, Type pluginType)
        {
            this.pluginType = pluginType;
            this.commonPluginHost = commonPluginHost;

            pluginConfiguration = commonPluginHost.HostConfiguration.PluginConfigurations.FirstOrDefault(d => d.PluginType == pluginType.FullName);
        }

        /// <summary>
        /// Occures when the plugin start phase executes
        /// </summary>
        public event EventHandler<ServicesEventArgs> StartServices
        {
            add { commonPluginHost.StartServices += value; }
            remove { commonPluginHost.StartServices -= value; }
        }

        /// <summary>
        /// Occures when the plugin stop phase executes
        /// </summary>
        public event EventHandler<ServicesEventArgs> StopServices
        {
            add { commonPluginHost.StopServices += value; }
            remove { commonPluginHost.StopServices -= value; }
        }

        /// <summary>
        /// Occurs when a service container is created.
        /// </summary>
        public event EventHandler<ServiceContainerEventArgs> ServiceContainerCreated
        {
            add { commonPluginHost.ServiceContainerCreated += value; }
            remove { commonPluginHost.ServiceContainerCreated -= value; }
        }

        /// <summary>
        /// Occurs when a service container is disposed.
        /// </summary>
        public event EventHandler<ServiceContainerEventArgs> ServiceContainerDisposed
        {
            add { commonPluginHost.ServiceContainerDisposed += value; }
            remove { commonPluginHost.ServiceContainerDisposed -= value; }
        }

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        public RuntimeConfiguration RuntimeConfiguration
        {
            get { return commonPluginHost.RuntimeConfiguration; }
        }

        /// <summary>
        /// Gets the collection of active service containers.
        /// </summary>
        public Collection<IServiceContainer> ServiceContainers
        {
            get { return commonPluginHost.ServiceContainers; }
        }

        /// <summary>
        /// Gets or sets the XAML serializable configuration object of this plugin.
        /// </summary>
        public object LoadConfigurationObject()
        {
            if (pluginConfiguration == null)
                return null;
            else
                return XNodeSerializer.Deserialize<object>(pluginConfiguration.ConfigurationObject);
        }

        /// <summary>
        /// Gets or sets the XAML serializable configuration object of this plugin.
        /// </summary>
        public void SaveConfigurationObject(object value)
        {
            if (pluginConfiguration == null)
            {
                pluginConfiguration = new PluginConfiguration(pluginType.FullName);
                commonPluginHost.HostConfiguration.PluginConfigurations.Add(pluginConfiguration);
            }

            configurationObject = value;
            pluginConfiguration.ConfigurationObject = XNodeSerializer.Serialize(value);
        }

        /// <summary>
        /// Adds the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to attach.</param>
        public void AttachLogSink(ILogSink logSink)
        {
            commonPluginHost.AttachLogSink(logSink);
        }

        /// <summary>
        /// Removes the log sink as a listener for log messages of the system log.
        /// </summary>
        /// <param name="logSink">The log sink to detach.</param>
        public void DetachLogSink(Logging.ILogSink logSink)
        {
            commonPluginHost.DetachLogSink(logSink);
        }
    }
}
