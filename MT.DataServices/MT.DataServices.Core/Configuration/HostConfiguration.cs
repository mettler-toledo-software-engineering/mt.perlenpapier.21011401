﻿using MT.DataServices.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MT.DataServices.Configuration
{
    /// <summary>
    /// Contains the full configuration data for a plugin host.
    /// </summary>
    public class HostConfiguration : IFreezable
    {
        private readonly XCollection<PluginConfiguration> pluginConfigurations;
        private readonly XDocument document;

        /// <summary>
        /// Initializes a new instance of the <see cref="HostConfiguration"/> class.
        /// </summary>
        public HostConfiguration()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HostConfiguration"/> class.
        /// </summary>
        public HostConfiguration(XDocument document)
        {
            if (document == null)
            {
                document = new XDocument();
                var root = new XElement("MT.DataService.Configuration");
                var host = new XElement("Host");

                host.Add(new XElement("PluginFolders", ""));
                host.Add(new XElement("DataFolder", ""));
                host.Add(new XElement("LogFolder", ""));

                root.Add(host);
                root.Add(new XElement("Plugins", ""));
                document.Add(root);                               
            }

            this.document = document;
            pluginConfigurations = new XCollection<PluginConfiguration>(document.Root.Element("Plugins"));   
        }

        /// <summary>
        /// Gets or sets the folder(s) containing the plugins seperated by semicolons.
        /// </summary>
        public string PluginFolders
        {
            get { return document.Root.Element("Host").Element("PluginFolders").Value; }
            set
            {
                this.VerifyWriteAccess("PluginFolders");
                document.Root.Element("Host").Element("PluginFolders").Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the folder containing the data files
        /// </summary>
        public string DataFolder
        {
            get { return document.Root.Element("Host").Element("DataFolder").Value; }
            set
            {
                this.VerifyWriteAccess("DataFolder");
                document.Root.Element("Host").Element("DataFolder").Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the folder containing the log files.
        /// </summary>
        public string LogFolder
        {
            get { return document.Root.Element("Host").Element("LogFolder").Value; }
            set
            {
                this.VerifyWriteAccess("LogFolder");
                document.Root.Element("Host").Element("LogFolder").Value = value;
            }
        }

        /// <summary>
        /// Gets the configuration for the particular plugins.
        /// </summary>
        public IList<PluginConfiguration> PluginConfigurations
        {
            get { return pluginConfigurations; }
        }

        internal XDocument Document
        {
            get { return document; }
        }

        #region IFreezable Implementation

        /// <summary>
        /// Gets a value indicating whether this instance is frozen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is frozen; otherwise, <c>false</c>.
        /// </value>
        public bool IsFrozen
        {
            get { return isFrozen; }
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Freeze()
        {
            pluginConfigurations.Freeze();

            isFrozen = true;
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Unfreeze()
        {
            isFrozen = false;

            pluginConfigurations.Unfreeze();
        }

        private bool isFrozen;

        #endregion 
    }
}
