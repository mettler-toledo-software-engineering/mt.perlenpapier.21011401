﻿using MT.DataServices.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MT.DataServices.Configuration
{
    /// <summary>
    /// Stores the configuration for a single plugin.
    /// </summary>
    public sealed class PluginConfiguration : IFreezable, IXCollectionItem
    {
        private XElement element;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginConfiguration"/> class.
        /// </summary>
        public PluginConfiguration()
        {
            element = new XElement("Plugin");            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginConfiguration"/> class.
        /// </summary>
        public PluginConfiguration(string pluginType)
            : this()
        {
            PluginType = pluginType;
        }

        /// <summary>
        /// Gets or sets the type of the plugin that this configuration belongs to.
        /// </summary>
        public string PluginType
        {
            get { return (string)element.Attribute("Type"); }
            set
            {
                this.VerifyWriteAccess("PluginType");
                element.SetAttributeValue("Type", value);
            }
        }

        /// <summary>
        /// Gets or sets an XAML serializable object that contains the configuration for the plugin.
        /// </summary>
        public XElement ConfigurationObject
        {
            get { return element.Elements().FirstOrDefault(); }
            set
            {
                this.VerifyWriteAccess("ConfigurationObject");
                if (element.HasElements)
                    element.RemoveNodes();

                if (value != null)
                    element.Add(value);
            }
        }

        #region IFreezable Implementation

        /// <summary>
        /// Gets a value indicating whether this instance is frozen.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is frozen; otherwise, <c>false</c>.
        /// </value>
        public bool IsFrozen
        {
            get { return isFrozen; }
        }

        /// <summary>
        /// Freezes this instance.
        /// </summary>
        public void Freeze()
        {
            isFrozen = true;
        }

        /// <summary>
        /// Unfreezes this instance.
        /// </summary>
        public void Unfreeze()
        {
            isFrozen = false;
        }

        private bool isFrozen;

        #endregion 
    
        XElement IXCollectionItem.Element
        {
            get { return element; }
            set { element = value; }
        }
    }
}
