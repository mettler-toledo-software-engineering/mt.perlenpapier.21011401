﻿using MT.DataServices.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MT.DataServices.Configuration
{
    /// <summary>
    /// Provides access the fully expanded paths and provides runtime dependent configuration information.
    /// </summary>
    public class RuntimeConfiguration
    {        
        private readonly HostConfiguration configuration;
        private readonly StringExpander expander;
        private readonly string homeFolder;

        private string logFolderCache;
        private string logFolderOverride;
        private string dataFolderCache;
        private string dataFolderOverride;
        private string pluginFoldersCache;
        private string pluginFolderOverride;


        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeConfiguration"/> class.
        /// </summary>
        public RuntimeConfiguration(HostConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            this.configuration = configuration;

            homeFolder = PathUtilities.GetHomeFolder();

            expander = new StringExpander(StringExpanderOptions.ExpandEnvironmentVariables | StringExpanderOptions.Recursive);
            expander.Expansions["HOME"] = homeFolder;
        }

        /// <summary>
        /// Gets the host configuration.
        /// </summary>
        public HostConfiguration HostConfiguration
        {
            get { return configuration; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the system is running in debug mode. Use this flag to turn on verbose logging.
        /// </summary>
        public bool DebugMode { get; set; }

        /// <summary>
        /// Gets the home folder (i.e. the folder containing the host executable).
        /// </summary>
        public string HomeFolder
        {
            get { return homeFolder; }
        }

        /// <summary>
        /// Gets or sets the folder(s) containing the plugins seperated by semicolons.
        /// </summary>
        public string PluginFolders
        {
            get { return Cache(ref pluginFoldersCache, pluginFolderOverride, () => expander.Expand(configuration.PluginFolders)); }
            set { pluginFolderOverride = value; }
        }

        /// <summary>
        /// Gets or sets the folder containing the data files
        /// </summary>
        public string DataFolder
        {
            get { return Cache(ref dataFolderCache, dataFolderOverride, () => expander.Expand(configuration.DataFolder)); }
            set { dataFolderOverride = value; }
        }

        /// <summary>
        /// Gets or sets the folder containing the log files.
        /// </summary>
        public string LogFolder
        {
            get { return Cache(ref logFolderCache, logFolderOverride, () => expander.Expand(configuration.LogFolder)); }
            set { logFolderOverride = value; }
        }

        private T Cache<T>(ref T cache, T @override, Func<T> valueProvider)
            where T : class
        {
            if (@override != null)
                return @override;

            if (cache == null || !configuration.IsFrozen)
                cache = valueProvider();

            return cache;
        }
    }
}
