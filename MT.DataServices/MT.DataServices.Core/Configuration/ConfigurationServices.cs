﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xaml;
using System.Xml.Linq;

namespace MT.DataServices.Configuration
{
    /// <summary>
    /// Provides methods to load and save the configuration in a file.
    /// </summary>
    public static class ConfigurationServices
    {
        /// <summary>
        /// Loads the configuration from a file.
        /// </summary>
        /// <param name="fileName">Name of the configuration file.</param>
        /// <returns>The </returns>
        public static HostConfiguration LoadConfiguration(string fileName)
        {
            return new HostConfiguration(XDocument.Load(fileName));                
        }

        /// <summary>
        /// Saves the configuration in the specified file.
        /// </summary>
        /// <param name="fileName">Name of the configuration file.</param>
        /// <param name="configuration">The configuration to save.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="configuration" /> is <c>null</c>.</exception>
        public static void SaveConfiguration(string fileName, HostConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            configuration.Document.Save(fileName);
        }
    }
}
