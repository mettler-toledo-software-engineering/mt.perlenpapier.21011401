﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Declares constants specifing different kinds of log messages
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// The message is a notification that an action is executed
        /// </summary>
        Action,

        /// <summary>
        /// An action has successfully completed
        /// </summary>
        Success,

        /// <summary>
        /// A hint for improving configuration
        /// </summary>
        Hint,

        /// <summary>
        /// A warning that a problem might occur
        /// </summary>
        Warning,

        /// <summary>
        /// An error that prevents the current action from being completed
        /// </summary>
        Error,

        /// <summary>
        /// An error that prevent the execution of a complete process
        /// </summary>
        Fatal
    }
}