﻿using System;
using System.Linq;
using System.Text;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// A serializable class that contains the data of an exception
    /// </summary>
    public sealed class ExceptionData
    {
        private string stringRepresentation;

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the stack trace.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Gets or sets the source of the exception
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the type of the exception
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the inner exception data
        /// </summary>
        public ExceptionData InnerException { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionData"/> class.
        /// </summary>
        public ExceptionData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionData"/> class.
        /// </summary>
        /// <param name="ex">The exception to be documented.</param>
        public ExceptionData(Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException("ex");

            Type = ex.GetType().FullName;
            Message = ex.Message;
            StackTrace = ex.StackTrace;
            InnerException = ex.InnerException != null ? new ExceptionData(ex.InnerException) : null;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (stringRepresentation == null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0,-10}: {1}", "Type", Type).AppendLine();
                sb.AppendFormat("{0,-10}: {1}", "Message", Message).AppendLine();
                sb.Append("Stack Trace: ").Append('-', 30).AppendLine();
                sb.Append(StackTrace);
                sb.AppendLine();
                if (InnerException != null)
                {
                    sb.AppendLine();
                    sb.Append("Inner Exception: ").Append('-', 30).AppendLine();
                    sb.Append(InnerException.ToString());
                }

                stringRepresentation = sb.ToString();
            }

            return stringRepresentation;
        }
    }
}