﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Extracts log entries that meet a certain criteria.
    /// </summary>
    public class LogSplitter : ILogSink, IDisposable
    {
        private readonly Func<LogMessage, bool> splitCriteria;
        private readonly ILogSink innerLogSink;

        /// <summary>
        /// Occurs when a messaged was logged that met the specified criteria.
        /// </summary>
        public event Action<LogMessage> MessageReceived;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogSplitter"/> class.
        /// </summary>
        public LogSplitter(Func<LogMessage, bool> splitCriteria, ILogSink innerLogSink)
        { 
            if (splitCriteria == null)
                throw new ArgumentNullException("splitCriteria");

            if (innerLogSink == null)
                throw new ArgumentNullException("innerLogSink");

            this.splitCriteria = splitCriteria;
            this.innerLogSink = innerLogSink;
        }

        /// <summary>
        /// Handles a <see cref="LogMessage"/>
        /// </summary>
        /// <param name="message">Message to handle</param>
        /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
        public void Log(LogMessage message)
        {
            if (MessageReceived != null)
            {
                if (splitCriteria(message))
                    MessageReceived(message);
            }

            innerLogSink.Log(message);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            var disposableLogSink = innerLogSink as IDisposable;

            if (disposableLogSink != null)
                disposableLogSink.Dispose();            
        }
    }
}