﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Distributes a single <see cref="LogMessage"/> to multiple <see cref="ILogSink"/>s
    /// </summary>
    public class LogDistributor : Collection<ILogSink>, ILogSink
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance should lock itself while distributing a log message.
        /// </summary>
        public bool LockDuringLogging { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogDistributor"/> class.
        /// </summary>
        public LogDistributor()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogDistributor"/> class.
        /// </summary>
        public LogDistributor(IEnumerable<ILogSink> targets)
        {
            if (targets == null)
                throw new ArgumentNullException("targets");

            foreach (var target in targets)
            {
                Add(target);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogDistributor"/> class.
        /// </summary>
        public LogDistributor(params ILogSink[] targets)
            : this((IEnumerable<ILogSink>)targets)
        {
        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />
        /// </summary>
        /// <param name="message">Message to handle</param>
        /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
        public void Log(LogMessage message)
        {
            if (LockDuringLogging)
            {
                lock (this)
                {
                    foreach (var sink in this)
                    {
                        sink.Log(message);
                    }
                }
            }
            else
            {
                foreach (var sink in this)
                {
                    sink.Log(message);
                }
            }
        }
    }
}