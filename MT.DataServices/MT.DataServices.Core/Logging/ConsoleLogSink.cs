﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Displays the log message in the console.
    /// </summary>
    public class ConsoleLogSink : ILogSink
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleLogSink"/> class.
        /// </summary>
        public ConsoleLogSink()
        {

        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />.
        /// </summary>
        /// <param name="message">Message to handle.</param>
        public void Log(LogMessage message)
        {
            Console.ForegroundColor = LogLevelToConsoleColor(message.Level);
            Console.WriteLine(message.ToShortString());
            Console.ResetColor();
        }

        private static ConsoleColor LogLevelToConsoleColor(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Action:
                    return ConsoleColor.Gray;
                case LogLevel.Success:
                    return ConsoleColor.DarkGreen;
                case LogLevel.Hint:
                    return ConsoleColor.DarkCyan;
                case LogLevel.Warning:
                    return ConsoleColor.DarkYellow;
                case LogLevel.Error:
                    return ConsoleColor.DarkRed;
                case LogLevel.Fatal:
                    return ConsoleColor.Red;
                default:
                    throw new ArgumentOutOfRangeException("level");
            }
        }

    }
}
