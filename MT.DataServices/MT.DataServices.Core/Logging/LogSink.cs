﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Contains helper methods for the <see cref="ILogSink"/> interface
    /// </summary>
    public static class LogSink
    {
        /// <summary>
        /// Gets a log sink that discards all incoming messages
        /// </summary>
        public static readonly ILogSink Empty = DummyLogSink.Instance;

        /// <summary>
        /// Logs the execution of an action.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Action(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Action, message, self.GetSource()));
        }

        /// <summary>
        /// Logs the successful completition of an action.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Success(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Success, message, self.GetSource()));
        }

        /// <summary>
        /// Logs a hint.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Hint(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Hint, message, self.GetSource()));
        }

        /// <summary>
        /// Logs a hint.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Hint(this ILogSink self, string message, Exception ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Hint, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs a hint.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Hint(this ILogSink self, string message, ExceptionData ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Hint, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs a warning.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Warning(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Warning, message, self.GetSource()));
        }

        /// <summary>
        /// Logs a warning.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Warning(this ILogSink self, string message, Exception ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Warning, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs a warning.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Warning(this ILogSink self, string message, ExceptionData ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Warning, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs an error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Error(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Error, message, self.GetSource()));
        }

        /// <summary>
        /// Logs an error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Error(this ILogSink self, string message, Exception ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Error, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs an error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Error(this ILogSink self, string message, ExceptionData ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Error, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs a fatal error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        public static void Fatal(this ILogSink self, string message)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Fatal, message, self.GetSource()));
        }

        /// <summary>
        /// Logs a fatal error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Fatal(this ILogSink self, string message, Exception ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Fatal, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Logs a fatal error.
        /// </summary>
        /// <param name="self">The <see cref="ILogSink"/> that handles the log message.</param>
        /// <param name="message">The message that describes the action.</param>
        /// <param name="ex">The attached exception</param>
        public static void Fatal(this ILogSink self, string message, ExceptionData ex)
        {
            if (self == null)
                throw new ArgumentNullException("self");

            self.Log(new LogMessage(LogLevel.Fatal, message, self.GetSource(), ex));
        }

        /// <summary>
        /// Gets the source of a log message
        /// </summary>
        /// <param name="self">A log sink to determine the source name from</param>
        private static string GetSource(this ILogSink self)
        {
            if (self is IProvideLogSource)
                return ((IProvideLogSource)self).Source;
            else
                return null;            
        }
    }
}