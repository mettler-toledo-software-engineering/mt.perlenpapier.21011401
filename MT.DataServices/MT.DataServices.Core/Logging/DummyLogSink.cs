﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Log sink that discards all incoming log messages
    /// </summary>
    public class DummyLogSink : ILogSink
    {
        /// <summary>
        /// Gets an instance of the <see cref="DummyLogSink"/>.
        /// </summary>
        public static readonly DummyLogSink Instance = new DummyLogSink();

        /// <summary>
        /// Handles a <see cref="LogMessage"/>
        /// </summary>
        /// <param name="message">Message to handle</param>
        /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
        public void Log(LogMessage message)
        {
        }
    }
}