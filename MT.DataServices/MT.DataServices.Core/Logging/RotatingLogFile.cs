﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Writes the log information into a rotating log file
    /// </summary>
    public class RotatingLogFile : ILogSink, IDisposable
    {
        /// <summary>
        /// The maximum log size that is used when no size limit is specified
        /// </summary>
        public static readonly int DefaultMaximumLogSize = 100 * 1024;

        /// <summary>
        /// The minimum size of a log file.
        /// </summary>
        public static readonly int MinimumLogSize = 1024;

        private readonly int maximumLogSize;
        private readonly int maximumLogFileCount;
        private readonly string fileNameBase;
        private readonly string extension;
        private readonly byte[] writeBuffer = new byte[1024];
        private readonly Encoding encoding = Encoding.UTF8;

        private int counter;
        private bool disabled;
        private FileStream logFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="RotatingLogFile"/> class.
        /// </summary>
        public RotatingLogFile(string fileName)
            : this(fileName, DefaultMaximumLogSize, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RotatingLogFile" /> class.
        /// </summary>
        /// <param name="fileName">Name of the log file.</param>
        /// <param name="maximumLogSize">Maximum size of the log file in bytes.</param>
        /// <param name="maximumLogFileCount">The maximum number of log files concurrently on the disk. Use <c>0</c> for unlimited files.</param>
        public RotatingLogFile(string fileName, int maximumLogSize, int maximumLogFileCount)
        {
            if (fileName == null)
                throw new ArgumentNullException("fileName");

            if (maximumLogSize < MinimumLogSize)
                throw new ArgumentOutOfRangeException("maximumLogSize", string.Format("The log file must be allowed to grow at least {0} bytes in size.", MinimumLogSize));

            if (maximumLogFileCount < 0)
                throw new ArgumentOutOfRangeException("maximumLogFileCount");

            this.maximumLogSize = maximumLogSize;
            this.maximumLogFileCount = maximumLogFileCount;

            fileNameBase = Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName)) + ".";
            extension = Path.GetExtension(fileName);
            counter = 1;

            DeleteExcessLogFiles();
            DetectAndOpenLogFile();
        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />
        /// </summary>
        /// <param name="message">Message to handle.</param>
        public void Log(LogMessage message)
        {
            if (disabled || message == null)
                return;

            try
            {
                string messageText = message.ToString();
                int size = encoding.GetByteCount(messageText);
                byte[] buffer;

                if (size <= writeBuffer.Length)
                    buffer = writeBuffer;
                else
                    buffer = new byte[size];

                encoding.GetBytes(messageText, 0, messageText.Length, buffer, 0);

                if (logFile.Length + size > maximumLogSize)
                {
                    counter++;
                    logFile.Dispose();
                    try
                    {
                        logFile = new FileStream(GetCurrentFileName(), FileMode.Append, FileAccess.Write, FileShare.Read);
                    }
                    catch (IOException)
                    {
                        disabled = true;
                        logFile = null;
                        return;
                    }

                    DeleteExcessLogFiles();
                }

                logFile.Write(buffer, 0, size);
                logFile.Flush();
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (logFile != null)
            {
                logFile.Dispose();
                logFile = null;
                disabled = true;
            }
        }

        private void DetectAndOpenLogFile()
        {
            try
            {
                string directoryName = Path.GetDirectoryName(fileNameBase);
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string[] existingFiles = Directory.GetFiles(directoryName, Path.GetFileName(fileNameBase) + "*" + extension);
                for (int i = 0; i < existingFiles.Length; i++)
                {
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(existingFiles[i]);
                    int lastDot = fileNameWithoutExtension.LastIndexOf('.');
                    if (lastDot == -1)
                        continue;

                    int number;
                    if (int.TryParse(fileNameWithoutExtension.Substring(lastDot + 1), out number))
                    {
                        if (number > counter)
                            counter = number;
                    }
                }

                logFile = new FileStream(GetCurrentFileName(), FileMode.Append, FileAccess.Write, FileShare.Read);
            }
            catch (UnauthorizedAccessException)
            {
                disabled = true;
            }
            catch (IOException)
            {
                disabled = true;
            }
        }

        private void DeleteExcessLogFiles()
        {
            if (maximumLogFileCount == 0)
                return;

            var fileNames = new List<Tuple<int, string>>();
            try
            {
                string directoryName = Path.GetDirectoryName(fileNameBase);
                if (!Directory.Exists(directoryName))
                    return;

                string[] existingFiles = Directory.GetFiles(directoryName, Path.GetFileName(fileNameBase) + "*" + extension);
                for (int i = 0; i < existingFiles.Length; i++)
                {
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(existingFiles[i]);
                    int lastDot = fileNameWithoutExtension.LastIndexOf('.');
                    if (lastDot == -1)
                        continue;

                    int number;
                    if (int.TryParse(fileNameWithoutExtension.Substring(lastDot + 1), out number))
                    {
                        fileNames.Add(Tuple.Create(number, existingFiles[i]));
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }
            catch (IOException)
            {
                return;
            }

            if (fileNames.Count > maximumLogFileCount)
            {
                fileNames.Sort((l, r) => l.Item1.CompareTo(r.Item1));
                for (int i = 0; i < fileNames.Count - maximumLogFileCount; i++)
                {
                    try
                    {
                        File.Delete(fileNames[i].Item2);
                    }
                    catch (UnauthorizedAccessException)
                    {
                    }
                    catch (IOException)
                    {
                    }
                }
            }
        }

        private string GetCurrentFileName()
        {
            return fileNameBase + counter.ToString() + extension;
        }
    }
}