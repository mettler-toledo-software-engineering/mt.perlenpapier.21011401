﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Exposes a property that allows to get a source of a log message
    /// </summary>
    public interface IProvideLogSource : ILogSink
    {
        /// <summary>
        /// Gets the source name of the log message
        /// </summary>
        string Source { get; }
    }
}