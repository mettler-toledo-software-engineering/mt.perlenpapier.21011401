﻿using System;
using System.Linq;
using System.Text;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Contains all information about one log message
    /// </summary>
    public sealed class LogMessage
    {       
        private string stringRepresentation;

        /// <summary>
        /// Gets or sets a user-defined identifier.
        /// </summary>
        public int Identifier { get; set; }

        /// <summary>
        /// Gets or sets the level of the log message
        /// </summary>
        public LogLevel Level { get; set; }

        /// <summary>
        /// Gets or sets the source of the message
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the message text
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the exception attached to this message
        /// </summary>
        public ExceptionData Exception { get; set; }

        /// <summary>
        /// Gets or sets the addinitional data
        /// </summary>
        public string AdditionalData { get; set; }

        /// <summary>
        /// Gets or sets the timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        public LogMessage()
        {
            Timestamp = DateTime.Now;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        public LogMessage(LogLevel level, string message)
            : this()
        {
            Level = level;
            Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        public LogMessage(LogLevel level, string message, string source)
            : this(level, message)
        {
            Source = source;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        /// <param name="ex">The attached exception</param>
        public LogMessage(LogLevel level, string message, string source, Exception ex)
            : this(level, message, source)
        {
            if (ex != null)
                Exception = new ExceptionData(ex);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        /// <param name="ex">The attached exception</param>
        public LogMessage(LogLevel level, string message, string source, ExceptionData ex)
            : this(level, message, source)
        {
            Exception = ex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        /// <param name="ex">The attached exception</param>
        /// <param name="additionalData">The additional data for the message.</param>
        public LogMessage(LogLevel level, string message, string source, Exception ex, string additionalData)
            : this(level, message, source, ex)
        {
            AdditionalData = additionalData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        /// <param name="ex">The attached exception</param>
        /// <param name="additionalData">The additional data for the message.</param>
        public LogMessage(LogLevel level, string message, string source, ExceptionData ex, string additionalData)
            : this(level, message, source, ex)
        {
            AdditionalData = additionalData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> class.
        /// </summary>
        /// <param name="level">The level of message.</param>
        /// <param name="message">The log message.</param>
        /// <param name="source">The source of the message.</param>
        /// <param name="ex">The attached exception</param>
        /// <param name="additionalData">The additional data for the message.</param>
        /// <param name="timestamp">The time this log message was recorded</param>
        public LogMessage(LogLevel level, string message, string source, ExceptionData ex, string additionalData, DateTime timestamp)
            : this(level, message, source, ex, additionalData)
        {
            Timestamp = timestamp;
        }

        /// <summary>
        /// Returns a short <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public string ToShortString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Timestamp.ToString());
            sb.Append(" [");

            string levelName;

            switch (Level)
            {
                case LogLevel.Action:
                    levelName = "ACTION";
                    break;
                case LogLevel.Success:
                    levelName = "SUCCESS";
                    break;
                case LogLevel.Hint:
                    levelName = "HINT";
                    break;
                case LogLevel.Warning:
                    levelName = "WARNING";
                    break;
                case LogLevel.Error:
                    levelName = "ERROR";
                    break;
                case LogLevel.Fatal:
                    levelName = "FATAL";
                    break;
                default:
                    levelName = "UNKNOWN";
                    break;
            }

            sb.Append(levelName);
            sb.Append(' ', 7 - levelName.Length);
            sb.Append("] - ");
            sb.Append(Message);

            return sb.ToString();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (stringRepresentation == null)
            {
                StringBuilder sb = new StringBuilder(ToShortString());
                sb.AppendLine();

                if (!string.IsNullOrEmpty(AdditionalData))
                {
                    sb.Append("Additional Information: ").Append('-', 30).AppendLine();
                    sb.AppendLine(AdditionalData);
                }

                if (Exception != null)
                {
                    sb.Append("Exception: ").Append('-', 30).AppendLine();
                    sb.AppendLine(Exception.ToString());
                }

                stringRepresentation = sb.ToString();
            }

            return stringRepresentation;
        }
    }
}