﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Filters log messages before passing them on
    /// </summary>
    public class LogFilter : ILogSink, IDisposable
    {
        private readonly Func<LogMessage, bool> condition;
        private readonly LogLevel minimumLevel;

        /// <summary>
        /// Gets the target for the messages that pass the filter
        /// </summary>
        public ILogSink Target { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogFilter"/> class.
        /// </summary>
        /// <param name="target">The target for the messages that pass the filter.</param>
        /// <param name="minimumLevel">The minimum level the messages must have to be passed on</param>
        public LogFilter(ILogSink target, LogLevel minimumLevel)
        {
            if (target == null)
                throw new ArgumentNullException("target");
            
            if (minimumLevel < LogLevel.Action || minimumLevel > LogLevel.Fatal)
                throw new ArgumentOutOfRangeException("Invalid log level enum value.", "minimumLevel");

            this.minimumLevel = minimumLevel;
            Target = target;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogFilter"/> class.
        /// </summary>
        /// <param name="target">The target for the messages that pass the filter.</param>
        /// <param name="condition">The condition the messages must met to be passed on.</param>
        public LogFilter(ILogSink target, Func<LogMessage, bool> condition)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            if (condition == null)
                throw new ArgumentNullException("condition");

            this.condition = condition;
            Target = target;
        }

        /// <summary>
        /// Handles a <see cref="LogMessage"/>
        /// </summary>
        /// <param name="message">Message to handle</param>
        /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
        public void Log(LogMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            if (condition != null)
            {
                if (condition(message))
                    Target.Log(message);
            }
            else if (message.Level >= minimumLevel)
                Target.Log(message);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            var disposableTarget = Target as IDisposable;
            if (disposableTarget != null)
                disposableTarget.Dispose();
        }
    }
}