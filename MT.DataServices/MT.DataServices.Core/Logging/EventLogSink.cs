﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public class EventLogSink : ILogSink
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogSink"/> class.
        /// </summary>
        public EventLogSink()
        {
            
        }

        /// <summary>
        /// Handles a <see cref="LogMessage" />.
        /// </summary>
        /// <param name="message">Message to handle.</param>
        public void Log(LogMessage message)
        {
            EventLog.WriteEntry("MT.DataServices", message.ToString(), LogLevelToEventLogEntryType(message.Level));
        }

        private static EventLogEntryType LogLevelToEventLogEntryType(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Action:
                    return EventLogEntryType.Information;
                case LogLevel.Success:
                    return EventLogEntryType.SuccessAudit;
                case LogLevel.Hint:
                    return EventLogEntryType.Information;
                case LogLevel.Warning:
                    return EventLogEntryType.Warning;
                case LogLevel.Error:
                    return EventLogEntryType.Error;
                case LogLevel.Fatal:
                    return EventLogEntryType.Error;
                default:
                    throw new ArgumentOutOfRangeException("level");
            }
        }
    }
}
