﻿using System;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Exposes a method that allows to generate log messages
    /// </summary>
    public interface ILogSink
    {
        /// <summary>
        /// Handles a <see cref="LogMessage"/>.
        /// </summary>
        /// <param name="message">Message to handle.</param>
        /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
        void Log(LogMessage message);
    }
}