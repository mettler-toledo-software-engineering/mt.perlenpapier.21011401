﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Logging
{
    /// <summary>
    /// Combines multiple log sources and aggregates all log messages and sends them to one target.
    /// </summary>
    public class LogAggregator
    {
        /// <summary>
        /// Gets a list of sources
        /// </summary>
        public ReadOnlyCollection<ILogSink> Sources { get; private set; }

        private readonly List<ILogSink> sources = new List<ILogSink>();

        /// <summary>
        /// Gets the target for all the log messages
        /// </summary>
        public ILogSink Target { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogAggregator"/> class.
        /// </summary>
        public LogAggregator(ILogSink target)
        {
            Sources = new ReadOnlyCollection<ILogSink>(sources);
            Target = target;
        }

        /// <summary>
        /// Creates a new named log sink.
        /// </summary>
        /// <param name="name">The name of the log sink.</param>
        /// <returns>The newly created log sink</returns>
        public ILogSink CreateLogSink(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            var newSink = new LogAggregatorLogSink(this, name);
            sources.Add(newSink);

            return newSink;
        }

        /// <summary>
        /// Removes the specified sink from the aggregator.
        /// </summary>
        /// <param name="sink">The sink to remove.</param>
        public void Remove(ILogSink sink)
        {
            var typedSink = sink as LogAggregatorLogSink;

            if (typedSink == null || (typedSink.target != null && typedSink.target != this))
                throw new ArgumentException("Invalid log sink type", "typedSink");

            typedSink.Stop();
            sources.Remove(sink);
        }

        /// <summary>
        /// Log sink for the log aggeragtor
        /// </summary>
        private class LogAggregatorLogSink : ILogSink, IProvideLogSource
        {
            internal LogAggregator target;

            /// <summary>
            /// Gets the source name of the log sink
            /// </summary>
            public string Source { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="LogAggregatorLogSink"/> class.
            /// </summary>
            public LogAggregatorLogSink(LogAggregator target, string source)
            {
                if (target == null)
                    throw new ArgumentNullException("target");

                this.target = target;
                Source = source;
            }

            /// <summary>
            /// Handles a <see cref="LogMessage" />
            /// </summary>
            /// <param name="message">Message to handle</param>
            /// <exception cref="InvalidOperationException">The user is trying to write to a closed log sink.</exception>
            public void Log(LogMessage message)
            {
                if (target == null)
                    throw new InvalidOperationException("The log sink has been disconnected from the aggregator.");

                if (target.Target != null)
                    target.Target.Log(message);
            }

            /// <summary>
            /// Disconnects the log sink
            /// </summary>
            internal void Stop()
            {
                target = null;
            }
        }
    }
}