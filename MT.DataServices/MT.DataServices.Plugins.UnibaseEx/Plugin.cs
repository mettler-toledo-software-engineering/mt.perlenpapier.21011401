﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using MT.DataServices.Plugins.UnibaseEx.Configuration;
using MT.DataServices.Plugins.UnibaseEx.Utilities;
using MT.DataServices.Plugins.UnibaseEx.Communication;
using MT.DataServices.Logging;
using MT.DataServices.Configuration;

namespace MT.DataServices.Plugins.UnibaseEx
{
    /// <summary>
    /// Implements the interface the MT.DataServices host system.
    /// </summary>
    [Export(typeof(IPlugin))]
    public sealed class Plugin : IPlugin
    {
        private UnibaseExConfiguration configuration;
        private IPluginHost host;
        private List<IServiceContainer> services = new List<IServiceContainer>();

        [Import]
        private ServiceProviderRegistry registry = null;

        [Import]
        private ILogSink log = null;

        
        /// <summary>
        /// Activates the plugin.
        /// </summary>
        /// <param name="host">The plugin host environment.</param>
        public void Activate(IPluginHost host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            this.host = host;

            LoadConfiguration();

            host.StartServices += OnStartServices;
            host.StopServices += OnStopServices;
        }
  
        /// <summary>
        /// Creates a user control that provides the UI for configuring this plugin.
        /// </summary>
        /// <param name="configurationHost">The configuration dialog host object.</param>
        public IEnumerable<IPluginConfigurationPane> CreateConfigurationControl(IPluginConfigurationHost configurationHost)
        {
            if (configurationHost == null)
                throw new ArgumentNullException("configurationHost");

            yield return new ConfigurationDialog(this, configuration, configurationHost, registry);
        }

        /// <summary>
        /// Gets the manufacturer of the plugin.
        /// </summary>
        public string Manufacturer
        {
            get { return ProductInformation.Manufacturer; }
        }

        /// <summary>
        /// Gets the name of this plugin.
        /// </summary>
        public string Name
        {
            get { return "UnibaseEx"; }
        }

        /// <summary>
        /// Gets the version of the plugin.
        /// </summary>
        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            
        }

        internal RuntimeConfiguration RuntimeConfiguration
        {
            get { return host.RuntimeConfiguration; }
        }

        internal UnibaseExConfiguration Configuration
        {
            get { return configuration; }
        }

        internal ServiceProviderRegistry Registry
        {
            get { return registry; }
        }

        internal void LoadConfiguration()
        {
            configuration = host.LoadConfigurationObject() as UnibaseExConfiguration ?? new UnibaseExConfiguration();
        }

        internal void SaveConfiguration()
        {
            host.SaveConfigurationObject(configuration);
        }

        private void OnStartServices(object sender, ServicesEventArgs e)
        {
            services.Clear();

            foreach (var endpoint in configuration.EndPoints)
            {
                var endpointProvider = registry.EndpointProviders.FirstOrDefault(d => d.GetType().FullName == endpoint.EndpointType);
                if (endpointProvider == null)
                {
                    log.Error("Cannot find endpoint provider of type " + endpoint.EndpointType);
                    continue;
                }
                
                var endpointHostingService = new EndPointHostingService(this, endpoint, endpointProvider);
                var serviceContainer = e.ServiceManager.CreateServiceContainer(endpointHostingService, TimeSpan.FromSeconds(10));
                services.Add(serviceContainer);
                serviceContainer.Start();
            }
        }

        private void OnStopServices(object sender, ServicesEventArgs e)
        {
            while (services.Count > 0)
            {
                services[0].Dispose();
                services.RemoveAt(0);
            }
        }
    }
}
