﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Communication;
using MT.DataServices.Plugins.UnibaseEx.Sql;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities
{
    [Export]
    internal class ServiceProviderRegistry
    {
        [ImportMany]
        internal IEnumerable<ISqlFormatterProvider> SqlFormatters { get; private set; }

        [ImportMany]
        internal IEnumerable<IRelationalBackendProvider> RelationalBackends { get; private set; }

        [ImportMany]
        internal IEnumerable<IEndPointProvider> EndpointProviders { get; private set; }

        [ImportMany]
        internal IEnumerable<IExtensionProvider> ExtensionProviders { get; private set; }
    }
}
