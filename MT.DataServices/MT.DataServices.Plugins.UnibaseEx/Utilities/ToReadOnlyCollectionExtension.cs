﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities
{
    /// <summary>
    /// Provides converter methods from different enumerable types to <see cref="ReadOnlyCollection{T}"/>
    /// </summary>
    public static class ToReadOnlyCollectionExtension
    {
        /// <summary>
        /// Creates a new <see cref="ReadOnlyCollection{T}"/> from the given enumerable.
        /// </summary>
        /// <typeparam name="T">Type of the list elements.</typeparam>
        /// <param name="enumerable">The enumerable of elements to convert.</param>
        /// <returns>The newly created instance of <see cref="ReadOnlyCollection{T}"/>.</returns>
        public static ReadOnlyCollection<T> ToReadOnlyCollection<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException("enumerable");

            var readOnlyCollection = enumerable as ReadOnlyCollection<T>;
            if (readOnlyCollection != null)
                return readOnlyCollection;

            var list = enumerable as IList<T>;
            if (list != null)
                return new ReadOnlyCollection<T>(list);

            return new ReadOnlyCollection<T>(enumerable.ToList());
        }
    }
}