﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities
{
    internal class EmptyReadOnlyCollection<T>
    {
        internal static readonly ReadOnlyCollection<T> Instance = new ReadOnlyCollection<T>(new List<T>());
    }
}