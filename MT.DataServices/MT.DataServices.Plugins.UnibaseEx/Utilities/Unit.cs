﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities
{
    /// <summary>
    /// Represents an empty result value. Similar to the unit-type () in functional programming languages.
    /// </summary>
    public class Unit
    {
        /// <summary>
        /// Represents the only instance of the <see cref="Unit"/> type.
        /// </summary>
        public static readonly Unit Identity = null;

        private Unit()
        {            
        }
    }
}