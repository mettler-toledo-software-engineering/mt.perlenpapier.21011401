﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    /// <summary>
    /// Provides methods for reading strongly typed data from an <see cref="IRelationalResultSet"/>
    /// </summary>
    public class TypedTableReader
    {
        private readonly IRelationalResultSet resultSet;
        private Type rowType;

        /// <summary>
        /// Initializes a new instance of the <see cref="TypedTableReader"/> class.
        /// </summary>
        public TypedTableReader(IRelationalResultSet resultSet)
        {
            if (resultSet == null)
                throw new ArgumentNullException("resultSet");

            this.resultSet = resultSet;
        }

        /// <summary>
        /// Loads the result set into memory.
        /// </summary>
        /// <returns>The memory copy of the result set.</returns>
        public TypedTable LoadIntoMemory()
        {
            return (TypedTable)typeof(TypedTable<>).MakeGenericType(GetRowType()).GetConstructors()[0].Invoke(new object[] { this });
        }

        /// <summary>
        /// Gets the result set that this instance is reading from.
        /// </summary>
        public IRelationalResultSet ResultSet
        {
            get { return resultSet; }
        }

        internal Type GetRowType()
        {
            if (rowType == null)
            {
                Type sliceType;
                if (resultSet.Count == 0)
                    sliceType = typeof(ClosingSlice);
                else
                {
                    var columnCount = resultSet.Count;
                    var types = new Type[9];

                    int lastSliceCount = columnCount & 0x7;
                    if (lastSliceCount == 0)
                        sliceType = typeof(ClosingSlice);
                    else
                    {
                        int lastSliceMinIndex = columnCount & ~0x7;
                        for (int j = 0; j < 8; j++)
                        {
                            types[j] = (j >= lastSliceCount) ? TypedCellTypes.Empty : TypedCellTypes.GetCellType(resultSet.GetType(lastSliceMinIndex + j));
                        }
                        types[8] = typeof(ClosingSlice);

                        sliceType = typeof(TypedTableRowSlice<,,,,,,,,>).MakeGenericType(types);
                    }

                    int fullSliceCount = columnCount >> 3;
                    for (int i = 0; i < fullSliceCount; i++)
                    {
                        int startIndex = (fullSliceCount - i - 1) * 8;
                        for (int j = 0; j < 8; j++)
                        {
                            types[j] = TypedCellTypes.GetCellType(resultSet.GetType(startIndex + j));
                        }
                        types[8] = sliceType;

                        sliceType = typeof(TypedTableRowSlice<,,,,,,,,>).MakeGenericType(types);
                    }                    
                }

                rowType = typeof(TypedTableRow<>).MakeGenericType(sliceType);
            }

            return rowType;
        }        
    }
}
