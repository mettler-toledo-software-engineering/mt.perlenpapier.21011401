﻿ 
using System;
using System.Data;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal class TypedCells 
    {

        internal struct BooleanCell : ITypedCell
        {
			internal Boolean value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetBoolean(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Boolean)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct ByteCell : ITypedCell
        {
			internal Byte value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetByte(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Byte)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct CharCell : ITypedCell
        {
			internal Char value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetChar(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Char)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct Int16Cell : ITypedCell
        {
			internal Int16 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetInt16(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Int16)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct Int32Cell : ITypedCell
        {
			internal Int32 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetInt32(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Int32)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct Int64Cell : ITypedCell
        {
			internal Int64 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetInt64(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Int64)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct StringCell : ITypedCell
        {
			internal String value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetString(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (String)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct DecimalCell : ITypedCell
        {
			internal Decimal value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetDecimal(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Decimal)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct DoubleCell : ITypedCell
        {
			internal Double value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetDouble(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Double)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct SingleCell : ITypedCell
        {
			internal Single value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetFloat(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Single)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct DateTimeCell : ITypedCell
        {
			internal DateTime value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetDateTime(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (DateTime)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct GuidCell : ITypedCell
        {
			internal Guid value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = reader.GetGuid(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (Guid)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct BinaryCell : ITypedCell
        {
			internal byte[] value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				if (reader.IsDBNull(fieldIndex))
					flags |= 1 << (fieldIndex & 0x7);
				else
					value = (byte[])reader.GetValue(fieldIndex);
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (byte[])value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct SByteCell : ITypedCell
        {
			internal SByte value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				throw new NotSupportedException();
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (SByte)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct UInt16Cell : ITypedCell
        {
			internal UInt16 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				throw new NotSupportedException();
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (UInt16)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct UInt32Cell : ITypedCell
        {
			internal UInt32 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				throw new NotSupportedException();
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (UInt32)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

        internal struct UInt64Cell : ITypedCell
        {
			internal UInt64 value;

			void ITypedCell.ReadValue(ref int flags, IDataReader reader, int fieldIndex)
			{
				throw new NotSupportedException();
			}

			void ITypedCell.SetValue(ref int flags, object value, int fieldIndex)
			{
				if (Convert.IsDBNull(value))
					flags |= 1 << (fieldIndex & 0x7);
				else
					this.value = (UInt64)value;
			}

			object ITypedCell.GetValue(ref int flags, int fieldIndex)
			{
				return (flags & (1 << (fieldIndex & 0x7))) != 0 ? DBNull.Value : (object)value;
			}
        }

    }
}
