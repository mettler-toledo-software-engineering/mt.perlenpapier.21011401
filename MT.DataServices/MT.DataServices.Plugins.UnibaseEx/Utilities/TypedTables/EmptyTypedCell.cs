﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal struct EmptyTypedCell : ITypedCell
    {
        public void ReadValue(ref int flags, System.Data.IDataReader reader, int fieldIndex)
        {
            throw new NotImplementedException();
        }

        public void SetValue(ref int flags, object value, int fieldIndex)
        {
            throw new NotImplementedException();
        }

        public object GetValue()
        {
            throw new NotImplementedException();
        }

        public object GetValue(ref int flags, int fieldIndex)
        {
            throw new NotImplementedException();
        }
    }
}
