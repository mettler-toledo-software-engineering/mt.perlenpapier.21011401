﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal struct ClosingSlice : ITypedTableRowSlice
    {
        public void SetValues(IRelationalResultSet reader, int start)
        {
        }

        public void ReadValues(System.Data.IDataReader reader, int start)
        {
        }

        public object GetValue(int field)
        {
            return null;
        }

        public void GetValues(object[] values, int start)
        {
            
        }
    }
}
