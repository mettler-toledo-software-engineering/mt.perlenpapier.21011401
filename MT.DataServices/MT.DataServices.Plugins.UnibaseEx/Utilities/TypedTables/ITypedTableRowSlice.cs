using System;
using System.Data;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal interface ITypedTableRowSlice
    {
        void SetValues(IRelationalResultSet reader, int start);
        void ReadValues(IDataReader reader, int start);
        object GetValue(int field);
        void GetValues(object[] values, int start);
    }
}