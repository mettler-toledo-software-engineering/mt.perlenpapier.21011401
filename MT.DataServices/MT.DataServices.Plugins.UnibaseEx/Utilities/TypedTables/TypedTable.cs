﻿using System;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal class TypedTable<TRow> : TypedTable
        where TRow : ITypedTableRow
    {
        private TRow[] data;
        private int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="TypedTable"/> class.
        /// </summary>
        public TypedTable(TypedTableReader reader)
            : base(reader)
        {
            var dataReader = reader.ResultSet as IDataRelationalResultSet;
            if (dataReader != null)
                ReadFromDataReader(dataReader.Reader);
            else
                ReadFromResultSet(reader.ResultSet);
        }

        private void ReadFromDataReader(IDataReader dataReader)
        {
            data = new TRow[1];
            int index = 0;
            while (dataReader.Read())
            {
                if (index == data.Length)
                {
                    if (data.Length == 1)
                        Array.Resize(ref data, 8);
                    else
                        Array.Resize(ref data, data.Length * 2);
                }

                data[index++].ReadValues(dataReader, 0);
            }

            count = index;
            TrimExcess();
        }

        private void ReadFromResultSet(IRelationalResultSet resultSet)
        {
            data = new TRow[1];
            int index = 0;
            while (resultSet.Read())
            {
                if (index == data.Length)
                {
                    if (data.Length == 1)
                        Array.Resize(ref data, 8);
                    else
                        Array.Resize(ref data, data.Length * 2);
                }

                data[index++].SetValues(resultSet, 0);
            }

            count = index;
            TrimExcess();
        }
  
        private void TrimExcess()
        {
            // If we waste more than 10% of memory, trim the excess rows by copying 
            if (count * 100 / data.Length < 90)
            {
                Array.Resize(ref data, count);
            }
        }

        public override int RowCount
        {
            get { return count;  }
        }

        public override object GetValue(int row, int column)
        {
            if (row < 0 || row >= count)
                throw new ArgumentOutOfRangeException("row");

            if (column < 0 || column >= ColumnCount)
                throw new ArgumentOutOfRangeException("column");

            return data[row].GetValue(column);
        }

        public override void GetValues(int row, object[] values)
        {
            if (row < 0 || row >= count)
                throw new ArgumentOutOfRangeException("row");

            if (values == null)
                throw new ArgumentNullException("values");

            if (values.Length < columnCount)
                throw new ArgumentException("The value array is to short.", "values");

            data[row].GetValues(values, 0);
        }
    }

    /// <summary>
    /// Represents a memory copy of the data from a server.
    /// </summary>
    public abstract class TypedTable
    {
        private readonly TypedTableReader reader;

        /// <summary>
        /// The number of columns in the table.
        /// </summary>
        protected int columnCount;

        internal TypedTable(TypedTableReader reader)
        {
            this.reader = reader;
            columnCount = reader.ResultSet.Count;
        }

        /// <summary>
        /// Gets the number of rows in the table.
        /// </summary>
        public abstract int RowCount { get; }

        /// <summary>
        /// Gets the number of columns in the table.
        /// </summary>
        public int ColumnCount
        {
            get { return columnCount; }
        }

        /// <summary>
        /// Gets the value in the table at the specified position.
        /// </summary>
        /// <param name="row">The row index.</param>
        /// <param name="column">The column index.</param>
        /// <returns>The value at the given position.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// <paramref name="row"/> is negative or equal or larger than <see cref="RowCount"/>
        /// or
        /// <paramref name="column"/> is negative or equal or larger than <see cref="ColumnCount"/>
        /// </exception>
        public abstract object GetValue(int row, int column);

        /// <summary>
        /// Gets all values in the table at the specified row.
        /// </summary>
        /// <param name="row">The row index.</param>
        /// <param name="values">The array to fill with the column values. Must be at least <see cref="ColumnCount"/> elements long.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"><paramref name="row" /> is negative or equal or larger than <see cref="RowCount" /></exception>
        public abstract void GetValues(int row, object[] values);
    }
}
