﻿using System;
using System.Data;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal struct TypedTableRowSlice<T1, T2, T3, T4, T5, T6, T7, T8, TNext> : ITypedTableRowSlice
        where T1 : ITypedCell
        where T2 : ITypedCell
        where T3 : ITypedCell
        where T4 : ITypedCell
        where T5 : ITypedCell
        where T6 : ITypedCell
        where T7 : ITypedCell
        where T8 : ITypedCell
        where TNext : ITypedTableRowSlice
    {
        int flags;

#pragma warning disable 649 
        // The compiler warns here that the cells will always have their default value, which is not true
        // in these circumstances.

        internal T1 cell0;
        internal T2 cell1;
        internal T3 cell2;
        internal T4 cell3;
        internal T5 cell4;
        internal T6 cell5;
        internal T7 cell6;
        internal T8 cell7;
        internal TNext next;

#pragma warning restore 649

        public void ReadValues(IDataReader reader, int start)
        {
            switch (8 - Math.Min(8, reader.FieldCount - start))
            {
                case 0:
                    cell7.ReadValue(ref flags, reader, start + 7);
                    goto case 1;
                case 1:
                    cell6.ReadValue(ref flags, reader, start + 6);
                    goto case 2;
                case 2:
                    cell5.ReadValue(ref flags, reader, start + 5);
                    goto case 3;
                case 3:
                    cell4.ReadValue(ref flags, reader, start + 4);
                    goto case 4;
                case 4:
                    cell3.ReadValue(ref flags, reader, start + 3);
                    goto case 5;
                case 5:
                    cell2.ReadValue(ref flags, reader, start + 2);
                    goto case 6;
                case 6:
                    cell1.ReadValue(ref flags, reader, start + 1);
                    goto case 7;
                case 7:
                    cell0.ReadValue(ref flags, reader, start + 0);
                    break;
            }

            next.ReadValues(reader, start + 8);
        }

        public void SetValues(IRelationalResultSet reader, int start)
        {
            switch (8 - Math.Min(8, reader.Count - start))
            {
                case 0:
                    cell7.SetValue(ref flags, reader.Get(start + 7), start + 7);
                    goto case 1;
                case 1:
                    cell6.SetValue(ref flags, reader.Get(start + 6), start + 6);
                    goto case 2;
                case 2:
                    cell5.SetValue(ref flags, reader.Get(start + 5), start + 5);
                    goto case 3;
                case 3:
                    cell4.SetValue(ref flags, reader.Get(start + 4), start + 4);
                    goto case 4;
                case 4:
                    cell3.SetValue(ref flags, reader.Get(start + 3), start + 3);
                    goto case 5;
                case 5:
                    cell2.SetValue(ref flags, reader.Get(start + 2), start + 2);
                    goto case 6;
                case 6:
                    cell1.SetValue(ref flags, reader.Get(start + 1), start + 1);
                    goto case 7;
                case 7:
                    cell0.SetValue(ref flags, reader.Get(start + 0), start + 0);
                    break;
            }

            next.SetValues(reader, start + 8);
        }

        public object GetValue(int field)
        {
            switch (field)
            {
                case 0: return cell0.GetValue(ref flags, field);
                case 1: return cell1.GetValue(ref flags, field);
                case 2: return cell2.GetValue(ref flags, field);
                case 3: return cell3.GetValue(ref flags, field);
                case 4: return cell4.GetValue(ref flags, field);
                case 5: return cell5.GetValue(ref flags, field);
                case 6: return cell6.GetValue(ref flags, field);
                case 7: return cell7.GetValue(ref flags, field);
                default: return next.GetValue(field - 8);
            }
        }


        public void GetValues(object[] values, int start)
        {
            switch (8 - Math.Min(8, values.Length - start))
            {
                case 0:
                    values[start + 7] = cell7.GetValue(ref flags, start + 7);
                    goto case 1;
                case 1:
                    values[start + 6] = cell6.GetValue(ref flags, start + 6);
                    goto case 2;
                case 2:
                    values[start + 5] = cell5.GetValue(ref flags, start + 5);
                    goto case 3;
                case 3:
                    values[start + 4] = cell4.GetValue(ref flags, start + 4);
                    goto case 4;
                case 4:
                    values[start + 3] = cell3.GetValue(ref flags, start + 3);
                    goto case 5;
                case 5:
                    values[start + 2] = cell2.GetValue(ref flags, start + 2);
                    goto case 6;
                case 6:
                    values[start + 1] = cell1.GetValue(ref flags, start + 1);
                    goto case 7;
                case 7:
                    values[start + 0] = cell0.GetValue(ref flags, start + 0);
                    break;
            }

            next.GetValues(values, start + 8);
        }
    }
}
