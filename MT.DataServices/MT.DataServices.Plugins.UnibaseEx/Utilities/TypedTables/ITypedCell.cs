﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal interface ITypedCell
    {
        void ReadValue(ref int flags, IDataReader reader, int fieldIndex);

        void SetValue(ref int flags, object value, int fieldIndex);

        object GetValue(ref int flags, int fieldIndex);
    }
}
