﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal class TypedCellTypes
    {
        private static readonly Type BinaryType = typeof(byte[]);
        private static readonly Type GuidType = typeof(Guid);

        internal static readonly Type Empty = typeof(EmptyTypedCell);
        internal static readonly Type Boolean = typeof(TypedCells.BooleanCell);
        internal static readonly Type SByte = typeof(TypedCells.SByteCell);
        internal static readonly Type Byte = typeof(TypedCells.ByteCell);
        internal static readonly Type Char = typeof(TypedCells.CharCell);
        internal static readonly Type Int16 = typeof(TypedCells.Int16Cell);
        internal static readonly Type UInt16 = typeof(TypedCells.UInt16Cell);
        internal static readonly Type Int32 = typeof(TypedCells.Int32Cell);
        internal static readonly Type UInt32 = typeof(TypedCells.UInt32Cell);
        internal static readonly Type Int64 = typeof(TypedCells.Int64Cell);
        internal static readonly Type UInt64 = typeof(TypedCells.UInt64Cell);
        internal static readonly Type Single = typeof(TypedCells.SingleCell);
        internal static readonly Type Double = typeof(TypedCells.DoubleCell);
        internal static readonly Type Decimal = typeof(TypedCells.DecimalCell);
        internal static readonly Type String = typeof(TypedCells.StringCell);
        internal static readonly Type Binary = typeof(TypedCells.BinaryCell);
        internal static readonly Type DateTime = typeof(TypedCells.DateTimeCell);
        internal static readonly Type Guid = typeof(TypedCells.GuidCell);
        
        internal static Type GetCellType(Type dataType)
        {
            switch (Type.GetTypeCode(dataType))
            {
                case TypeCode.Boolean:
                    return Boolean;
                case TypeCode.SByte:
                    return SByte;
                case TypeCode.Byte:
                    return Byte;
                case TypeCode.Char:
                    return Char;
                case TypeCode.DateTime:
                    return DateTime;
                case TypeCode.Decimal:
                    return Decimal;
                case TypeCode.Double:
                    return Double;
                case TypeCode.Int16:
                    return Int16;
                case TypeCode.UInt16:
                    return UInt16;
                case TypeCode.Int32:
                    return Int32;
                case TypeCode.UInt32:
                    return UInt32;
                case TypeCode.Int64:
                    return Int64;
                case TypeCode.UInt64:
                    return UInt64;
                case TypeCode.Single:
                    return Single;
                case TypeCode.String:
                    return String;
                default:
                    if (BinaryType.Equals(dataType))
                        return Binary;
                    else if (GuidType.Equals(dataType))
                        return Guid;
                    else
                        throw new NotSupportedException(string.Format("The given column datatype {0} is not supported.", dataType.FullName));
            }
        }
    }
}
