﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables
{
    internal struct TypedTableRow<TSlice> : ITypedTableRow
        where TSlice : ITypedTableRowSlice
    {
#pragma warning disable 649
        private TSlice slice;
#pragma warning restore 649

        public void SetValues(IRelationalResultSet reader, int start)
        {
            slice.SetValues(reader, start);    
        }

        public void ReadValues(System.Data.IDataReader reader, int start)
        {
            slice.ReadValues(reader, start);
        }

        public object GetValue(int field)
        {
            return slice.GetValue(field);
        }

        public void GetValues(object[] values, int start)
        {
            slice.GetValues(values, start);
        }
    }
}
