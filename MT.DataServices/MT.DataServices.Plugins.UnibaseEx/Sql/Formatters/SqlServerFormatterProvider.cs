﻿using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Formatters
{
    /// <summary>
    /// Exports the "Microsoft SQL Server" SQL formatter as a MEF component.
    /// </summary>
    [Export(typeof(ISqlFormatterProvider))]
    public class SqlServerFormatterProvider : ISqlFormatterProvider
    {
        /// <summary>
        /// Gets the name of the SQL dialect that this formatter supports.
        /// </summary>
        public string Name
        {
            get { return "Microsoft SQL Server"; }
        }

        /// <summary>
        /// Returns a formatter that allows to convert a <see cref="MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree.StatementSequence" /> object into a specific SQL dialect.
        /// </summary>
        /// <returns>An instance that implements the <see cref="ISqlFormatter"/> interface.</returns>
        public ISqlFormatter CreateFormatter()
        {
            return SqlServerFormatter.Instance;
        }
    }
}
