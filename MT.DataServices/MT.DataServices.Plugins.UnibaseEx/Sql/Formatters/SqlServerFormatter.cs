﻿using System;
using System.Linq;
using System.Text;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;
using MT.DataServices.Plugins.UnibaseEx.Sql;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Formatters
{
    internal class SqlServerFormatter : QueryVisitorBase<Unit, StringBuilder>, ISqlFormatter
    {
        public static readonly SqlServerFormatter Instance = new SqlServerFormatter();

        /// <summary>
        /// Prevents a default instance of the <see cref="SqlServerFormatter"/> class from being created.
        /// </summary>
        private SqlServerFormatter()
        {

        }

        public string FormatStatement(StatementSequence sequence)
        {
            var sb = new StringBuilder();
            Visit(sequence, sb);
            return sb.ToString();
        }

        public override Unit VisitBinaryExpression(BinaryExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            Visit(expr.Left, arg);
            arg.Append(' ');
            arg.Append(GetBinaryOperator(expr.Operator));
            arg.Append(' ');
            Visit(expr.Right, arg);
            arg.Append(')');
            return null;
        }

        public override Unit VisitBooleanLiteral(BooleanLiteral expr, StringBuilder arg)
        {
            if (expr.Value)
                arg.Append("1");
            else
                arg.Append("0");

            return null;
        }

        public override Unit VisitDeleteStatement(DeleteStatement stmt, StringBuilder arg)
        {
            arg.Append("DELETE FROM ");
            FormatDataQueryStatement(stmt, arg);
            return null;
        }

        public override Unit VisitFieldExpression(FieldExpression expr, StringBuilder arg)
        {
            Visit(expr.Expression, arg);
            if (expr.Name != null)
            {
                arg.Append(" AS ");
                arg.Append(expr.Name);
            }
            return null;
        }

        public override Unit VisitIdentifier(Identifier expr, StringBuilder arg)
        {
            arg.Append(expr.ToString());
            return null;
        }

        public override Unit VisitInsertStatement(InsertStatement stmt, StringBuilder arg)
        {
            arg.Append("INSERT INTO ");
            Visit(stmt.Relation, arg);

            if (stmt.Fields != null)
            {
                arg.Append(" ");
                Visit(stmt.Fields, arg);
            }

            Visit(stmt.Values, arg);
            return null;
        }

        public override Unit VisitJoinModifier(JoinModifier expr, StringBuilder arg)
        {
            switch (expr.Mode)
            {
                case JoinMode.Inner:
                    arg.Append("INNER JOIN ");
                    break;
                case JoinMode.Left:
                    arg.Append("LEFT JOIN ");
                    break;
                case JoinMode.Right:
                    arg.Append("RIGHT JOIN ");
                    break;
                case JoinMode.Full:
                    arg.Append("FULL JOIN ");
                    break;
                default:
                    throw new ArgumentOutOfRangeException("expr");
            }

            Visit(expr.Relation, arg);
            arg.Append(" ON ");
            Visit(expr.Condition, arg);

            return null;
        }

        public override Unit VisitNumericLiteral<T>(NumericLiteral<T> expr, StringBuilder arg)
        {
            arg.Append(System.Convert.ToString((object)expr.Value));
            return null;
        }

        public override Unit VisitOrderModifier(OrderModifier expr, StringBuilder arg)
        {
            Visit(expr.Expression, arg);

            if (expr.Direction == SortDirection.Ascending)
                arg.Append(" ASC");
            else
                arg.Append(" DESC");

            return null;
        }

        public override Unit VisitSelectStatement(SelectStatement stmt, StringBuilder arg)
        {
            arg.Append("SELECT ");

            if (stmt.Take != -1)
            {
                arg.Append(" TOP ");
                arg.Append(stmt.Take);
            }

            for (int i = 0; i < stmt.Fields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(stmt.Fields[i], arg);
            }

            arg.Append(" FROM ");
            FormatDataQueryStatement(stmt, arg);
            return null;
        }

        public override Unit VisitSetModifier(SetModifier expr, StringBuilder arg)
        {
            Visit(expr.Target, arg);
            arg.Append(" = ");
            Visit(expr.Value, arg);
            return null;
        }

        public override Unit VisitStatementSequence(StatementSequence stmt, StringBuilder arg)
        {
            for (int i = 0; i < stmt.Statements.Count; i++)
            {
                if (i > 0)
                    arg.Append("; ");

                Visit(stmt.Statements[i], arg);
            }

            return null;
        }

        public override Unit VisitStringLiteral(StringLiteral expr, StringBuilder arg)
        {
            arg.Append('\'').Append(expr.Value.Replace("'", "''")).Append('\'');
            return null;
        }

        public override Unit VisitTupleExpression(TupleExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            for (int i = 0; i < expr.Elements.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Elements[i], arg);
            }
            arg.Append(')');
            return null;
        }

        public override Unit VisitValuesListExpression(ValuesListExpression expr, StringBuilder arg)
        {
            arg.Append(" VALUES ");
            for (int i = 0; i < expr.Tuples.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Tuples[i], arg);
            }
            return null;
        }

        public override Unit VisitUnaryExpression(UnaryExpression expr, StringBuilder arg)
        {
            switch (expr.Operator)
            {
                case UnaryOperator.Not:
                    arg.Append("NOT ");
                    Visit(expr.Expression, arg);
                    return null;
                case UnaryOperator.Negate:
                    arg.Append("-");
                    Visit(expr.Expression, arg);
                    return null;
                case UnaryOperator.IsNull:
                    Visit(expr.Expression, arg);
                    arg.Append(" IS NULL");
                    return null;
                default:
                    throw new ArgumentOutOfRangeException("expr");
            }
        }

        public override Unit VisitUpdateStatement(UpdateStatement stmt, StringBuilder arg)
        {
            arg.Append("UPDATE ");

            Visit(stmt.Relation, arg);

            foreach (var join in stmt.Joins)
            {
                Visit(join, arg);
            }

            arg.Append(" SET ");

            for (int i = 0; i < stmt.Setters.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");
                Visit(stmt.Setters[i], arg);
            }

            if (stmt.Condition != null)
            {
                arg.Append(" WHERE ");
                Visit(stmt.Condition, arg);
            }

            return null;
        }

        public override Unit VisitRelationReferenceExpression(RelationReferenceExpression expr, StringBuilder arg)
        {
            Visit(expr.Relation, arg);
            if (expr.Alias != null)
            {
                arg.Append(' ');
                Visit(expr.Alias, arg);
            }
            return null;
        }

        public override Unit VisitSelectEvalStatement(SelectEvalStatement stmt, StringBuilder arg)
        {
            arg.Append("SELECT ");          

            for (int i = 0; i < stmt.Fields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(stmt.Fields[i], arg);
            }
            return null;
        }

        public override Unit VisitSubqueryExpression(SubqueryExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            Visit(expr.Subquery);
            arg.Append(')');
            return null;
        }

        private void FormatDataQueryStatement(DataQueryStatement stmt, StringBuilder arg)
        {
            Visit(stmt.Relation, arg);

            foreach (var join in stmt.Joins)
            {
                Visit(join, arg);
            }

            if (stmt.Condition != null)
            {
                arg.Append(" WHERE ");
                Visit(stmt.Condition, arg);
            }

            if (stmt.Orderings.Count > 0)
            {
                arg.Append(" ORDER BY ");
                for (int i = 0; i < stmt.Orderings.Count; i++)
                {
                    if (i > 0)
                        arg.Append(", ");

                    Visit(stmt.Orderings[i], arg);
                }
            }

            if (stmt.Grouping != null)
                Visit(stmt.Grouping, arg);           
        }

        public override Unit VisitParameterExpression(ParameterExpression expr, StringBuilder arg)
        {
            arg.Append('@');
            arg.Append(expr.Name);
            return null;
        }

        public override Unit VisitStarExpression(StarExpression expr, StringBuilder arg)
        {
            arg.Append('*');
            return Unit.Identity;
        }

        public override Unit VisitGroupingModifier(GroupingModifier expr, StringBuilder arg)
        {
            arg.Append(" GROUP BY ");
            for (int i = 0; i < expr.GroupByFields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.GroupByFields[i], arg);
            }

            if (expr.HavingCondition != null)
            {
                arg.Append(" HAVING ");
                Visit(expr.HavingCondition, arg);
            }

            return Unit.Identity;
        }

        public override Unit VisitFunctionCallExpression(FunctionCallExpression expr, StringBuilder arg)
        {
            arg.Append(expr.Name).Append('(');

            for (int i = 0; i < expr.Arguments.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Arguments[i], arg);
            }

            arg.Append(')');
            return Unit.Identity;
        }

        public override Unit VisitRawStatement(ExecRawStatement stmt, StringBuilder arg)
        {
            arg.Append(stmt.Statement);
            return null;
        }

        private string GetBinaryOperator(BinaryOperator op)
        {
            switch (op)
            {
                case BinaryOperator.Add:
                    return "+";
                case BinaryOperator.Subtract:
                    return "-";
                case BinaryOperator.Mulitply:
                    return "*";
                case BinaryOperator.Divide:
                    return "/";
                case BinaryOperator.Equal:
                    return "=";
                case BinaryOperator.Unequal:
                    return "!=";
                case BinaryOperator.GreaterThan:
                    return ">";
                case BinaryOperator.LessThan:
                    return "<";
                case BinaryOperator.GreaterOrEqual:
                    return ">=";
                case BinaryOperator.LessOrEqual:
                    return "<=";
                case BinaryOperator.And:
                    return "AND";
                case BinaryOperator.Or:
                    return "OR";
                default:
                    throw new ArgumentOutOfRangeException("op");
            }
        }
    }
}
