﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    /// <summary>
    /// Provides static methods to convert queries to SQL text and vice-versa.
    /// </summary>
    public static class QueryFormatter
    {
        /// <summary>
        /// Saves the specified query as a string.
        /// </summary>
        /// <param name="query">The query to save.</param>
        /// <returns>The query represented as a string.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="query"/> is null.</exception>
        public static string Save(Node query)
        {
            if (query == null)
                throw new ArgumentNullException("query");

            return QueryWriter.QueryToString(query);
        }

        /// <summary>
        /// Parses the specified query text.
        /// </summary>
        /// <param name="queryText">The query text to parse.</param>
        /// <returns>The query represented as a <see cref="StatementSequence"/>.</returns>
        public static StatementSequence Parse(string queryText)
        {
            if (queryText == null)
                throw new ArgumentNullException("queryText");

            return QueryParser.Parse(queryText);
        }
    }
}