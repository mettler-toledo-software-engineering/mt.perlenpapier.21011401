﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    /// <summary>
    /// Contains helpers methods for escaping and unescaping strings in sql commands.
    /// </summary>
    internal static class SqlStringEscaping
    {
        internal static string Escape(string str)
        {
            return str;
        }

        internal static string Unescape(string str)
        {
            return str;
        }
    }
}