﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    enum SymbolIndex : int
    {
        @EOF = 0, // (EOF)
        @ERROR = 1, // (Error)
        @WHITESPACE = 2, // Whitespace
        @MINUS = 3, // '-'
        @EXCLAMEQ = 4, // '!='
        @LPAREN = 5, // '('
        @RPAREN = 6, // ')'
        @TIMES = 7, // '*'
        @COMMA = 8, // ','
        @DOT = 9, // '.'
        @DIV = 10, // '/'
        @SEMI = 11, // ';'
        @PLUS = 12, // '+'
        @LT = 13, // '<'
        @LTEQ = 14, // '<='
        @EQ = 15, // '='
        @GT = 16, // '>'
        @GTEQ = 17, // '>='
        @AND = 18, // AND
        @AS = 19, // AS
        @ASC = 20, // ASC
        @AUTOINCREMENT = 21, // AUTOINCREMENT
        @BINARY = 22, // BINARY
        @BOOL = 23, // BOOL
        @BY = 24, // BY
        @BYTE = 25, // BYTE
        @CHAR = 26, // CHAR
        @CREATE = 27, // CREATE
        @DATABASE = 28, // DATABASE
        @DATETIME = 29, // DATETIME
        @DECIMAL = 30, // DECIMAL
        @DELETE = 31, // DELETE
        @DESC = 32, // DESC
        @DOUBLE = 33, // DOUBLE
        @DROP = 34, // DROP
        @ESCAPEDIDENTIFIER = 35, // EscapedIdentifier
        @EXEC = 36, // EXEC
        @FALSE = 37, // FALSE
        @FLOAT = 38, // Float
        @FOREIGN = 39, // FOREIGN
        @FROM = 40, // FROM
        @FULL = 41, // FULL
        @GROUP = 42, // GROUP
        @HAVING = 43, // HAVING
        @INNER = 44, // INNER
        @INSERT = 45, // INSERT
        @INT = 46, // INT
        @INTEGER = 47, // Integer
        @INTO = 48, // INTO
        @IS = 49, // IS
        @JOIN = 50, // JOIN
        @KEY = 51, // KEY
        @LEFT = 52, // LEFT
        @LIMIT = 53, // LIMIT
        @LONG = 54, // LONG
        @MAX = 55, // MAX
        @NOT = 56, // NOT
        @NULL = 57, // NULL
        @ON = 58, // ON
        @OR = 59, // OR
        @ORDER = 60, // ORDER
        @OUTER = 61, // OUTER
        @PARAMETER = 62, // Parameter
        @PRIMARY = 63, // PRIMARY
        @RAW = 64, // RAW
        @RAWIDENTIFIER = 65, // RawIdentifier
        @REFERENCES = 66, // REFERENCES
        @RIGHT = 67, // RIGHT
        @SELECT = 68, // SELECT
        @SET = 69, // SET
        @SHORT = 70, // SHORT
        @STRINGLITERAL = 71, // StringLiteral
        @TABLE = 72, // TABLE
        @TRUE = 73, // TRUE
        @UPDATE = 74, // UPDATE
        @VALUES = 75, // VALUES
        @VARCHAR = 76, // VARCHAR
        @WHERE = 77, // WHERE
        @ADDEXP = 78, // <Add Exp>
        @ARGUMENT = 79, // <Argument>
        @ARGUMENTLIST = 80, // <Argument List>
        @BOOLEAN = 81, // <Boolean>
        @CMPEXPR = 82, // <Cmp Expr>
        @CREATECOLUMN = 83, // <Create Column>
        @CREATEDATABASE = 84, // <Create Database>
        @CREATEFOREIGNKEY = 85, // <Create Foreign Key>
        @CREATEPRIMARYKEY = 86, // <Create Primary Key>
        @CREATETABLE = 87, // <Create Table>
        @DATATYPE = 88, // <Data Type>
        @DATATYPESIZE = 89, // <Data Type Size>
        @DELETEQUERY = 90, // <Delete Query>
        @DROPDATABASE = 91, // <Drop Database>
        @DROPTABLE = 92, // <Drop Table>
        @EXPRESSION = 93, // <Expression>
        @EXPRESSIONORQUERY = 94, // <Expression Or Query>
        @EXPRESSIONORSTAR = 95, // <Expression Or Star>
        @FIELD = 96, // <Field>
        @FIELDS = 97, // <Fields>
        @FUNCTION = 98, // <Function>
        @GROUPING = 99, // <Grouping>
        @GROUPINGITEM = 100, // <Grouping Item>
        @GROUPINGLIST = 101, // <Grouping List>
        @HAVING2 = 102, // <Having>
        @IDENTIFIER = 103, // <Identifier>
        @IDENTIFIERLIST = 104, // <Identifier List>
        @INSERTQUERY = 105, // <Insert Query>
        @INSERTSOURCE = 106, // <Insert Source>
        @ISNULL = 107, // <IsNull>
        @JOIN2 = 108, // <Join>
        @JOINLIST = 109, // <Join List>
        @JOINMODE = 110, // <Join Mode>
        @LIMIT2 = 111, // <Limit>
        @MULTEXP = 112, // <Mult Exp>
        @NEGATEEXP = 113, // <Negate Exp>
        @NEWTABLEELEMENT = 114, // <New Table Element>
        @NEWTABLEELEMENTLIST = 115, // <New Table Element List>
        @NUMBER = 116, // <Number>
        @OPTARGUMENTLIST = 117, // <Opt Argument List>
        @OPTAS = 118, // <Opt As>
        @OPTAUTOINCREMENT = 119, // <Opt AutoIncrement>
        @OPTGROUPING = 120, // <Opt Grouping>
        @OPTHAVING = 121, // <Opt Having>
        @OPTIDENTIFIER = 122, // <Opt Identifier>
        @OPTJOINLIST = 123, // <Opt Join List>
        @OPTLIMIT = 124, // <Opt Limit>
        @OPTORDERINGS = 125, // <Opt Orderings>
        @OPTPRIMARYKEY = 126, // <Opt Primary Key>
        @OPTSELECTSOURCE = 127, // <Opt Select Source>
        @OPTTAKE = 128, // <Opt Take>
        @OPTTUPLE = 129, // <Opt Tuple>
        @OPTWHERE = 130, // <Opt Where>
        @ORDERDIRECTION = 131, // <Order Direction>
        @ORDERING = 132, // <Ordering>
        @ORDERINGLIST = 133, // <Ordering List>
        @ORDERINGS = 134, // <Orderings>
        @PROGRAM = 135, // <Program>
        @QIDENT = 136, // <QIdent>
        @QUERY = 137, // <Query>
        @RAWSQL = 138, // <Raw Sql>
        @SELECTQUERY = 139, // <Select Query>
        @SETLIST = 140, // <Set List>
        @SETTER = 141, // <Setter>
        @STAREXPRESSION = 142, // <Star Expression>
        @STATEMENT = 143, // <Statement>
        @STATEMENTLIST = 144, // <Statement List>
        @SUBQUERY = 145, // <Subquery>
        @TABLENAME = 146, // <Table Name>
        @TABLEREFERENCE = 147, // <Table Reference>
        @TUPLE = 148, // <Tuple>
        @TUPLEELEMENTS = 149, // <Tuple Elements>
        @TUPLELIST = 150, // <Tuple List>
        @UPDATEQUERY = 151, // <Update Query>
        @VALUE = 152, // <Value>
        @WHERE2 = 153  // <Where>
    };

    enum ProdIndex : int
    {
        @PROGRAM = 0, // <Program> ::= <Statement List>
        @STATEMENTLIST_SEMI = 1, // <Statement List> ::= <Statement List> ';' <Statement>
        @STATEMENTLIST = 2, // <Statement List> ::= <Statement>
        @STATEMENT = 3, // <Statement> ::= 
        @STATEMENT2 = 4, // <Statement> ::= <Query>
        @STATEMENT3 = 5, // <Statement> ::= <Create Database>
        @STATEMENT4 = 6, // <Statement> ::= <Create Table>
        @STATEMENT5 = 7, // <Statement> ::= <Drop Database>
        @STATEMENT6 = 8, // <Statement> ::= <Drop Table>
        @STATEMENT7 = 9, // <Statement> ::= <Raw Sql>
        @QUERY = 10, // <Query> ::= <Select Query>
        @QUERY2 = 11, // <Query> ::= <Insert Query>
        @QUERY3 = 12, // <Query> ::= <Update Query>
        @QUERY4 = 13, // <Query> ::= <Delete Query>
        @SELECTQUERY_SELECT = 14, // <Select Query> ::= SELECT <Fields> <Opt Select Source>
        @OPTSELECTSOURCE = 15, // <Opt Select Source> ::= 
        @OPTSELECTSOURCE_FROM = 16, // <Opt Select Source> ::= FROM <Table Name> <Opt Join List> <Opt Where> <Opt Orderings> <Opt Grouping> <Opt Limit>
        @INSERTQUERY_INSERT_INTO = 17, // <Insert Query> ::= INSERT INTO <Table Name> <Opt Tuple> <Insert Source>
        @UPDATEQUERY_UPDATE_SET = 18, // <Update Query> ::= UPDATE <Table Name> <Opt Join List> SET <Set List> <Opt Where> <Opt Limit>
        @DELETEQUERY_DELETE_FROM = 19, // <Delete Query> ::= DELETE FROM <Table Name> <Opt Join List> <Opt Where> <Opt Orderings> <Opt Limit>
        @CREATEDATABASE_CREATE_DATABASE = 20, // <Create Database> ::= CREATE DATABASE <QIdent>
        @CREATETABLE_CREATE_TABLE_LPAREN_RPAREN = 21, // <Create Table> ::= CREATE TABLE <QIdent> '(' <New Table Element List> ')'
        @DROPDATABASE_DROP_DATABASE = 22, // <Drop Database> ::= DROP DATABASE <QIdent>
        @DROPTABLE_DROP_TABLE = 23, // <Drop Table> ::= DROP TABLE <QIdent>
        @RAWSQL_EXEC_RAW_STRINGLITERAL = 24, // <Raw Sql> ::= EXEC RAW StringLiteral
        @NEWTABLEELEMENTLIST_COMMA = 25, // <New Table Element List> ::= <New Table Element List> ',' <New Table Element>
        @NEWTABLEELEMENTLIST = 26, // <New Table Element List> ::= <New Table Element>
        @NEWTABLEELEMENT = 27, // <New Table Element> ::= <Create Column>
        @NEWTABLEELEMENT2 = 28, // <New Table Element> ::= <Create Primary Key>
        @NEWTABLEELEMENT3 = 29, // <New Table Element> ::= <Create Foreign Key>
        @CREATECOLUMN = 30, // <Create Column> ::= <Identifier> <Data Type> <Opt AutoIncrement>
        @DATATYPE_CHAR = 31, // <Data Type> ::= CHAR <Data Type Size>
        @DATATYPE_VARCHAR = 32, // <Data Type> ::= VARCHAR <Data Type Size>
        @DATATYPE_BOOL = 33, // <Data Type> ::= BOOL
        @DATATYPE_BYTE = 34, // <Data Type> ::= BYTE
        @DATATYPE_SHORT = 35, // <Data Type> ::= SHORT
        @DATATYPE_INT = 36, // <Data Type> ::= INT
        @DATATYPE_LONG = 37, // <Data Type> ::= LONG
        @DATATYPE_FLOAT = 38, // <Data Type> ::= Float
        @DATATYPE_DOUBLE = 39, // <Data Type> ::= DOUBLE
        @DATATYPE_DECIMAL = 40, // <Data Type> ::= DECIMAL
        @DATATYPE_BINARY = 41, // <Data Type> ::= BINARY
        @DATATYPE_DATETIME = 42, // <Data Type> ::= DATETIME
        @DATATYPESIZE_LPAREN_INTEGER_RPAREN = 43, // <Data Type Size> ::= '(' Integer ')'
        @DATATYPESIZE_LPAREN_MAX_RPAREN = 44, // <Data Type Size> ::= '(' MAX ')'
        @OPTAUTOINCREMENT = 45, // <Opt AutoIncrement> ::= 
        @OPTAUTOINCREMENT_AUTOINCREMENT = 46, // <Opt AutoIncrement> ::= AUTOINCREMENT
        @CREATEPRIMARYKEY_PRIMARY_KEY_LPAREN_RPAREN = 47, // <Create Primary Key> ::= PRIMARY KEY '(' <Identifier List> ')'
        @CREATEFOREIGNKEY_FOREIGN_KEY_LPAREN_RPAREN_REFERENCES = 48, // <Create Foreign Key> ::= FOREIGN KEY '(' <Identifier List> ')' REFERENCES <QIdent> <Opt Primary Key>
        @OPTPRIMARYKEY = 49, // <Opt Primary Key> ::= 
        @OPTPRIMARYKEY_LPAREN_RPAREN = 50, // <Opt Primary Key> ::= '(' <Identifier List> ')'
        @IDENTIFIERLIST_COMMA = 51, // <Identifier List> ::= <Identifier List> ',' <Identifier>
        @IDENTIFIERLIST = 52, // <Identifier List> ::= <Identifier>
        @FIELDS_COMMA = 53, // <Fields> ::= <Fields> ',' <Field>
        @FIELDS = 54, // <Fields> ::= <Field>
        @FIELD = 55, // <Field> ::= <Expression Or Star> <Opt As>
        @EXPRESSIONORSTAR = 56, // <Expression Or Star> ::= <Expression>
        @EXPRESSIONORSTAR2 = 57, // <Expression Or Star> ::= <Star Expression>
        @OPTAS = 58, // <Opt As> ::= 
        @OPTAS_AS = 59, // <Opt As> ::= AS <QIdent>
        @OPTJOINLIST = 60, // <Opt Join List> ::= 
        @OPTJOINLIST2 = 61, // <Opt Join List> ::= <Join List>
        @JOINLIST = 62, // <Join List> ::= <Join List> <Join>
        @JOINLIST2 = 63, // <Join List> ::= <Join>
        @JOINMODE_JOIN = 64, // <Join Mode> ::= JOIN
        @JOINMODE_INNER_JOIN = 65, // <Join Mode> ::= INNER JOIN
        @JOINMODE_RIGHT_JOIN = 66, // <Join Mode> ::= RIGHT JOIN
        @JOINMODE_LEFT_JOIN = 67, // <Join Mode> ::= LEFT JOIN
        @JOINMODE_OUTER_JOIN = 68, // <Join Mode> ::= OUTER JOIN
        @JOINMODE_FULL_OUTER_JOIN = 69, // <Join Mode> ::= FULL OUTER JOIN
        @JOIN_ON = 70, // <Join> ::= <Join Mode> <Table Name> ON <Expression>
        @OPTWHERE = 71, // <Opt Where> ::= 
        @OPTWHERE2 = 72, // <Opt Where> ::= <Where>
        @WHERE_WHERE = 73, // <Where> ::= WHERE <Expression>
        @OPTORDERINGS = 74, // <Opt Orderings> ::= 
        @OPTORDERINGS2 = 75, // <Opt Orderings> ::= <Orderings>
        @ORDERINGS_ORDER_BY = 76, // <Orderings> ::= ORDER BY <Ordering List>
        @ORDERINGLIST_COMMA = 77, // <Ordering List> ::= <Ordering List> ',' <Ordering>
        @ORDERINGLIST = 78, // <Ordering List> ::= <Ordering>
        @ORDERING = 79, // <Ordering> ::= <Expression> <Order Direction>
        @ORDERDIRECTION = 80, // <Order Direction> ::= 
        @ORDERDIRECTION_ASC = 81, // <Order Direction> ::= ASC
        @ORDERDIRECTION_DESC = 82, // <Order Direction> ::= DESC
        @OPTGROUPING = 83, // <Opt Grouping> ::= 
        @OPTGROUPING2 = 84, // <Opt Grouping> ::= <Grouping>
        @GROUPING_GROUP_BY = 85, // <Grouping> ::= GROUP BY <Grouping List> <Opt Having>
        @GROUPINGLIST_COMMA = 86, // <Grouping List> ::= <Grouping List> ',' <Grouping Item>
        @GROUPINGLIST = 87, // <Grouping List> ::= <Grouping Item>
        @GROUPINGITEM = 88, // <Grouping Item> ::= <QIdent>
        @OPTHAVING = 89, // <Opt Having> ::= 
        @OPTHAVING2 = 90, // <Opt Having> ::= <Having>
        @HAVING_HAVING = 91, // <Having> ::= HAVING <Expression>
        @OPTLIMIT = 92, // <Opt Limit> ::= 
        @OPTLIMIT2 = 93, // <Opt Limit> ::= <Limit>
        @LIMIT_LIMIT_INTEGER = 94, // <Limit> ::= LIMIT Integer <Opt Take>
        @OPTTAKE = 95, // <Opt Take> ::= 
        @OPTTAKE_COMMA_INTEGER = 96, // <Opt Take> ::= ',' Integer
        @OPTTUPLE = 97, // <Opt Tuple> ::= 
        @OPTTUPLE2 = 98, // <Opt Tuple> ::= <Tuple>
        @TUPLE_LPAREN_RPAREN = 99, // <Tuple> ::= '(' <Tuple Elements> ')'
        @TUPLEELEMENTS_COMMA = 100, // <Tuple Elements> ::= <Tuple Elements> ',' <Expression>
        @TUPLEELEMENTS = 101, // <Tuple Elements> ::= <Expression>
        @INSERTSOURCE = 102, // <Insert Source> ::= <Select Query>
        @INSERTSOURCE_VALUES = 103, // <Insert Source> ::= VALUES <Tuple List>
        @TUPLELIST_COMMA = 104, // <Tuple List> ::= <Tuple List> ',' <Tuple>
        @TUPLELIST = 105, // <Tuple List> ::= <Tuple>
        @SETLIST_COMMA = 106, // <Set List> ::= <Set List> ',' <Setter>
        @SETLIST = 107, // <Set List> ::= <Setter>
        @SETTER_EQ = 108, // <Setter> ::= <QIdent> '=' <Expression>
        @TABLENAME = 109, // <Table Name> ::= <Table Reference> <Opt Identifier>
        @OPTIDENTIFIER = 110, // <Opt Identifier> ::= 
        @OPTIDENTIFIER2 = 111, // <Opt Identifier> ::= <Identifier>
        @TABLEREFERENCE = 112, // <Table Reference> ::= <QIdent>
        @TABLEREFERENCE2 = 113, // <Table Reference> ::= <Subquery>
        @SUBQUERY_LPAREN_RPAREN = 114, // <Subquery> ::= '(' <Query> ')'
        @EXPRESSIONORQUERY = 115, // <Expression Or Query> ::= <Expression>
        @EXPRESSIONORQUERY2 = 116, // <Expression Or Query> ::= <Query>
        @EXPRESSION_AND = 117, // <Expression> ::= <Expression> AND <Cmp Expr>
        @EXPRESSION_OR = 118, // <Expression> ::= <Expression> OR <Cmp Expr>
        @EXPRESSION = 119, // <Expression> ::= <Cmp Expr>
        @CMPEXPR_GT = 120, // <Cmp Expr> ::= <Cmp Expr> '>' <Add Exp>
        @CMPEXPR_LT = 121, // <Cmp Expr> ::= <Cmp Expr> '<' <Add Exp>
        @CMPEXPR_LTEQ = 122, // <Cmp Expr> ::= <Cmp Expr> '<=' <Add Exp>
        @CMPEXPR_GTEQ = 123, // <Cmp Expr> ::= <Cmp Expr> '>=' <Add Exp>
        @CMPEXPR_EQ = 124, // <Cmp Expr> ::= <Cmp Expr> '=' <Add Exp>
        @CMPEXPR_EXCLAMEQ = 125, // <Cmp Expr> ::= <Cmp Expr> '!=' <Add Exp>
        @CMPEXPR = 126, // <Cmp Expr> ::= <Add Exp>
        @ADDEXP_PLUS = 127, // <Add Exp> ::= <Add Exp> '+' <Mult Exp>
        @ADDEXP_MINUS = 128, // <Add Exp> ::= <Add Exp> '-' <Mult Exp>
        @ADDEXP = 129, // <Add Exp> ::= <Mult Exp>
        @MULTEXP_TIMES = 130, // <Mult Exp> ::= <Mult Exp> '*' <Negate Exp>
        @MULTEXP_DIV = 131, // <Mult Exp> ::= <Mult Exp> '/' <Negate Exp>
        @MULTEXP = 132, // <Mult Exp> ::= <Negate Exp>
        @NEGATEEXP_MINUS = 133, // <Negate Exp> ::= '-' <Value>
        @NEGATEEXP_NOT = 134, // <Negate Exp> ::= NOT <Value>
        @NEGATEEXP = 135, // <Negate Exp> ::= <Value>
        @IDENTIFIER_RAWIDENTIFIER = 136, // <Identifier> ::= RawIdentifier
        @IDENTIFIER_ESCAPEDIDENTIFIER = 137, // <Identifier> ::= EscapedIdentifier
        @QIDENT_DOT = 138, // <QIdent> ::= <QIdent> '.' <Identifier>
        @QIDENT = 139, // <QIdent> ::= <Identifier>
        @ISNULL_IS_NULL = 140, // <IsNull> ::= <Value> IS NULL
        @NUMBER_INTEGER = 141, // <Number> ::= Integer
        @NUMBER_FLOAT = 142, // <Number> ::= Float
        @BOOLEAN_TRUE = 143, // <Boolean> ::= TRUE
        @BOOLEAN_FALSE = 144, // <Boolean> ::= FALSE
        @FUNCTION_LPAREN_RPAREN = 145, // <Function> ::= <Identifier> '(' <Opt Argument List> ')'
        @OPTARGUMENTLIST = 146, // <Opt Argument List> ::= 
        @OPTARGUMENTLIST2 = 147, // <Opt Argument List> ::= <Argument List>
        @ARGUMENTLIST_COMMA = 148, // <Argument List> ::= <Argument List> ',' <Argument>
        @ARGUMENTLIST = 149, // <Argument List> ::= <Argument>
        @ARGUMENT = 150, // <Argument> ::= <Expression>
        @ARGUMENT2 = 151, // <Argument> ::= <Star Expression>
        @STAREXPRESSION_TIMES = 152, // <Star Expression> ::= '*'
        @VALUE = 153, // <Value> ::= <QIdent>
        @VALUE_LPAREN_RPAREN = 154, // <Value> ::= '(' <Expression Or Query> ')'
        @VALUE2 = 155, // <Value> ::= <IsNull>
        @VALUE3 = 156, // <Value> ::= <Number>
        @VALUE_STRINGLITERAL = 157, // <Value> ::= StringLiteral
        @VALUE4 = 158, // <Value> ::= <Boolean>
        @VALUE_PARAMETER = 159, // <Value> ::= Parameter
        @VALUE5 = 160  // <Value> ::= <Function>
    };
}