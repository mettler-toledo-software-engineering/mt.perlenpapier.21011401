﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal struct FiniteAutomatonEdge
    {
        internal CharacterSet set;
        internal ushort target;

        internal FiniteAutomatonEdge(CharacterSet set, ushort target)
        {
            this.set = set;
            this.target = target;
        }
    }
}