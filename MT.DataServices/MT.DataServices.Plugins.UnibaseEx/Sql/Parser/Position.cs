﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Describes the position inside of a document
    /// </summary>
    public struct Position
    {
        internal int line;
        internal int column;

        /// <summary>
        /// Gets the column of the position represented by this instance.
        /// </summary>
        public int Column
        {
            get { return column; }
        }
       
        /// <summary>
        /// Gets the line of the position represented by this instance.
        /// </summary>
        public int Line
        {
            get { return line; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class.
        /// </summary>
        public Position(int line, int column)
        {
            this.line = line;
            this.column = column;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Position"/> class.
        /// </summary>
        public Position(Position position)
        {
            line = position.line;
            column = position.column;
        }
    }
}