﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Factory class that creates <see cref="ParserContext"/> objects for streams of source text.
    /// </summary>
    public sealed class ParserFactory
    {
        internal ushort initialDFAState;
        internal ushort initialLRState;

        internal Symbol[] symbolTable;
        internal CharacterSet[] charsetTable;
        internal Production[] productionTable;
        internal FiniteAutomatonState[] dfa;
        internal LRState[] lr;

        internal Group[] groupTable;

        internal Symbol errorSymbol;
        internal Symbol endSymbol;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParserFactory"/> class.
        /// </summary>
        public ParserFactory(Stream grammarDefinition)
        {
            if (grammarDefinition == null)
                throw new ArgumentNullException("grammarDefinition");

            LoadTables(grammarDefinition);
        }

        /// <summary>
        /// Creates a new parser context for the specified source.
        /// </summary>
        /// <param name="source">The source to parse. Required.</param>
        /// <returns>A new instance of the <see cref="ParserContext"/> class</returns>
        public ParserContext Parse(TextReader source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return new ParserContext(source, this);
        }

        private void LoadTables(Stream grammarDefinition)
        {
            using (EGTReader reader = new EGTReader(new BinaryReader(grammarDefinition)))
            {
                bool success = true;

                while (!reader.AtEndOfStream || !success)
                {
                    reader.ReadNextRecord();

                    var s = (EGTRecord)reader.ReadByte();
                    switch (s)
                    {
                        case EGTRecord.InitialStates:
                            initialDFAState = reader.ReadUInt16();
                            initialLRState = reader.ReadUInt16();
                            break;
                        case EGTRecord.Symbol:
                            {
                                ushort index = reader.ReadUInt16();
                                string name = reader.ReadString();
                                SymbolType type = (SymbolType)reader.ReadUInt16();

                                symbolTable[index] = new Symbol(name, type, index);
                            }
                            break;
                        case EGTRecord.Production:
                            {
                                ushort index = reader.ReadUInt16();
                                ushort headIndex = reader.ReadUInt16();
                                reader.Skip();
                                
                                Production production = new Production(symbolTable[headIndex], index);
                                List<Symbol> symbols = new List<Symbol>();
                                while (!reader.AtEndOfRecord)
                                {
                                    symbols.Add(symbolTable[reader.ReadUInt16()]);
                                }

                                production.Update(symbols.ToArray());

                                productionTable[index] = production;
                            }
                            break;
                        case EGTRecord.DFAState:
                            {
                                ushort index = reader.ReadUInt16();
                                bool accepts = reader.ReadBoolean();
                                ushort acceptIndex = reader.ReadUInt16();
                                reader.Skip();

                                FiniteAutomatonState state = new FiniteAutomatonState();

                                if (accepts)
                                    state.accept = symbolTable[acceptIndex];

                                List<FiniteAutomatonEdge> edges = new List<FiniteAutomatonEdge>();
                                while (!reader.AtEndOfRecord)
                                {
                                    ushort charsetIndex = reader.ReadUInt16();
                                    ushort target = reader.ReadUInt16();
                                    reader.Skip();

                                    edges.Add(new FiniteAutomatonEdge(charsetTable[charsetIndex], target));
                                }

                                state.edges = edges.ToArray();
                                state.Optimize();
                                
                                dfa[index] = state;
                            }
                            break;
                        case EGTRecord.LRState:
                            {
                                ushort index = reader.ReadUInt16();
                                reader.Skip();

                                List<LRAction> actions = new List<LRAction>();
                                while (!reader.AtEndOfRecord)
                                {
                                    ushort symIndex = reader.ReadUInt16();
                                    ActionType action = (ActionType)reader.ReadUInt16();
                                    ushort target = reader.ReadUInt16();
                                    reader.Skip();

                                    actions.Add(new LRAction(symbolTable[symIndex], action, target));
                                }

                                lr[index] = new LRState(actions.ToArray());
                            }
                            break;
                        case EGTRecord.Property:
                            break;
                        case EGTRecord.CharRanges:
                            {
                                ushort index = reader.ReadUInt16();
                                reader.Skip();
                                reader.ReadUInt16();
                                reader.Skip();

                                var charset = new CharacterSet();
                                while (!reader.AtEndOfRecord)
                                {
                                    charset.Add(new CharacterRange(reader.ReadUInt16(), reader.ReadUInt16()));
                                }

                                charsetTable[index] = charset;
                            }
                            break;
                        case EGTRecord.Group:
                            {
                                Group group = new Group();
                                group.tableIndex = reader.ReadUInt16();
                                group.name = reader.ReadString();
                                group.container = symbolTable[reader.ReadUInt16()];
                                group.start = symbolTable[reader.ReadUInt16()];
                                group.end = symbolTable[reader.ReadUInt16()];
                                group.advance = (Group.AdvanceMode)reader.ReadUInt16();
                                group.ending = (Group.EndingMode)reader.ReadUInt16();
                                reader.Skip();

                                group.nesting = new ushort[reader.ReadUInt16()];
                                for (int i = 0; i < group.nesting.Length; i++)
                                {
                                    group.nesting[i] = reader.ReadUInt16();
                                }

                                group.container.group = group;
                                group.start.group = group;
                                group.end.group = group;

                                groupTable[group.tableIndex] = group;
                            }
                            break;
                        case EGTRecord.TableCounts:
                            symbolTable = new Symbol[reader.ReadUInt16()];
                            charsetTable = new CharacterSet[reader.ReadUInt16()];
                            productionTable = new Production[reader.ReadUInt16()];
                            dfa = new FiniteAutomatonState[reader.ReadUInt16()];
                            lr = new LRState[reader.ReadUInt16()];
                            groupTable = new Group[reader.ReadUInt16()];
                            break;
                        default:
                            break;
                    }
                }
            }

            errorSymbol = symbolTable.FirstOrDefault(d => d.type == SymbolType.Error);
            endSymbol = symbolTable.FirstOrDefault(d => d.type == SymbolType.End);
        }
    }
}