﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Represents a single token 
    /// </summary>
    public sealed class Token
    {
        internal Position position;
        internal Symbol symbol;
        internal object data;
        internal ushort state;

        /// <summary>
        /// Gets or sets the data that is contained inside the token.
        /// </summary>
        public object Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// Gets the symbol that this token is an instance of.
        /// </summary>
        public Symbol Symbol
        {
            get { return symbol; }
        }

        /// <summary>
        /// Gets the position of the token.
        /// </summary>
        public Position Position
        {
            get { return position; }
        }

        internal Token()
        {
        }
    }
}