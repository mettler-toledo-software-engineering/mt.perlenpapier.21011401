﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal struct ReducedEdge
    {
        internal ushort start;
        internal ushort end;
        internal ushort target;
        internal ushort left;
        internal ushort right;
    }
}