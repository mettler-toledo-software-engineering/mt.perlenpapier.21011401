﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal enum ActionType
    {
        Shift = 1,
        Reduce = 2,
        Goto = 3,
        Accept = 4,
        Error = 5
    }
}