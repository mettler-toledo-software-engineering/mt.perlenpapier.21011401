﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Contains the context information for parsing a single document
    /// </summary>
    public sealed class ParserContext : IDisposable
    {
        private readonly TextReader reader;
        private char[] lookaheadBuffer;
        private int bufferPosition;
        private int bufferSize;
        private bool atEndOfStream;
        private Token inputToken;

        private readonly ParserFactory parser;
        private Position sysPosition;
        private Position currentPosition;
        private readonly Stack<Token> groupStack = new Stack<Token>();
        private readonly Stack<Token> stack = new Stack<Token>();
        private readonly List<Symbol> expectedSymbols = new List<Symbol>();

        private ushort currentLALR;
        private bool haveReduction;

        private bool trimReduction = false;

        /// <summary>
        /// Gets or sets a value indicating whether reductions that only have a single non-terminal should not be passed back.
        /// </summary>
        /// <value>
        ///   <c>true</c> if reductions that only have a single non-terminal should not be passed back; otherwise, <c>false</c>.
        /// </value>
        public bool TrimReductions
        {
            get { return trimReduction; }
            set { trimReduction = value; }
        }

        internal ParserContext(TextReader reader, ParserFactory parser)
        {
            this.reader = reader;
            this.parser = parser;
            
            lookaheadBuffer = new char[0];
            currentLALR = parser.initialLRState;
            stack.Push(new Token()
            {
                state = parser.initialLRState
            });
        }

        /// <summary>
        /// Gets the current parsing position.
        /// </summary>
        public Position CurrentPosition
        {
            get { return currentPosition; }
        }

        /// <summary>
        /// Gets or sets the current reduction object.
        /// </summary>
        public object CurrentReduction
        {
            get { return haveReduction ? stack.Peek().data : null; }
            set
            {
                if (haveReduction)
                    stack.Peek().data = value;
            }
        }

        private string GetLookaheadSlice(int count)
        {
            if (count > bufferSize)
                count = bufferSize;

            return new string(lookaheadBuffer, bufferPosition, count);
        }

        private char LookaheadAt(int index)
        {
            if (index >= bufferSize)
            {
                if (index + bufferPosition >= lookaheadBuffer.Length)
                {
                    if (!atEndOfStream)
                    {
                        char[] newBuffer = new char[Math.Max(256, bufferPosition + index - lookaheadBuffer.Length)];
                        int availableData = lookaheadBuffer.Length - bufferPosition;
                        Array.Copy(lookaheadBuffer, bufferPosition, newBuffer, 0, availableData);
                        bufferPosition = 0;
                        lookaheadBuffer = newBuffer;

                        int fillUp = newBuffer.Length - availableData;
                        int charsRead = reader.Read(lookaheadBuffer, availableData, fillUp);
                        if (charsRead < fillUp)
                        {
                            atEndOfStream = true;
                            bufferSize = Math.Min(lookaheadBuffer.Length, index + 1);
                        }
                        else
                            bufferSize = index + 1;
                    }
                }
                else
                    bufferSize = index + 1;
            }

            if (index >= bufferSize)
                return (char)0;
            else
                return lookaheadBuffer[index + bufferPosition];
        }

        private ParseResult ParseLALR(Token nextToken)
        {
            LRAction action;
            if (parser.lr[currentLALR].TryFind(nextToken.symbol, out action))
            {
                haveReduction = false;

                switch (action.type)
                {
                    case ActionType.Accept:
                        haveReduction = true;
                        return ParseResult.Accept;

                    case ActionType.Shift:
                        currentLALR = action.value;
                        nextToken.state = currentLALR;
                        stack.Push(nextToken);
                        return ParseResult.Shift;

                    case ActionType.Reduce:
                        var prod = parser.productionTable[action.value];
                        ParseResult result;
                        Token head;

                        if (trimReduction && prod.containsOneNonTerminal)
                        {
                            head = stack.Pop();
                            head.symbol = prod.head;

                            result = ParseResult.ReduceEliminated;
                        }
                        else
                        {
                            haveReduction = true;

                            int length = prod.handle.Length;
                            var newReduction = new Reduction(length, prod);
                            object[] data = newReduction.data;

                            for (int i = length - 1; i >= 0; i--)
                            {
                                data[i] = stack.Pop();
                            }

                            head = new Token()
                            {
                                symbol = prod.head,
                                data = newReduction
                            };

                            result = ParseResult.ReduceNormal;
                        }

                        var index = stack.Peek().state;

                        LRAction nextAction;
                        if (parser.lr[index].TryFind(prod.head, out nextAction))
                        {
                            currentLALR = nextAction.value;

                            head.state = currentLALR;
                            stack.Push(head);
                        }
                        else
                            return ParseResult.InternalError;

                        return result;
                }

                return ParseResult.InternalError;
            }
            else
            {
                expectedSymbols.Clear();
                foreach (var possibleAction in parser.lr[currentLALR].actions)
                {
                    var type = possibleAction.symbol.type;
                    if (type == SymbolType.Content || type == SymbolType.End || type == SymbolType.GroupStart || type == SymbolType.GroupEnd)
                        expectedSymbols.Add(possibleAction.symbol);
                }

                return ParseResult.SyntaxError;
            }
        }

        /// <summary>
        /// Executes the next step of the parsing process.
        /// </summary>
        /// <returns>The result of the parsing process.</returns>
        public ParseMessage Parse()
        {
            for (;;)
            {
                if (inputToken == null)
                {
                    inputToken = ProduceToken();
                    return ParseMessage.TokenRead;
                }
                else
                {
                    currentPosition = inputToken.position;

                    if (groupStack.Count > 0)
                        return ParseMessage.GroupError;
                    else if (inputToken.symbol.type == SymbolType.Noise)
                        inputToken = null;
                    else if (inputToken.symbol.type == SymbolType.Error)
                        return ParseMessage.LexicalError;
                    else
                    {
                        switch (ParseLALR(inputToken))
                        {
                            case ParseResult.Accept:
                                return ParseMessage.Accept;

                            case ParseResult.Shift:
                                inputToken = null;
                                break;
                            case ParseResult.ReduceNormal:
                                return ParseMessage.Reduction;

                            case ParseResult.SyntaxError:
                                return ParseMessage.SyntaxError;

                            case ParseResult.InternalError:
                                return ParseMessage.InternalError;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        private Token ProduceToken()
        {
            for (;;)
            {
                Token token = LookaheadDFA();

                bool nestGroup = false;
                if (token.symbol.type == SymbolType.GroupStart)
                {
                    if (groupStack.Count == 0)
                        nestGroup = true;
                    else
                        nestGroup = groupStack.Peek().symbol.group.nesting.Contains(token.symbol.group.tableIndex);
                }

                if (nestGroup)
                {
                    ConsumeBuffer(((string)token.Data).Length);
                    groupStack.Push(token);
                }
                else if (groupStack.Count == 0)
                {
                    ConsumeBuffer(((string)token.Data).Length);
                    return token;
                }
                else if (groupStack.Peek().symbol.group.end == token.symbol)
                {
                    var end = groupStack.Pop();

                    if (end.symbol.group.ending == Group.EndingMode.Closed)
                    {
                        end.data = (string)end.data + (string)token.data;
                        ConsumeBuffer(((string)token.Data).Length);
                    }

                    if (groupStack.Count == 0)
                    {
                        end.symbol = end.symbol.group.container;
                        return end;
                    }
                    else
                    {
                        var newEnd = groupStack.Peek();
                        newEnd.data = (string)newEnd.data + (string)end.data;
                    }
                }
                else if (token.symbol.type == SymbolType.End)
                    return token;
                else
                {
                    var end = groupStack.Peek();

                    if (end.symbol.group.advance == Group.AdvanceMode.Token)
                    {
                        end.data = (string)end.data + (string)token.data;
                        ConsumeBuffer(((string)token.Data).Length);
                    }
                    else
                    {
                        end.data = (string)end.data + ((string)token.data)[0].ToString();
                        ConsumeBuffer(1);
                    }
                }
            }
        }

        private void ConsumeBuffer(int count)
        {
            int end = bufferPosition + count;
            for (; bufferPosition < end; bufferPosition++)
            {
                bufferSize--;
                char c = lookaheadBuffer[bufferPosition];
                if (c == (char)0x10)
                {
                    sysPosition.column = 0;
                    sysPosition.line++;
                }
                else if (c != (char)0x13)
                    sysPosition.column++;
            }
        }

        private Token LookaheadDFA()
        {
            FiniteAutomatonState[] dfa = parser.dfa;
            ushort currentDfa = parser.initialDFAState;
            ushort target = 0;
            int currentPosition = 0;
            int lastAcceptState = -1;
            int lastAcceptPosition = -1;

            char c = LookaheadAt(0);
            if (c != (char)0)
            {
                for (;;)
                {
                    c = currentPosition < bufferSize ? lookaheadBuffer[currentPosition + bufferPosition] : LookaheadAt(currentPosition);
                    if (c != (char)0)
                        target = dfa[currentDfa].GetEdge((ushort)c);
                    else
                        target = ushort.MaxValue;

                    if (target < ushort.MaxValue)
                    {
                        if (dfa[target].accept != null)
                        {
                            lastAcceptState = target;
                            lastAcceptPosition = currentPosition;
                        }

                        currentDfa = target;
                        currentPosition++;
                    }
                    else
                    {
                        if (lastAcceptState == -1)
                        {
                            return new Token()
                            {
                                symbol = parser.errorSymbol,
                                data = GetLookaheadSlice(1),
                                position = sysPosition
                            };
                        }
                        else
                        {
                            return new Token()
                            {
                                symbol = dfa[lastAcceptState].accept,
                                data = GetLookaheadSlice(lastAcceptPosition + 1),
                                position = sysPosition
                            };
                        }
                    }
                }
            }
            else
            {
                return new Token()
                {
                    symbol = parser.endSymbol,
                    data = string.Empty,
                    position = sysPosition
                };
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            reader.Dispose();
        }
    }
}