﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal enum EGTRecord : byte
    {
        InitialStates = 73,
        Symbol = 83,
        Production = 82,
        DFAState = 68,
        LRState = 76,
        Property = 112,
        CharRanges = 99,
        Group = 103,
        TableCounts = 116
    }
}