﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal enum ParseResult
    {
        Accept,
        Shift,
        ReduceNormal,
        ReduceEliminated,
        SyntaxError,
        InternalError
    }
}