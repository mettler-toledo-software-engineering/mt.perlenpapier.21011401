﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class FiniteAutomatonState
    {
        internal FiniteAutomatonEdge[] edges;
        internal Symbol accept;

        internal ReducedEdge[] reducedEdges;
        internal ushort start;

        internal ushort GetEdge(int charcode)
        {
            ushort current = start;
            while (current != ushort.MaxValue)
            {
                if (reducedEdges[current].start <= charcode)
                {
                    if (reducedEdges[current].end >= charcode)
                        break;
                    else
                        current = reducedEdges[current].right;
                }
                else
                    current = reducedEdges[current].left;
            }

            if (current == ushort.MaxValue)
                return ushort.MaxValue;
            else
                return reducedEdges[current].target;
        }

        internal void Optimize()
        {
            List<ReducedEdge> ranges = new List<ReducedEdge>();

            foreach (var edge in edges)
            {
                foreach (var set in edge.set.OrderBy(d => d.start))
                {
                    int i;

                    for (i = 0; i < ranges.Count; i++)
                    {
                        if (ranges[i].start <= set.start && ranges[i].end >= set.end)
                        {
                            // set starts inside of ranges[i]
                            ushort end = set.end;

                            if (i < ranges.Count - 1 && end > ranges[i + 1].start)
                                end = (ushort)(ranges[i + 1].start - 1);

                            ushort start = (ushort)(ranges[i].end + 1);

                            if (start <= end)
                            {
                                ranges.Insert(i + 1, new ReducedEdge()
                                {
                                    start = start,
                                    end = end,
                                    target = edge.target,
                                    left = ushort.MaxValue,
                                    right = ushort.MaxValue
                                });
                            }

                            break;
                        }
                        else if (i < ranges.Count - 1 && ranges[i].end < set.start && set.start < ranges[i + 1].start)
                        {
                            // set starts between ranges[i] and ranges[i+1]
                            // ranges[i+1] has higher priority, so shorten set                           
                            ranges.Insert(i + 1, new ReducedEdge()
                            {
                                start = set.start,
                                end = Math.Min(set.end, (ushort)(ranges[i + 1].start - 1)),
                                target = edge.target,
                                left = ushort.MaxValue,
                                right = ushort.MaxValue
                            });

                            break;
                        }
                    }

                    if (i == ranges.Count)
                    {
                        // set is at the end of the list
                        ranges.Add(new ReducedEdge()
                        {
                            start = set.start,
                            end = set.end,
                            target = edge.target,
                            left = ushort.MaxValue,
                            right = ushort.MaxValue
                        });
                    }
                }
            }

            // Now we have a sorted list of ranges, lets create a tree

            if (ranges.Count == 0)
            {
                start = ushort.MaxValue;
                return;
            }

            reducedEdges = ranges.ToArray();
            start = GenerateTree(0, reducedEdges.Length);
        }

        private ushort GenerateTree(int left, int right)
        {
            if (left >= right)
                return ushort.MaxValue;

            int index = left + (right - left) / 2;
            reducedEdges[index].left = GenerateTree(left, index);
            reducedEdges[index].right = GenerateTree((index + 1), right);

            return (ushort)index;
        }
    }
}