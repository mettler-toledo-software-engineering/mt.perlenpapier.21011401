﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class LRState
    {
        private static readonly LRAction empty = new LRAction();

        internal LRAction[] actions;
        private readonly int[] hashCodeMapping;

        public LRState(LRAction[] actions)
        {
            this.actions = actions;

            if (actions.Length > 10)
            {
                hashCodeMapping = new int[actions.Length];
                for (int i = 0; i < actions.Length; i++)
                {
                    hashCodeMapping[i] = actions[i].symbol.GetHashCode();
                }

                Array.Sort(hashCodeMapping, actions);
            }
        }

        internal bool TryFind(Symbol symbol, out LRAction action)
        {
            if (hashCodeMapping == null)
            {
                int length = actions.Length;
                for (int i = 0; i < length; i++)
                {
                    action = actions[i];
                    if (action.symbol == symbol)
                        return true;
                }

                action = empty;
                return false;
            }
            else
            {
                int index = Array.BinarySearch(hashCodeMapping, symbol.GetHashCode());
                if (index < 0)
                {
                    action = empty;
                    return false;
                }

                action = actions[index];
                return true;
            }
        }
    }
}