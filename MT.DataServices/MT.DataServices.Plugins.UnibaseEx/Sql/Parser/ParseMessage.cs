﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Declares constants specifing different parsing results
    /// </summary>
    public enum ParseMessage : byte
    {
        /// <summary>
        /// A token was read
        /// </summary>
        TokenRead = 0,

        /// <summary>
        /// A reduction is taking place
        /// </summary>
        Reduction = 1,

        /// <summary>
        /// The source document has been accepted
        /// </summary>
        Accept = 2,

        /// <summary>
        /// A lexical error occured
        /// </summary>
        LexicalError = 4,

        /// <summary>
        /// A syntax error occured
        /// </summary>
        SyntaxError = 5,
        
        /// <summary>
        /// A group error occured
        /// </summary>
        GroupError = 6,

        /// <summary>
        /// An internal error occured
        /// </summary>
        InternalError = 7
    }
}