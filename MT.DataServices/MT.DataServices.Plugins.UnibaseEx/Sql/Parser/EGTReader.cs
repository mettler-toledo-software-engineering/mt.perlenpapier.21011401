﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class EGTReader : IDisposable
    {
        private enum EntryType : byte
        {
            Empty = 69,
            UInt16 = 73,
            String = 83,
            Boolean = 66,
            Byte = 98
        }

        private readonly BinaryReader reader;
        private int entryCount;
        private int entriesRead;
        private readonly string fileHeader;

        /// <summary>
        /// Initializes a new instance of the <see cref="EGTReader"/> class.
        /// </summary>
        public EGTReader(BinaryReader reader)
        {
            this.reader = reader;
            fileHeader = ReadRawString();
        }

        public bool AtEndOfStream
        {
            get { return reader.BaseStream.Position >= reader.BaseStream.Length; }
        }

        public bool AtEndOfRecord
        {
            get { return entriesRead >= entryCount; }
        }

        public string FileHeader
        {
            get { return fileHeader; }
        }

        internal void Close()
        {
            reader.Close();
        }

        private string ReadRawString()
        {
            StringBuilder result = new StringBuilder();

            ushort value;
            while ((value = reader.ReadUInt16()) != 0)
            {
                result.Append(Convert.ToChar(value));
            }

            return result.ToString();
        }

        private void Expect(EntryType type)
        {
            entriesRead++;
            if (entriesRead > entryCount || (EntryType)reader.ReadByte() != type)
                throw new InvalidOperationException("Unexpected element in file");
        }

        internal void Skip()
        {
            entriesRead++;
            switch ((EntryType)reader.ReadByte())
            {
                case EntryType.Empty:
                    break;
                case EntryType.UInt16:
                    reader.ReadUInt16();
                    break;
                case EntryType.String:
                    while (reader.ReadUInt16() != 0)
                    {
                    }
                    break;
                case EntryType.Boolean:
                    reader.ReadByte();
                    break;
                case EntryType.Byte:
                    reader.ReadByte();
                    break;
            }
        }

        internal string ReadString()
        {
            Expect(EntryType.String);
            return ReadRawString();
        }

        internal ushort ReadUInt16()
        {
            Expect(EntryType.UInt16);
            return reader.ReadUInt16();
        }

        internal byte ReadByte()
        {
            Expect(EntryType.Byte);
            return reader.ReadByte();
        }

        internal bool ReadBoolean()
        {
            Expect(EntryType.Boolean);
            return reader.ReadByte() == 1;
        }

        internal bool ReadNextRecord()
        {
            while (entriesRead < entryCount)
            {
                Skip();
            }

            if (reader.ReadByte() == 77)
            {
                entryCount = reader.ReadUInt16();
                entriesRead = 0;
                return true;
            }
            else
                return false;
        }
        
        public void Dispose()
        {
            Close();
        }
    }
}