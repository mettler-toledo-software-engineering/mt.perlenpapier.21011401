﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal struct LRAction
    {
        internal Symbol symbol;
        internal ActionType type;
        internal ushort value;

        /// <summary>
        /// Initializes a new instance of the <see cref="LRAction"/> class.
        /// </summary>
        public LRAction(Symbol symbol, ActionType type, ushort value)
        {
            this.symbol = symbol;
            this.type = type;
            this.value = value;
        }
    }
}