﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class CharacterSet : List<CharacterRange>
    {
        public bool Contains(ushort charcode)
        {
            var length = Count;
            for (int i = 0; i < length; i++)
            {
                var current = this[i];
                if (charcode >= current.start && charcode <= current.end)
                    return true;
            }

            return false;
        }
    }
}