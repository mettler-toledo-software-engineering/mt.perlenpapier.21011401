﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class Group
    {
        internal enum AdvanceMode : byte
        {
            Token = 0,
            Character = 1
        }

        internal enum EndingMode : byte
        {
            Open = 0,
            Closed = 1
        }

        internal ushort tableIndex;
        internal string name;
        internal Symbol container;
        internal Symbol start;
        internal Symbol end;
        internal AdvanceMode advance;
        internal EndingMode ending;
        internal ushort[] nesting;

        internal Group()
        {
            advance = AdvanceMode.Character;
            ending = EndingMode.Closed;
        }
    }
}