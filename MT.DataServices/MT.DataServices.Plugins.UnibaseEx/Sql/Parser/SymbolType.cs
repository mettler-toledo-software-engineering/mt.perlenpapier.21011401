﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Declares constants specifing different types of symbols
    /// </summary>
    public enum SymbolType : byte
    {
        /// <summary>
        /// The symbol is a non-terminal
        /// </summary>
        NonTerminal = 0,

        /// <summary>
        /// The symbol is a terminal
        /// </summary>
        Content = 1,

        /// <summary>
        /// The symbol is noise (ignored by the interpreter)
        /// </summary>
        Noise = 2,

        /// <summary>
        /// The symbol represents the end of the file
        /// </summary>
        End = 3,

        /// <summary>
        /// The symbol is the startsymbol of a group
        /// </summary>
        GroupStart = 4,
        
        /// <summary>
        /// The symbol is the endsymbol of a group
        /// </summary>
        GroupEnd = 5,

        /// <summary>
        /// The symbol represents an failed operation.
        /// </summary>
        Error = 7
    }
}