﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal struct CharacterRange
    {
        internal ushort start;
        internal ushort end;

        internal CharacterRange(ushort start, ushort end)
        {
            this.start = start;
            this.end = end;
        }
    }
}