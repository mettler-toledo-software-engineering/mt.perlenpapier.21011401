﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Represents the reduction operation of rule
    /// </summary>
    public sealed class Reduction
    {
        internal Token[] data;
        internal Production parent;

        /// <summary>
        /// Initializes a new instance of the <see cref="Reduction"/> class.
        /// </summary>
        internal Reduction(int size, Production parent)
        {
            this.parent = parent;
            data = new Token[size];
        }

        /// <summary>
        /// Gets the index of the production rule.
        /// </summary>
        public int Index
        {
            get { return parent.tableIndex; }
        }

        /// <summary>
        /// Gets or sets the data object of this production at the specified index.
        /// </summary>
        public object this[int index]
        {
            get { return data[index].data; }
            set { data[index].data = value; }
        }

        /// <summary>
        /// Gets the count of data objects.
        /// </summary>
        public int Count
        {
            get { return data.Length; }
        }

        /// <summary>
        /// Gets the first data object.
        /// </summary>
        public object First
        {
            get { return data[0].data; }
        }

        /// <summary>
        /// Gets the last data object.
        /// </summary>
        public object Last
        {
            get { return data[data.Length - 1].data; }
        }
    }
}