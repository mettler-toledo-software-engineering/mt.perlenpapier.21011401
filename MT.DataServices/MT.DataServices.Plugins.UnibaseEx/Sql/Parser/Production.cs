﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    internal sealed class Production
    {
        internal Symbol head;
        internal Symbol[] handle;
        internal ushort tableIndex;
        internal bool containsOneNonTerminal;

        public ushort TableIndex
        {
            get { return tableIndex; }
        }

        public ReadOnlyCollection<Symbol> Handle
        {
            get { return new ReadOnlyCollection<Symbol>(handle); }
        }

        public Symbol Head
        {
            get { return head; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Production"/> class.
        /// </summary>
        public Production(Symbol head, ushort tableIndex)
        {
            this.head = head;
            this.tableIndex = tableIndex;
        }

        internal void Update(Symbol[] handle)
        {
            this.handle = handle;
            containsOneNonTerminal = handle.Length == 1 && handle[0].type == SymbolType.NonTerminal;
        }
    }
}