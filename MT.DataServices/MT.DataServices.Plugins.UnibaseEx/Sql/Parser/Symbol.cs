﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.Parser
{
    /// <summary>
    /// Represent one of the different types of a token
    /// </summary>
    public sealed class Symbol
    {
        internal string name;
        internal ushort tableIndex;
        internal SymbolType type;
        internal Group group;

        /// <summary>
        /// Gets the type of the symbol.
        /// </summary>
        public SymbolType Type
        {
            get { return type; }
        }

        /// <summary>
        /// Gets the index of symbol inside the symbol table.
        /// </summary>
        public ushort TableIndex
        {
            get { return tableIndex; }
        }

        /// <summary>
        /// Gets the name of the symbol.
        /// </summary>
        public string Name 
        {
            get { return name; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Symbol"/> class.
        /// </summary>
        public Symbol(string name, SymbolType type, ushort tableIndex)
        {
            this.name = name;
            this.type = type;
            this.tableIndex = tableIndex;
        }
    }
}