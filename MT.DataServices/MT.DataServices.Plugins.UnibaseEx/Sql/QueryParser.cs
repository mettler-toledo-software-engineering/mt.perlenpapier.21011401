﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using MT.DataServices.Plugins.UnibaseEx.Sql.Parser;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    internal class QueryParser
    {
        private static readonly QueryParser instance = new QueryParser();

        internal static StatementSequence Parse(string text)
        {
            return instance.ParseInternal(text);
        }

        private readonly ParserFactory parserFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParser"/> class.
        /// </summary>
        private QueryParser()
        {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MT.DataServices.Plugins.UnibaseEx.Sql.EmbeddedSql.egt"))
            {
                parserFactory = new ParserFactory(stream);
            }
        }

        private StatementSequence ParseInternal(string text)
        {
            using (var parser = parserFactory.Parse(new StringReader(text)))
            { 
                for (;;)
                {
                    var status = parser.Parse();
                    switch (status)
                    {
                        case ParseMessage.TokenRead:
                            continue;
                        case ParseMessage.Reduction:
                            break;
                        case ParseMessage.Accept:
                            return (StatementSequence)parser.CurrentReduction;
                        case ParseMessage.LexicalError:
                        case ParseMessage.SyntaxError:
                        case ParseMessage.GroupError:
                        case ParseMessage.InternalError:
                        default:
                            throw new QueryParseException("The query is invalid.", parser.CurrentPosition);
                    }

                    parser.CurrentReduction = HandleReduction((Reduction)parser.CurrentReduction);
                }
            }
        }

        private object HandleReduction(Reduction r)
        {
            switch ((ProdIndex)r.Index)
            {
                case ProdIndex.PROGRAM:
                    return new StatementSequence(new ReadOnlyCollection<Statement>((List<Statement>)r.First));
                case ProdIndex.STATEMENTLIST_SEMI:
                    return ConstructList<Statement>(r);
                case ProdIndex.STATEMENTLIST:
                    return InitializeList<Statement>(r);
                case ProdIndex.STATEMENT:
                    return null;

                case ProdIndex.STATEMENT2:
                case ProdIndex.QUERY:
                case ProdIndex.QUERY2:
                case ProdIndex.QUERY3:
                case ProdIndex.QUERY4:
                    return r.First;

                case ProdIndex.RAWSQL_EXEC_RAW_STRINGLITERAL:
                    return new ExecRawStatement((string)r[2]);

                case ProdIndex.SELECTQUERY_SELECT:
                    if (r[2] != null)
                    {
                        var fromClause = (FromClause)r[2];
                        return new SelectStatement(ToReadOnlyCollection<FieldExpression>(r[1]), fromClause.relation, fromClause.joins, fromClause.condition,
                            fromClause.orderings, fromClause.grouping, fromClause.limit.skip, fromClause.limit.take);
                    }
                    else
                        return new SelectEvalStatement(ToReadOnlyCollection<FieldExpression>(r[1]));

                case ProdIndex.OPTSELECTSOURCE:
                    return null;
                case ProdIndex.OPTSELECTSOURCE_FROM:
                    return new FromClause
                    {
                        relation = (RelationReferenceExpression)r[1],
                        joins = ToReadOnlyCollection<JoinModifier>(r[2]),
                        condition = (Expression)r[3],
                        orderings = ToReadOnlyCollection<OrderModifier>(r[4]),
                        grouping = (GroupingModifier)r[5],
                        limit = (LimitClause)r[6]
                    };
                case ProdIndex.INSERTQUERY_INSERT_INTO:
                    return new InsertStatement((RelationReferenceExpression)r[2], (TupleExpression)r[3], (Expression)r[4]);
                case ProdIndex.UPDATEQUERY_UPDATE_SET:
                    return new UpdateStatement(
                        ToReadOnlyCollection<SetModifier>(r[4]),
                        (RelationReferenceExpression)r[1],
                        ToReadOnlyCollection<JoinModifier>(r[2]),
                        (Expression)r[5],
                        null,
                        ((LimitClause)r[6]).skip,
                        ((LimitClause)r[6]).take);
                case ProdIndex.DELETEQUERY_DELETE_FROM:
                    return new DeleteStatement(
                        (RelationReferenceExpression)r[1],
                        (ReadOnlyCollection<JoinModifier>)r[2],
                        (Expression)r[3],
                        (ReadOnlyCollection<OrderModifier>)r[4],
                        ((LimitClause)r[5]).skip,
                        ((LimitClause)r[5]).take);

                case ProdIndex.FIELDS_COMMA:
                    return ConstructList<FieldExpression>(r);
                case ProdIndex.FIELDS:
                    return InitializeList<FieldExpression>(r);
                case ProdIndex.FIELD:
                    return new FieldExpression((Identifier)r[1], (Expression)r[0]);

                case ProdIndex.EXPRESSIONORSTAR:
                case ProdIndex.EXPRESSIONORSTAR2:
                    return r.First;

                case ProdIndex.STAREXPRESSION_TIMES:
                    return new StarExpression();

                case ProdIndex.OPTAS:
                    return null;
                case ProdIndex.OPTAS_AS:
                    return r[1];
                case ProdIndex.OPTJOINLIST:
                    return null;
                case ProdIndex.OPTJOINLIST2:
                    return ToReadOnlyCollection<JoinModifier>(r.First);
                case ProdIndex.JOINLIST:
                    return ConstructList<JoinModifier>(r);
                case ProdIndex.JOINLIST2:
                    return InitializeList<JoinModifier>(r);
                case ProdIndex.JOINMODE_JOIN:
                case ProdIndex.JOINMODE_INNER_JOIN:
                    return JoinMode.Inner;
                case ProdIndex.JOINMODE_LEFT_JOIN:
                    return JoinMode.Left;
                case ProdIndex.JOINMODE_RIGHT_JOIN:
                    return JoinMode.Right;
                case ProdIndex.JOINMODE_FULL_OUTER_JOIN:
                case ProdIndex.JOINMODE_OUTER_JOIN:
                    return JoinMode.Full;
                case ProdIndex.JOIN_ON:
                    return new JoinModifier(
                        (RelationReferenceExpression)r[1],
                        (JoinMode)r[0],
                        (Expression)r[3]);
                case ProdIndex.OPTWHERE:
                    return null;
                case ProdIndex.OPTWHERE2:
                    return r.First;
                case ProdIndex.WHERE_WHERE:
                    return r[1];
                case ProdIndex.OPTORDERINGS:
                    return null;
                case ProdIndex.OPTORDERINGS2:
                    return r.First;
                case ProdIndex.ORDERINGS_ORDER_BY:
                    return ToReadOnlyCollection<OrderModifier>(r[2]);
                case ProdIndex.ORDERINGLIST_COMMA:
                    return ConstructList<OrderModifier>(r);
                case ProdIndex.ORDERINGLIST:
                    return InitializeList<OrderModifier>(r);
                case ProdIndex.ORDERING:
                    return new OrderModifier(
                        (Expression)r[0],
                        (SortDirection)r[1]);
                case ProdIndex.ORDERDIRECTION:
                case ProdIndex.ORDERDIRECTION_ASC:
                    return SortDirection.Ascending;
                case ProdIndex.ORDERDIRECTION_DESC:
                    return SortDirection.Descending;

                case ProdIndex.OPTGROUPING:
                    return null;
                case ProdIndex.OPTGROUPING2:
                    return r.First;
                case ProdIndex.GROUPING_GROUP_BY:
                    return new GroupingModifier(ToReadOnlyCollection<Identifier>(r[1]), (Expression)r[2]);
                case ProdIndex.GROUPINGLIST_COMMA:
                    return ConstructList<Identifier>(r);
                case ProdIndex.GROUPINGLIST:
                    return InitializeList<Identifier>(r);
                case ProdIndex.GROUPINGITEM:
                    return r.First;
                case ProdIndex.OPTHAVING:
                    return null;
                case ProdIndex.OPTHAVING2:
                    return r.First;
                case ProdIndex.HAVING_HAVING:
                    return r[1];
                case ProdIndex.OPTLIMIT:
                    return new LimitClause();
                case ProdIndex.OPTLIMIT2:
                    return r.First;
                case ProdIndex.LIMIT_LIMIT_INTEGER:
                    if (r[2] == null)
                        return new LimitClause
                        {
                            take = int.Parse((string)r[1])
                        };
                    else
                        return new LimitClause
                        {
                            skip = int.Parse((string)r[1]), take = int.Parse((string)r[1])
                        };
                case ProdIndex.OPTTAKE:
                    return null;
                case ProdIndex.OPTTAKE_COMMA_INTEGER:
                    return r[1];
                case ProdIndex.OPTTUPLE:
                    return null;
                case ProdIndex.OPTTUPLE2:
                    return r.First;
                case ProdIndex.@TUPLE_LPAREN_RPAREN:
                    return new TupleExpression(ToReadOnlyCollection<Expression>(r[1]));
                case ProdIndex.TUPLEELEMENTS_COMMA:
                    return ConstructList<Expression>(r);
                case ProdIndex.TUPLEELEMENTS:
                    return InitializeList<Expression>(r);
                case ProdIndex.INSERTSOURCE:
                    return new SubqueryExpression((Statement)r.First);
                case ProdIndex.INSERTSOURCE_VALUES:
                    return new ValuesListExpression(ToReadOnlyCollection<TupleExpression>(r[1]));
                case ProdIndex.TUPLELIST_COMMA:
                    return ConstructList<TupleExpression>(r);
                case ProdIndex.TUPLELIST:
                    return InitializeList<TupleExpression>(r);
                case ProdIndex.SETLIST_COMMA:
                    return ConstructList<SetModifier>(r);
                case ProdIndex.SETLIST:
                    return InitializeList<SetModifier>(r);
                case ProdIndex.SETTER_EQ:
                    return new SetModifier((Expression)r[0], (Expression)r[2]);
                case ProdIndex.TABLENAME:
                    var alias = r[1] != null ? new Identifier((string)r[1]) : null;
                    return new RelationReferenceExpression((Expression)r[0], alias);
                case ProdIndex.OPTIDENTIFIER:
                    return null;
                case ProdIndex.OPTIDENTIFIER2:
                    return r.First;
                case ProdIndex.TABLEREFERENCE:
                case ProdIndex.TABLEREFERENCE2:
                    return r.First;
                case ProdIndex.SUBQUERY_LPAREN_RPAREN:
                    return new SubqueryExpression((Statement)r[1]);
                case ProdIndex.EXPRESSIONORQUERY:
                    return r.First;
                case ProdIndex.EXPRESSIONORQUERY2:
                    return new SubqueryExpression((Statement)r.First);
                case ProdIndex.EXPRESSION_AND:
                    return CreateBinary(r, BinaryOperator.And);
                case ProdIndex.EXPRESSION_OR:
                    return CreateBinary(r, BinaryOperator.Or);
                case ProdIndex.EXPRESSION:
                    return r.First;
                case ProdIndex.CMPEXPR_GT:
                    return CreateBinary(r, BinaryOperator.GreaterThan);
                case ProdIndex.CMPEXPR_LT:
                    return CreateBinary(r, BinaryOperator.LessThan);
                case ProdIndex.CMPEXPR_LTEQ:
                    return CreateBinary(r, BinaryOperator.LessOrEqual);
                case ProdIndex.CMPEXPR_GTEQ:
                    return CreateBinary(r, BinaryOperator.GreaterOrEqual);
                case ProdIndex.CMPEXPR_EQ:
                    return CreateBinary(r, BinaryOperator.Equal);
                case ProdIndex.CMPEXPR_EXCLAMEQ:
                    return CreateBinary(r, BinaryOperator.Unequal);
                case ProdIndex.CMPEXPR:
                    return r.First;
                case ProdIndex.ADDEXP_PLUS:
                    return CreateBinary(r, BinaryOperator.Add);
                case ProdIndex.ADDEXP_MINUS:
                    return CreateBinary(r, BinaryOperator.Subtract);
                case ProdIndex.ADDEXP:
                    return r.First;
                case ProdIndex.MULTEXP_TIMES:
                    return CreateBinary(r, BinaryOperator.Mulitply);
                case ProdIndex.MULTEXP_DIV:
                    return CreateBinary(r, BinaryOperator.Divide);
                case ProdIndex.MULTEXP:
                    return r.First;
                case ProdIndex.NEGATEEXP_MINUS:
                    return new UnaryExpression((Expression)r[1], UnaryOperator.Negate);
                case ProdIndex.NEGATEEXP_NOT:
                    return new UnaryExpression((Expression)r[1], UnaryOperator.Not);
                case ProdIndex.NEGATEEXP:
                    return r.First;
                case ProdIndex.IDENTIFIER_ESCAPEDIDENTIFIER:
                    var escapedIdentifier = (string)r[0];
                    return escapedIdentifier.Substring(1, escapedIdentifier.Length - 2);
                case ProdIndex.IDENTIFIER_RAWIDENTIFIER:
                    return (string)r[0];
                case ProdIndex.QIDENT_DOT:
                    return ((Identifier)r.First).Append((string)r.Last);
                case ProdIndex.QIDENT:
                    return new Identifier((string)r.First);
                case ProdIndex.ISNULL_IS_NULL:
                    return new UnaryExpression((Expression)r[0], UnaryOperator.IsNull);
                case ProdIndex.NUMBER_INTEGER:
                    return new NumericLiteral<int>(int.Parse((string)r[0]));
                case ProdIndex.NUMBER_FLOAT:
                    return new NumericLiteral<double>(double.Parse((string)r[0]));
                case ProdIndex.VALUE:
                case ProdIndex.VALUE2:
                case ProdIndex.VALUE3:
                case ProdIndex.VALUE4:
                case ProdIndex.VALUE5:
                    return r.First;
                case ProdIndex.VALUE_LPAREN_RPAREN:
                    return r[1];
                case ProdIndex.VALUE_STRINGLITERAL:
                    var rawString = (string)r[0];
                    return new StringLiteral(SqlStringEscaping.Unescape(rawString.Substring(1, rawString.Length - 2)));
                case ProdIndex.BOOLEAN_TRUE:
                    return BooleanLiteral.True;
                case ProdIndex.BOOLEAN_FALSE:
                    return BooleanLiteral.False;
                case ProdIndex.VALUE_PARAMETER:
                    return new ParameterExpression(((string)r[0]).Substring(1));
                case ProdIndex.FUNCTION_LPAREN_RPAREN:
                    return new FunctionCallExpression(((Identifier)r[0]).ToString(), ToReadOnlyCollection<Expression>(r[2]));
                case ProdIndex.OPTARGUMENTLIST:
                    return null;
                case ProdIndex.OPTARGUMENTLIST2:
                    return r.First;
                case ProdIndex.ARGUMENTLIST_COMMA:
                    return ConstructList<Expression>(r);
                case ProdIndex.ARGUMENTLIST:
                    return InitializeList<Expression>(r);

                default:
                    throw new NotImplementedException();
            }
        }

        private BinaryExpression CreateBinary(Reduction r, BinaryOperator op)
        {
            return new BinaryExpression((Expression)r.First, op, (Expression)r.Last);
        }

        private List<T> InitializeList<T>(Reduction r)
        {
            if (r.First != null)
                return new List<T>()
                {
                    (T)r.First
                };
            else
                return new List<T>();
        }

        private List<T> ConstructList<T>(Reduction r)
        {
            var list = (List<T>)r.First;
            if (r.Last != null)
                list.Add((T)r.Last);
            return list;
        }

        public ReadOnlyCollection<T> ToReadOnlyCollection<T>(object value)
        {
            if (value == null)
                return null;

            return ((IEnumerable<T>)value).ToReadOnlyCollection();
        }

        private class FromClause
        {
            public RelationReferenceExpression relation;
            public ReadOnlyCollection<JoinModifier> joins;
            public Expression condition;
            public ReadOnlyCollection<OrderModifier> orderings;
            public LimitClause limit;
            public GroupingModifier grouping;
        }

        private class LimitClause
        {
            public int skip;
            public int take;

            /// <summary>
            /// Initializes a new instance of the <see cref="LimitClause"/> class.
            /// </summary>
            public LimitClause()
            {
                skip = 0;
                take = -1;
            }
        }
    }
}