﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    /// <summary>
    /// Provides metadata and exposes a factory method for creating an <see cref="ISqlFormatter"/>.
    /// </summary>
    public interface ISqlFormatterProvider
    {
        /// <summary>
        /// Gets the name of the SQL dialect that this formatter supports.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Returns a formatter that allows to convert a <see cref="MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree.StatementSequence" /> object into a specific SQL dialect.
        /// </summary>
        /// <returns>An instance that implements the <see cref="ISqlFormatter"/> interface.</returns>
        ISqlFormatter CreateFormatter();
    }
}
