﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql.Parser;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    /// <summary>
    /// Occures when a query contains a syntax error.
    /// </summary>
    [Serializable]
    public class QueryParseException : Exception
    {
        /// <summary>
        /// Gets the position where parse exception occured.
        /// </summary>
        public Position Position { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParseException" /> class.
        /// </summary>
        public QueryParseException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParseException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="position">The position in the query.</param>
        public QueryParseException(string message, Position position)
            : base(message)
        {
            Position = position;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParseException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="position">The position in the query.</param>
        /// <param name="inner">The inner exception.</param>
        public QueryParseException(string message, Position position, Exception inner)
            : base(message, inner)
        {
            Position = position;
        }

#if !Compact

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParseException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected QueryParseException(
          global::System.Runtime.Serialization.SerializationInfo info,
          global::System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

#endif
    }
}