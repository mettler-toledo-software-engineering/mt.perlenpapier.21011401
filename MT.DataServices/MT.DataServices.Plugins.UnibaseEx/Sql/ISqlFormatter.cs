﻿using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    /// <summary>
    /// Exposes a method that allows to convert a <see cref="StatementSequence"/> object into a specific SQL dialect.
    /// </summary>
    public interface ISqlFormatter
    {
        /// <summary>
        /// Formats the given statement sequence.
        /// </summary>
        /// <param name="sequence">The sequence to convert to SQL.</param>
        /// <returns>The SQL text that is semantically equal to the given <see cref="StatementSequence"/>.</returns>
        string FormatStatement(StatementSequence sequence);
    }
}
