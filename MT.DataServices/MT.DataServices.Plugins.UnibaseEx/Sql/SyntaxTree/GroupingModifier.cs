﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents an expression by which the rows are grouped by.
    /// </summary>
    public class GroupingModifier : Node
    {
        private readonly ReadOnlyCollection<Identifier> groupByFields;
        private readonly Expression havingCondition;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupingModifier"/> class.
        /// </summary>
        public GroupingModifier(ReadOnlyCollection<Identifier> groupByFields, Expression havingCondition)
        {
            this.groupByFields = groupByFields ?? EmptyReadOnlyCollection<Identifier>.Instance;
            this.havingCondition = havingCondition;
        }

        /// <summary>
        /// Gets list of fields that the result should be grouped by.
        /// </summary>
        public ReadOnlyCollection<Identifier> GroupByFields
        {
            get { return groupByFields; }
        }

        /// <summary>
        /// Gets the condition in the 'HAVING' clause.
        /// </summary>
        public Expression HavingCondition
        {
            get { return havingCondition; }
        }

        /// <inheritdocs />
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            return visitor.VisitGroupingModifier(this, arg);
        }
    }
}