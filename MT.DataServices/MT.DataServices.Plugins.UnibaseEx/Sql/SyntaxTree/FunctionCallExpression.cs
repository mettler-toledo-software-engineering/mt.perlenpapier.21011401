﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a call to a function.
    /// </summary>
    public class FunctionCallExpression : Expression
    {
        private readonly ReadOnlyCollection<Expression> arguments;
        private readonly string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="FunctionCallExpression"/> class.
        /// </summary>
        public FunctionCallExpression(string name, ReadOnlyCollection<Expression> arguments)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("The function name cannot be null or empty.");

            this.name = name;
            this.arguments = arguments ?? EmptyReadOnlyCollection<Expression>.Instance;
        }

        /// <summary>
        /// Gets the name of the callee.
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the list of arguments.
        /// </summary>
        public ReadOnlyCollection<Expression> Arguments
        {
            get { return arguments; }
        }

        /// <inheritdocs />
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            return visitor.VisitFunctionCallExpression(this, arg);
        }
    }
}