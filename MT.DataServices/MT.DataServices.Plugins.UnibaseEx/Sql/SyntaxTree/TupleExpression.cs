﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a tuple of expressions
    /// </summary>
    public sealed class TupleExpression : Expression
    {
        internal ReadOnlyCollection<Expression> elements;

        /// <summary>
        /// Initializes a new instance of the <see cref="TupleExpression"/> class.
        /// </summary>
        public TupleExpression(params Expression[] elements)
            : this(elements.ToReadOnlyCollection())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TupleExpression"/> class.
        /// </summary>
        public TupleExpression(IEnumerable<Expression> elements)
        {
            if (elements == null)
                throw new ArgumentNullException("elements");

            this.elements = elements.ToReadOnlyCollection(); 
        }

        /// <summary>
        /// Gets the elements of the tuple.
        /// </summary>
        public ReadOnlyCollection<Expression> Elements
        {
            get { return elements; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitTupleExpression(this, arg);
        }
    }
}