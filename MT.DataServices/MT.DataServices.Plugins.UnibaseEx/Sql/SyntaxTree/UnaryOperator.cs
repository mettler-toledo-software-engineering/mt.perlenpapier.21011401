﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Declares constants specifying different unary query operators
    /// </summary>
    public enum UnaryOperator
    {
        /// <summary>
        /// The logical inverse operator.
        /// </summary>
        Not,

        /// <summary>
        /// The numeric inverse operator.
        /// </summary>
        Negate,

        /// <summary>
        /// The is null operator.
        /// </summary>
        IsNull
    }
}