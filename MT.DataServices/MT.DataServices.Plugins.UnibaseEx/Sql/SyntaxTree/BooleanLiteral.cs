﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a boolean literal value.
    /// </summary>
    public class BooleanLiteral : Expression
    {
        private static readonly BooleanLiteral trueLiteral = new BooleanLiteral(true);
        private static readonly BooleanLiteral falseLiteral = new BooleanLiteral(false);        
        internal bool value;

        /// <summary>
        /// Gets the literal object for the value <c>true</c>.
        /// </summary>
        public static BooleanLiteral True
        {
            get { return trueLiteral; }
        }

        /// <summary>
        /// Gets the literal object for the value <c>false</c>.
        /// </summary>
        public static BooleanLiteral False
        {
            get { return falseLiteral; }
        }

        /// <summary>
        /// Returns the literal for the given value.
        /// </summary>
        /// <param name="value">The value that the literal should represent.</param>
        /// <returns>The literal object that represents the given boolean value.</returns>
        public static BooleanLiteral Get(bool value)
        {
            return value ? True : False;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="BooleanLiteral" /> class from being created.
        /// </summary>
        /// <param name="value">The value that this instance represents.</param>
        private BooleanLiteral(bool value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the value of this literal.
        /// </summary>
        public bool Value
        {
            get { return value; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitBooleanLiteral(this, arg);
        }
    }
}