﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents the reference to a relation
    /// </summary>
    public class RelationReferenceExpression : Expression
    {
        private readonly Expression relation;
        private readonly Identifier alias;

        /// <summary>
        /// Initializes a new instance of the <see cref="RelationReferenceExpression"/> class.
        /// </summary>
        public RelationReferenceExpression(Expression relation, Identifier alias)
        {
            if (relation == null)
                throw new ArgumentNullException("relation");

            this.relation = relation;
            this.alias = alias;
        }

        /// <summary>
        /// Gets the relation this reference refers to.
        /// </summary>
        public Expression Relation
        {
            get { return relation; }
        }

        /// <summary>
        /// Gets the alias for the referenced table.
        /// </summary>
        public Identifier Alias
        {
            get { return alias; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitRelationReferenceExpression(this, arg);
        }
    }
}