﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a select query without a FROM clause.
    /// </summary>
    public class SelectEvalStatement : Statement
    {
        private readonly ReadOnlyCollection<FieldExpression> fields;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectEvalStatement"/> class.
        /// </summary>
        public SelectEvalStatement(IEnumerable<FieldExpression> fields)
        {
            if (fields == null)
                throw new ArgumentNullException("fields");

            this.fields = fields.ToReadOnlyCollection();
        }

        /// <summary>
        /// Gets the fields of this query.
        /// </summary>
        public ReadOnlyCollection<FieldExpression> Fields
        {
            get { return fields; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitSelectEvalStatement(this, arg);
        }
    }
}