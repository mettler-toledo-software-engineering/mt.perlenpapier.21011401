﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a select statement
    /// </summary>
    public class SelectStatement : DataQueryStatement
    {
        private readonly ReadOnlyCollection<FieldExpression> fields;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectStatement"/> class.
        /// </summary>
        public SelectStatement(ReadOnlyCollection<FieldExpression> fields, RelationReferenceExpression relation, ReadOnlyCollection<JoinModifier> joins, Expression condition, ReadOnlyCollection<OrderModifier> orderings,
            GroupingModifier grouping, int skip, int take)
            : base(relation, joins, condition, orderings, grouping, skip, take)
        {
            if (fields == null)
                throw new ArgumentNullException("fields");

            this.fields = fields;
        }

        /// <summary>
        /// Gets the result fields of this query.
        /// </summary>
        public ReadOnlyCollection<FieldExpression> Fields
        {
            get { return fields; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitSelectStatement(this, arg);
        }
    }
}