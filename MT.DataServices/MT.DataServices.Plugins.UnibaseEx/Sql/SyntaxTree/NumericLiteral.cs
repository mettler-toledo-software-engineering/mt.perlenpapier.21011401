﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a numeric literal value in a query.
    /// </summary>
    /// <typeparam name="T">Type of the literal value.</typeparam>
    public class NumericLiteral<T> : NumericLiteral
        where T : struct
    {
        internal T value;

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericLiteral"/> class.
        /// </summary>
        public NumericLiteral(T value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the numeric value.
        /// </summary>
        public T Value
        {
            get { return value; }
        }

        /// <summary>
        /// Gets the type of the value.
        /// </summary>
        public override Type ValueType
        {
            get { return typeof(T); }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitNumericLiteral<T>(this, arg);
        }
    }

    /// <summary>
    /// Base class for all numeric literal values
    /// </summary>
    public abstract class NumericLiteral : Expression
    {
        /// <summary>
        /// Gets the type of the numeric value.
        /// </summary>
        public abstract Type ValueType { get; }
    }
}