﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Describes a join operation with another relation.
    /// </summary>
    public sealed class JoinModifier : Node
    {
        internal RelationReferenceExpression relation;
        internal Expression condition;
        internal JoinMode mode;

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinModifier" /> class.
        /// </summary>
        /// <param name="relation">The relation to execute the join operation with.</param>
        /// <param name="mode">One of the <see cref="JoinMode"/> values.</param>
        /// <param name="condition">The condition that must be sattisfied when joining two rows.</param>
        public JoinModifier(RelationReferenceExpression relation, JoinMode mode, Expression condition)
        {
            if (relation == null)
                throw new ArgumentNullException("relation");

            if (condition == null)
                throw new ArgumentNullException("condition");

            this.relation = relation;
            this.mode = mode;
            this.condition = condition;
        }

        /// <summary>
        /// Gets the relation involved in the query.
        /// </summary>
        public Expression Relation
        {
            get { return relation; }
        }

        /// <summary>
        /// Gets the condition that the joined row must suffice.
        /// </summary>
        public Expression Condition
        {
            get { return condition; }
        }

        /// <summary>
        /// Gets the behavior when joining a row fails.
        /// </summary>
        public JoinMode Mode
        {
            get { return mode; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitJoinModifier(this, arg);
        }
    }
}