﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Base class for a query visitor.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <typeparam name="TArgument">The type of the argument.</typeparam>
    public abstract class QueryVisitorBase<TResult, TArgument> : IQueryVisitor<TResult, TArgument>
    {
        /// <summary>
        /// Visits the specified node.
        /// </summary>
        /// <param name="node">The node to visit.</param>
        /// <param name="arg">The visitor argument.</param>
        /// <returns>The result of the visitor.</returns>
        protected virtual TResult Visit(Node node, TArgument arg) 
        {
            return node.Visit(this, arg);
        }

        /// <summary>
        /// Visits the specified node.
        /// </summary>
        /// <param name="node">The node to visit.</param>
        /// <returns>
        /// The result of the visitor.
        /// </returns>
        protected virtual TResult Visit(Node node)
        {
            return node.Visit(this, default(TArgument));
        }

        #region IQueryVisitor<TResult,TArgument> Members

        /// <inheritdoc />
        public virtual TResult VisitIdentifier(Identifier expr, TArgument arg)
        {
            throw new NotImplementedException();            
        }

        /// <inheritdoc />
        public virtual TResult VisitBinaryExpression(BinaryExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitUnaryExpression(UnaryExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitJoinModifier(JoinModifier expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitOrderModifier(OrderModifier expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitBooleanLiteral(BooleanLiteral expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitNumericLiteral<T>(NumericLiteral<T> expr, TArgument arg) where T : struct
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitStringLiteral(StringLiteral expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitFieldExpression(FieldExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitSetModifier(SetModifier expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitTupleExpression(TupleExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitStatementSequence(StatementSequence stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitSelectStatement(SelectStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitInsertStatement(InsertStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitUpdateStatement(UpdateStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitDeleteStatement(DeleteStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitRelationReferenceExpression(RelationReferenceExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitSelectEvalStatement(SelectEvalStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitSubqueryExpression(SubqueryExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitValuesListExpression(ValuesListExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitParameterExpression(ParameterExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitGroupingModifier(GroupingModifier expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitStarExpression(StarExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitFunctionCallExpression(FunctionCallExpression expr, TArgument arg)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public virtual TResult VisitRawStatement(ExecRawStatement stmt, TArgument arg)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}