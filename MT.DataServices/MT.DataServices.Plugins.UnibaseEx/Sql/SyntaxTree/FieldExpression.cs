﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a named expression in the result set of a query
    /// </summary>
    public class FieldExpression : Expression
    {
        private readonly Identifier name;
        private readonly Expression expression;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldExpression"/> class.
        /// </summary>
        public FieldExpression(Identifier name, Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            this.name = name;
            this.expression = expression;
        }

        /// <summary>
        /// Gets the name of the field.
        /// </summary>
        public Identifier Name
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        public Expression Expression
        {
            get { return expression; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitFieldExpression(this, arg);
        }
    }
}