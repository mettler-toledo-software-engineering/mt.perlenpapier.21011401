﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Base class for all statements that can occur in a query.
    /// </summary>
    public abstract class Statement : Node
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Statement"/> class.
        /// </summary>
        public Statement()
        {
        }
    }
}