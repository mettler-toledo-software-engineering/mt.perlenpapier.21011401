﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateStatement : DataQueryStatement
    {
        private readonly ReadOnlyCollection<SetModifier> setters;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectStatement"/> class.
        /// </summary>
        public UpdateStatement(ReadOnlyCollection<SetModifier> setters, RelationReferenceExpression relation, ReadOnlyCollection<JoinModifier> joins, Expression condition, ReadOnlyCollection<OrderModifier> orderings,
            int skip, int take)
            : base(relation, joins, condition, orderings, null, skip, take)
        {
            if (setters == null)
                throw new ArgumentNullException("setters");

            this.setters = setters;
        }

        /// <summary>
        /// Gets the result fields of this query.
        /// </summary>
        public ReadOnlyCollection<SetModifier> Setters
        {
            get { return setters; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitUpdateStatement(this, arg);
        }
    }
}