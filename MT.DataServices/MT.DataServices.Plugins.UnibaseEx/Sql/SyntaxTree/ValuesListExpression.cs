﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents static values in a INSERT statement
    /// </summary>
    public class ValuesListExpression : Expression
    {
        private readonly ReadOnlyCollection<TupleExpression> tuples;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValuesListExpression"/> class.
        /// </summary>
        public ValuesListExpression(ReadOnlyCollection<TupleExpression> tuples)
        {
            if (tuples == null)
                throw new ArgumentNullException("tuples");

            this.tuples = tuples;
        }

        /// <summary>
        /// Gets the list of tuples.
        /// </summary>
        public ReadOnlyCollection<TupleExpression> Tuples
        {
            get { return tuples; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitValuesListExpression(this, arg);
        }
    }
}