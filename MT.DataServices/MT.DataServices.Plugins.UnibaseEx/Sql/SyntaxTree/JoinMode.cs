﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Declares constants specifying different behaviors when joining two relations.
    /// </summary>
    public enum JoinMode
    {
        /// <summary>
        /// Only rows for which the join condition holds are returned.
        /// </summary>
        Inner,

        /// <summary>
        /// Rows for which the join condition holds are returned and all other rows from the left table.
        /// </summary>
        Left,

        /// <summary>
        /// Rows for which the join condition holds are returned and all other rows from the right table.
        /// </summary>
        Right,

        /// <summary>
        /// Rows for which the join condition holds are returned and all other rows from both tables.
        /// </summary>
        Full
    }
}