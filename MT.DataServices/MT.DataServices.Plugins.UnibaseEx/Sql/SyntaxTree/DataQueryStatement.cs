﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Base class for all statements that operate on data and not on the schema.
    /// </summary>
    public abstract class DataQueryStatement : Statement
    {
        private static ReadOnlyCollection<JoinModifier> emptyJoins;
        private static ReadOnlyCollection<OrderModifier> emptyOrderings;

        internal RelationReferenceExpression relation;
        internal int skip;
        internal int take;
        internal ReadOnlyCollection<JoinModifier> joins;
        internal ReadOnlyCollection<OrderModifier> orderings;
        internal GroupingModifier grouping;
        internal Expression condition;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataQueryStatement"/> class.
        /// </summary>
        public DataQueryStatement(RelationReferenceExpression relation, ReadOnlyCollection<JoinModifier> joins, Expression condition, ReadOnlyCollection<OrderModifier> orderings,
            GroupingModifier grouping, int skip, int take)
        {
            if (relation == null)
                throw new ArgumentNullException("relation");

            this.relation = relation;
            this.joins = joins ?? (emptyJoins = (emptyJoins ?? new ReadOnlyCollection<JoinModifier>(new JoinModifier[]
            {
            })));
            this.orderings = orderings ?? (emptyOrderings = (emptyOrderings ?? new ReadOnlyCollection<OrderModifier>(new OrderModifier[]
            {
            })));
            this.condition = condition;
            this.grouping = grouping;
            this.skip = skip;
            this.take = take;
        }

        /// <summary>
        /// Gets the relation.
        /// </summary>
        public RelationReferenceExpression Relation
        {
            get { return relation; }
        }

        /// <summary>
        /// Gets or sets the number of rows to skip at the beginning of the result set.
        /// </summary>
        public int Skip
        {
            get { return skip; }
        }

        /// <summary>
        /// Gets or sets the number of rows to take into the result set.
        /// </summary>
        public int Take
        {
            get { return take; }
        }

        /// <summary>
        /// Gets the join operations of the query.
        /// </summary>
        public ReadOnlyCollection<JoinModifier> Joins
        {
            get { return joins; }
        }

        /// <summary>
        /// Gets the ordering operations of the query.
        /// </summary>
        public ReadOnlyCollection<OrderModifier> Orderings
        {
            get { return orderings; }
        }

        /// <summary>
        /// Gets the condition the tuples in the resultset must suffice to.
        /// </summary>
        public Expression Condition
        {
            get { return condition; }
        }

        /// <summary>
        /// Gets the grouping information.
        /// </summary>
        public GroupingModifier Grouping
        {
            get { return grouping; }
        }
    }
}