﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a SQL statement that is directly sent to the database backend without preprocessing.
    /// </summary>
    public class ExecRawStatement : Statement
    {
        private readonly string statement;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExecRawStatement"/> class.
        /// </summary>
        public ExecRawStatement(string statement)
        {
            this.statement = statement;
        }

        /// <summary>
        /// Gets the raw SQL statement.
        /// </summary>
        public string Statement
        {
            get { return statement; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            return visitor.VisitRawStatement(this, arg);    
        }
    }
}
