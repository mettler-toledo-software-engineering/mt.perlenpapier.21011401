﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents the * in queries (e.g. in 'SELECT * FROM Table' or 'SELECT COUNT(*) FROM Table')
    /// </summary>
    public class StarExpression : Expression
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StarExpression"/> class.
        /// </summary>
        public StarExpression()            
        {
        }

        /// <inheritdoc />
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            return visitor.VisitStarExpression(this, arg);              
        }
    }
}