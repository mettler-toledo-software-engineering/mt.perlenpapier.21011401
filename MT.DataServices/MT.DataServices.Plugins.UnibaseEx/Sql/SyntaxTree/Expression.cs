﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Base class for all expressions that an occur in a query.
    /// </summary>
    public abstract class Expression : Node
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Expression"/> class.
        /// </summary>
        public Expression()
        {
        }
    }
}