﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Exposes methods for every type of query node.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <typeparam name="TArgument">The type of the argument.</typeparam>
    public interface IQueryVisitor<TResult, TArgument>
    {
        /// <exclude />
        TResult VisitIdentifier(Identifier expr, TArgument arg);

        /// <exclude />
        TResult VisitBinaryExpression(BinaryExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitUnaryExpression(UnaryExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitJoinModifier(JoinModifier expr, TArgument arg);

        /// <exclude />
        TResult VisitOrderModifier(OrderModifier expr, TArgument arg);

        /// <exclude />
        TResult VisitGroupingModifier(GroupingModifier expr, TArgument arg);

        /// <exclude />
        TResult VisitBooleanLiteral(BooleanLiteral expr, TArgument arg);

        /// <exclude />
        TResult VisitNumericLiteral<T>(NumericLiteral<T> expr, TArgument arg)
            where T : struct;

        /// <exclude />
        TResult VisitStarExpression(StarExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitStringLiteral(StringLiteral expr, TArgument arg);

        /// <exclude />
        TResult VisitFieldExpression(FieldExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitSetModifier(SetModifier expr, TArgument arg);

        /// <exclude />
        TResult VisitTupleExpression(TupleExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitRelationReferenceExpression(RelationReferenceExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitSubqueryExpression(SubqueryExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitValuesListExpression(ValuesListExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitParameterExpression(ParameterExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitFunctionCallExpression(FunctionCallExpression expr, TArgument arg);

        /// <exclude />
        TResult VisitStatementSequence(StatementSequence stmt, TArgument arg);

        /// <exclude />
        TResult VisitSelectStatement(SelectStatement stmt, TArgument arg);

        /// <exclude />
        TResult VisitSelectEvalStatement(SelectEvalStatement stmt, TArgument arg);

        /// <exclude />
        TResult VisitInsertStatement(InsertStatement stmt, TArgument arg);

        /// <exclude />
        TResult VisitUpdateStatement(UpdateStatement stmt, TArgument arg);

        /// <exclude />
        TResult VisitDeleteStatement(DeleteStatement stmt, TArgument arg);

        /// <exclude />
        TResult VisitRawStatement(ExecRawStatement stmt, TArgument arg);        
    }
}