﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a binary operation in a query.
    /// </summary>
    public sealed class BinaryExpression : Expression
    {
        internal BinaryOperator op;
        internal Expression left;
        internal Expression right;

        /// <summary>
        /// Initializes a new instance of the <see cref="BinaryExpression" /> class.
        /// </summary>
        /// <param name="left">The left expression.</param>
        /// <param name="op">The operator.</param>
        /// <param name="right">The right expression.</param>
        public BinaryExpression(Expression left, BinaryOperator op, Expression right)
        {
            if (left == null)
                throw new ArgumentNullException("left");

            if (right == null)
                throw new ArgumentNullException("right");

            this.left = left;
            this.op = op;
            this.right = right;
        }

        /// <summary>
        /// Gets the operator.
        /// </summary>
        public BinaryOperator Operator
        {
            get { return op; }
        }

        /// <summary>
        /// Gets the left expression.
        /// </summary>
        public Expression Left
        {
            get { return left; }
        }

        /// <summary>
        /// Gets the right expression.
        /// </summary>
        public Expression Right
        {
            get { return right; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitBinaryExpression(this, arg);
        }
    }
}