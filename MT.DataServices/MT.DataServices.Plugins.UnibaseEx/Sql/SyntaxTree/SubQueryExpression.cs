﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents an expression that consists of a subquery.
    /// </summary>
    public class SubqueryExpression : Expression
    {
        private Statement subquery;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubqueryExpression"/> class.
        /// </summary>
        public SubqueryExpression(Statement subquery)
        {
            if (subquery == null)
                throw new ArgumentNullException("subquery");

            this.subquery = subquery;
        }

        /// <summary>
        /// Gets the subquery.
        /// </summary>
        public Statement Subquery
        {
            get { return subquery; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitSubqueryExpression(this, arg);
        }
    }
}