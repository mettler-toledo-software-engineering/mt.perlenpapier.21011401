﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents an identifier in a query.
    /// </summary>
    public sealed class Identifier : Expression, IEnumerable<string>
    {
        private readonly string[] qualifier;
        private readonly string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="Identifier"/> class.
        /// </summary>
        public Identifier(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            this.name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Identifier"/> class.
        /// </summary>
        public Identifier(params string[] qualifiedName)
        {
            if (qualifiedName.Length == 0)
                throw new ArgumentException("The name must consist of at least one element.");

            if (qualifiedName.Length == 1)
                name = qualifiedName[0];
            else
                qualifier = qualifiedName;
        }

        /// <summary>
        /// Gets number of elements in the qualifier.
        /// </summary>
        public int Count
        {
            get { return name != null ? 1 : qualifier.Length; }
        }

        /// <summary>
        /// Gets the qualifier at the specified index.
        /// </summary>
        /// <param name="index">The index of the qualifier element.</param>
        /// <returns>The qualifier at the specified index.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">The index is out of range.</exception>
        public string this[int index]
        {
            get
            {
                if (name != null)
                {
                    if (index != 0)
                        throw new ArgumentOutOfRangeException("index");

                    return name;
                }
                else
                    return qualifier[index];
            }
        }

        /// <summary>
        /// Creates an array from the identifier elements.
        /// </summary>
        /// <returns>An array consisting of the qualifiers of this identifier.</returns>
        public string[] ToArray()
        {
            if (name != null)
                return new string[]
                {
                    name
                };
            else
                return (string[])qualifier.Clone();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            if (qualifier != null)
            {
                for (int i = 0; i < qualifier.Length; i++)
                {
                    if (i > 0)
                        s.Append('.');
                    s.Append('[').Append(qualifier[i]).Append(']');
                }
            }
            else
                s.Append('[').Append(name).Append(']');
            return s.ToString();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerator<string> GetEnumerator()
        {
            if (name != null)
                return Enumerable.Repeat(name, 1).GetEnumerator();
            else
                return (IEnumerator<string>)qualifier.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitIdentifier(this, arg);
        }

        /// <summary>
        /// Creates a new identifier by appending the given element.
        /// </summary>
        /// <param name="element">The element to append.</param>
        /// <returns>The newly created identifier</returns>
        public Identifier Append(string element)
        {
            if (name != null)
                return new Identifier(name, element);
            else
            {
                string[] list = new string[qualifier.Length + 1];
                Array.Copy(qualifier, list, qualifier.Length);
                list[list.Length - 1] = element;
                return new Identifier(element);
            }
        }
    }
}