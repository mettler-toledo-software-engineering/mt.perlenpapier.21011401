﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a parameter in a query.
    /// </summary>
    public class ParameterExpression : Expression
    {
        private readonly string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterExpression"/> class.
        /// </summary>
        public ParameterExpression(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            this.name = name;
        }

        /// <summary>
        /// Gets the parameter name.
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitParameterExpression(this, arg);
        }
    }
}