﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents an unary expression
    /// </summary>
    public sealed class UnaryExpression : Expression
    {
        internal Expression expression;
        internal UnaryOperator op;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnaryExpression"/> class.
        /// </summary>
        public UnaryExpression(Expression expression, UnaryOperator op)
        {
            this.op = op;
            this.expression = expression;
        }

        /// <summary>
        /// Gets the operator.
        /// </summary>
        public UnaryOperator Operator
        {
            get { return op; }
        }

        /// <summary>
        /// Gets the inner expression.
        /// </summary>
        public Expression Expression
        {
            get { return expression; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitUnaryExpression(this, arg);
        }
    }
}