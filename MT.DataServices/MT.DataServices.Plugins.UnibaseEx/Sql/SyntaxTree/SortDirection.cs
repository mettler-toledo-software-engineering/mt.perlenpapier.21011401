﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Declares constants specifying different sorting directions
    /// </summary>
    public enum SortDirection
    {
        /// <summary>
        /// The values are sorted from lowest to highest.
        /// </summary>
        Ascending,

        /// <summary>
        /// The values are sorted from highest to lowest.
        /// </summary>
        Descending
    }
}