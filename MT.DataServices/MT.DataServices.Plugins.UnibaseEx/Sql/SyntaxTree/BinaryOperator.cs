﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Declares constants specifying different types of binary operators in queries
    /// </summary>
    public enum BinaryOperator
    {
        /// <summary>
        /// The add operator
        /// </summary>
        Add,

        /// <summary>
        /// The subtraction operator
        /// </summary>
        Subtract,

        /// <summary>
        /// The multiplication operator
        /// </summary>
        Mulitply,

        /// <summary>
        /// The division operator
        /// </summary>
        Divide,

        /// <summary>
        /// The equality operator
        /// </summary>
        Equal,

        /// <summary>
        /// The inequality operator
        /// </summary>
        Unequal,

        /// <summary>
        /// The greather than operator
        /// </summary>
        GreaterThan,

        /// <summary>
        /// The less than operator
        /// </summary>
        LessThan,

        /// <summary>
        /// The greather or equal operator
        /// </summary>
        GreaterOrEqual,

        /// <summary>
        /// The less or equal operator
        /// </summary>
        LessOrEqual,

        /// <summary>
        /// The logical and operator
        /// </summary>
        And,

        /// <summary>
        /// The logical or operator
        /// </summary>
        Or
    }
}