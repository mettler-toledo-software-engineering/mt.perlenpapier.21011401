﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents an insert statement
    /// </summary>
    public class InsertStatement : Statement
    {
        internal RelationReferenceExpression relation;
        internal TupleExpression fields;
        internal Expression values;

        /// <summary>
        /// Initializes a new instance of the <see cref="InsertStatement"/> class.
        /// </summary>
        public InsertStatement(RelationReferenceExpression relation, TupleExpression fields, Expression values)
        {
            if (relation == null)
                throw new ArgumentNullException("relation");

            if (values == null)
                throw new ArgumentNullException("values");

            this.relation = relation;
            this.fields = fields;
            this.values = values;
        }

        /// <summary>
        /// Gets the relation to insert the values into.
        /// </summary>
        public RelationReferenceExpression Relation
        {
            get { return relation; }
        }

        /// <summary>
        /// Gets the fields to insert the values into.
        /// </summary>
        public TupleExpression Fields
        {
            get { return fields; }
        }

        /// <summary>
        /// Gets the values to insert.
        /// </summary>
        public Expression Values
        {
            get { return values; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitInsertStatement(this, arg);
        }
    }
}