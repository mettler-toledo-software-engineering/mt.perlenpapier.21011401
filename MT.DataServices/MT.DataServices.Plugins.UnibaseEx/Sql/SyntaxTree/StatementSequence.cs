﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a sequence of statements
    /// </summary>
    public sealed class StatementSequence : Statement
    {
        internal ReadOnlyCollection<Statement> statements;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatementSequence"/> class.
        /// </summary>
        public StatementSequence(params Statement[] statements)
            : this(statements.ToReadOnlyCollection())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatementSequence"/> class.
        /// </summary>
        public StatementSequence(IEnumerable<Statement> statements)
        {
            if (statements == null)
                throw new ArgumentNullException("statements");

            this.statements = statements.ToReadOnlyCollection();
        }

        /// <summary>
        /// Gets the statements in this sequence.
        /// </summary>
        public ReadOnlyCollection<Statement> Statements
        {
            get { return statements; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitStatementSequence(this, arg);
        }
    }
}