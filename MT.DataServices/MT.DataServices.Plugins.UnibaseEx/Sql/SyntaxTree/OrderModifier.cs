﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Describes an ordering on the result set.
    /// </summary>
    public sealed class OrderModifier : Node
    {
        internal Expression expression;
        internal SortDirection direction;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderModifier"/> class.
        /// </summary>
        public OrderModifier(Expression expression, SortDirection direction)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            this.expression = expression;
            this.direction = direction;
        }

        /// <summary>
        /// Gets the expression to sort by.
        /// </summary>
        public Expression Expression
        {
            get { return expression; }
        }

        /// <summary>
        /// Gets the sorting direction.
        /// </summary>
        public SortDirection Direction
        {
            get { return direction; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitOrderModifier(this, arg);
        }
    }
}