﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree
{
    /// <summary>
    /// Represents a SET clause in an update statement
    /// </summary>
    public sealed class SetModifier : Node
    {
        internal Expression value;
        internal Expression target;

        /// <summary>
        /// Initializes a new instance of the <see cref="SetModifier"/> class.
        /// </summary>
        public SetModifier(Expression target, Expression value)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            if (value == null)
                throw new ArgumentNullException("value");

            this.target = target;
            this.value = value;
        }

        /// <summary>
        /// Gets the value to be assigned.
        /// </summary>
        public Expression Value
        {
            get { return value; }
        }

        /// <summary>
        /// Gets the target of the assignment.
        /// </summary>
        public Expression Target
        {
            get { return target; }
        }

        /// <summary>
        /// Calls the method on the visitor for this expression type.
        /// </summary>
        /// <typeparam name="TResult">The type of the visitors result.</typeparam>
        /// <typeparam name="TArgument">The type of the visitors argument.</typeparam>
        /// <param name="visitor">The visitor.</param>
        /// <param name="arg">The visitors argument.</param>
        /// <returns>
        /// The visitors result.
        /// </returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="visitor"/> is null.</exception>
        public override TResult Visit<TResult, TArgument>(IQueryVisitor<TResult, TArgument> visitor, TArgument arg)
        {
            if (visitor == null)
                throw new ArgumentNullException("visitor");

            return visitor.VisitSetModifier(this, arg);
        }
    }
}