﻿using System;
using System.Linq;
using System.Text;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Sql
{
    internal class QueryWriter : QueryVisitorBase<Unit, StringBuilder>
    {
        private static readonly QueryWriter writer = new QueryWriter();

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryWriter"/> class.
        /// </summary>
        public QueryWriter()
        {
        }
        
        internal static string QueryToString(Node node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            StringBuilder sb = new StringBuilder();
            writer.Visit(node, sb);
            return sb.ToString();
        }

        public override Unit VisitBinaryExpression(BinaryExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            Visit(expr.Left, arg);
            arg.Append(' ');
            arg.Append(GetBinaryOperator(expr.Operator));
            arg.Append(' ');
            Visit(expr.Right, arg);
            arg.Append(')');
            return null;
        }

        public override Unit VisitBooleanLiteral(BooleanLiteral expr, StringBuilder arg)
        {
            if (expr.Value)
                arg.Append("TRUE");
            else
                arg.Append("FALSE");

            return null;
        }

        public override Unit VisitDeleteStatement(DeleteStatement stmt, StringBuilder arg)
        {
            arg.Append("DELETE FROM ");
            FormatDataQueryStatement(stmt, arg);
            return null;
        }

        public override Unit VisitFieldExpression(FieldExpression expr, StringBuilder arg)
        {
            Visit(expr.Expression, arg);
            if (expr.Name != null)
            {
                arg.Append(" AS ");
                arg.Append(expr.Name);
            }
            return null;
        }

        public override Unit VisitIdentifier(Identifier expr, StringBuilder arg)
        {
            arg.Append(expr.ToString());
            return null;
        }

        public override Unit VisitInsertStatement(InsertStatement stmt, StringBuilder arg)
        {
            arg.Append("INSERT INTO ");
            Visit(stmt.Relation, arg);

            if (stmt.Fields != null)
            {
                arg.Append(" ");
                Visit(stmt.Fields, arg);
            }

            if (stmt.Values is TupleExpression)
                arg.Append(" VALUES ");

            Visit(stmt.Values, arg);
            return null;
        }

        public override Unit VisitJoinModifier(JoinModifier expr, StringBuilder arg)
        {
            switch (expr.Mode)
            {
                case JoinMode.Inner:
                    arg.Append("INNER JOIN ");
                    break;
                case JoinMode.Left:
                    arg.Append("LEFT JOIN ");
                    break;
                case JoinMode.Right:
                    arg.Append("RIGHT JOIN ");
                    break;
                case JoinMode.Full:
                    arg.Append("FULL JOIN ");
                    break;
                default:
                    throw new ArgumentOutOfRangeException("expr");
            }

            Visit(expr.Relation, arg);            
            arg.Append(" ON ");
            Visit(expr.Condition, arg);

            return null;
        }

        public override Unit VisitNumericLiteral<T>(NumericLiteral<T> expr, StringBuilder arg)
        {
            arg.Append(Convert.ToString((object)expr.Value));
            return null;
        }

        public override Unit VisitOrderModifier(OrderModifier expr, StringBuilder arg)
        {
            Visit(expr.Expression, arg);

            if (expr.Direction == SortDirection.Ascending)
                arg.Append(" ASC");
            else
                arg.Append(" DESC");

            return null;
        }

        public override Unit VisitSelectStatement(SelectStatement stmt, StringBuilder arg)
        {
            arg.Append("SELECT ");

            for (int i = 0; i < stmt.Fields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(stmt.Fields[i], arg);
            }

            arg.Append(" FROM ");
            FormatDataQueryStatement(stmt, arg);
            return null;
        }

        public override Unit VisitSetModifier(SetModifier expr, StringBuilder arg)
        {
            Visit(expr.Target, arg);
            arg.Append(" = ");
            Visit(expr.Value, arg);
            return null;
        }

        public override Unit VisitStatementSequence(StatementSequence stmt, StringBuilder arg)
        {
            foreach (var singleStatement in stmt.Statements)
            {
                Visit(singleStatement, arg);
                arg.Append("; ");
            }
            return null;
        }

        public override Unit VisitStringLiteral(StringLiteral expr, StringBuilder arg)
        {
            arg.Append('"').Append(SqlStringEscaping.Escape(expr.Value)).Append('"');
            return null;
        }

        public override Unit VisitTupleExpression(TupleExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            for (int i = 0; i < expr.Elements.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Elements[i], arg);
            }
            arg.Append(')');
            return null;
        }

        public override Unit VisitValuesListExpression(ValuesListExpression expr, StringBuilder arg)
        {
            arg.Append(" VALUES ");
            for (int i = 0; i < expr.Tuples.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Tuples[i], arg);
            }
            return null;
        }

        public override Unit VisitUnaryExpression(UnaryExpression expr, StringBuilder arg)
        {
            switch (expr.Operator)
            {
                case UnaryOperator.Not:
                    arg.Append("NOT ");
                    Visit(expr.Expression, arg);
                    return null;
                case UnaryOperator.Negate:
                    arg.Append("-");
                    Visit(expr.Expression, arg);
                    return null;
                case UnaryOperator.IsNull:
                    Visit(expr.Expression, arg);
                    arg.Append(" IS NULL");
                    return null;
                default:
                    throw new ArgumentOutOfRangeException("expr");
            }
        }

        public override Unit VisitUpdateStatement(UpdateStatement stmt, StringBuilder arg)
        {
            arg.Append("UPDATE ");
            
            Visit(stmt.Relation, arg);

            foreach (var join in stmt.Joins)
            {
                Visit(join, arg);
            }

            arg.Append(" SET ");

            for (int i = 0; i < stmt.Setters.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");
                Visit(stmt.Setters[i], arg);
            }

            if (stmt.Condition != null)
            {
                arg.Append(" WHERE ");
                Visit(stmt.Condition, arg);
            }

            return null;
        }

        public override Unit VisitRelationReferenceExpression(RelationReferenceExpression expr, StringBuilder arg)
        {
            Visit(expr.Relation, arg);
            if (expr.Alias != null)
            {
                arg.Append(' ');
                Visit(expr.Alias, arg);
            }
            return null;
        }

        public override Unit VisitSelectEvalStatement(SelectEvalStatement stmt, StringBuilder arg)
        {
            arg.Append("SELECT ");

            for (int i = 0; i < stmt.Fields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(stmt.Fields[i], arg);
            }
            return null;
        }

        public override Unit VisitSubqueryExpression(SubqueryExpression expr, StringBuilder arg)
        {
            arg.Append('(');
            Visit(expr.Subquery);
            arg.Append(')');
            return null;
        }

        private void FormatDataQueryStatement(DataQueryStatement stmt, StringBuilder arg)
        {
            Visit(stmt.Relation, arg);

            foreach (var join in stmt.Joins)
            {
                Visit(join, arg);
            }

            if (stmt.Condition != null)
            {
                arg.Append(" WHERE ");
                Visit(stmt.Condition, arg);
            }

            if (stmt.Orderings.Count > 0)
            {
                arg.Append(" ORDER BY ");
                for (int i = 0; i < stmt.Orderings.Count; i++)
                {
                    if (i > 0)
                        arg.Append(", ");

                    Visit(stmt.Orderings[i], arg);
                }
            }

            if (stmt.Grouping != null)
                Visit(stmt.Grouping, arg);

            if (stmt.Skip != 0 || stmt.Take != -1)
            {
                arg.Append(" LIMIT ");
                if (stmt.Skip != 0)
                    arg.Append(stmt.Skip);

                if (stmt.Skip != 0 && stmt.Take != -1)
                    arg.Append(", ");

                if (stmt.Take != -1)
                    arg.Append(stmt.Take);
            }
        }

        public override Unit VisitStarExpression(StarExpression expr, StringBuilder arg)
        {
            arg.Append('*');
            return Unit.Identity;
        }

        public override Unit VisitGroupingModifier(GroupingModifier expr, StringBuilder arg)
        {
            arg.Append(" GROUP BY ");
            for (int i = 0; i < expr.GroupByFields.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.GroupByFields[i], arg);
            }

            if (expr.HavingCondition != null)
            {
                arg.Append(" HAVING ");
                Visit(expr.HavingCondition, arg);
            }

            return Unit.Identity;
        }

        public override Unit VisitFunctionCallExpression(FunctionCallExpression expr, StringBuilder arg)
        {
            arg.Append(expr.Name).Append('(');

            for (int i = 0; i < expr.Arguments.Count; i++)
            {
                if (i > 0)
                    arg.Append(", ");

                Visit(expr.Arguments[i], arg);
            }

            arg.Append(')');
            return Unit.Identity;
        }

        public override Unit VisitParameterExpression(ParameterExpression expr, StringBuilder arg)
        {
            arg.Append('@');
            arg.Append(expr.Name);
            return null;
        }

        public override Unit VisitRawStatement(ExecRawStatement stmt, StringBuilder arg)
        {
            arg.Append("EXEC RAW \"");
            arg.Append(SqlStringEscaping.Escape(stmt.Statement));
            arg.Append("\"");
            return null;
        }

        private string GetBinaryOperator(BinaryOperator op)
        {
            switch (op)
            {
                case BinaryOperator.Add:
                    return "+";
                case BinaryOperator.Subtract:
                    return "-";
                case BinaryOperator.Mulitply:
                    return "*";
                case BinaryOperator.Divide:
                    return "/";
                case BinaryOperator.Equal:
                    return "=";
                case BinaryOperator.Unequal:
                    return "!=";
                case BinaryOperator.GreaterThan:
                    return ">";
                case BinaryOperator.LessThan:
                    return "<";
                case BinaryOperator.GreaterOrEqual:
                    return ">=";
                case BinaryOperator.LessOrEqual:
                    return "<=";
                case BinaryOperator.And:
                    return "AND";
                case BinaryOperator.Or:
                    return "OR";
                default:
                    throw new ArgumentOutOfRangeException("op");
            }
        }
    }
}