﻿using System;
using System.Data;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal class RemoteDataTable : RemoteTable
    {
        private readonly DataTable table;
        private int current;

        internal RemoteDataTable(int handle, DataTable table)
            : base(handle)
        {
            this.table = table;
        }

        internal override void MoveFirst()
        {
            current = 0;
        }

        internal override void MovePrevious()
        {
            if (current > 0)
                current--;
        }

        internal override void MoveNext()
        {
            if (current < RowCount - 1)
                current++;
        }

        internal override void MoveLast()
        {
            current = RowCount - 1;
        }

        internal override void Goto(int row)
        {
            if (row < 0 || row >= RowCount)
                throw new UnibaseExException(UnibaseExError.IllegalRow, "Illegal row index.");

            current = row;
        }

        internal override object[] GetValues()
        {
            if (current < 0 || current >= RowCount)
                throw new UnibaseExException(UnibaseExError.IllegalRow, "Illegal row index.");

            return table.Rows[current].ItemArray;
        }

        internal override int RowCount
        {
            get { return table.Rows.Count; }
        }

        internal override int ColumnCount
        {
            get { return table.Columns.Count; }
        }

        internal override string[] GetColumnNames()
        {
            return table.Columns.Cast<DataColumn>().Select(d => d.ColumnName).ToArray();
        }
    }
}
