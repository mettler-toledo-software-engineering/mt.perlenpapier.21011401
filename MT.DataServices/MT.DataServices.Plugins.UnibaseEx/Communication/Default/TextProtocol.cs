﻿using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Sql;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Implements the text based UnibaseEx protocol.
    /// </summary>
    internal class TextProtocol : IProtocolImplementation
    {
        private readonly ILogSink log;
        private readonly RemoteTable[] remoteTables = new RemoteTable[10];
        private readonly Encoding defaultEncoding = Encoding.GetEncoding(850);
        private readonly Stopwatch queryTimer = new Stopwatch();

        private readonly DefaultEndPointClientContextBase context;

        private readonly object handlerLock = new object();
        private readonly bool debugMode;

        private bool isOpen;

        private IBackendConnectionContext backendConnectionContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextProtocol"/> class.
        /// </summary>
        public TextProtocol(DefaultEndPointClientContextBase context)
        {
            this.context = context;
            log = this.context.DefaultEndPoint.EndPointHostingService.Log;
            debugMode = context.DefaultEndPoint.EndPointHostingService.RuntimeConfiguration.DebugMode;
        }

        public int HandleMessage(ArraySegment<byte> message)
        {
            int bytesProcessed = 0;
            int prevEndIndex = message.Offset;
            do
            {
                int startIndex = Array.IndexOf<byte>(message.Array, (byte)'#', prevEndIndex, message.Count);
                if (startIndex == -1)
                    return bytesProcessed;

                int endIndex = Array.IndexOf<byte>(message.Array, 12, startIndex, message.Offset + message.Count - startIndex);
                if (endIndex == -1)
                    return bytesProcessed;

                bytesProcessed = endIndex + 1 - message.Offset;
                HandleUnibaseExCommand(startIndex, endIndex, message);

                if (bytesProcessed == message.Count)
                    return bytesProcessed;

                prevEndIndex = endIndex;
            }
            while (true);
        }

        private void HandleUnibaseExCommand(int startIndex, int endIndex, ArraySegment<byte> message)
        {
            if (endIndex - startIndex < 2)
                log.Warning("Received invalid message (less than 3 chars).");
            else
            {
                Task.Factory.StartNew(() =>
                {
                    // The next request can overtake an previous one (primarily because of the "Flush" when sending the result
                    // that causes a thread switch). Since the rest of the command handling code is not thread-safe,
                    // we must lock until the first operation has completed.

                    lock (handlerLock)
                    {
                        try
                        {
                            if (debugMode)
                                log.Action("[RECV] " + defaultEncoding.GetString(message.Array, startIndex, endIndex - startIndex).Replace("\b", "\\b").Replace("\n", "\\n").Replace("\r", "\\r").Replace("\f", "\\f").Replace("\v", "\\v"));

                            var command = (char)message.Array[startIndex + 1];
                            var argument = defaultEncoding.GetString(message.Array, startIndex + 2, endIndex - startIndex - 2);
                            ExecuteCommand(command, argument);
                        }
                        catch (UnibaseExException ex)
                        {
                            log.Error("An exception occured during the execution of a command.", ex);
                            SendResponse("E" + ((int)ex.UnibaseError).ToString());
                        }
                        catch (Exception ex)
                        {
                            log.Fatal("An unexpected exception occured.", ex);
                            SendResponse("E" + ((int)UnibaseExError.QueryError).ToString());
                        }
                    }
                });
            }
        }

        private void ExecuteCommand(char command, string argument)
        {
            switch (command)
            {
                case 'E':
                    ExecuteQuery(argument, QueryExecutionMode.NonQuery);
                    break;
                case 'S':
                    ExecuteQuery(argument, QueryExecutionMode.Scalar);
                    break;
                case 'D':
                    ExecuteQuery(argument, QueryExecutionMode.Reader);
                    break;
                case 'F':
                    ExecuteMove(argument, table =>
                    {
                        log.Hint(string.Format("Moved to first in table {0}.", table.Handle.ToString()));
                        table.MoveFirst();
                    });
                    break;
                case 'L':
                    ExecuteMove(argument, table =>
                    {
                        log.Hint(string.Format("Moved to last in table {0}.", table.Handle.ToString()));
                        table.MoveLast();
                    });
                    break;
                case 'N':
                    ExecuteMove(argument, table =>
                    {
                        log.Hint(string.Format("Moved to next in table {0}.", table.Handle.ToString()));
                        table.MoveNext();
                    });
                    break;
                case 'P':
                    ExecuteMove(argument, table =>
                    {
                        log.Hint(string.Format("Moved to previous in table {0}.", table.Handle.ToString()));
                        table.MovePrevious();
                    });
                    break;
                case 'C':
                    CloseTable(argument);
                    break;
                case 'G':
                    GotoRow(argument);
                    break;
                case 'A':
                    CloseAllTables();
                    break;
                case 'X':
                    InvokeExtensionMethod(argument);
                    break;
                default:
                    if ((command >= '0' && command <= '9') || command == ':' || command == ';' || command == '<')
                        log.Warning("Unsupported Unibase command received.");
                    else
                        log.Warning("Unknown command received.");
                    break;
            }
        }

        private void ExecuteQuery(string command, QueryExecutionMode executionMode)
        {
            string[] parts = command.Split('\x08');
            int parameterCountInQuery;
            string commandText = ReformatAsQuery(parts[0], out parameterCountInQuery);

            if (parts.Length - 1 != parameterCountInQuery)
                throw new UnibaseExException(UnibaseExError.ParamCountMismatch, "The submitted number of arguments does not match the number of parameters in the query.");

            ParameterValue[] arguments = new ParameterValue[parameterCountInQuery];
            for (int i = 0; i < parameterCountInQuery; i++)
            {
                arguments[i] = new ParameterValue("@p" + (i + 1).ToString(), parts[i + 1]);
            }

            log.Hint(string.Format("Execute{0}: {1} (Args: {2})", executionMode.ToString(), commandText, string.Join(", ", arguments.Select(d => d.Name + " = " + d.Value))));

            queryTimer.Restart();

            if (backendConnectionContext == null)
            {
                try
                {
                    backendConnectionContext = context.DefaultEndPoint.EndPointHostingService.OpenBackendConnection();
                }
                catch (InvalidOperationException ex)
                {
                    throw new UnibaseExException(UnibaseExError.ConnectionNotFound, "Cannot find the connection or a component required for the connection.", ex);
                }
            }

            try
            {
                if (!isOpen)
                    backendConnectionContext.Open();

                isOpen = true;
            }
            catch (DbException ex)
            {
                throw new UnibaseExException(UnibaseExError.ConnectionOpenError, "An exception occured while opening the database connection.", ex);
            }

            try
            {
                using (var result = backendConnectionContext.ExecuteQuery(commandText, arguments))
                {
                    switch (executionMode)
                    {
                        case QueryExecutionMode.Reader:
                            HandleReaderResult(result);
                            break;
                        case QueryExecutionMode.Scalar:
                            HandleScalarResult(result);
                            break;
                        case QueryExecutionMode.NonQuery:
                            HandleNonQueryResult(result);
                            break;
                    }
                }
            }
            catch (QueryParseException ex)
            {
                throw new UnibaseExException(UnibaseExError.QueryError, string.Format("Syntax error in embedded sql query at line {0} column {1}.", ex.Position.Line, ex.Position.Column), ex);
            }
            catch (DbException ex)
            {
                throw new UnibaseExException(UnibaseExError.QueryError, "The query has failed.", ex);
            }

            queryTimer.Stop();
            log.Action(string.Format("[PERF] Query took {0} ms.", queryTimer.ElapsedMilliseconds));
        }

        private void HandleReaderResult(IRelationalResultSet result)
        {
            int handle = AllocateHandle();

            var table = remoteTables[handle] = new RemoteTypedTable(handle, result);
            SendTableHeader(table);

            log.Success(string.Format("Query successfully executed. Result set with {0} rows and {1} columns.", table.RowCount, table.ColumnCount));
        }

        private void HandleScalarResult(IRelationalResultSet result)
        {
            object value = null;
            if (result.Read())
            {
                if (result.Count == 0)
                    log.Warning("Query successfully executed. The scalar query returned zero rows.");
                else
                {
                    value = result.Get(0);

                    string valueString = "<null>";
                    if (value != null)
                        valueString = string.Format("\"{0}\" ({1})", value, value.GetType().Name);

                    log.Success("Query successfully executed. Result: " + valueString);
                }
            }
            else
                log.Warning("Query successfully executed. The scalar query returned zero rows.");

            SendResponse(EncodeValue(value));
        }

        private void HandleNonQueryResult(IRelationalResultSet result)
        {
            log.Success("Query successfully executed. Rows affected: " + result.RecordsAffected.ToString());
            SendResponse("L" + result.RecordsAffected.ToString());
        }

        private void ExecuteMove(string argument, Action<RemoteTable> action)
        {
            var table = GetTable(argument);
            action(table);
            SendCurrentRow(table);
        }

        private void GotoRow(string argument)
        {
            var parts = argument.Split('\x08');
            int targetRow;
            if (parts.Length != 2 || !int.TryParse(parts[1], out targetRow))
                throw new UnibaseExException(UnibaseExError.IllegalRow);

            var table = GetTable(parts[0]);
            table.Goto(targetRow);
            log.Hint(string.Format("Moved to row {0} in table {1}.", targetRow, table.Handle.ToString()));
            SendCurrentRow(table);
        }

        private void CloseTable(string argument)
        {
            var table = GetTable(argument);
            remoteTables[table.Handle] = null;
            SendResponse("C");
            log.Hint(string.Format("Closed table {0}.", table.Handle.ToString()));
        }

        private void CloseAllTables()
        {
            log.Hint("Closed all tables.");
            for (int i = 0; i < remoteTables.Length; i++)
            {
                remoteTables[i] = null;
            }
            SendResponse("C");
        }

        private void InvokeExtensionMethod(string command)
        {
            string[] parts = command.Split('\b');
            string name = parts[0].Trim(' ', '\t', '?');

            object[] arguments = new object[parts.Length - 1];
            Array.Copy(parts, 1, arguments, 0, arguments.Length);

            log.Hint(string.Format("Invoke Extension {0} (Args {1})", name, string.Join(", ", arguments)));

            object returnValue = context.DefaultEndPoint.EndPointHostingService.InvokeExtension(name, arguments);

            if (returnValue is DataTable)
            {
                var dataTable = returnValue as DataTable;
                log.Hint(string.Format("Result: DataTable<{0} Cols,{1} Rows>", dataTable.Columns.Count, dataTable.Rows.Count));

                int handle = AllocateHandle();
                var table = remoteTables[handle] = new RemoteDataTable(handle, dataTable);
                SendTableHeader(table);
            }
            else
            {
                log.Hint("Result: " + (returnValue != null ? returnValue.ToString() : "<Null>"));
                SendResponse(EncodeValue(returnValue));
            }
        }

        private int AllocateHandle()
        {
            int handle = Array.IndexOf(remoteTables, null);
            if (handle == -1)
                throw new UnibaseExException(UnibaseExError.NoHandlesAvailable, "No additional handles are available");
            return handle;
        }

        private void SendTableHeader(RemoteTable table)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("T");
            sb.Append(table.Handle);
            sb.Append('\x08').Append(table.RowCount);

            var columnNames = table.GetColumnNames();
            for (int i = 0; i < columnNames.Length; i++)
            {
                var name = columnNames[i];
                if (name.Length > 20)
                    name = name.Substring(0, 20);

                sb.Append('\x08').Append(name);
            }

            SendResponse(sb.ToString());
        }

        private RemoteTable GetTable(string handleString)
        {
            int handle;
            if (!int.TryParse(handleString, out handle))
                throw new UnibaseExException(UnibaseExError.IllegalHandle, "The handle cannot be parsed.");

            if (handle < 0 || handle >= remoteTables.Length)
                throw new UnibaseExException(UnibaseExError.IllegalHandle, "The handle is out of range.");

            var table = remoteTables[handle];
            if (table == null)
                throw new UnibaseExException(UnibaseExError.IllegalHandle, "No table with this handle.");

            Debug.Assert(table.Handle == handle);
            return table;
        }

        private void SendCurrentRow(RemoteTable table)
        {
            SendResponse(string.Join("\x08", table.GetValues().Select(EncodeValue)));
        }

        public string EncodeValue(object value)
        {
            char type;
            string stringValue = "";

            if (Convert.IsDBNull(value) || value == null)
                type = 'N';
            else
            {
                switch (Type.GetTypeCode(value.GetType()))
                {
                    case TypeCode.Int16:
                    case TypeCode.UInt16:
                    case TypeCode.Int32:
                        type = 'l';
                        stringValue = value.ToString();
                        break;
                    case TypeCode.UInt32:
                        type = 'L';
                        stringValue = value.ToString();
                        break;
                    case TypeCode.Double:
                        type = 'D';
                        stringValue = ((double)value).ToString("R");
                        break;
                    case TypeCode.Single:
                        type = 'D';
                        stringValue = ((float)value).ToString("R");
                        break;
                    case TypeCode.Decimal:
                        type = 'D';
                        stringValue = value.ToString();
                        break;
                    default:
                        type = 'S';
                        stringValue = (value ?? "").ToString();
                        break;
                }
            }
            return type + stringValue;
        }

        private string ReformatAsQuery(string query, out int paramCountInQuery)
        {
            int i = -1;
            int paramCount = 0;
            do
            {
                i = query.IndexOf('?', i + 1);
                if (i != -1)
                {
                    paramCount++;
                    query = query.Remove(i, 1).Insert(i, ("@p" + paramCount.ToString()));
                }
            }
            while (i != -1);

            paramCountInQuery = paramCount;
            return query;
        }

        private void SendResponse(string response)
        {
            if (debugMode)
                log.Action("[SEND] " + response.Replace("\b", "\\b").Replace("\n", "\\n").Replace("\r", "\\r").Replace("\f", "\\f").Replace("\v", "\\v"));

            using (var outputStream = context.CreateOutputStreamInternal())
            {
                outputStream.Write((byte)'#');

                var outputBuffer = outputStream.Buffer;
                int length = defaultEncoding.GetByteCount(response);
                if (length > outputBuffer.Count - outputBuffer.BytesUsed)
                {
                    byte[] buffer = new byte[length];
                    defaultEncoding.GetBytes(response, 0, response.Length, buffer, 0);
                    outputStream.Write(buffer, 0, buffer.Length);
                }
                else
                {
                    int bytesWritten = defaultEncoding.GetBytes(response, 0, response.Length, outputStream.Buffer.Buffer,
                        outputBuffer.Offset + outputBuffer.BytesUsed);

                    Debug.Assert(bytesWritten < outputBuffer.Count - outputBuffer.BytesUsed);
                    outputBuffer.BytesUsed += bytesWritten;
                }

                outputStream.Write(12);
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < remoteTables.Length; i++)
            {
                if (remoteTables[i] != null)
                {
                    log.Warning("Remote client did not close table " + i + ".");
                    remoteTables[i] = null;
                }
            }

            if (backendConnectionContext != null)
            {
                backendConnectionContext.Dispose();
                backendConnectionContext = null;
            }
        }
    }
}
