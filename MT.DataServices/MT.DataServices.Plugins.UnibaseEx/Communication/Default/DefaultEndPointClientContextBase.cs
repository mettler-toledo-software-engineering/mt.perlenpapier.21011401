﻿using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Base class for the communication context of a client
    /// </summary>
    public abstract class DefaultEndPointClientContextBase : IDisposable
    {
        private readonly DefaultEndPointBase defaultEndPoint;
        private readonly ILogSink log;
        private IProtocolImplementation protocolImplementation;
        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultEndPointClientContextBase"/> class.
        /// </summary>
        public DefaultEndPointClientContextBase(DefaultEndPointBase defaultEndPoint)
        {
            this.defaultEndPoint = defaultEndPoint;
            log = defaultEndPoint.EndPointHostingService.Log;
            protocolImplementation = new TextProtocol(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="DefaultEndPointClientContextBase"/> class.
        /// </summary>
        ~DefaultEndPointClientContextBase()
        {
            Dispose(false);
        }
  
        /// <summary>
        /// Gets the default end point.
        /// </summary>
        public DefaultEndPointBase DefaultEndPoint
        {
            get { return defaultEndPoint; }
        }

        /// <summary>
        /// Handles a message from the client.
        /// </summary>
        /// <param name="message">The message to handle.</param>
        /// <returns>The number of bytes handled.</returns>
        public virtual int HandleMessage(ArraySegment<byte> message)
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            return protocolImplementation.HandleMessage(message);
        }

        /// <summary>
        /// Creates an output stream to send data back to the client.
        /// </summary>
        protected abstract IOutputStream CreateOutputStream();

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !isDisposed)
            {              
                if (protocolImplementation != null)
                {
                    protocolImplementation.Dispose();
                    protocolImplementation = null;
                }

                isDisposed = true;
            }
        }

        internal IOutputStream CreateOutputStreamInternal()
        {
            return CreateOutputStream();
        }
    }
}
