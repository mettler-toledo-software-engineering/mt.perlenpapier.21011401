﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal enum UnibaseExError
    {
        NoError = 0,
        ConnectionNotFound = 1,
        ConnectionOpenError = 2,
        QueryError = 3,
        ParamCountMismatch = 4,
        NoHandlesAvailable = 5,
        IllegalHandle = 6,
        IllegalRow = 7,
        ExtensionNotFound = 8,
        ExtensionException = 9,
        TypeConversionError = 10
    }
}
