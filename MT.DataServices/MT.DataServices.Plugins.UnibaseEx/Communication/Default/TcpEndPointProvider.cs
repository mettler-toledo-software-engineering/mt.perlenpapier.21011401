﻿using MT.DataServices.Plugins.UnibaseEx.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Exports the TCP version of the default protocol as a MEF component.
    /// </summary>
    [Export(typeof(IEndPointProvider))]
    public sealed class TcpEndPointProvider : IEndPointProvider
    {
        /// <summary>
        /// Gets the name of the end point.
        /// </summary>
        public string Name
        {
            get { return "UnibaseEx (TCP/IP)"; }
        }

        /// <summary>
        /// Creates a new the end point.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>
        /// The newly created instance implementing <see cref="IEndPoint" /> interface.
        /// </returns>
        /// <exception cref="System.ArgumentException">The configuration has the wrong type.</exception>
        public IEndPoint CreateEndPoint(IEndPointHostingService host, object configuration)
        {
            var typedConfiguration = configuration as TcpEndPointConfiguration;
            if (typedConfiguration == null)
                throw new ArgumentException("The configuration has the wrong type.", "configuration");

            return new TcpEndPoint(host, typedConfiguration);
        }

        /// <summary>
        /// Creates a configuration control for this type of endpoint.
        /// </summary>
        /// <param name="configurationHost">The configuration host.</param>
        /// <returns>
        /// The newly created configuration user control.
        /// </returns>
        public Control CreateConfigurationControl(IEndPointConfigurationHost configurationHost)
        {
            if (configurationHost == null)
                throw new ArgumentNullException("configurationHost");

            return new DefaultTcpEndPointEditor(configurationHost);    
        }
    }
}
