﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Contains the data of a table that was opened by a remote client
    /// </summary>
    internal abstract class RemoteTable
    {
        public RemoteTable(int handle)
        {
            Handle = handle;
        }

        internal int Handle { get; private set; }

        internal abstract void MoveFirst();
        internal abstract void MovePrevious();
        internal abstract void MoveNext();
        internal abstract void MoveLast();
        internal abstract void Goto(int row);
        internal abstract object[] GetValues();

        internal abstract int RowCount { get; }
        internal abstract int ColumnCount { get; }
        internal abstract string[] GetColumnNames();
    }
}
