﻿using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Utilities.TypedTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal class RemoteTypedTable : RemoteTable
    {
        private readonly TypedTableReader reader;
        private readonly TypedTable table;
        private readonly string[] columnNames;
        private readonly object[] values;

        private int current;

        public RemoteTypedTable(int handle, IRelationalResultSet resultSet)
            : base(handle)
        {
            columnNames = new string[resultSet.Count];
            for (int i = 0; i < resultSet.Count; i++)
            {
                columnNames[i] = resultSet.GetName(i);
            }

            values = new object[resultSet.Count];
            reader = new TypedTableReader(resultSet);
            table = reader.LoadIntoMemory();            
            current = 0;
        }

        internal override void MoveFirst()
        {
            current = 0;
        }

        internal override void MovePrevious()
        {
            if (current > 0)
                current--;
        }

        internal override void MoveNext()
        {
            if (current < RowCount - 1)
                current++;
        }

        internal override void MoveLast()
        {
            current = RowCount - 1;
        }

        internal override void Goto(int row)
        {
            if (row < 0 || row >= RowCount)
                throw new UnibaseExException(UnibaseExError.IllegalRow, "Illegal row index.");

            current = row;
        }

        internal override object[] GetValues()
        {
            if (current < 0 || current >= RowCount)
                throw new UnibaseExException(UnibaseExError.IllegalRow, "Illegal row index.");

            table.GetValues(current, values);
            return values;
        }

        internal override int RowCount
        {
            get { return table.RowCount; }
        }

        internal override int ColumnCount
        {
            get { return table.ColumnCount; }
        }

        internal override string[] GetColumnNames()
        {
            return (string[])columnNames.Clone();
        }
    }
}
