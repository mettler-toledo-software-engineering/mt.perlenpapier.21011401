﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal class SocketAsyncEventArgsPool
    {
        private Stack<SocketAsyncEventArgs> pool;
        private int poolSize;
        private List<SocketAsyncEventArgs> originalPool;

        internal SocketAsyncEventArgsPool(int poolSize, EventHandler<SocketAsyncEventArgs> completionHandler)
        {
            Debug.Assert(poolSize > 0);

            this.poolSize = poolSize;

            pool = new Stack<SocketAsyncEventArgs>(poolSize);
            originalPool = new List<SocketAsyncEventArgs>(poolSize);

            for (int i = 0; i < poolSize; i++)
            {
                var saea = new SocketAsyncEventArgs();
                saea.Completed += completionHandler;
                pool.Push(saea);
                originalPool.Add(saea);
            }
        }

        internal int PoolSize
        {
            get { return poolSize; }
        }

        internal void SetBuffers(byte[] buffer, int pageSize)
        {
            lock (pool)
            {
                int index = 0;
                foreach (var saea in pool)
                {
                    TransceiverUserToken token = new TransceiverUserToken(buffer, index * pageSize, pageSize);
                    saea.UserToken = token;
                    token.SetBuffer(saea);
                    index++;
                }
            }
        }

        internal bool TryAcquire(out SocketAsyncEventArgs saea)
        {
            lock (pool)
            {
                if (pool.Count == 0)
                {
                    saea = null;
                    return false;
                }
                else
                {
                    saea = pool.Pop();
                    return true;
                }
            }
        }

        internal void Release(SocketAsyncEventArgs saea)
        {
            lock (pool)
            {
                Debug.Assert(originalPool.Contains(saea) && !pool.Contains(saea));

                pool.Push(saea);
            }
        }
    }
}
