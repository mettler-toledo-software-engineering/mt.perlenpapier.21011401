﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Exposes methods that allow to write binary data.
    /// </summary>
    public interface IOutputStream : IDisposable
    {
        /// <summary>
        /// Writes the specified part of the given buffer into the output stream.
        /// </summary>
        /// <param name="buffer">The buffer containing the data.</param>
        /// <param name="offset">The offset where the data starts.</param>
        /// <param name="count">The number of bytes to write.</param>
        void Write(byte[] buffer, int offset, int count);

        /// <summary>
        /// Writes a single byte into the output buffer.
        /// </summary>
        /// <param name="b">The byte to write.</param>
        void Write(byte b);

        /// <summary>
        /// Flushes the buffered output data into the stream.
        /// </summary>
        void Flush();

        /// <summary>
        /// Returns the underlying buffer.
        /// </summary>
        OutputBuffer Buffer { get; }
    }
}
