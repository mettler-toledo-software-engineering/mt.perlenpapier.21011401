﻿using MT.DataServices.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Base class for implementing endpoints based on the default UnibaseEx protocol.
    /// </summary>
    public abstract class DefaultEndPointBase : IEndPoint
    {
        private readonly IEndPointHostingService host;
        private CancellationToken cancellationToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultEndPointBase" /> class.
        /// </summary>
        /// <param name="host">The host of this endpoint.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="host"/> is <c>null</c>.</exception>
        public DefaultEndPointBase(IEndPointHostingService host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            this.host = host;
        }

        /// <summary>
        /// Runs the communication interface services synchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for shutting down the service.</param>
        public void Run(CancellationToken cancellationToken)
        {
            this.cancellationToken = cancellationToken;

            try
            {
                RunCommunication();
            }
            finally
            {
                cancellationToken = CancellationToken.None;
            }
        }

        /// <summary>
        /// Gets the end point hosting service of this end point.
        /// </summary>
        public IEndPointHostingService EndPointHostingService
        {
            get { return host; }
        }

        /// <summary>
        /// Gets the log sink for this endpoint.
        /// </summary>
        protected ILogSink Log
        {
            get { return host.Log; }
        }

        /// <summary>
        /// Gets the cancellation token.
        /// </summary>
        protected CancellationToken CancellationToken
        {
            get { return cancellationToken; }
        }

        /// <summary>
        /// Runs the communication with the clients.
        /// </summary>
        protected abstract void RunCommunication();
    }
}
