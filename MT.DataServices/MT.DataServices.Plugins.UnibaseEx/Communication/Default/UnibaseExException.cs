﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    [Serializable]
    internal class UnibaseExException : Exception
    {
        public UnibaseExError UnibaseError { get; private set; }

        public UnibaseExException(UnibaseExError error)
        {
            UnibaseError = error;
        }

        public UnibaseExException(UnibaseExError error, string message)
            : base(message)
        {
            UnibaseError = error;
        }

        public UnibaseExException(UnibaseExError error, string message, Exception inner)
            : base(message, inner)
        {
            UnibaseError = error;
        }

        protected UnibaseExException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
