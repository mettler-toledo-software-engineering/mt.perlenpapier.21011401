﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class OutputBuffer
    {
        private readonly byte[] buffer;
        private readonly int offset;
        private readonly int count;

        /// <summary>
        /// Initializes a new instance of the <see cref="OutputBuffer"/> class.
        /// </summary>
        public OutputBuffer(byte[] buffer, int offset, int count)
        {
            this.count = count;
            this.offset = offset;
            this.buffer = buffer;
        }

        /// <summary>
        /// Gets or sets the number of bytes used in the buffer.
        /// </summary>
        public int BytesUsed { get; set; }

        /// <summary>
        /// Gets the count.
        /// </summary>
        public int Count
        {
            get { return count; }
        }

        /// <summary>
        /// Gets the offset.
        /// </summary>
        public int Offset
        {
            get { return offset; }
        }

        /// <summary>
        /// Gets the buffer.
        /// </summary>
        public byte[] Buffer
        {
            get { return buffer; }
        }

        /// <summary>
        /// Flushes contents of the buffer.
        /// </summary>
        public abstract void Flush();
    }
}
