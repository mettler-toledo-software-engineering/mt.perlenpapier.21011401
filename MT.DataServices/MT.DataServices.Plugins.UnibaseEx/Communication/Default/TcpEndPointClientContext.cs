﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using MT.DataServices.Logging;
using System.Net;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal class TcpEndPointClientContext : DefaultEndPointClientContextBase
    {
        private volatile IOutputStream stream;

        private readonly Socket socket;
        private readonly TcpEndPoint endPoint;
        private readonly IPEndPoint remoteEndPoint;

        internal TcpEndPointClientContext(TcpEndPoint endPoint, Socket socket)
            : base(endPoint)
        {
            this.endPoint = endPoint;
            this.socket = socket;
            this.remoteEndPoint = (IPEndPoint)socket.RemoteEndPoint;
        }

        public IPEndPoint RemoteEndPoint
        {
            get { return remoteEndPoint; }
        }

        internal Socket Socket
        {
            get { return socket; }
        }

        /// <summary>
        /// Creates an output stream to send data back to the client.
        /// </summary>
        protected override IOutputStream CreateOutputStream()
        {
            if (stream != null)
                throw new InvalidOperationException("Only one output stream can be open at the same time.");

            SocketAsyncEventArgs saea;
            var success = endPoint.TransceivePool.TryAcquire(out saea);
            Debug.Assert(success);

            var token = (TransceiverUserToken)saea.UserToken;
            token.Reset();
            token.Context = this;
            
            stream = new TcpOutputStream(endPoint, saea);
            return stream;
        }

        internal void NotifyOutputStreamDisposed()
        {
            stream = null;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (socket != null)
                {
                    try
                    {
                        socket.Shutdown(SocketShutdown.Both);
                    }
                    catch (SocketException)
                    {
                        // Ignore the exception thrown when the socket is already closed.
                    }

                    socket.Close();
                }

                if (stream != null)
                    stream.Dispose();

                endPoint.EndPointHostingService.Log.Hint("Disconnected from client " + remoteEndPoint.ToString());
            }
        }
    }
}
