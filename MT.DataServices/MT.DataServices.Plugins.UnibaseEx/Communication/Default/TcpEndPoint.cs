﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using MT.DataServices.Logging;
using System.Diagnostics;
using System.Collections.Generic;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Implementation of the UnibaseEx protocol over TCP/IP.
    /// </summary>
    internal class TcpEndPoint : DefaultEndPointBase
    {
        private readonly TcpEndPointConfiguration configuration;
        private readonly SocketAsyncEventArgsPool acceptPool;
        private readonly SocketAsyncEventArgsPool transceivePool;
        private readonly byte[] buffer;
        private readonly List<TcpEndPointClientContext> activeContexts = new List<TcpEndPointClientContext>();
        
        private Socket listenerSocket;

        internal TcpEndPoint(IEndPointHostingService host, TcpEndPointConfiguration configuration)
            : base(host)
        {
            IPAddress dummy;

            if (!IPAddress.TryParse(configuration.Address, out dummy))
                throw new ArgumentException("The given address cannot be parsed as an IP address.", "configuration");

            if (configuration.Port <= 0)
                throw new ArgumentException("The given port number is invalid", "configuration");

            if (configuration.ConcurrentConnections <= 0)
                throw new ArgumentException("The number of concurrent connections must be at least 1", "configuration");

            this.configuration = configuration;

            acceptPool = new SocketAsyncEventArgsPool(configuration.ConcurrentConnections + 1, OnAcceptConnection);
            transceivePool = new SocketAsyncEventArgsPool(configuration.ConcurrentConnections * 2 + 1, OnDataReceived);

            buffer = new byte[4096 * transceivePool.PoolSize];
            transceivePool.SetBuffers(buffer, 4096);
        }

        /// <summary>
        /// Gets the transceive pool.
        /// </summary>
        internal SocketAsyncEventArgsPool TransceivePool
        {
            get { return transceivePool; }
        }

        /// <summary>
        /// Runs the communication with the clients.
        /// </summary>
        protected override void RunCommunication()
        {
            // Initialize the socket for accepting clients
            listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listenerSocket.Bind(new IPEndPoint(IPAddress.Parse(configuration.Address), configuration.Port));
            listenerSocket.Listen(10);

            StartAccept();

            CancellationToken.WaitHandle.WaitOne();

            listenerSocket.Close();

            lock (activeContexts)
            {
                foreach (var context in activeContexts)
                {
                    context.Dispose();
                }

                activeContexts.Clear();
            }
        }

        /// <summary>
        /// Starts an asynchronous accept operation.
        /// </summary>
        private void StartAccept()
        {
            if (CancellationToken.IsCancellationRequested)
                return;

            SocketAsyncEventArgs acceptAsyncEventArgs;
            if (acceptPool.TryAcquire(out acceptAsyncEventArgs))
            {
                bool ioOperationPending = listenerSocket.AcceptAsync(acceptAsyncEventArgs);
                if (!ioOperationPending)
                    OnAcceptConnection(listenerSocket, acceptAsyncEventArgs);
            }
        }

        /// <summary>
        /// Called when a socket was accepted by the listener.
        /// </summary>
        private void OnAcceptConnection(object sender, SocketAsyncEventArgs saea)
        {
            // If the accept operation failed
            if (saea.SocketError != SocketError.Success)
            {
                // Close the socket, it may be corrupted
                saea.AcceptSocket.Close();

                // And restart the accept
                StartAccept();

                // Release this SAEA into the accept pool
                acceptPool.Release(saea);
                return;
            }

            // Start the accept operation for the next client
            StartAccept();

            var socket = saea.AcceptSocket;

            // Start the communcation with this new client
            InitiateClientConnection(socket);

            // Prepare the SAEA for reuse
            saea.AcceptSocket = null;
            acceptPool.Release(saea);
        }

        /// <summary>
        /// Initiates the context for a client connection.
        /// </summary>
        /// <param name="socket">The socket that connects to the new client.</param>
        private void InitiateClientConnection(Socket socket)
        {
            Log.Hint("Incoming connection from " + socket.RemoteEndPoint.ToString());

            var context = new TcpEndPointClientContext(this, socket);
            lock (activeContexts)
            {
                activeContexts.Add(context);
            }

            StartReceive(context);
        }

        /// <summary>
        /// Starts the receive operation for a new client.
        /// </summary>
        private void StartReceive(TcpEndPointClientContext context)
        {
            SocketAsyncEventArgs saea;
            
            // Get a next event from the pool
            var success = transceivePool.TryAcquire(out saea);
            Debug.Assert(success, "The transceive pool is empty.");

            // Assign the context to the event
            var userToken = (TransceiverUserToken)saea.UserToken;
            userToken.Context = context;

            // Start the receive operation.
            RestartReceive(saea);
        }

        /// <summary>
        /// Restarts the receiving operation on the given <see cref="SocketAsyncEventArgs"/>.
        /// </summary>
        private void RestartReceive(SocketAsyncEventArgs saea)
        {
            if (CancellationToken.IsCancellationRequested)
                return;

            // Update the receive buffer if necessary
            var userToken = (TransceiverUserToken)saea.UserToken;
            userToken.SetBuffer(saea);

            // Start the receive operation
            bool ioOperationPending = userToken.Context.Socket.ReceiveAsync(saea);
            if (!ioOperationPending)
                OnDataReceived(saea.AcceptSocket, saea);
        }

        /// <summary>
        /// Called when data was received from the client.
        /// </summary>
        private void OnDataReceived(object sender, SocketAsyncEventArgs saea)
        {
            if (saea.LastOperation == SocketAsyncOperation.Receive)
                ProcessReceive(saea);
            else
            {
                //This exception will occur if you code the Completed event of some
                //operation to come to this method, by mistake.
                throw new ArgumentException("The last operation completed of the socket was not a receive.");
            }
        }

        /// <summary>
        /// Processes the received data.
        /// </summary>
        private void ProcessReceive(SocketAsyncEventArgs saea)
        {
            var token = (TransceiverUserToken)saea.UserToken;
            TcpEndPointClientContext context = token.Context;

            // Was the connection closed?
            if (saea.SocketError != SocketError.Success || saea.BytesTransferred == 0)
            {
                if (saea.SocketError != SocketError.Success)
                {
                    // Report to the user that the connection was closed with an error
                    // This callback can happen after the service has already been disposed and the log will be null.
                    var log = Log;
                    if (log != null)
                        Log.Warning(string.Format("Connection to {0} closed with error {1}", context.RemoteEndPoint.ToString(), saea.SocketError));
                }

                CloseContext(context);
                token.Reset();
                transceivePool.Release(saea);

                return;
            }

            // Adjust the displacement to reflect the newly received data
            token.Displacement += saea.BytesTransferred;

            // The buffer contains now the received message between token.Offset and token.Offset + token.Displacement
            ArraySegment<byte> segment = new ArraySegment<byte>(token.Buffer, token.Offset, token.Displacement);

            // Try to handle the message
            int bytesHandled = context.HandleMessage(segment);

            // Could only parts of the message be handled?
            if (bytesHandled > 0 && bytesHandled < token.Displacement)
            {
                // Remove the handled message by moving the remaining data to the start of the buffer
                Buffer.BlockCopy(token.Buffer, token.Offset + bytesHandled, token.Buffer, token.Offset, token.Displacement - bytesHandled);
            }

            // Update the displacement to reflect the handled message
            token.Displacement -= bytesHandled;

            // There is no space left in the buffer to receive additional data
            if (token.Count - token.Displacement == 0)
            {
                // Extend the buffer space
                token.SwitchOrExtendEphemeralBuffer();
            }

            // Restart receiving data
            SocketAsyncEventArgs nextSaea;
            var success = transceivePool.TryAcquire(out nextSaea);
            Debug.Assert(success);

            // Switch the context information and buffers of the saea
            saea.UserToken = nextSaea.UserToken;
            nextSaea.UserToken = token;

            RestartReceive(nextSaea);

            // Release the current event to the pool
            transceivePool.Release(saea);
        }

        /// <summary>
        /// Closes the context of a client, because the connection was closed or broke.
        /// </summary>
        private void CloseContext(TcpEndPointClientContext context)
        {         
            lock (activeContexts)
            {
                if (activeContexts.Remove(context))
                    context.Dispose();
            }           
        }
    }
}
