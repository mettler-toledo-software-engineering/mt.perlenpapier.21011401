﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal sealed class TcpOutputStream : OutputBuffer, IOutputStream
    {
        private readonly object lockObject = new object();
        private readonly SocketAsyncEventArgs saea;
        private readonly TransceiverUserToken token;
        private readonly TcpEndPoint endPoint;
        private volatile bool isDisposed;

        internal TcpOutputStream(TcpEndPoint endPoint, SocketAsyncEventArgs saea)
            : this((TransceiverUserToken)saea.UserToken)
        {
            this.endPoint = endPoint;
            this.saea = saea;
        }

        private TcpOutputStream(TransceiverUserToken token)
            : base(token.Buffer, token.Offset, token.Count)
        {
            this.token = token;
            BytesUsed = token.Displacement;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (buffer == null)
                throw new ArgumentNullException("buffer");

            if (offset < 0 || offset >= buffer.Length)
                throw new ArgumentOutOfRangeException("offset");

            if (count < 0 || offset + count > buffer.Length)
                throw new ArgumentOutOfRangeException("count");

            if (count == 0)
                return;

            int bufferOffset = 0;
            do
            {
                int bytesToWrite = Math.Min(Count - BytesUsed, count - bufferOffset);
                System.Buffer.BlockCopy(buffer, bufferOffset, Buffer, Offset + BytesUsed, bytesToWrite);
                BytesUsed += bytesToWrite;
                bufferOffset += bytesToWrite;

                if (BytesUsed == Count)
                    Flush();
            } while (count - bufferOffset > 0);
        }

        public void Write(byte b)
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (BytesUsed == Count)
                Flush();

            Buffer[Offset + BytesUsed++] = b;
        }

        public override void Flush()
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (BytesUsed > 0)
            {
                try
                {
                    token.Context.Socket.Send(Buffer, Offset, BytesUsed, SocketFlags.None);
                }
                catch (ObjectDisposedException)
                {
                    // This exception occurs when the server is shutting down, has severed the connection
                    // and there is still data in the output buffer.
                }

                BytesUsed = 0;
            }
        }

        public void Dispose()
        {
            lock (lockObject)
            {
                if (isDisposed)
                    return;

                Flush();

                isDisposed = true;
            }

            var context = token.Context;
            token.Reset();
            endPoint.TransceivePool.Release(saea);

            if (context != null)
                context.NotifyOutputStreamDisposed();
        }

        OutputBuffer IOutputStream.Buffer
        {
            get
            {
                if (isDisposed)
                    throw new ObjectDisposedException(GetType().FullName);

                return this;
            }
        }
    }
}
