﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal enum QueryExecutionMode
    {
        Reader,
        Scalar,
        NonQuery
    }
}
