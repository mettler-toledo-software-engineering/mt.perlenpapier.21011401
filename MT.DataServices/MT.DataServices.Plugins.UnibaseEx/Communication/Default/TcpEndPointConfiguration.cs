﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Contains the confguration specific to a TcpEndPoint
    /// </summary>
    public class TcpEndPointConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TcpEndPointConfiguration"/> class.
        /// </summary>
        public TcpEndPointConfiguration()
        {
            Address = IPAddress.Any.ToString();
            Port = 20000;
            ConcurrentConnections = 5;
        }

        /// <summary>
        /// Gets or sets the IP address of the network card that this endpoint is bound to.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the port to listen on.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets the number of concurrent connections allowed by the system.
        /// </summary>
        public int ConcurrentConnections { get; set; }
    }
}
