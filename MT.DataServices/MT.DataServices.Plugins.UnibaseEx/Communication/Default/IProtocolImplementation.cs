using System;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Backends;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    /// <summary>
    /// Exposes a method 
    /// </summary>
    internal interface IProtocolImplementation : IDisposable
    {
        int HandleMessage(ArraySegment<byte> message);
    }
}