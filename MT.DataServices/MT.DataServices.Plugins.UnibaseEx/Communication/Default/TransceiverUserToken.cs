﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication.Default
{
    internal class TransceiverUserToken
    {
        private readonly byte[] permanentBuffer;
        private readonly int permanentOffset;
        private readonly int permanentCount;

        private byte[] buffer;
        private int offset;
        private int displacement;
        private int count;

        public TcpEndPointClientContext Context { get; set; }

        internal TransceiverUserToken(byte[] buffer, int offset, int count)
        {
            this.permanentBuffer = buffer;
            this.permanentOffset = offset;
            this.permanentCount = count;

            this.buffer = permanentBuffer;
            this.offset = permanentOffset;
            this.count = permanentCount;
        }

        internal int Count
        {
            get { return count; }
        }

        internal int Offset
        {
            get { return offset; }
        }

        internal int Displacement
        {
            get { return displacement; }
            set { displacement = value; }
        }

        internal byte[] Buffer
        {
            get { return buffer; }
        }        

        internal void SwitchOrExtendEphemeralBuffer()
        {
            if (buffer == permanentBuffer)
            {
                buffer = new byte[count * 2];
                System.Buffer.BlockCopy(permanentBuffer, offset, buffer, 0, displacement);
            }
            else
                Array.Resize(ref buffer, buffer.Length * 2);

            offset = 0;
            count = buffer.Length;
        }

        internal void Reset()
        {
            Context = null;
            buffer = permanentBuffer;
            offset = permanentOffset;
            count = permanentCount;
            displacement = 0;
        }

        internal void SetBuffer(SocketAsyncEventArgs saea)
        {
            Debug.Assert(this == saea.UserToken);

            saea.SetBuffer(buffer, offset + displacement, count - displacement);
        }
    }
}
