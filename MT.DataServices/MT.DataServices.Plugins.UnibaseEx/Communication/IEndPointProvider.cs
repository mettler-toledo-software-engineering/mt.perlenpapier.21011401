﻿using System;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Plugins.UnibaseEx.Configuration;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Provides metadata and exposes factory methods for creating an <see cref="IEndPoint"/> and its respective configuration control.
    /// </summary>
    public interface IEndPointProvider
    {
        /// <summary>
        /// Gets the name of the end point.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Creates a new the end point.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>The newly created instance implementing <see cref="IEndPoint"/> interface.</returns>
        IEndPoint CreateEndPoint(IEndPointHostingService host, object configuration);

        /// <summary>
        /// Creates a configuration control for this type of endpoint.
        /// </summary>
        /// <param name="configurationHost">The configuration host.</param>
        /// <returns>The newly created configuration user control.</returns>
        Control CreateConfigurationControl(IEndPointConfigurationHost configurationHost);
    }
}
