﻿using MT.DataServices.Plugins.UnibaseEx.Backends;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Exposes a method that allow to execute a query on this context.
    /// </summary>
    public interface IBackendConnectionContext : IDisposable
    {
        /// <summary>
        /// Executes the given database query.
        /// </summary>
        /// <param name="commandText">The SQL command text received from the client.</param>
        /// <param name="arguments">The query arguments.</param>
        /// <returns>
        /// The result set of the query.
        /// </returns>
        IRelationalResultSet ExecuteQuery(string commandText, params ParameterValue[] arguments);

        /// <summary>
        /// Opens the underlying connection to the database.
        /// </summary>
        void Open();

        /// <summary>
        /// Closes the underlying connection to the database.
        /// </summary>
        void Close();
    }
}
