﻿using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Configuration;
using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Exposes members that allow to access environment of a UnibaseEx endpoint.
    /// </summary>
    public interface IEndPointHostingService
    {
        /// <summary>
        /// Gets the log sink for this end point.
        /// </summary>
        ILogSink Log { get; }

        /// <summary>
        /// Gets the runtime configuration.
        /// </summary>
        RuntimeConfiguration RuntimeConfiguration { get; }

        /// <summary>
        /// Gets the end point configuration.
        /// </summary>
        EndPointConfiguration EndPointConfiguration { get; }

        /// <summary>
        /// Opens a connection to the relational database backend.
        /// </summary>
        /// <returns>The <see cref="IBackendConnectionContext"/> of the connection.</returns>
        IBackendConnectionContext OpenBackendConnection();

        /// <summary>
        /// Invokes the extension method with the given name.
        /// </summary>
        /// <param name="name">The fully qualified name of the extension method.</param>
        /// <param name="arguments">The arguments of the method call.</param>
        /// <returns>The result of the method call.</returns>
        object InvokeExtension(string name, object[] arguments);
    }
}
