﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Exposes a factory method for an object whose methods can be invoked by a UnibaseEx client.
    /// </summary>
    public interface IExtensionProvider 
    {
        /// <summary>
        /// Gets the name of the extension.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Creates the extension object for the given endpoint.
        /// </summary>
        /// <param name="endpointHost">The hosting service of the endpoint that invokes the extension method.</param>
        /// <returns>An object containing methods that should be callable from a UnibaseEx client.</returns>
        object CreateExtensionObject(IEndPointHostingService endpointHost);
    }
}
