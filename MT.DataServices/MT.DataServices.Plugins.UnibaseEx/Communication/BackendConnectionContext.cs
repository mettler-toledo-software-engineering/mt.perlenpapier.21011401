﻿using System;
using System.Data.Common;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;
using MT.DataServices.Plugins.UnibaseEx.Sql;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    internal class BackendConnectionContext : IBackendConnectionContext
    {
        private readonly ILogSink log;
        private readonly bool rawQuery;
        private readonly IRelationalBackend backend;

        internal BackendConnectionContext(IRelationalBackend backend, bool rawQuery, ILogSink log)
        {
            this.backend = backend;
            this.rawQuery = rawQuery;
            this.log = log;
        }

        /// <summary>
        /// Executes the given database query.
        /// </summary>
        /// <param name="commandText">The SQL command text received from the client.</param>
        /// <param name="arguments">The query arguments.</param>
        /// <returns>
        /// The result set of the query.
        /// </returns>
        public IRelationalResultSet ExecuteQuery(string commandText, params ParameterValue[] arguments)
        {
            StatementSequence statementSequence;
            if (rawQuery)
                statementSequence = new StatementSequence(new ExecRawStatement(commandText));
            else
                statementSequence = new StatementSequence(QueryParser.Parse(commandText));

            return backend.Execute(statementSequence, arguments);
        }

        /// <summary>
        /// Opens the underlying connection to the database.
        /// </summary>
        public void Open()
        {
            backend.Open();
        }

        /// <summary>
        /// Closes the underlying connection to the database.
        /// </summary>
        public void Close()
        {
            backend.Close();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            backend.Dispose();
        }
    }
}
