﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins.UnibaseEx.Backends;
using MT.DataServices.Plugins.UnibaseEx.Communication.Default;
using MT.DataServices.Plugins.UnibaseEx.Configuration;
using MT.DataServices.Plugins.UnibaseEx.Sql;
using System.Reflection;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    internal sealed class EndPointHostingService : IService, IEndPointHostingService
    {
        private readonly Plugin plugin;
        private readonly EndPointConfiguration endPointConfiguration;
        private readonly IEndPointProvider endpointProvider;
        internal readonly Stack<BackendConnectionContext> recycledContexts = new Stack<BackendConnectionContext>();

        private ILogSink log;
        private volatile bool shuttingDown;


        /// <summary>
        /// Initializes a new instance of the <see cref="EndPointHostingService"/> class.
        /// </summary>
        internal EndPointHostingService(
            Plugin plugin, 
            EndPointConfiguration endPointConfiguration, 
            IEndPointProvider endpointProvider)
        {
            this.plugin = plugin;
            this.endpointProvider = endpointProvider;
            this.endPointConfiguration = endPointConfiguration;            
        }

        string IService.Name
        {
            get { return endPointConfiguration.Name; }
        }

        void IService.Run(IServiceHost serviceHost)
        {
            log = serviceHost.Log;
            try
            {
                var endpoint = endpointProvider.CreateEndPoint(this, endPointConfiguration.EndpointConfiguration);
                endpoint.Run(serviceHost.CancellationToken);
            }
            finally
            {
                lock (recycledContexts)
                {
                    shuttingDown = true;

                    while(recycledContexts.Count > 0)
                    {
                        recycledContexts.Pop().Dispose();
                    }
                }
                log = null;
            }
        }

        ILogSink IEndPointHostingService.Log
        {
            get { return log; }
        }

        RuntimeConfiguration IEndPointHostingService.RuntimeConfiguration 
        {
            get { return plugin.RuntimeConfiguration; }
        }

        EndPointConfiguration IEndPointHostingService.EndPointConfiguration
        {
            get { return endPointConfiguration; }
        }

        IBackendConnectionContext IEndPointHostingService.OpenBackendConnection()
        {
            BackendConnectionContext context;
            lock (recycledContexts)
            {
                if (shuttingDown)
                    throw new OperationCanceledException("The endpoint is currently shutting down, new requests cannot be processed.");

                if (recycledContexts.Count == 0)
                    context = CreateNewBackendContext();
                else
                    context = recycledContexts.Pop();
            }

            return new RecyclableBackendConnectionContext(this, context);
        }        

        object IEndPointHostingService.InvokeExtension(string name, object[] arguments)
        {
            if (name.IndexOf(".") == -1)
                throw new UnibaseExException(UnibaseExError.ExtensionNotFound, "The extension method identifier does not contain a dot (.).");

            string[] extensionName = name.Split(new char[] { '.' }, 2);
            IExtensionProvider extensionProvider = plugin.Registry.ExtensionProviders.FirstOrDefault(d => d.Name == extensionName[0]);
            if (extensionProvider == null)
                throw new UnibaseExException(UnibaseExError.ExtensionNotFound, string.Format("The extension provider \"{0}\" could not be found.", extensionName));

            var extensionObject = extensionProvider.CreateExtensionObject(this);
            if (extensionObject == null)
                throw new UnibaseExException(UnibaseExError.ExtensionNotFound, string.Format("The extension provider \"{0}\" returned null when prompted to create a context object.", extensionName));

            var method = extensionObject.GetType().GetMethod(extensionName[1], BindingFlags.Public | BindingFlags.Instance);
            if (method == null || method.GetCustomAttributes(typeof(ExtensionContractAttribute), false).Length == 0)
                throw new UnibaseExException(UnibaseExError.ExtensionNotFound, "The method \"" + extensionName[1] + "\" could not be found on the extension object.");

            var parameterList = method.GetParameters();
            if (parameterList.Length != arguments.Length)
                throw new UnibaseExException(UnibaseExError.ParamCountMismatch, "The number of supplied arguments does not match the parameter count.");

            var parameters = new object[parameterList.Length];
            for (int i = 0; i < parameterList.Length; i++)
            {
                try
                {
                    parameters[i] = Convert.ChangeType(arguments[i], parameterList[i].ParameterType);
                }
                catch (Exception ex)
                {
                    throw new UnibaseExException(UnibaseExError.TypeConversionError, "The value (" + arguments[i] + ") for parameter " + parameterList[i].Name + " could not be cast into the required target type " + parameterList[i].ParameterType.Name + ".", ex);
                }
            }

            try
            {
                return method.Invoke(extensionObject, parameters);
            }
            catch (UnibaseExException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new UnibaseExException(UnibaseExError.ExtensionException, "The extension method threw an exception.", ex);
            }
        }

        internal void ReleaseBackendContext(BackendConnectionContext context)
        {
            lock (recycledContexts)
            {
                if (shuttingDown)
                    context.Dispose();
                else
                    recycledContexts.Push(context);
            }
        }

        private BackendConnectionContext CreateNewBackendContext()
        {
            var connectionConfiguration = plugin.Configuration.Connections.FirstOrDefault(d => d.Id == endPointConfiguration.ConnectionId);
            if (connectionConfiguration == null)
                throw new InvalidOperationException(string.Format("No connection with the id {0} could be found. Was it removed without updating the endpoint?", connectionConfiguration));

            IRelationalBackendProvider backendProvider = plugin.Registry.RelationalBackends.FirstOrDefault(d => d.GetType().FullName == connectionConfiguration.BackendDataProvider);
            if (backendProvider == null)
                throw new InvalidOperationException(string.Format("No backend provider of type {0} could be found.", connectionConfiguration.BackendDataProvider));

            if (connectionConfiguration.SqlFormatter == ConnectionConfiguration.DisabledSqlFormatter)
                return new BackendConnectionContext(backendProvider.CreateBackend(connectionConfiguration.ConnectionString, null), true, log);
            else 
            { 
                ISqlFormatter formatter;
                if (connectionConfiguration.SqlFormatter == ConnectionConfiguration.StandardSqlFormatter)
                    formatter = null;
                else
                {
                    var formatterProvider = plugin.Registry.SqlFormatters.FirstOrDefault(d => d.GetType().FullName == connectionConfiguration.SqlFormatter);
                    if (formatterProvider == null)
                        throw new InvalidOperationException(string.Format("No SQL formatter provider of type {0} could be found.", connectionConfiguration.SqlFormatter));

                    formatter = formatterProvider.CreateFormatter();
                }

                return new BackendConnectionContext(backendProvider.CreateBackend(connectionConfiguration.ConnectionString, formatter), false, log);
            }
        }
    }
}
