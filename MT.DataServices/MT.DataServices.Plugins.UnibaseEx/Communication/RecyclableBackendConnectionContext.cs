﻿using MT.DataServices.Plugins.UnibaseEx.Backends;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Implementation of the <see cref="IBackendConnectionContext"/> that allows to recycle the internal context for reuse when a context is requested later.
    /// </summary>
    internal class RecyclableBackendConnectionContext : IBackendConnectionContext
    {
        private readonly EndPointHostingService host;

        private BackendConnectionContext context;
        private bool isDisposed;
        private bool isOpen;

        internal RecyclableBackendConnectionContext(EndPointHostingService host, BackendConnectionContext context)
        {
            this.host = host;
            this.context = context;
        }

        /// <summary>
        /// Executes the given database query.
        /// </summary>
        /// <param name="commandText">The SQL command text received from the client.</param>
        /// <param name="arguments">The query arguments.</param>
        /// <returns>
        /// The result set of the query.
        /// </returns>
        public IRelationalResultSet ExecuteQuery(string commandText, params ParameterValue[] arguments)
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            return context.ExecuteQuery(commandText, arguments);
        }

        /// <summary>
        /// Opens the underlying connection to the database.
        /// </summary>
        public void Open()
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            context.Open();
            isOpen = true;
        }

        /// <summary>
        /// Closes the underlying connection to the database.
        /// </summary>
        public void Close()
        {
            if (isDisposed)
                throw new ObjectDisposedException(GetType().FullName);

            isOpen = false;
            context.Close();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (isOpen)
                Close();

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!isDisposed)
                {
                    isDisposed = true;
                    host.ReleaseBackendContext(context);
                    context = null;
                }
            }
        }
    }
}
