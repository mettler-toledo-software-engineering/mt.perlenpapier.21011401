﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Exposes a method that allows to run the communcation interface services.
    /// </summary>
    public interface IEndPoint
    {
        /// <summary>
        /// Runs the communication interface services synchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for shutting down the service.</param>
        void Run(CancellationToken cancellationToken);
    }
}
