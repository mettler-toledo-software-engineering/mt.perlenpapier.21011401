﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Communication
{
    /// <summary>
    /// Marks a method as callable from a UnibaseEx client.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public sealed class ExtensionContractAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionContractAttribute"/> class.
        /// </summary>
        public ExtensionContractAttribute()
        {
        }
    }
}
