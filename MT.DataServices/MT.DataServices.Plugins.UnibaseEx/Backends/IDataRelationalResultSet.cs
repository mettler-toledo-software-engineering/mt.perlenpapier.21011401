﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Exposes the internally used <see cref="IDataReader"/> which allows for higher performance.
    /// </summary>
    public interface IDataRelationalResultSet : IRelationalResultSet
    {
        /// <summary>
        /// Gets the inner data reader.
        /// </summary>
        IDataReader Reader { get; }
    }
}
