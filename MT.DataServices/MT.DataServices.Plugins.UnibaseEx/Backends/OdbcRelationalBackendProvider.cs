﻿using System;
using System.ComponentModel.Composition;
using System.Data.Odbc;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql.Formatters;
using MT.DataServices.Plugins.UnibaseEx.Sql;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Exports the ODBC database backend as a MEF component.
    /// </summary>
    [Export(typeof(IRelationalBackendProvider))]
    public class OdbcRelationalBackendProvider : IRelationalBackendProvider
    {
        /// <summary>
        /// Gets the name of the relational backend.
        /// </summary>
        public string Name
        {
            get { return "ODBC"; }
        }

        /// <summary>
        /// Creates a new backend implementation with the given configuration.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="formatter">The instance that should be used to format the SQL commands or <c>null</c> if the default formatter should be used.</param>
        /// <returns>
        /// A newly created instance that implements the <see cref="IRelationalBackend" /> interface.
        /// </returns>
        public IRelationalBackend CreateBackend(string connectionString, ISqlFormatter formatter)
        {
            return new DbDataProvider(new OdbcConnection(connectionString), formatter ?? SqlServerFormatter.Instance);
        }
    }
}
