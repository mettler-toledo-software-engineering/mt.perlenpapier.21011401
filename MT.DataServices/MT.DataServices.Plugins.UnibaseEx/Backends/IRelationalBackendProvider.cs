﻿using System;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Provides metadata and exposes a factory method for creating an <see cref="IRelationalBackend"/> to communicate with a relational dbms.
    /// </summary>
    public interface IRelationalBackendProvider
    {
        /// <summary>
        /// Gets the name of the relational backend.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Creates a new backend implementation with the given configuration.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="formatter">The instance that should be used to format the SQL commands or <c>null</c> if the default formatter should be used.</param>
        /// <returns>A newly created instance that implements the <see cref="IRelationalBackend"/> interface.</returns>
        IRelationalBackend CreateBackend(string connectionString, ISqlFormatter formatter);
    }
}
