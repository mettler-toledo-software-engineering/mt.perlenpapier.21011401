﻿using System;
using System.Data;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Wraps an <see cref="IDataReader"/> inside a <see cref="IRelationalResultSet"/>.
    /// </summary>
    public class DataReaderResultSet : IDataRelationalResultSet
    {
        private readonly IDataReader reader;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataReaderResultSet"/> class.
        /// </summary>
        public DataReaderResultSet(IDataReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");

            this.reader = reader;
        }

        /// <summary>
        /// Gets the inner data reader.
        /// </summary>
        public IDataReader Reader
        {
            get { return reader; }
        }

        /// <summary>
        /// Gets the type of the field with the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>
        /// The managed type of the value in the element with the given index.
        /// </returns>
        public Type GetType(int index)
        {
            return reader.GetFieldType(index);
        }

        /// <summary>
        /// Gets the name of the column with the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>
        /// The name of the column with the given index.
        /// </returns>
        public string GetName(int index)
        {
            return reader.GetName(index);
        }

        /// <summary>
        /// Advances to the result of the next statement.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if a new result has been loaded; if no more additional results are available <c>false</c>.
        /// </returns>
        public bool NextResult()
        {
            return reader.NextResult();
        }

        /// <summary>
        /// Reads the new tuple from the result.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if a new tuple has been loaded; if no more additional tuples are available <c>false</c>.
        /// </returns>
        public bool Read()
        {
            return reader.Read();
        }

        /// <summary>
        /// Gets the number of affected rows.
        /// </summary>
        public int RecordsAffected
        {
            get { return reader.RecordsAffected; }
        }

        /// <summary>
        /// Gets the element count in the tuple.
        /// </summary>
        public int Count
        {
            get { return reader.FieldCount; }
        }

        /// <summary>
        /// Gets the value of the element in the currently active tuple with the specified name.
        /// </summary>
        /// <param name="name">The name of the field to read.</param>
        /// <returns></returns>
        public object Get(string name)
        {
            return reader.GetValue(reader.GetOrdinal(name));
        }

        /// <summary>
        /// Gets the value of the element in the currently active tuple with the specified index.
        /// </summary>
        /// <param name="index">The index of the field.</param>
        /// <returns></returns>
        public object Get(int index)
        {
            return reader.GetValue(index);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            reader.Dispose();
        }       
    }
}