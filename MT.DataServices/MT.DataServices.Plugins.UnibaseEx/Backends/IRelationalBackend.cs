﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Exposes a methods that allow to run queries against a relational dbms.
    /// </summary>
    public interface IRelationalBackend : IDisposable
    {
        /// <summary>
        /// Executes the specified sequence of statements.
        /// </summary>
        /// <param name="statements">The statements to execute. Not null.</param>
        /// <param name="parameterValues">The query parameter values. Can be null if no parameter values are set.</param>
        /// <returns>
        /// The result of the query.
        /// </returns>
        IRelationalResultSet Execute(StatementSequence statements, IList<ParameterValue> parameterValues);

        /// <summary>
        /// Opens the connection to the backend.
        /// </summary>
        void Open();

        /// <summary>
        /// Closes the connection to the backend.
        /// </summary>
        void Close();
    }
}