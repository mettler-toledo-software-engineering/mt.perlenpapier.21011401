﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql;
using MT.DataServices.Plugins.UnibaseEx.Sql.SyntaxTree;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Implements the <see cref="IRelationalBackend"/> interface by wrapping the common System.Data types.
    /// </summary>
    public class DbDataProvider : IRelationalBackend
    {
        private readonly IDbConnection connection;
        private readonly ISqlFormatter formatter;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbDataProvider"/> class.
        /// </summary>
        public DbDataProvider(IDbConnection connection, ISqlFormatter formatter)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");

            if (formatter == null)
                throw new ArgumentNullException("formatter");

            this.connection = connection;
            this.formatter = formatter;
        }

        /// <summary>
        /// Gets the formatter.
        /// </summary>
        public ISqlFormatter Formatter
        {
            get { return formatter; }
        }

        /// <summary>
        /// Gets the database connection.
        /// </summary>
        public IDbConnection Connection
        {
            get { return connection; }
        }

        /// <summary>
        /// Executes the specified sequence of statements.
        /// </summary>
        /// <param name="statements">The statements to execute. Not null.</param>
        /// <param name="parameterValues">The query parameter values. Can be null if no parameter values are set.</param>
        /// <returns>
        /// The result of the query.
        /// </returns>
        public virtual IRelationalResultSet Execute(StatementSequence statements, IList<ParameterValue> parameterValues)
        {
            IDbCommand command = connection.CreateCommand();
            command.CommandText = formatter.FormatStatement(statements);

            foreach (var parameterValue in parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = parameterValue.Name;
                parameter.Value = parameterValue.Value;
                command.Parameters.Add(parameter);
            }

            return new DataReaderResultSet(command.ExecuteReader());
        }
       
        /// <summary>
        /// Opens the connection to the backend.
        /// </summary>
        public virtual void Open()
        {
            connection.Open();
        }

        /// <summary>
        /// Closes the connection to the backend.
        /// </summary>
        public virtual void Close()
        {
            connection.Close();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            connection.Dispose();
        }
    }
}
