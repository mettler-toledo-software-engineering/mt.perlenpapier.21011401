﻿using System;
using System.ComponentModel.Composition;
using System.Data.SqlClient;
using System.Linq;
using MT.DataServices.Plugins.UnibaseEx.Sql;
using MT.DataServices.Plugins.UnibaseEx.Sql.Formatters;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Exports the SqlServer database backend as a MEF component.
    /// </summary>
    [Export(typeof(IRelationalBackendProvider))]
    public class SqlRelationalBackendProvider : IRelationalBackendProvider
    {
        /// <summary>
        /// Gets the name of the relational backend.
        /// </summary>
        public string Name
        {
            get { return "Microsoft SQL Server"; }
        }

        /// <summary>
        /// Creates a new backend implementation with the given configuration.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="formatter">The instance that should be used to format the SQL commands or <c>null</c> if the default formatter should be used.</param>
        /// <returns>
        /// A newly created instance that implements the <see cref="IRelationalBackend" /> interface.
        /// </returns>
        public IRelationalBackend CreateBackend(string connectionString, ISqlFormatter formatter)
        {
            return new DbDataProvider(new SqlConnection(connectionString), formatter ?? SqlServerFormatter.Instance);
        }
    }
}
