﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Represents a name/value pair for specifying the value of a query parameter.
    /// </summary>
    public struct ParameterValue
    {
        private readonly string name;
        private readonly object value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterValue" /> class.
        /// </summary>
        /// <param name="name">The name of the query parameter.</param>
        /// <param name="value">The value that should be substituted.</param>
        public ParameterValue(string name, object value)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            this.name = name;
            this.value = value;
        }

        /// <summary>
        /// Gets the name of the query parameter.
        /// </summary>
        public String Name
        {
            get { return name; }
        }

        /// <summary>
        /// Gets the value that should be substituted.
        /// </summary>
        public object Value
        {
            get { return value; }
        }
    }
}