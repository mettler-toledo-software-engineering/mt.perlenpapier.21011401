﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Backends
{
    /// <summary>
    /// Provides access to the results of query executed by a <see cref="IRelationalBackend"/>
    /// </summary>
    public interface IRelationalResultSet : IDisposable
    {
        /// <summary>
        /// Advances to the result of the next statement.
        /// </summary>
        /// <returns><c>true</c> if a new result has been loaded; if no more additional results are available <c>false</c>.</returns>
        bool NextResult();

        /// <summary>
        /// Reads the new tuple from the result.
        /// </summary>
        /// <returns><c>true</c> if a new tuple has been loaded; if no more additional tuples are available <c>false</c>.</returns>
        bool Read();

        /// <summary>
        /// Gets the number of affected rows.
        /// </summary>
        int RecordsAffected { get; }

        /// <summary>
        /// Gets the element count in the tuple.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Gets the type of the field with the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The managed type of the value in the element with the given index.</returns>
        Type GetType(int index);

        /// <summary>
        /// Gets the name of the column with the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The name of the column with the given index.</returns>
        string GetName(int index);

        /// <summary>
        /// Gets the value of the element in the currently active tuple with the specified name.
        /// </summary>
        /// <param name="name">The name of the field to read.</param>
        object Get(string name);

        /// <summary>
        /// Gets the value of the element in the currently active tuple with the specified index.
        /// </summary>
        /// <param name="index">The index of the field.</param>
        object Get(int index);
    }
}