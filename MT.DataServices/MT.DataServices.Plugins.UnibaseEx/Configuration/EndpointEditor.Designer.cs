﻿namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    partial class EndpointEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.endpointTypeComboBox = new System.Windows.Forms.ComboBox();
            this.databaseConnectionComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.endpointConfiguratorGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.endpointTypeComboBox);
            this.groupBox1.Controls.Add(this.databaseConnectionComboBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 96);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allgemeine Einstellungen";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Endpunkt Typ:";
            // 
            // endpointTypeComboBox
            // 
            this.endpointTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.endpointTypeComboBox.FormattingEnabled = true;
            this.endpointTypeComboBox.Location = new System.Drawing.Point(150, 57);
            this.endpointTypeComboBox.Name = "endpointTypeComboBox";
            this.endpointTypeComboBox.Size = new System.Drawing.Size(190, 21);
            this.endpointTypeComboBox.TabIndex = 1;
            this.endpointTypeComboBox.SelectedValueChanged += new System.EventHandler(this.OnEndpointTypeComboBoxSelectedValueChanged);
            // 
            // databaseConnectionComboBox
            // 
            this.databaseConnectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.databaseConnectionComboBox.FormattingEnabled = true;
            this.databaseConnectionComboBox.Location = new System.Drawing.Point(150, 27);
            this.databaseConnectionComboBox.Name = "databaseConnectionComboBox";
            this.databaseConnectionComboBox.Size = new System.Drawing.Size(190, 21);
            this.databaseConnectionComboBox.TabIndex = 1;
            this.databaseConnectionComboBox.SelectedValueChanged += new System.EventHandler(this.OnDatabaseConnectionComboBoxSelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datenbankverbindung:";
            // 
            // endpointConfiguratorGroupBox
            // 
            this.endpointConfiguratorGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.endpointConfiguratorGroupBox.Location = new System.Drawing.Point(3, 105);
            this.endpointConfiguratorGroupBox.Name = "endpointConfiguratorGroupBox";
            this.endpointConfiguratorGroupBox.Size = new System.Drawing.Size(384, 152);
            this.endpointConfiguratorGroupBox.TabIndex = 1;
            this.endpointConfiguratorGroupBox.TabStop = false;
            this.endpointConfiguratorGroupBox.Text = "Verbindungseinstellungen";
            // 
            // EndpointEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.endpointConfiguratorGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Name = "EndpointEditor";
            this.Size = new System.Drawing.Size(390, 260);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox endpointTypeComboBox;
        private System.Windows.Forms.ComboBox databaseConnectionComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox endpointConfiguratorGroupBox;
    }
}
