﻿using System;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Plugins.UnibaseEx.Utilities;
using System.Drawing;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    internal partial class EndpointEditor : UserControl, IEndPointConfigurationHost
    {
        private readonly ServiceProviderRegistry registry;
        private EndPointConfiguration endPointConfiguration;

        internal EndpointEditor(
            UnibaseExConfiguration configuration,
            ServiceProviderRegistry registry,
            EndPointConfiguration endPointConfiguration)
        {
            InitializeComponent();

            this.registry = registry;
            this.endPointConfiguration = endPointConfiguration;

            Bind(databaseConnectionComboBox, configuration.Connections.Select(c => new NameValuePair(c.Name, c.Id) { Tag = c }).ToList());
            Bind(endpointTypeComboBox, registry.EndpointProviders.Select(c => new NameValuePair(c.Name, c.GetType().FullName) { Tag = c }).ToList());

            if (endPointConfiguration.ConnectionId != null)
                databaseConnectionComboBox.SelectedValue = endPointConfiguration.ConnectionId;

            if (endPointConfiguration.EndpointType != null)
                endpointTypeComboBox.SelectedValue = endPointConfiguration.EndpointType;            
        }

        private void Bind(ComboBox comboBox, object dataSource)
        {
            comboBox.DisplayMember = "Text";
            comboBox.ValueMember = "Value";
            comboBox.DataSource = dataSource;
        }

        private void OnDatabaseConnectionComboBoxSelectedValueChanged(object sender, EventArgs e)
        {
            endPointConfiguration.ConnectionId = (string)databaseConnectionComboBox.SelectedValue;
        }

        private void OnEndpointTypeComboBoxSelectedValueChanged(object sender, EventArgs e)
        {
            var provider = registry.EndpointProviders.FirstOrDefault(d => d.GetType().FullName == endpointTypeComboBox.SelectedValue as string);
            if (provider != null)
            {
                endPointConfiguration.EndpointType = (string)endpointTypeComboBox.SelectedValue;

                Control control;
                try
                {
                    control = provider.CreateConfigurationControl(this);
                }
                catch (Exception ex)
                {
                    control = new Label() { 
                        Text = "Beim Erstellen des Editors ist ein Fehler aufgetreten: " + ex.Message, 
                        AutoSize = false,
                        ForeColor = Color.DarkRed
                    };
                }

                endpointConfiguratorGroupBox.Controls.Clear();
                endpointConfiguratorGroupBox.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }
        }

        /// <summary>
        /// Gets or sets the XAML serializable object containing the configuration data.
        /// </summary>
        public object ConfigurationObject
        {
            get { return endPointConfiguration.EndpointConfiguration; }
            set { endPointConfiguration.EndpointConfiguration = value; }
        }
    }
}
