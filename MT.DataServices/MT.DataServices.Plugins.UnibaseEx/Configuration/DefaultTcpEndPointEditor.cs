﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MT.DataServices.Plugins.UnibaseEx.Communication.Default;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    internal partial class DefaultTcpEndPointEditor : UserControl
    {
        private readonly IEndPointConfigurationHost configurationHost;
        private readonly TcpEndPointConfiguration configuration;

        internal DefaultTcpEndPointEditor(IEndPointConfigurationHost configurationHost)
        {
            InitializeComponent();

            this.configurationHost = configurationHost;

            this.configuration = configurationHost.ConfigurationObject as TcpEndPointConfiguration;
            if (configuration == null)
            {
                configuration = new TcpEndPointConfiguration();
                configurationHost.ConfigurationObject = configuration;
            }

            portNumericUpDown.Value = configuration.Port;
            concurrentConnectionsNumericUpDown.Value = configuration.ConcurrentConnections;
        }

        private void OnPortNumericUpDownValueChanged(object sender, EventArgs e)
        {
            configuration.Port = (int)portNumericUpDown.Value;
        }

        private void OnConcurrentConnectionsNumericUpDownValueChanged(object sender, EventArgs e)
        {
            configuration.ConcurrentConnections = (int)concurrentConnectionsNumericUpDown.Value;
        }
    }
}
