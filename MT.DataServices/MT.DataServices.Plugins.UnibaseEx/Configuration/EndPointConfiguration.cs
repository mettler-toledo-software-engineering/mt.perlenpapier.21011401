﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    /// <summary>
    /// Contains the configuration of a connection end point.
    /// </summary>
    public class EndPointConfiguration
    {
        /// <summary>
        /// Gets or sets the name of this endpoint.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id of the <see cref="ConnectionConfiguration"/> that should be used to connect to the backend.
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// Gets or sets the full name of the endpoint type.
        /// </summary>
        public string EndpointType { get; set; }

        /// <summary>
        /// Gets or sets the endpoint configuration object specific to the endpoint type.
        /// </summary>
        public object EndpointConfiguration { get; set; }
    }
}
