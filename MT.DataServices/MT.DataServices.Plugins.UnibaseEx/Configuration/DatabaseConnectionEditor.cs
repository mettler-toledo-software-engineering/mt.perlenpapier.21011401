﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    /// <summary>
    /// 
    /// </summary>
    internal partial class DatabaseConnectionEditor : UserControl
    {
        private List<NameValuePair> backendProviders;
        private List<NameValuePair> sqlFormatterProviders;

        internal DatabaseConnectionEditor(UnibaseExConfiguration configuration, ServiceProviderRegistry registry)
        {
            InitializeComponent();

            backendProviders = registry.RelationalBackends.Select(d => new NameValuePair(d.Name, d.GetType().FullName) { Tag = d }).ToList();
            sqlFormatterProviders = registry.SqlFormatters.Select(d => new NameValuePair(d.Name, d.GetType().FullName) { Tag = d }).ToList();

            sqlFormatterProviders.Insert(0, new NameValuePair("Keiner", ConnectionConfiguration.DisabledSqlFormatter));
            sqlFormatterProviders.Insert(0, new NameValuePair("Standard", ConnectionConfiguration.StandardSqlFormatter));

            Bind(backendProviderColumn, backendProviders);
            Bind(sqlFormatterColumn, sqlFormatterProviders);

            connectionConfigurationBindingSource.DataSource = configuration.Connections;
        }

        private void Bind(DataGridViewComboBoxColumn column, object dataSource)
        {
            column.DataSource = dataSource;
            column.DisplayMember = "Text";
            column.ValueMember = "Value";
        }

        private void OnAddConnectionButtonClick(object sender, EventArgs e)
        {
            connectionConfigurationBindingSource.Add(new ConnectionConfiguration() {
                Id = Guid.NewGuid().ToString("N"),
                Name = "Neue Verbindung",
                BackendDataProvider = backendProviders.Select(d => d.Value).FirstOrDefault(),
                SqlFormatter = sqlFormatterProviders.Select(d => d.Value).FirstOrDefault(),
                ConnectionString = ""
            });            
        }

        private void OnRemoveConnectionButtonClick(object sender, EventArgs e)
        {
            var current = connectionConfigurationBindingSource.Current as ConnectionConfiguration;
            if (current != null && MessageBox.Show(string.Format("Sind sie sicher, dass sie die Verbindung \"{0}\" löschen möchten?", current.Name), ProductInformation.FullName, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                connectionConfigurationBindingSource.RemoveCurrent();
            }
        }
    }
}
