﻿namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    partial class DatabaseConnectionEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.connectionConfigurationDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backendProviderColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sqlFormatterColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.connectionConfigurationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.removeConnectionButton = new System.Windows.Forms.Button();
            this.addConnectionButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.connectionConfigurationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionConfigurationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datenbankverbindungen:";
            // 
            // connectionConfigurationDataGridView
            // 
            this.connectionConfigurationDataGridView.AllowUserToAddRows = false;
            this.connectionConfigurationDataGridView.AllowUserToDeleteRows = false;
            this.connectionConfigurationDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectionConfigurationDataGridView.AutoGenerateColumns = false;
            this.connectionConfigurationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.connectionConfigurationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.backendProviderColumn,
            this.sqlFormatterColumn,
            this.dataGridViewTextBoxColumn5});
            this.connectionConfigurationDataGridView.DataSource = this.connectionConfigurationBindingSource;
            this.connectionConfigurationDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.connectionConfigurationDataGridView.Location = new System.Drawing.Point(6, 22);
            this.connectionConfigurationDataGridView.Name = "connectionConfigurationDataGridView";
            this.connectionConfigurationDataGridView.Size = new System.Drawing.Size(603, 313);
            this.connectionConfigurationDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // backendProviderColumn
            // 
            this.backendProviderColumn.DataPropertyName = "BackendDataProvider";
            this.backendProviderColumn.HeaderText = "Verbindungsprovider";
            this.backendProviderColumn.Name = "backendProviderColumn";
            this.backendProviderColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.backendProviderColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.backendProviderColumn.Width = 150;
            // 
            // sqlFormatterColumn
            // 
            this.sqlFormatterColumn.DataPropertyName = "SqlFormatter";
            this.sqlFormatterColumn.HeaderText = "SQL Formatierer";
            this.sqlFormatterColumn.Name = "sqlFormatterColumn";
            this.sqlFormatterColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sqlFormatterColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sqlFormatterColumn.Width = 150;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ConnectionString";
            this.dataGridViewTextBoxColumn5.HeaderText = "Connection String";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.Width = 300;
            // 
            // connectionConfigurationBindingSource
            // 
            this.connectionConfigurationBindingSource.DataSource = typeof(MT.DataServices.Plugins.UnibaseEx.Configuration.ConnectionConfiguration);
            // 
            // removeConnectionButton
            // 
            this.removeConnectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.removeConnectionButton.Image = global::MT.DataServices.Plugins.UnibaseEx.Properties.Resources.Delete;
            this.removeConnectionButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.removeConnectionButton.Location = new System.Drawing.Point(170, 341);
            this.removeConnectionButton.Name = "removeConnectionButton";
            this.removeConnectionButton.Size = new System.Drawing.Size(157, 28);
            this.removeConnectionButton.TabIndex = 3;
            this.removeConnectionButton.Text = "Verbindung löschen";
            this.removeConnectionButton.UseVisualStyleBackColor = true;
            this.removeConnectionButton.Click += new System.EventHandler(this.OnRemoveConnectionButtonClick);
            // 
            // addConnectionButton
            // 
            this.addConnectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addConnectionButton.Image = global::MT.DataServices.Plugins.UnibaseEx.Properties.Resources.Add;
            this.addConnectionButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addConnectionButton.Location = new System.Drawing.Point(6, 341);
            this.addConnectionButton.Name = "addConnectionButton";
            this.addConnectionButton.Size = new System.Drawing.Size(158, 28);
            this.addConnectionButton.TabIndex = 3;
            this.addConnectionButton.Text = "Verbindung hinzufügen";
            this.addConnectionButton.UseVisualStyleBackColor = true;
            this.addConnectionButton.Click += new System.EventHandler(this.OnAddConnectionButtonClick);
            // 
            // DatabaseConnectionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.removeConnectionButton);
            this.Controls.Add(this.addConnectionButton);
            this.Controls.Add(this.connectionConfigurationDataGridView);
            this.Controls.Add(this.label1);
            this.Name = "DatabaseConnectionEditor";
            this.Size = new System.Drawing.Size(612, 372);
            ((System.ComponentModel.ISupportInitialize)(this.connectionConfigurationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionConfigurationBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource connectionConfigurationBindingSource;
        private System.Windows.Forms.DataGridView connectionConfigurationDataGridView;
        private System.Windows.Forms.Button addConnectionButton;
        private System.Windows.Forms.Button removeConnectionButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn backendProviderColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn sqlFormatterColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    }
}
