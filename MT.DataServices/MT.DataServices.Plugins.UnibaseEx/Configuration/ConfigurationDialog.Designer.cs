﻿namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    partial class ConfigurationDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("UnibaseEx");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationDialog));
            this.rootContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addEndPointMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.optionsTreeView = new System.Windows.Forms.TreeView();
            this.treeViewImageList = new System.Windows.Forms.ImageList(this.components);
            this.detailsPanel = new System.Windows.Forms.Panel();
            this.endpointContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rootContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.endpointContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // rootContextMenuStrip
            // 
            this.rootContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEndPointMenuItem});
            this.rootContextMenuStrip.Name = "rootContextMenuStrip";
            this.rootContextMenuStrip.Size = new System.Drawing.Size(189, 26);
            // 
            // addEndPointMenuItem
            // 
            this.addEndPointMenuItem.Image = global::MT.DataServices.Plugins.UnibaseEx.Properties.Resources.Add;
            this.addEndPointMenuItem.Name = "addEndPointMenuItem";
            this.addEndPointMenuItem.Size = new System.Drawing.Size(188, 22);
            this.addEndPointMenuItem.Text = "Endpunkt hinzufügen";
            this.addEndPointMenuItem.Click += new System.EventHandler(this.OnAddEndPointMenuItemClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.optionsTreeView);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.detailsPanel);
            this.splitContainer1.Size = new System.Drawing.Size(799, 452);
            this.splitContainer1.SplitterDistance = 211;
            this.splitContainer1.TabIndex = 0;
            // 
            // optionsTreeView
            // 
            this.optionsTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optionsTreeView.ImageIndex = 0;
            this.optionsTreeView.ImageList = this.treeViewImageList;
            this.optionsTreeView.LabelEdit = true;
            this.optionsTreeView.Location = new System.Drawing.Point(3, 3);
            this.optionsTreeView.Name = "optionsTreeView";
            treeNode1.ContextMenuStrip = this.rootContextMenuStrip;
            treeNode1.ImageKey = "Database";
            treeNode1.Name = "";
            treeNode1.SelectedImageKey = "Database";
            treeNode1.StateImageKey = "(none)";
            treeNode1.Text = "UnibaseEx";
            this.optionsTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.optionsTreeView.SelectedImageIndex = 0;
            this.optionsTreeView.Size = new System.Drawing.Size(205, 446);
            this.optionsTreeView.TabIndex = 0;
            this.optionsTreeView.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.OnOptionsTreeViewBeforeLabelEdit);
            this.optionsTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.OnOptionsTreeViewAfterLabelEdit);
            this.optionsTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.OnOptionsTreeViewAfterSelect);
            // 
            // treeViewImageList
            // 
            this.treeViewImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeViewImageList.ImageStream")));
            this.treeViewImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeViewImageList.Images.SetKeyName(0, "Endpoint");
            this.treeViewImageList.Images.SetKeyName(1, "Database");
            // 
            // detailsPanel
            // 
            this.detailsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsPanel.Location = new System.Drawing.Point(0, 0);
            this.detailsPanel.Name = "detailsPanel";
            this.detailsPanel.Size = new System.Drawing.Size(584, 452);
            this.detailsPanel.TabIndex = 0;
            // 
            // endpointContextMenuStrip
            // 
            this.endpointContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameMenuItem,
            this.removeMenuItem});
            this.endpointContextMenuStrip.Name = "endpointContextMenuStrip";
            this.endpointContextMenuStrip.Size = new System.Drawing.Size(147, 48);
            // 
            // renameMenuItem
            // 
            this.renameMenuItem.Name = "renameMenuItem";
            this.renameMenuItem.Size = new System.Drawing.Size(146, 22);
            this.renameMenuItem.Text = "Umbenennen";
            this.renameMenuItem.Click += new System.EventHandler(this.OnRenameMenuItemClick);
            // 
            // removeMenuItem
            // 
            this.removeMenuItem.Image = global::MT.DataServices.Plugins.UnibaseEx.Properties.Resources.Delete;
            this.removeMenuItem.Name = "removeMenuItem";
            this.removeMenuItem.Size = new System.Drawing.Size(146, 22);
            this.removeMenuItem.Text = "Entfernen";
            this.removeMenuItem.Click += new System.EventHandler(this.OnRemoveMenuItemClick);
            // 
            // ConfigurationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "ConfigurationDialog";
            this.Size = new System.Drawing.Size(799, 452);
            this.rootContextMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.endpointContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView optionsTreeView;
        private System.Windows.Forms.ImageList treeViewImageList;
        private System.Windows.Forms.Panel detailsPanel;
        private System.Windows.Forms.ContextMenuStrip rootContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addEndPointMenuItem;
        private System.Windows.Forms.ContextMenuStrip endpointContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem renameMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeMenuItem;
    }
}
