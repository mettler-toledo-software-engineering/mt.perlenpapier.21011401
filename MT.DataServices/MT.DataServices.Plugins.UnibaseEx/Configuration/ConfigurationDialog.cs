﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MT.DataServices.Plugins.UnibaseEx.Utilities;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    internal partial class ConfigurationDialog : UserControl, IPluginConfigurationPane
    {
        private readonly UnibaseExConfiguration configuration;
        private readonly ServiceProviderRegistry registry;
        private readonly DatabaseConnectionEditor connectionEditor;
        private readonly Plugin plugin;
        private TreeNode rootNode;

        internal ConfigurationDialog(
            Plugin plugin,
            UnibaseExConfiguration configuration,
            IPluginConfigurationHost configurationHost,
            ServiceProviderRegistry registry)
        {
            this.plugin = plugin;
            InitializeComponent();

            this.registry = registry;
            this.configuration = configuration;
            
            rootNode = optionsTreeView.Nodes[0];
            
            connectionEditor = new DatabaseConnectionEditor(configuration, registry);
            rootNode.Tag = connectionEditor;

            foreach (var endpoint in configuration.EndPoints)
            {
                CreateNodeForEndPoint(endpoint);
            }

            optionsTreeView.SelectedNode = rootNode;
            configurationHost.FormClosed += OnConfigurationClosed;
        }

        void OnConfigurationClosed(object sender, PluginConfigurationHostClosingEventArgs e)
        {
            if (e.SaveChanges)
                plugin.SaveConfiguration();
            else
                plugin.LoadConfiguration();            
        }

        /// <summary>
        /// Gets the control that contains the configuration user interface.
        /// </summary>
        Control IPluginConfigurationPane.Control
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the title that should be displayed in the tab panel.
        /// </summary>
        public string Title
        {
            get { return "UnibaseEx"; }
        }

        private TreeNode CreateNodeForEndPoint(EndPointConfiguration endPointConfiguration)
        {
            var endPointNode = rootNode.Nodes.Add(endPointConfiguration.Name);
            endPointNode.Tag = endPointConfiguration;
            endPointNode.ImageKey = "Endpoint";
            endPointNode.ContextMenuStrip = endpointContextMenuStrip;

            rootNode.Expand();

            return endPointNode;
        }

        private void OnOptionsTreeViewAfterSelect(object sender, TreeViewEventArgs e)
        {
            var control = e.Node.Tag as Control;
            if (control != null)
            {
                detailsPanel.Controls.Clear();
                detailsPanel.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }
            else
            {
                var endPointConfiguration = e.Node.Tag as EndPointConfiguration;
                if (endPointConfiguration != null)
                {
                    control = new EndpointEditor(configuration, registry, endPointConfiguration);

                    detailsPanel.Controls.Clear();
                    detailsPanel.Controls.Add(control);
                    control.Dock = DockStyle.Fill;
                }
            }
        }

        private void OnAddEndPointMenuItemClick(object sender, EventArgs e)
        {
            var endPointConfiguration = new EndPointConfiguration()
            {
                ConnectionId = configuration.Connections.Select(d => d.Id).FirstOrDefault(),
                Name = "Neuer Endpunkt"
            };

            CreateNodeForEndPoint(endPointConfiguration);            

            configuration.EndPoints.Add(endPointConfiguration);
        }
          
        private void OnRenameMenuItemClick(object sender, EventArgs e)
        {
            if (optionsTreeView.SelectedNode != null)
                optionsTreeView.SelectedNode.BeginEdit();
        }

        private void OnRemoveMenuItemClick(object sender, EventArgs e)
        {
            EndPointConfiguration endPointConfiguration;
            if (optionsTreeView.SelectedNode != null && 
                (endPointConfiguration = optionsTreeView.SelectedNode.Tag as EndPointConfiguration) != null) 
            {
                var prompt = string.Format("Sind sie sicher, dass sie den Endpunkt \"{0}\" löschen möchten?", endPointConfiguration.Name);
                if (MessageBox.Show(prompt, ProductInformation.ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    configuration.EndPoints.Remove(endPointConfiguration);
                    optionsTreeView.SelectedNode.Remove();
                    optionsTreeView.SelectedNode = rootNode;
                }
            }
        }       

        private void OnOptionsTreeViewBeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            e.CancelEdit = e.Node.Tag as EndPointConfiguration == null;
        }

        private void OnOptionsTreeViewAfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            var endpointConfiguration = e.Node.Tag as EndPointConfiguration;
            if (endpointConfiguration != null && e.Label != null)
                endpointConfiguration.Name = e.Label;
            else
                e.CancelEdit = true;
        }
    }
}
