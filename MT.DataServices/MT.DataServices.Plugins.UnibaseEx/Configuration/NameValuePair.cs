﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    internal class NameValuePair
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NameValuePair"/> class.
        /// </summary>
        public NameValuePair(string text, string value)
        {
            Value = value;
            Text = text;
        }

        /// <summary>
        /// Gets the text that should be displayed.
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Gets the internal value representing this item.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Gets or sets a user-defined object that is attached to this instance.
        /// </summary>
        public object Tag { get; set; }
    }
}
