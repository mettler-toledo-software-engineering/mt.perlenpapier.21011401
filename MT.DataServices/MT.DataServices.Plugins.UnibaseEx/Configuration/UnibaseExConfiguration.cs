﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    /// <summary>
    /// Contains the configuration of the UnibaseEx plugin.
    /// </summary>
    public class UnibaseExConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnibaseExConfiguration"/> class.
        /// </summary>
        public UnibaseExConfiguration()
        {
            Connections = new Collection<ConnectionConfiguration>();
            EndPoints = new Collection<EndPointConfiguration>();
        }
  
        /// <summary>
        /// Gets the connections.
        /// </summary>
        public Collection<ConnectionConfiguration> Connections { get; private set; }

        /// <summary>
        /// Gets the end points.
        /// </summary>
        public Collection<EndPointConfiguration> EndPoints { get; private set; }
    }
}
