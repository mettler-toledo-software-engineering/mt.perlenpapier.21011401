﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    /// <summary>
    /// Exposes members that allow a configurator for an endpoint to communicate with its host environment.
    /// </summary>
    public interface IEndPointConfigurationHost
    {
        /// <summary>
        /// Gets or sets the XAML serializable object containing the configuration data.
        /// </summary>
        object ConfigurationObject { get; set; }
    }
}
