﻿namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    partial class DefaultTcpEndPointEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.concurrentConnectionsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.portNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.concurrentConnectionsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.portNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "Maximale Anzahl gleichzeitiger Verbindungen:";
            // 
            // concurrentConnectionsNumericUpDown
            // 
            this.concurrentConnectionsNumericUpDown.Location = new System.Drawing.Point(150, 40);
            this.concurrentConnectionsNumericUpDown.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.concurrentConnectionsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.concurrentConnectionsNumericUpDown.Name = "concurrentConnectionsNumericUpDown";
            this.concurrentConnectionsNumericUpDown.Size = new System.Drawing.Size(61, 20);
            this.concurrentConnectionsNumericUpDown.TabIndex = 2;
            this.concurrentConnectionsNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.concurrentConnectionsNumericUpDown.ValueChanged += new System.EventHandler(this.OnConcurrentConnectionsNumericUpDownValueChanged);
            // 
            // portNumericUpDown
            // 
            this.portNumericUpDown.Location = new System.Drawing.Point(150, 3);
            this.portNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.portNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.portNumericUpDown.Name = "portNumericUpDown";
            this.portNumericUpDown.Size = new System.Drawing.Size(61, 20);
            this.portNumericUpDown.TabIndex = 2;
            this.portNumericUpDown.Value = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.portNumericUpDown.ValueChanged += new System.EventHandler(this.OnPortNumericUpDownValueChanged);
            // 
            // DefaultTcpEndPointEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.portNumericUpDown);
            this.Controls.Add(this.concurrentConnectionsNumericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DefaultTcpEndPointEditor";
            this.Size = new System.Drawing.Size(303, 100);
            ((System.ComponentModel.ISupportInitialize)(this.concurrentConnectionsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.portNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown concurrentConnectionsNumericUpDown;
        private System.Windows.Forms.NumericUpDown portNumericUpDown;
    }
}
