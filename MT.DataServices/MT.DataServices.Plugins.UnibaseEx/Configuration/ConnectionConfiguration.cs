﻿using System;
using System.Linq;

namespace MT.DataServices.Plugins.UnibaseEx.Configuration
{
    /// <summary>
    /// Contains the configuration details of database connection.
    /// </summary>
    public class ConnectionConfiguration
    {
        internal static readonly string DisabledSqlFormatter = "Disabled";
        internal static readonly string StandardSqlFormatter = "Standard";

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the connection.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the backend data provider type name.
        /// </summary>
        public string BackendDataProvider { get; set; }

        /// <summary>
        /// Gets or sets the SQL formatter type name.
        /// </summary>
        public string SqlFormatter { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
