﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MT.TerminalIND930
{
    [Serializable]
    public class LocalizationInfo
    {
        public static readonly LocalizationInfo Default = new LocalizationInfo();
        public static Dictionary<string, LocalizationInfo> Localizations { get; private set; }
        public static List<LocalizationInfo> LocalizationsList { get; private set; }
        static LocalizationInfo()
        {
            Localizations = new Dictionary<string, LocalizationInfo>();
            LocalizationsList = new List<LocalizationInfo>();
        }

        public static void Export()
        {
            if (!File.Exists(@"C:\Temp\Localization.xml"))
            {
                using (var fileStream = new FileStream(@"C:\Temp\Localization.xml", FileMode.Create))
                {
                    XmlSerializer seri = new XmlSerializer(typeof(List<LocalizationInfo>));
                    List<LocalizationInfo> infoList = new List<LocalizationInfo>();
                    infoList.Add(new LocalizationInfo());
                    seri.Serialize(fileStream, infoList);
                }
            }
        }

        public static void Load()
        {
            using (var fileStream = new FileStream(@"C:\Temp\Localization.xml", FileMode.Open))
            {
                XmlSerializer seri = new XmlSerializer(typeof(List<LocalizationInfo>));
                var infoList = (List<LocalizationInfo>)seri.Deserialize(fileStream);
                foreach (var item in infoList)
                {
                    Localizations.Add(item.LanguageId, item);
                    LocalizationsList.Add(item);
                }
            }
        }

        [XmlAttribute("langId")]
        public string LanguageId { get; set; }

        public string Language { get; set; }

        public string WelcomeText { get; set; }
        public string EnterYourReferenceNo { get; set; }
        public string Continue { get; set; }
        public string Remove { get; set; }
        public string DriveOnScale { get; set; }
        public string MudDelivery { get; set; }
        public string ManOrder { get; set; }
        public string PleaseWait { get; set; }
        public string GoToThePorter { get; set; }
        public string EnterTruckLicensePlateInfo { get; set; }
        public string EnterTrailerLicensePlateInfo { get; set; }

        public string RegistrationPending { get; set; }
        public string SecondRegistration { get; set; }
        public string ChooseCarrier { get; set; }
        public string ChooseShipTo { get; set; }
        public string ChooseMaterial { get; set; }

        public string Yes { get; set; }
        public string No { get; set; }
        public string Close { get; set; }
        public string OK { get; set; }
        public string Cancel { get; set; }
        public string LeaveMessage { get; set; }
        public string ScanOrder { get; set; }
        public string ScanOrder2 { get; set; }
        public string WaitMessage { get; set; }
        public string RePrint { get; set; }
        public string TechnicalExceptionOccured { get; set; }
        public string CancelCheckInQuestion { get; set; }
        public string Message2 { get; set; }



        public LocalizationInfo()
        {
            LanguageId = "DE";
            Language = "Deutsch";
            WelcomeText = "Willkommen bei Perlen Papier AG";
            EnterYourReferenceNo = "Geben sie ihre Referenznummer(n) ein:";
            Continue = "Weiter";
            Remove = "Entfernen";
            DriveOnScale = "Bitte fahren Sie auf die Waage.";
            MudDelivery = "Schlamm/Asche";
            ManOrder = "Eingabe Bestell - Nr.";
            PleaseWait = "Bitte warten...";
            EnterTruckLicensePlateInfo = "Autokennzeichen des Zugfahrzeugs";
            EnterTrailerLicensePlateInfo = "Autokennzeichen des Anhängers";
            RegistrationPending = " Bestell - Nr.bereits registriert";
            SecondRegistration = "Zweite Anmeldung";
            GoToThePorter = "Gehen sie bitte zum Pförtner";
            ChooseCarrier = "Spediteur auswählen";
            ChooseShipTo = "Warenempfänger auswählen";
            ChooseMaterial = "Material auswählen";
            Yes = "Ja";
            No = "Nein";
            Close = "Schliessen";
            OK = "OK";
            Cancel = "Abbrechen";
            LeaveMessage = " Bitte Ticket entnehmen und weiterfahren!";
            ScanOrder = "Scannen Sie ihre Bestellnummer ein";
            ScanOrder2 = "Int. Problem: Bitte nochmals anmelden!";
            WaitMessage = "Bitte weiterfahren!";
            RePrint = "Ticket nachdrucken";
            TechnicalExceptionOccured = "Ein technischer Fehler ist aufgetreten.";
            CancelCheckInQuestion = "Möchten sie die Anmeldung abbrechen?";
            Message2 = "Dummy";
        }
    }
}
