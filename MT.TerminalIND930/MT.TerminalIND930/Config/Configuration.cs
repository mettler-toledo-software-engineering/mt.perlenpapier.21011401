﻿using System.ComponentModel;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace MT.TerminalIND930.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {
        public Configuration()
        {
        }

        
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("http://10.41.7.9:5000/IND930hub")]
        public virtual string ServerURL
        {
            get { return (string)GetLocal(); }
            set
            {
                SetLocal(value); 
                
            }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(false)]
        public virtual bool ScaleTareOn
        {
            get { return (bool)GetLocal(); }
            set
            {
                SetLocal(value);

            }
        }
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(false)]
        public virtual bool DebugOn
        {
            get { return (bool)GetLocal(); }
            set
            {
                SetLocal(value);

            }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

    }
}
