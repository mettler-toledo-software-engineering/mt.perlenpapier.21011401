﻿
using System;
using System.Threading.Tasks;
using MT.TerminalIND930.Update;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Presentation;

namespace MT.TerminalIND930.Config
{
    class SystemSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;
        private readonly IComponents _customerComponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public SystemSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.SystemConfig), configuration, customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                _configuration.UsbUpdateResult = "";
                // ServerURL
                var serverURLTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ServerURL);
                var serverURLTarget = new TextSetupMenuItem(_context, serverURLTitle, _configuration, "ServerURL");

                // Tare Tasten on/off
                var scaleKeysTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.ScaleKeys);
                var scaleKeysTarget = new CheckBoxSetupMenuItem(_context, scaleKeysTitle, _configuration, "ScaleTareOn");

                //// Tare Tasten on/off
                //var debugTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DebugText);
                //var debugTarget = new CheckBoxSetupMenuItem(_context, debugTitle, _configuration, "DebugOn");

                // USB Update
                var usbUpdateResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.UpdateStatus);
                var usbUpdateResultTarget = new TextBlockSetupMenuItem(_context, usbUpdateResultTitle, _configuration, "UsbUpdateResult");

                var button = new ImageButtonSetupMenuItem(_context, "embedded://MT.TerminalIND930/MT.TerminalIND930.Images.FlashDisk.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, LocalSystemUpdate);

                var rangeGroup1 = new GroupedSetupMenuItems(_context,  scaleKeysTarget);

                var rangeGroup4 = new GroupedSetupMenuItems(_context, usbUpdateResultTarget);

                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup4, button);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error System Setup Node.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }
        private void LocalSystemUpdate()
        {
            bool success;
            success = SystemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.UsbUpdateResult = "Update copied.";
            }
            else
            {
                _configuration.UsbUpdateResult = "Update failed!";
            }
        }

    }
}
