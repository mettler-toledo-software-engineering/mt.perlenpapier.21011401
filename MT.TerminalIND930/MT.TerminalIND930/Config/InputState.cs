﻿using System;

using System.Collections.Generic;
using System.Text;

namespace MT.TerminalIND930.Config
{
     enum InputState
    {
        Init = 1,
        WaitOnLoadScale,
        InputNr,
        WaitOnFPNumber,
        WaitOnSPNumber,
        SignOn,
        WaitOnSignOnReturn,
        WaitOnAlibiReturn,
        WaitOnAllCarrierReturn,
        WaitOnChooseCarrierReturn,
        SendWoodInWeight,
        WaitOnAlibiReturn1,
        WaitOnMessageBox1,
        WaitOnSendWeightReturn,
        WaitOnChooseMudDataReturn,
        WaitOnMudDataReturn,
        WaitOnAlibiReturn2,
        WaitOnReprintReturn,
        WaitOnUnloadScale

    }
}
