﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using MT.Singularity.Composition;
using MT.Singularity.Presentation.Controls;
using MT.TerminalIND930.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System.Threading.Tasks;
using MT.TerminalIND930.View;
using System.Timers;
using log4net;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.LogInfrastructure;
using MT.Singularity.Platform.Memories;
using MT.Singularity.Presentation;
using MT.TerminalIND930.Barcode;
using MT.TerminalIND930.Config;
using MT.TerminalIND930.Data;

namespace MT.TerminalIND930.ViewModel
{
    public class StartPageViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(StartPageViewModel);
        public Visual _parent;
        private readonly CompositionContainer _container;
        private readonly Context _context;
        private readonly ISignalManager _signalManager;
        private readonly HubConnection _connection;
        private readonly INavigationService _navigationService;
#if DEBUG
        private ZebraBarcodeReader _barcodeService;
#else
        private BarcodeReader _barcodeService;
#endif
        private DigitalIo _digitalIo;
        private Task<TerminalIND930ResultSignOn> _result = null;
        private Task<TerminalIND930ResultWeight> _result1 = null;
        private Task<TerminalIND930ResultMudData> _result2 = null;
        private Task<DialogResult> _resultDialog = null;
        private IAlibiLogComponent _alibiLog;
        private InputState _state;
        private Task<LogInsertStatus> _logStatusAlibi;

        private string _force = "";
        private double _lastGrossWeight = 0;
        private int _scannerTimeout = 0;
        private readonly Timer _workingTimer = new Timer(1000);

        public StartPageViewModel(Visual parent, CompositionContainer container, INavigationService navigationService)
        {
            Logger.Info("StartPage start");
            _parent = parent;
            _container = container;
            _navigationService = navigationService;
            _context = _container.Resolve<Context>();
            _digitalIo = _container.Resolve<DigitalIo>();
            _signalManager = _context.SignalManager;
#if DEBUG
            _barcodeService = _container.Resolve<ZebraBarcodeReader>();
#else
            _barcodeService = _container.Resolve<BarcodeReader>();
#endif
            _signalManager.RegisterCallback<IBarcodeReaderSignal>(OnCustomSignal);
            _workingTimer.AutoReset = true;
            _workingTimer.Elapsed += _workingTimer_Elapsed;
            _state = InputState.Init;
            Localization = LocalizationInfo.LocalizationsList.First();
            InitializeAlibi();
            ClearValues();

            _connection = new HubConnectionBuilder()
#if DEBUG
                .WithUrl("http://localhost:5000/IND930hub")
#else
                    .WithUrl("http://10.41.7.9:5000/IND930hub")
                    //.WithUrl("http://192.168.1.136:5000/IND930hub")
                   // .WithUrl("http://192.168.174.55:5000/IND930hub")
#endif
                .ConfigureLogging(logging =>
                {
                    logging.AddConsole();
                    logging.SetMinimumLevel(LogLevel.Information);
                })
                .Build();

            _connection.Closed += async (error) =>
            {
                await Task.Delay(15000);
                await _connection.StartAsync();
            };
            _workingTimer.Start();
        }

        private async void InitializeAlibi()
        {
            var platformEngine = _container.Resolve<IPlatformEngine>();
            var logService = await platformEngine.GetLogServiceAsync();
            _alibiLog = await logService.GetAlibiLogComponentAsync();
        }

        private async void _workingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            _workingTimer.Interval = 250;
            if (_context._weightInformation == null)
                return;
            try
            {
                Debug.WriteLine(_state);
                //  Logger.InfoEx(_state, SourceClass);
                switch (_state)
                {
                    case InputState.Init:
                        ClearValues();
                        _digitalIo.SetLightSignalRed();
                        GoReprintVisibility = Visibility.Hidden;
                        UpdateLine2(nameof(Localization.DriveOnScale));
                        _state = InputState.WaitOnLoadScale;
                        break;
                    case InputState.WaitOnLoadScale:
                        if (_context._weightInformation.GrossWeight > 79)
                        {
                            await _barcodeService.StartScanning();
                            _scannerTimeout = 0;
                            UpdateLine2(nameof(Localization.ScanOrder));
                            _state = InputState.InputNr;
                            Logger.InfoEx($"Scale loaded...", SourceClass);
                        }
                        break;
                    case InputState.InputNr:
                        _scannerTimeout++;
                        if (!string.IsNullOrEmpty(_context.OrderNr) || !string.IsNullOrEmpty(_context.MudNr))
                        {
                            await _barcodeService.StopScanning();
                            UpdateLine2(nameof(Localization.EnterTruckLicensePlateInfo));
                            _resultDialog = PlateNrEntry(Localization.EnterTruckLicensePlateInfo);
                            _state = InputState.WaitOnFPNumber;
                        }

                        if (_context.MtId > 0)
                        {
                            await _barcodeService.StopScanning();
                            UpdateLine2(nameof(Localization.PleaseWait));
                            if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                            _logStatusAlibi = _alibiLog.SaveAlibiRecordAsync(1, UnitType.Display, _context.MtId.ToString());
                            Logger.InfoEx($"SaveAlibiRecordAsync called...", SourceClass);
                            _state = InputState.WaitOnAlibiReturn;
                        }

                        if (_context._weightInformation.GrossWeight < 80)
                        {
                            _barcodeService.StopScanning();
                            Logger.InfoEx($"Scale unloaded 1...", SourceClass);
                            _state = InputState.Init;
                        }
                        if (_scannerTimeout > 4 * 60 * 2.5)  // 4 pro Sek * 60 sek * 2.5 min
                        {                                   // Scanner schaltet nach 3 Min ab ! Retrigger
                            _barcodeService.StopScanning();
                            Logger.InfoEx($"Scanner Timeout...", SourceClass);
                            _state = InputState.WaitOnLoadScale;
                        }
                        break;
                    case InputState.WaitOnAlibiReturn:
                        if (_logStatusAlibi.IsCompleted)
                        {
                            _result1 = _connection.InvokeAsync<TerminalIND930ResultWeight>("SecondWeightIn",
                                _context.MtId, _logStatusAlibi.Result.RelatedWeight.GrossWeight, "O");
                            _state = InputState.WaitOnSendWeightReturn;
                            Logger.InfoEx($"SecondWeightIn called {_context.MtId} ...", SourceClass);
                        }
                        break;
                    case InputState.WaitOnFPNumber:
                        if (_resultDialog.IsCompleted)
                        {
                            if (_context._weightInformation.GrossWeight > 79)
                            {
                                if (_context.FPLicense.Length > 0)
                                {
                                    if (!String.IsNullOrEmpty(_context.OrderNr))
                                        _state = (_resultDialog.Result == DialogResult.OK) ? InputState.SignOn : InputState.Init;
                                    else if (_resultDialog.Result == DialogResult.OK)
                                    {
                                        UpdateLine2(nameof(Localization.PleaseWait));
                                        if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                                        _result2 = _connection.InvokeAsync<TerminalIND930ResultMudData>("MudData");
                                        _state = InputState.WaitOnMudDataReturn;
                                        Logger.ErrorEx($"MudData called...", SourceClass);
                                    }
                                    else
                                        _state = InputState.Init;
                                }
                                else
                                    _state = InputState.Init;
                            }
                            else
                                _state = InputState.Init;
                        }
                        break;
                    case InputState.SignOn:
                        if (_context.OrderNr.Length > 0 && _context.FPLicense.Length > 0)
                        {
                            UpdateLine2(nameof(Localization.PleaseWait));
                            _state = InputState.WaitOnSignOnReturn;
                            if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                            _result = _connection.InvokeAsync<TerminalIND930ResultSignOn>("SignOn",
                                _context.OrderNr, _context.FPLicense, _force);
                            Logger.InfoEx($"SignOn called...", SourceClass);
                        }
                        break;
                    case InputState.WaitOnSignOnReturn:
                        if (_result.IsCompleted)
                        {
                            switch (_result.Result.Status)
                            {
                                case 0: // Zweifach Anmeldung
                                    _resultDialog = InfoPopup.ShowAsync(_parent, Localization.RegistrationPending,
                                        Localization.SecondRegistration, MessageBoxIcon.Information, 
                                        MessageBoxButtons.YesNo);
                                    Logger.InfoEx($"Sign On return, second registration...", SourceClass);
                                    _state = InputState.WaitOnMessageBox1;
                                    break;
                                case -1: // Unbehebbare Fehlermeldung von SAP
                                    UpdateLine2(nameof(Localization.WaitMessage));
                                    _digitalIo.SetLightSignalGreen();
                                    _lastGrossWeight = _context._weightInformation.GrossWeight;
                                    _resultDialog = InfoPopup.ShowAsync(_parent, Localization.GoToThePorter, _result.Result.LiefName,
                                        MessageBoxIcon.Information, MessageBoxButtons.OK);
                                    Logger.InfoEx($"Sign On return, Status -1 ...", SourceClass);
                                    _state = InputState.WaitOnUnloadScale;
                                    break;
                                case 1: // Normale Anmeldung
                                    _context.MtId = _result.Result.Id;
                                    if (_result.Result.Carrier.Length == 1)
                                    {
                                        _context.AktCarrier = _result.Result.Carrier[0].Nr;
                                        _state = InputState.SendWoodInWeight;
                                    }
                                    else
                                    {
                                        _context.TerminalIND930ResultSignOn = _result.Result;
                                        _context.AktCarrier = "";
                                        _navigationService.NavigateTo(_container.Resolve<ChooseCarrierPage>());
                                        _state = InputState.WaitOnChooseCarrierReturn;
                                        Logger.InfoEx($"Sign On return, Multicarrier...", SourceClass);
                                    }
                                    break;
                            }
                        }
                        break;
                    case InputState.WaitOnChooseCarrierReturn:
                        if (!string.IsNullOrEmpty(_context.AktMaterial))
                        {
                            // Wenn hier Carrier leer = Abbruch
                            _state = (!string.IsNullOrEmpty(_context.AktCarrier))
                                ? InputState.SendWoodInWeight : _state = InputState.Init;
                        }
                        break;
                    case InputState.SendWoodInWeight:
                        if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                        _logStatusAlibi = _alibiLog.SaveAlibiRecordAsync(1, UnitType.Display, _context.MtId.ToString());
                        Logger.InfoEx($"SaveAlibiRecordAsync called...", SourceClass);
                        _state = InputState.WaitOnAlibiReturn1;
                        break;
                    case InputState.WaitOnAlibiReturn1:
                        if (_logStatusAlibi.IsCompleted)
                        {
                            _result1 = _connection.InvokeAsync<TerminalIND930ResultWeight>("WoodWeightIn",
                                _context.MtId, _logStatusAlibi.Result.RelatedWeight.GrossWeight, _context.AktCarrier);
                            _state = InputState.WaitOnSendWeightReturn;
                            Logger.InfoEx($"WoodWeightIn, {_context.MtId} ...", SourceClass);
                        }
                        break;
                    case InputState.WaitOnMessageBox1:
                        if (_resultDialog.IsCompleted)
                        {
                            if (_resultDialog.Result == DialogResult.Yes)
                            {
                                _force = "X";
                                _state = InputState.SignOn;
                                Logger.InfoEx($"WaitOnMessageBox1, second registration yes...", SourceClass);
                            }
                            else
                            {
                                UpdateLine2(nameof(Localization.WaitMessage));
                                _digitalIo.SetLightSignalGreen();
                                _lastGrossWeight = _context._weightInformation.GrossWeight;
                                _state = InputState.WaitOnUnloadScale;
                                Logger.InfoEx($"WaitOnMessageBox1, second registration no...", SourceClass);
                            }
                        }
                        break;
                    case InputState.WaitOnSendWeightReturn:
                        if (_result1.IsCompleted)
                        {
                            _context.MtId = _result1.Result.Id;
                            _digitalIo.SetLightSignalGreen();
                            GoReprintVisibility = Visibility.Visible;
                            UpdateLine2(nameof(Localization.LeaveMessage));
                            _lastGrossWeight = _context._weightInformation.GrossWeight;
                            _state = InputState.WaitOnUnloadScale;
                            Logger.InfoEx($"WaitOnSendWeightReturn completed, {_context.MtId}", SourceClass);
                        }
                        break;
                    case InputState.WaitOnMudDataReturn:
                        if (_result2.IsCompleted)
                        {
                            _context.TerminalInd930ResultMudData = _result2.Result;
                            _navigationService.NavigateTo(_container.Resolve<ChooseCarrierPage>());
                            _state = InputState.WaitOnChooseMudDataReturn;
                            Logger.InfoEx($"WaitOnMudDataReturn completed, select carrier...", SourceClass);
                        }

                        break;
                    case InputState.WaitOnChooseMudDataReturn:
                        if (!string.IsNullOrEmpty(_context.AktMaterial))
                        {
                            if (!_context.AktMaterial.StartsWith("A"))
                            {
                                await _barcodeService.StartScanning();
                                UpdateLine2(nameof(Localization.ScanOrder2));
                                ClearValues();
                                _context.MudNr = "55555";
                                Logger.InfoEx($"WaitOnChooseMudDataReturn completed, Values wrong...", 
                                    SourceClass);
                                _scannerTimeout = 0;
                                _state = InputState.InputNr;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(_context.AktCarrier)) // Wenn hier Carrier leer = Abbruch
                                {
                                    if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                                    _logStatusAlibi = _alibiLog.SaveAlibiRecordAsync(1, UnitType.Display, _context.MtId.ToString());
                                    Logger.InfoEx($"SaveAlibiRecordAsync called...", SourceClass);
                                    _state = InputState.WaitOnAlibiReturn2;
                                }
                                else
                                    _state = InputState.Init;
                            }
                        }
                        break;
                    case InputState.WaitOnAlibiReturn2:
                        if (_logStatusAlibi.IsCompleted)
                        {
                            _result1 = _connection.InvokeAsync<TerminalIND930ResultWeight>("MudWeightIn", 0,
                                            _logStatusAlibi.Result.RelatedWeight.GrossWeight, _context.AktCarrier,
                                            _context.AktPartner, _context.AktMaterial,
                                            _context.FPLicense, _context.SPLicense);
                            _state = InputState.WaitOnSendWeightReturn;
                            Logger.InfoEx($"WaitOnChooseMudDataReturn completed, MudWeightIn sent...", SourceClass);
                        }
                        break;
                    case InputState.WaitOnReprintReturn:
                        if (_result1.IsCompleted)
                            _state = InputState.WaitOnUnloadScale;
                        break;
                    case InputState.WaitOnUnloadScale:
                        if (_context._weightInformation.GrossWeight < 0.8 * _lastGrossWeight)
                        {
                            Logger.InfoEx($"Scale unloaded 2...", SourceClass);
                            _state = InputState.Init;
                        }

                        break;
                    default:
                        break;
                }
            }

            catch (Exception ex)
            {
                Logger.ErrorEx($"MainLoopCrash: {ex.Message}", SourceClass);
            }

        }

        private async void OnCustomSignal(object sender, IBarcodeReaderSignal signal)
        {
            Logger.InfoEx($"Signal received {signal.BarcodeType} {signal.Value}", SourceClass);
            _digitalIo.Hupe();
            if (signal.GetType() == typeof(ScannedValueSignal))
            {
                switch (signal.BarcodeType)
                {
                    case BarcodeType.OrderNr:
                        if (string.IsNullOrEmpty(_context.OrderNr))
                            _context.OrderNr = signal.Value;
                        break;
                    case BarcodeType.MTid:
                        if (_context.MtId == 0)
                            _context.MtId = Int32.Parse(signal.Value);
                        break;
                    case BarcodeType.MudNr:
                        if (string.IsNullOrEmpty(_context.MudNr))
                            _context.MudNr = signal.Value;
                        break;
                    case BarcodeType.Unknown:
                        //Restart Barcodescanner wenn falscher Code
                        bool res = await _barcodeService.StopScanning();
                        res = await _barcodeService.StartScanning();
                        break;
                }
            }
        }

        #region ClearValues

        private void ClearValues()
        {
            _context.OrderNr = "";
            _context.FPLicense = "";
            _context.SPLicense = "";
            _context.MudNr = "";
            _context.MtId = 0;
            _context.AktCarrier = "";
            _context.AktPartner = "";
            _context.AktMaterial = "";
            _force = "";
        }

        #endregion

        #region UpdateLine2

        private void UpdateLine2(string name)
        {

            PropertyInfo propertyInfo = LocalizationInfo.LocalizationsList[0].GetType().GetProperty(name);
            foreach (LocalizationInfo item in LocalizationInfo.LocalizationsList)
            {
                item.Message2 = (string)propertyInfo.GetValue(item);
            }
            NotifyPropertyChanged("Localization");
        }
        #endregion

        #region OrderNrEntry

        public async Task<DialogResult> OrderNrEntry()
        {
            var nd = new InputDialog(InputDialog.Layout.Numeric, Localization.EnterYourReferenceNo);
            nd.Width = 1100;
            _context.OrderNr = "";
#if DEBUG
            nd.Value = "4800047508";
#endif
            var dialogResult = await nd.ShowAsync(_parent);
            if (dialogResult == DialogResult.OK)
            {
                if (nd.Value.StartsWith("H"))
                {
                    int iresult;
                    if (Int32.TryParse(nd.Value.Substring(1), out iresult)) _context.MtId = iresult;
                }
                if (nd.Value.Length == 10)
                    _context.OrderNr = nd.Value;
                if (nd.Value.Length == 5)
                    _context.MudNr = nd.Value;

            }

            return dialogResult;
        }
        #endregion

        #region PlateNrEntry

        public async Task<DialogResult> PlateNrEntry(string which)
        {
            var nd = new InputDialog(InputDialog.Layout.AlphaNumeric, which);
            nd.Width = 1100;
#if DEBUG
            nd.Value = "TEST ZH1234";
#endif

            var dialogResult = await nd.ShowAsync(_parent);
            if (dialogResult == DialogResult.OK)
            {
                if (_context.FPLicense.Length == 0)
                    _context.FPLicense = nd.Value.ToUpper();
                else if (_context.SPLicense.Length == 0)
                    _context.SPLicense = nd.Value.ToUpper();

            }
            return dialogResult;
        }
        #endregion

        #region PropertyEventHandler

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        #region LocalizationSource

        private LocalizationInfo _localization;

        public LocalizationInfo Localization
        {
            get { return _localization; }
            set
            {
                if (_localization != value)
                {
                    _localization = value;
                    _context.Localization = _localization;
                    NotifyPropertyChanged("Localization");
                }
            }
        }

        #endregion

        #region Message2




        private string _message2;

        public string Message2
        {
            get { return _message2; }
            set
            {
                if (_message2 != value)
                {
                    _message2 = value;
                    NotifyPropertyChanged("Message2");
                }
            }
        }
        #endregion

        #region GoContinue

        public ICommand GoContinue => new DelegateCommand(DoGoContinue);

        private async void DoGoContinue()
        {
            if (_state == InputState.InputNr)
                await OrderNrEntry();
        }

        #endregion

        #region GoReprint

        public ICommand GoReprint => new DelegateCommand(DoGoReprint);

        private async void DoGoReprint()
        {
            if (_state != InputState.WaitOnUnloadScale)
                return;

            if (_connection.State == HubConnectionState.Disconnected) _connection.StartAsync();
            _result1 = _connection.InvokeAsync<TerminalIND930ResultWeight>("SecondWeightIn", _context.MtId, 0, "R");
            _state = InputState.WaitOnReprintReturn;
        }

        #endregion

        #region Visibility
        public Visibility GoReprintVisibility
        {
            get { return _goReprintVisibility; }
            set
            {
                if (_goReprintVisibility != value)
                {
                    _goReprintVisibility = value;
                    FunctionVisibility = (_goReprintVisibility == Visibility.Visible)
                       ? Visibility.Hidden : Visibility.Visible;
                    NotifyPropertyChanged("GoReprintVisibility");
                }
            }
        }

        private Visibility _goReprintVisibility = Visibility.Hidden;

        public Visibility FunctionVisibility
        {
            get { return _functionVisibility; }
            set
            {
                if (_functionVisibility != value)
                {
                    _functionVisibility = value;
                    NotifyPropertyChanged("FunctionVisibility");
                }
            }
        }

        private Visibility _functionVisibility = Visibility.Visible;
        #endregion

        #region GoMud

        public ICommand GoMud => new DelegateCommand(DoGoMud);

        private async void DoGoMud()
        {
            if (_state == InputState.InputNr)
                _context.MudNr = "55555";
        }

        #endregion

        #region GoChangeLanguage

        public ICommand GoChangeLanguage => new DelegateCommand<string>(DoGoChangeLanguage);

        private void DoGoChangeLanguage(string lang)
        {
            if (LocalizationInfo.Localizations.ContainsKey(lang))
                Localization = LocalizationInfo.Localizations[lang];
        }
        #endregion
    }


}
