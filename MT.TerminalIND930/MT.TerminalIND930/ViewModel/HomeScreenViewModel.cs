﻿// ReSharper disable once RedundantUsingDirective

using System;
using System.Globalization;
using System.Resources;
using Microsoft.AspNetCore.SignalR.Client;
using MT.Singularity;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using MT.Singularity.Presentation.Model;
using MT.TerminalIND930.Data;

namespace MT.TerminalIND930.ViewModel
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : PropertyChangedBase
    {

        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        public HomeScreenViewModel(HubConnection connection)
        {
            _connection = connection;
        }

        #region ICommand

        /// <summary>
        /// Gets the get weight.
        /// </summary>
        /// <value>
        /// The get weight.
        /// </value>
        public ICommand CommandButton
        {
            get
            {
                return new DelegateCommand(MethodCalledWhenButtonPressed);
            }
        }

        /// <summary>
        /// Called when the button had been pressed.
        /// </summary>
        private async void MethodCalledWhenButtonPressed()
        {
            try
            {
                // ...ToDo... Do something
               if (_connection.State == HubConnectionState.Disconnected) await _connection.StartAsync();
                //  public async Task<TerminalIND930Result> SignOn(string barcode, string plate1, string force)
                TerminalIND930Result x = await _connection.InvokeAsync<TerminalIND930Result>("SignOn", "", "SignalR", "");

                MyText = x.LiefName;
            }
            catch (Exception e)
            {
                Log4NetManager.ApplicationLogger.Error(e.Message);

            }

            await _connection.StopAsync();

        }

        #endregion ICommand

        /// <summary>
        /// Gets or sets my text in the View.
        /// </summary>
        /// <value>
        /// My text.
        /// </value>
        public string MyText
        {
            get { return myText; }
            set
            {
                if (myText != value)
                {
                    myText = value;
                    NotifyPropertyChanged();
                }
            }
        }
       private string myText = "HomeScreen";
      

        #region DeltaTrac

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public string Target
        {
            get
            {
                return target;
            }
            set
            {
                if (target != value)
                {
                    target = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string target = "3";

        /// <summary>
        /// Gets or sets the lower tolerance.
        /// </summary>
        /// <value>
        /// The lower tolerance.
        /// </value>
        public string LowerTolerance
        {
            get
            {
                return lowerTolerance;
            }
            set
            {
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (lowerTolerance != value)
                {
                    lowerTolerance = value;
                }
            }
        }

        private string lowerTolerance = "-1";

        /// <summary>
        /// Gets or sets the upper tolerance.
        /// </summary>
        /// <value>
        /// The upper tolerance.
        /// </value>
        public string UpperTolerance
        {
            get
            {
                return upperTolerance;
            }
            set
            {
                if (upperTolerance != value)
                {
                    upperTolerance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string upperTolerance = "2";

        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        public DeltaTracAppearance Appearance
        {
            get
            {
                return appearance;
            }
            set
            {
                if (appearance != value)
                {
                    appearance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private DeltaTracAppearance appearance = DeltaTracAppearance.BarGraph;
        private HubConnection _connection;

        #endregion DeltaTrac

    }
}
