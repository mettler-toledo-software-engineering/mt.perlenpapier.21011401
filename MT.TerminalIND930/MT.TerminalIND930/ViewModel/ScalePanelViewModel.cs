﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.TerminalIND930.Config;
using MT.TerminalIND930.Infrastructure;

namespace MT.TerminalIND930.ViewModel
{
    public class ScalePanelViewModel : PropertyChangedBase
    {
        private readonly IScaleService _scaleService;
        private  IComponents _customerComponent;
        private Configuration _configuration;


        public ScalePanelViewModel(Visual parent, IScaleService scaleService, IComponents customerComponent)
        {
            _customerComponent = customerComponent;
            _scaleService = scaleService;


            SelectedScaleVisibility = _scaleService.NumberOfScales > 1 ? Visibility.Visible : Visibility.Hidden;
            ZeroVisibility = Visibility.Collapsed;
            TareVisibility = Visibility.Collapsed;
            ClearTareVisibility = Visibility.Collapsed;
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewWeightInDisplayUnit;
            _scaleService.NumberOfScalesChanged += ScaleServiceOnNumberOfScalesChanged;
            InitializeViewModel();

        }

        private async void InitializeViewModel()
        {
            _configuration = await _customerComponent.GetConfigurationAsync();
            TareVisibility = (_configuration.ScaleTareOn == true) ? Visibility.Visible : Visibility.Collapsed;
            ClearTareVisibility = (_configuration.ScaleTareOn == true) ? Visibility.Visible : Visibility.Collapsed;
            _configuration.PropertyChanged -= ConfigurationOnPropertyChanged;
            _configuration.PropertyChanged += ConfigurationOnPropertyChanged;
        }

        private void ConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            TareVisibility = (_configuration.ScaleTareOn == true ) ? Visibility.Visible : Visibility.Collapsed;
            ClearTareVisibility = (_configuration.ScaleTareOn == true) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ScaleServiceOnNumberOfScalesChanged()
        {
            SelectedScaleVisibility = _scaleService.NumberOfScales > 1 ? Visibility.Visible : Visibility.Hidden;
        }


        private void SelectedScaleOnNewWeightInDisplayUnit(WeightInformation weight)
        {
            ZeroVisibility = weight.SelectedScaleIsZeroAble() ? Visibility.Visible : Visibility.Hidden;
            Debug.WriteLine(weight.NetWeight);
        }

        public ICommand ZeroCommand => new DelegateCommand(ZeroCommandExecute);

        private async void ZeroCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
            await _scaleService.SelectedScale.ZeroAsync();

        }

        public ICommand TareCommand => new DelegateCommand(TareCommandExecute);

        private async void TareCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
            await _scaleService.SelectedScale.TareAsync();
        }

        public ICommand ClearTareCommand => new DelegateCommand(ClearTareCommandExecute);

        private async void ClearTareCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
        }

        public ICommand SelectScaleCommand => new DelegateCommand(SelectScaleCommandExecute);


        private async void SelectScaleCommandExecute()
        {

            await _scaleService.SwitchSelectedScaleAsync();
            if (_scaleService.SelectedScale.ScaleError == ScaleErrors.ScaleNotConnected)
                await _scaleService.SwitchSelectedScaleAsync();
        }

        private Visibility _selectedScaleVisibility;

        public Visibility SelectedScaleVisibility
        {
            get { return _selectedScaleVisibility; }
            set
            {
                if (value != _selectedScaleVisibility)
                {
                    _selectedScaleVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _zeroVisibility;

        public Visibility ZeroVisibility
        {
            get { return _zeroVisibility; }
            set
            {
                if (value != _zeroVisibility)
                {
                    _zeroVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _tareVisibility;

        public Visibility TareVisibility
        {
            get { return _tareVisibility; }
            set
            {
                if (value != _tareVisibility)
                {
                    _tareVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _clearTareVisibility;
      

        public Visibility ClearTareVisibility
        {
            get { return _clearTareVisibility; }
            set
            {
                if (value != _clearTareVisibility)
                {
                    _clearTareVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}

