﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using MT.Singularity.Data;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Controls;
using System.Collections.ObjectModel;
using System.Linq;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Reflection.Cecil;
using MT.TerminalIND930.Data;
using MT.TerminalIND930.Infrastructure;
using MT.TerminalIND930.View;

namespace MT.TerminalIND930.ViewModel
{
    /// <summary>
    /// View model for the <see cref="ChooseCarrierPage"/>.
    /// </summary>
    public class ChooseCarrierPageViewModel : ObservableObject
    {
        private const int MaxTexts = 500;
        private Context _context;
        private List<PartnerT> allPartner = new List<PartnerT>();
        private List<MaterialT> allMaterial = new List<MaterialT>();
        private List<DeliveryT> allDelivery = new List<DeliveryT>();
        private const int ElementsProPage = 8;
        private int startPartnerSP = 0;
        private int startPartnerWE = 0;
        private int aktPage = 0;
        private INavigationService _navigationService;

        /// <summary>
        /// Initializes a new instance of the <see cref=" MT.TerminalIND930.ViewModel"/> class.
        /// </summary>
        public ChooseCarrierPageViewModel(Context context, INavigationService navigationService)
        {
            _navigationService = navigationService;
            _context = context;
            Localization = _context.Localization;
            buttonItems = new ObservableCollection<Button>();
            Initialize();
        }

        public void Initialize()
        {
            if (string.IsNullOrEmpty(_context.OrderNr))  // Mud oder Wood
            {
                if (string.IsNullOrEmpty(_context.AktCarrier))
                {
                    ChooseCarrierTitle = _context.Localization.ChooseCarrier;
                    if (allPartner.Count == 0)
                    {
                        startPartnerSP = 0;
                        allPartner = _context.TerminalInd930ResultMudData.Partners.Where(d => d.Role == "SP").OrderBy(e => e.Name).ToList();
                    }

                    buttonItems.Clear();
                    foreach (PartnerT item in allPartner.Skip(startPartnerSP).Take(ElementsProPage))
                    {
                        AddButtonToDynamicWrapPanel(item.Nr, item.Name, item.Ort);
                    }

                    NextVisibility = (allPartner.Count > startPartnerSP + ElementsProPage)
                        ? Visibility.Visible : Visibility.Hidden;
                    LastVisibility = (startPartnerSP > 0) ? Visibility.Visible : Visibility.Hidden;

                }
                else if (string.IsNullOrEmpty(_context.AktPartner))
                {
                    ChooseCarrierTitle = _context.Localization.ChooseShipTo;
                    if (allDelivery.Count == 0)
                    {
                        allPartner.Clear();
                        startPartnerWE = 0;
                        allDelivery = _context.TerminalInd930ResultMudData.Deliveries
                            .Where(d => d.CarrierNr == _context.AktCarrier)
                            .GroupBy(p => new { p.CarrierNr, p.ShipToNr  })
                            .Select(g => g.First())
                            .ToList();
                        foreach (DeliveryT item in allDelivery)
                        {
                            var x = _context.TerminalInd930ResultMudData.Partners.FirstOrDefault(d =>
                                d.Nr == item.ShipToNr);
                            if (x != null)
                                allPartner.Add(x);
                        }
                    }

                    buttonItems.Clear();
                    foreach (var item in allPartner.OrderBy(d => d.Land).ThenBy(e => e.Ort).Skip(startPartnerWE).Take(ElementsProPage))
                    {
                        AddButtonToDynamicWrapPanel(item.Nr, item.Name, $"{item.Land} {item.Ort}");
                    }
                    NextVisibility = (allPartner.Count > startPartnerWE + ElementsProPage)
                        ? Visibility.Visible : Visibility.Hidden;
                    LastVisibility = (startPartnerWE > 0) ? Visibility.Visible : Visibility.Hidden;
                }
                else if (string.IsNullOrEmpty(_context.AktMaterial))
                {
                    ChooseCarrierTitle = _context.Localization.ChooseMaterial;
                    if (allMaterial.Count == 0)
                    {
                        allDelivery = _context.TerminalInd930ResultMudData.Deliveries
                            .Where(d => d.CarrierNr == _context.AktCarrier)
                            .Where(e => e.ShipToNr == _context.AktPartner).ToList();
                        foreach (DeliveryT item in allDelivery)
                        {
                            var material = _context.TerminalInd930ResultMudData.Materials.FirstOrDefault(d =>
                                d.Nr == item.MatNr);
                            if (material != null)
                                allMaterial.Add(material);
                        }
                    }
                    buttonItems.Clear();
                    foreach (MaterialT item in allMaterial)
                    {
                        AddButtonToDynamicWrapPanel(item.Nr, item.MatName, "");
                    }
                    NextVisibility = Visibility.Hidden;
                    LastVisibility = Visibility.Hidden;
                }
                else
                    _navigationService.Back();

            }
            else   // Wood
            {
                if (string.IsNullOrEmpty(_context.AktCarrier))
                {
                    ChooseCarrierTitle = _context.Localization.ChooseCarrier;
                    int MaxTextBoxItems = Math.Min(_context.TerminalIND930ResultSignOn.Carrier.Length, 5);
                    for (int i = 0; i < MaxTextBoxItems; i++)
                    {
                        AddButtonToDynamicWrapPanel(_context.TerminalIND930ResultSignOn.Carrier[i].Nr,
                            _context.TerminalIND930ResultSignOn.Carrier[i].Name,
                            _context.TerminalIND930ResultSignOn.Carrier[i].Ort);
                    }
                }
            }
        }


        /// <summary>
        /// Adds the text block to dynamic wrap panel.
        /// </summary>
        private void AddButtonToDynamicWrapPanel(string Nr, string Name, string Ort)
        {
            var myButton = new Button();
            myButton.Foreground = Colors.Black;
            myButton.Width = 400;
            myButton.Height = 70;
            myButton.Margin = new Thickness(7);
            myButton.Foreground = new Color(255, 120, 120, 120);
            myButton.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            myButton.Content = new StackPanel(
                new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = Colors.Black,
                    Text = $"{Name.Substring(0, Math.Min(25, Name.Length))}",
                    FontSize = 25,
                },
                new TextBlock()
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = Colors.Black,
                    Text = $"{Ort.Substring(0, Math.Min(25, Ort.Length))}",
                    FontSize = 25
                });

            myButton.CommandParameter = Nr;
            myButton.Command = GoSaveValue;
            ButtonItems.Add(myButton);
        }

        #region ButtonItems
        /// <summary>
        /// Gets or sets the Buttons item.
        /// </summary>
        /// <value>
        /// The text block item.
        /// </value>
        public ObservableCollection<Button> ButtonItems
        {
            get
            {
                return buttonItems;
            }
            set
            {
                if (value != buttonItems)
                {
                    buttonItems = value;
                    NotifyPropertyChanged("ButtonItems");
                }
            }
        }
        private ObservableCollection<Button> buttonItems;

        #endregion

        #region GoSaveValue

        public ICommand GoSaveValue => new DelegateCommand<string>(DoGoSaveValue);

        private void DoGoSaveValue(string value)
        {
            if (_context.AktCarrier.Length == 0)
            {
                _context.AktCarrier = value;
                if (_context.OrderNr.Length == 0)  // Mud oder Wood ?
                    Initialize();
                else
                {
                    _context.AktMaterial = "Dummy";
                    _navigationService.Back();
                }
            }
            else if ((_context.AktPartner.Length == 0) && (value.Trim() != _context.AktCarrier.Trim()))            
            {
                _context.AktPartner = value;
                Initialize();
            }
            else if ((_context.AktMaterial.Length == 0) && (value.Trim() != _context.AktPartner.Trim()))
            {
                _context.AktMaterial = value;
                _navigationService.Back();
            }
        }
        #endregion


        #region LocalizationSource

        private LocalizationInfo _localization;

        public LocalizationInfo Localization
        {
            get { return _localization; }
            set
            {
                if (_localization != value)
                {
                    _localization = value;
                    NotifyPropertyChanged("Localization");
                }
            }
        }

        #endregion

        #region GoNext

        public ICommand GoNext => new DelegateCommand(DoGoNext);

        private void DoGoNext()
        {
            if (_context.AktCarrier.Length == 0)
                startPartnerSP += ElementsProPage;
            if (_context.AktPartner.Length == 0)
                startPartnerWE += ElementsProPage;
            Initialize();
        }
        #endregion

        #region GoBack

        public ICommand GoBack => new DelegateCommand(DoGoBack);

        private void DoGoBack()
        {
            if (_context.AktCarrier.Length == 0)
            {
                _context.AktCarrier = "";
                _context.AktMaterial = "Dummy";
                _navigationService.Back();
            }
            else if (_context.AktPartner.Length == 0)
            {
                allPartner.Clear();
                allDelivery.Clear();
                _context.AktCarrier = "";
                Initialize();
            }
            else
            {
                allPartner.Clear();
                allDelivery.Clear();
                allMaterial.Clear();
                _context.AktPartner = "";
               Initialize();
            }
        }
        #endregion

        #region GoPrevious

        public ICommand GoPrevious => new DelegateCommand(DoGoPrevious);

        private void DoGoPrevious()
        {
            if (_context.AktCarrier.Length == 0)
                startPartnerSP = Math.Max(0, startPartnerSP - ElementsProPage);
            if (_context.AktPartner.Length == 0)
                startPartnerWE = Math.Max(0, startPartnerWE - ElementsProPage);
            Initialize();
        }
        #endregion

        #region ChooseCarrierTitle
        /// <summary>
        /// Gets the dynamic wrap panel title.
        /// </summary>
        public string ChooseCarrierTitle
        {
            get
            {
                return chooseCarrierTitle;

            }
            set
            {
                chooseCarrierTitle = value;
                NotifyPropertyChanged("ChooseCarrierTitle");

            }
        }
        private string chooseCarrierTitle;
        #endregion

        #region NextVisibility

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility NextVisibility
        {
            get { return _nextVisibility; }
            set
            {
                if (_nextVisibility != value)
                {
                    _nextVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _nextVisibility = Visibility.Hidden;

        #endregion

        #region LastVisibility

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility LastVisibility
        {
            get { return _lastVisibility; }
            set
            {
                if (_lastVisibility != value)
                {
                    _lastVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _lastVisibility = Visibility.Hidden;

        #endregion
    }
}
