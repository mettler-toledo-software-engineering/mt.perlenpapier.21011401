﻿namespace MT.TerminalIND930.Infrastructure
{
    interface IBarcodeReaderSignal
    {
        string Value { get; set; }
        BarcodeType BarcodeType { get; set; }
    }
}
