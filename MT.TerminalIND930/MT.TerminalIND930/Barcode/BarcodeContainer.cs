﻿using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.TerminalIND930.Infrastructure;

namespace MT.TerminalIND930.Barcode
{
    public class BarcodeContainer : IBarcodeDataParser
    {
        private ISignalManager _signalManager;
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BarcodeContainer);
        public BarcodeContainer(ISignalManager signalManager)
        {
           _signalManager = signalManager;
        }

        public void ParseData(string data)
        {
            SendSignal(data);
        }

        private void SendSignal(string data)
        {
            IBarcodeReaderSignal signal = new ScannedValueSignal();
            data = data.Trim();
            int len = data.Length;
            Logger.InfoEx($"Signal sender {data}  {len}", SourceClass);
            if (len == 11)  
            {
                signal.Value = data.Substring(1,10);
                signal.BarcodeType = BarcodeType.OrderNr;
                Logger.InfoEx($"Order Signal sended {data}", SourceClass);
            }
            else if (len == 9)
            {
                signal.Value = data.Substring(2, 7);
                signal.BarcodeType = BarcodeType.MTid;
                Logger.InfoEx($"MTId Signal sended {data}", SourceClass);
            }
            else if (len == 6)
            {
                signal.Value = "55555";
                signal.BarcodeType = BarcodeType.MudNr;
            }
            else
            {
                signal.Value = "";
                signal.BarcodeType = BarcodeType.Unknown;
            }
            _signalManager.Send(this, signal);
        }
    }
}
