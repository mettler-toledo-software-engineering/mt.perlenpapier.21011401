﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;
using MT.TerminalIND930.Config;
using MT.TerminalIND930.Infrastructure;

namespace MT.TerminalIND930.Barcode
{
    [Export(typeof(BarcodeReader))]
    public class BarcodeReader 
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BarcodeReader);

        private readonly ISignalManager _signalManager;
        private string Big_data = "";
        private bool scanning;
       
        private StringSerializer _stringSerializer = null;
        private bool cOM2Enabled = false;
        private IPlatformEngine _engine;

        private BarcodeReader(IPlatformEngine engine, ISignalManager signal)
        {
            _engine = engine;
            _signalManager = signal;
        }

        public static async Task<BarcodeReader> CreateAsync(IPlatformEngine engine, ISignalManager signal)
        {
            var newInstance = new BarcodeReader(engine,signal);
            await newInstance.Start();
            return newInstance;
        }
       
        public async Task<bool> Start()
        {
            bool success = true;
            scanning = false;
           
            var _platformEngine = _engine;
            var _interfaceService = await _platformEngine.GetInterfaceServiceAsync();
            var _allInterfaces = await _interfaceService.GetAllInterfacesAsync();
            try
            {
                ISerialInterface _allSerialInterfaces = _allInterfaces.OfType<ISerialInterface>()
                .FirstOrDefault(item => item.LogicalPort == 4);
                Logger.InfoEx("BarcodeReader Start",SourceClass);
                if (_allSerialInterfaces == null)
                {
                    Logger.ErrorEx("Error: serial port 4 not installed",SourceClass);
                }
                else
                {
                    IConnectionChannel<DataSegment> _actualCommunicationChannel =
                        await _allSerialInterfaces.CreateConnectionChannelAsync();
                    DelimiterSerializer _delimiterSerializer =
                        new DelimiterSerializer(_actualCommunicationChannel, CommonDataSegments.Cr);
                    _stringSerializer = new StringSerializer(_delimiterSerializer);

                    if (_stringSerializer != null)
                    {
                        var _eventEmiiter = new ChannelEventEmitter<string>(_stringSerializer);
                        if (_stringSerializer.IsOpen)
                            await _stringSerializer.CloseAsync();
                       
                        await _stringSerializer.OpenAsync();
                        _eventEmiiter.MessageRead += BarcodeSerialPortOnDataReceived;
                        cOM2Enabled = true;
                    }

                    success = true;
                    Logger.InfoEx($"serial if on {_allSerialInterfaces.LogicalPort} found", SourceClass);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"Exception, no serial if 4 found", SourceClass, ex);
                success = false;
            }

            return success;
        }

        private void BarcodeSerialPortOnDataReceived(object sender, MessageReadEventArgs<string> e)
        {
            IBarcodeDataParser barcodeDataParser = null;

            Big_data += e.Message;
            Big_data = Big_data.Trim();
            Logger.InfoEx($"data received : {Big_data}", SourceClass);
            if (!Big_data.Contains("?"))
            {
                if (string.IsNullOrEmpty(Big_data) == false)
                {
                    barcodeDataParser = new BarcodeContainer(_signalManager);
                }

                barcodeDataParser?.ParseData(Big_data.Trim());
            }

            Big_data = "";
        }

        public async Task<bool> StartScanning()
        {
            // string start = String.Chr(22) + "T" + String.Chr(0x0d);
            
            string start = "\x0016T\n";
            if (!scanning)
            {
                if (cOM2Enabled)
                {
                    Logger.InfoEx("Start scanning", SourceClass);
                    await _stringSerializer.WriteAsync(start);
                }

                scanning = true;
            }

            return true;
        }
        public async Task<bool> StopScanning()
        {
            //string stop = String.Chr(22) + "U" + String.Chr(0x0d);
            string stop = "\x0016U\n";
            if (scanning)
            {
                if (cOM2Enabled)
                {
                    Logger.InfoEx("Stop scanning", SourceClass);
                    await _stringSerializer.WriteAsync(stop);
                }
                scanning = false;
            }
            return (true);
        }

    }



}

