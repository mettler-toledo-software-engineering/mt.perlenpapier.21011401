﻿using System.Threading.Tasks;

namespace MT.TerminalIND930.Barcode
{
    interface IBarcodeReader
    {
        Task<bool> StartScanning();
        Task<bool> StopScanning();
    }
}
