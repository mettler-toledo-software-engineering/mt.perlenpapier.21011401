﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.TerminalIND930.Barcode
{
    interface IBarcodeDataParser
    {
        void ParseData(string data);
    }
}
