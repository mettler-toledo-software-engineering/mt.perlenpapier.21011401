﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MT.TerminalIND930.Infrastructure
{
    public class ScannedValueSignal : IBarcodeReaderSignal
    {
        public string Value { get; set; }
        public BarcodeType BarcodeType { get; set; }
    }

    public enum BarcodeType
    {
        OrderNr,
        MudNr,
        MTid,
        Unknown
    }
}
