﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using System.IO.Ports;
using System.Threading.Tasks;
using MT.TerminalIND930.Infrastructure;


namespace MT.TerminalIND930.Barcode
{
    // [InjectionBehavior(IsSingleton = true)]
    [Export(typeof(ZebraBarcodeReader))]
    public class ZebraBarcodeReader

    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(ZebraBarcodeReader);

        private string _port;
        private readonly ISignalManager _signalManager;
        private SerialPort _serialConnection = new SerialPort();
        private int _containerNumberSize;
        private string Big_data = "";
        private bool cOMEnabled = false;

        private ZebraBarcodeReader(CompositionContainer composition)
        {
            var _context = composition.Resolve<Context>();
            _signalManager = _context.SignalManager;
        }

        public static async Task<ZebraBarcodeReader> CreateAsync(CompositionContainer composition)
        {
            var newInstance = new ZebraBarcodeReader(composition);
            await newInstance.InitializeAsync();
            newInstance.Start();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            _port = "COM1";
        }

        public void Start()
        {
            _serialConnection.BaudRate = 9600;
            _serialConnection.Parity = Parity.None;
            _serialConnection.DataBits = 8;
            _serialConnection.StopBits = StopBits.One;
            _serialConnection.Handshake = Handshake.None;
            _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;

            _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;
            try
            {
                if (_port != "")
                {
                    if (_serialConnection.IsOpen)
                    {
                        _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;
                        _serialConnection.Close();
                    }

                    _serialConnection.PortName = _port;
                    _serialConnection.Open();
                    _serialConnection.DataReceived += BarcodeSerialPortOnDataReceived;
                    Logger.InfoEx($"USB Scanner on {_port} present", SourceClass);
                }
            }

            catch (Exception ex)
            {
                Logger.ErrorEx($"USB Scanner on {_port} not present", SourceClass, ex);
                _port = "";
            }
        }

        private void BarcodeSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            IBarcodeDataParser barcodeDataParser = null;
            Big_data += _serialConnection.ReadExisting();
            try
            {
                if (Big_data.Contains("\r"))
                {
                    Big_data = Big_data.Trim();
                    if (string.IsNullOrEmpty(Big_data) == false)
                    {
                        barcodeDataParser = new BarcodeContainer(_signalManager);
                    }

                    barcodeDataParser?.ParseData(Big_data);
                    Big_data = "";
                }

            }

            catch (Exception ex)
            {
                Logger.ErrorEx($"Decoding of data '{Big_data}' failed", SourceClass, ex);
            }
        }

        public async Task<bool> StartScanning()
        {
            Logger.InfoEx("Start scanning", SourceClass);
            return (true);
        }

        public async Task<bool> StopScanning()
        {
            Logger.InfoEx("Stop scanning", SourceClass);
            return (true);
        }
    }
}
