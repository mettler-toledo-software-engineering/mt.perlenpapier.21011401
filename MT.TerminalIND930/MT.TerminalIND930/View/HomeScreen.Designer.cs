﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace MT.TerminalIND930.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame _navigationFrame;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.TextBox internal1;
            internal1 = new MT.Singularity.Presentation.Controls.TextBox();
            internal1.Text = "Bitte warten";
            internal1.FontSize = ((System.Nullable<System.Int32>)40);
            _navigationFrame = new MT.Singularity.Presentation.Controls.Navigation.AnimatedNavigationFrame();
            _navigationFrame.Content = internal1;
            _navigationFrame.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _navigationFrame.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Content = _navigationFrame;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
    }
}
