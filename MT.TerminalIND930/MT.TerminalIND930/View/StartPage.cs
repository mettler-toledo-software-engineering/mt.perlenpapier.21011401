﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Expressions;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.TerminalIND930.Infrastructure;
using MT.TerminalIND930.ViewModel;
using MT.Singularity.Presentation.Model;
using MT.TerminalIND930.Config;

namespace MT.TerminalIND930.View
{
    /// <summary>
    /// Interaction logic for StartPage
    /// </summary>
    [Export]
    public partial class StartPage
    {
        private StartPageViewModel _viewModel;
        private IScaleService _scaleService;
        private CompositionContainer _container;
        private Context _context;
        private INavigationService _navigationService;
        public StartPage(CompositionContainer container)
        {
            _container = container;
            _context = _container.Resolve<Context>();
            _scaleService = _container.Resolve<IScaleService>();
            _navigationService = _container.Resolve<INavigationService>();
            var customerComponent = _container.Resolve<IComponents>();
            _viewModel = new StartPageViewModel(this, _container, _navigationService);
            InitializeComponents();
            _context.scalePanelView = new ScalePanel(_scaleService, customerComponent);
            _scalePanel.Add(_context.scalePanelView);
            _context.scalePanelView.ActivateWeightWindow();
        }
        
        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage"/>.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            _context.scalePanelView.DeactivateWeightWindow();
            _scalePanel.Remove(_context.scalePanelView);
            var result = base.OnNavigatingAway(nextPage);
            if (result == NavigationResult.Proceed)
            {

            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            _scalePanel.Add(_context.scalePanelView);
            _context.scalePanelView.ActivateWeightWindow();
            base.OnNavigationReturning(previousPage);

        }


    }
}
