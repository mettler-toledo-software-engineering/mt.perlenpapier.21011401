﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.TerminalIND930.Config;
using MT.TerminalIND930.ViewModel;

namespace MT.TerminalIND930.View
{

    
    /// <summary>
    /// Interaction logic for ScalePanel
    /// </summary>
    public partial class ScalePanel
    {
        private ScalePanelViewModel _scalePanelViewModel;
        private IScaleService _scaleService;


        public ScalePanel(IScaleService scaleService, IComponents customerComponent)
        {
            _scaleService = scaleService;
            _scalePanelViewModel = new ScalePanelViewModel(this, _scaleService, customerComponent);
            InitializeComponents();
        }

        public void DeactivateWeightWindow()
        {
            _weightDisplayWindow.Deactivate();
        }

        public void ActivateWeightWindow()
        {
            _weightDisplayWindow.Activate();
        }
    }
}
