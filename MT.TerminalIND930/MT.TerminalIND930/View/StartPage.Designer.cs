﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Presentation.Drawing;
namespace MT.TerminalIND930.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class StartPage : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.Grid.DynamicGrid _scalePanel;
        private MT.Singularity.Presentation.Controls.StackPanel PageLogo;
        private MT.Singularity.Presentation.Controls.TextBlock lblVersion;
        private MT.Singularity.Presentation.Controls.TextBlock Message2;
        private MT.Singularity.Presentation.Controls.GroupPanel ButtonPanel;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Grid.Grid internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.Image internal3;
            MT.Singularity.Presentation.Controls.Image internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.Button internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.Button internal8;
            MT.Singularity.Presentation.Controls.Image internal9;
            MT.Singularity.Presentation.Controls.Button internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.Button internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.StackPanel internal15;
            MT.Singularity.Presentation.Controls.Button internal16;
            MT.Singularity.Presentation.Controls.GroupPanel internal17;
            MT.Singularity.Presentation.Controls.TextBlock internal18;
            MT.Singularity.Presentation.Controls.StackPanel internal19;
            MT.Singularity.Presentation.Controls.Button internal20;
            MT.Singularity.Presentation.Controls.GroupPanel internal21;
            MT.Singularity.Presentation.Controls.Image internal22;
            MT.Singularity.Presentation.Controls.TextBlock internal23;
            MT.Singularity.Presentation.Controls.StackPanel internal24;
            MT.Singularity.Presentation.Controls.Button internal25;
            MT.Singularity.Presentation.Controls.GroupPanel internal26;
            MT.Singularity.Presentation.Controls.TextBlock internal27;
            _scalePanel = new MT.Singularity.Presentation.Controls.Grid.DynamicGrid();
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(_scalePanel);
            internal2.Height = 200;
            internal2.GridColumn = 0;
            internal2.GridColumnSpan = 3;
            internal2.GridRow = 0;
            internal3 = new MT.Singularity.Presentation.Controls.Image();
            internal3.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Images.MTLogo.png";
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal3.Margin = new MT.Singularity.Presentation.Thickness(6);
            lblVersion = new MT.Singularity.Presentation.Controls.TextBlock();
            lblVersion.Margin = new MT.Singularity.Presentation.Thickness(16, 6, 0, 0);
            lblVersion.FontSize = ((System.Nullable<System.Int32>)12);
            lblVersion.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => lblVersion.Text,() =>  Infrastructure.Globals.ProgVersion,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4 = new MT.Singularity.Presentation.Controls.Image();
            internal4.Margin = new MT.Singularity.Presentation.Thickness(6, 50, 0, 0);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal4.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Images.PerlenLogo.png";
            PageLogo = new MT.Singularity.Presentation.Controls.StackPanel(internal3, lblVersion, internal4);
            PageLogo.GridColumn = 0;
            PageLogo.GridRow = 1;
            PageLogo.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            internal7.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Media.FlgDeu.png";
            internal6 = new MT.Singularity.Presentation.Controls.Button();
            internal6.Content = internal7;
            internal6.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal6.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6.Height = 61;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 3, 0, 3);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Command,() => _viewModel.GoChangeLanguage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.CommandParameter = "DE";
            internal9 = new MT.Singularity.Presentation.Controls.Image();
            internal9.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Media.FlgFra.png";
            internal8 = new MT.Singularity.Presentation.Controls.Button();
            internal8.Content = internal9;
            internal8.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal8.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(0, 3, 0, 3);
            internal8.Height = 61;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Command,() => _viewModel.GoChangeLanguage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8.CommandParameter = "FR";
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Media.FlgIta.png";
            internal10 = new MT.Singularity.Presentation.Controls.Button();
            internal10.Content = internal11;
            internal10.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal10.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal10.Margin = new MT.Singularity.Presentation.Thickness(0, 3, 0, 3);
            internal10.Height = 61;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Command,() => _viewModel.GoChangeLanguage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.CommandParameter = "IT";
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Media.FlgEng.png";
            internal12 = new MT.Singularity.Presentation.Controls.Button();
            internal12.Content = internal13;
            internal12.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal12.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal12.Margin = new MT.Singularity.Presentation.Thickness(0, 3, 0, 3);
            internal12.Height = 61;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Command,() => _viewModel.GoChangeLanguage,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12.CommandParameter = "EN";
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal8, internal10, internal12);
            internal5.GridColumn = 2;
            internal5.GridRow = 1;
            internal5.GridRowSpan = 3;
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.GridColumn = 1;
            internal14.GridRow = 1;
            internal14.FontSize = ((System.Nullable<System.Int32>)50);
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Text,() =>  _viewModel.Localization.WelcomeText,MT.Singularity.Expressions.BindingMode.OneWay,false);
            Message2 = new MT.Singularity.Presentation.Controls.TextBlock();
            Message2.GridColumn = 1;
            Message2.GridRow = 2;
            Message2.FontSize = ((System.Nullable<System.Int32>)50);
            Message2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            Message2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => Message2.Text,() =>  _viewModel.Localization.Message2,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal16 = new MT.Singularity.Presentation.Controls.Button();
            internal16.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16.Width = 400;
            internal16.Height = 100;
            internal16.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal16.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal18 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Text,() =>  _viewModel.Localization.MudDelivery,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal18.FontSize = ((System.Nullable<System.Int32>)40);
            internal17 = new MT.Singularity.Presentation.Controls.GroupPanel(internal18);
            internal16.Content = internal17;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Command,() => _viewModel.GoMud,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Visibility,() => _viewModel.FunctionVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.StackPanel(internal16);
            internal15.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal20 = new MT.Singularity.Presentation.Controls.Button();
            internal20.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal20.Width = 400;
            internal20.Height = 100;
            internal20.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal20.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal22 = new MT.Singularity.Presentation.Controls.Image();
            internal22.Source = "embedded://MT.TerminalIND930/MT.TerminalIND930.Images.Printer.al8";
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal22.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal23 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal23.Text,() =>  _viewModel.Localization.RePrint,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal23.FontSize = ((System.Nullable<System.Int32>)40);
            internal21 = new MT.Singularity.Presentation.Controls.GroupPanel(internal22, internal23);
            internal20.Content = internal21;
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.Command,() => _viewModel.GoReprint,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[12] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.Visibility,() => _viewModel.GoReprintVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal19 = new MT.Singularity.Presentation.Controls.StackPanel(internal20);
            internal19.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal25 = new MT.Singularity.Presentation.Controls.Button();
            internal25.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal25.Width = 400;
            internal25.Height = 100;
            internal25.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal25.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal27 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal27.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal27.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            this.bindings[13] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal27.Text,() =>  _viewModel.Localization.ManOrder,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal27.FontSize = ((System.Nullable<System.Int32>)40);
            internal26 = new MT.Singularity.Presentation.Controls.GroupPanel(internal27);
            internal25.Content = internal26;
            this.bindings[14] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal25.Command,() => _viewModel.GoContinue,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[15] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal25.Visibility,() => _viewModel.FunctionVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal24 = new MT.Singularity.Presentation.Controls.StackPanel(internal25);
            internal24.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            ButtonPanel = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal19, internal24);
            ButtonPanel.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            ButtonPanel.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            ButtonPanel.GridColumn = 0;
            ButtonPanel.GridColumnSpan = 3;
            ButtonPanel.GridRow = 3;
            internal1 = new MT.Singularity.Presentation.Controls.Grid.Grid(internal2, PageLogo, internal5, internal14, Message2, ButtonPanel);
            internal1.RowDefinitions = GridDefinitions.Create("200","1*","1*","110");
            internal1.ColumnDefinitions = GridDefinitions.Create("280", "1*","100");
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[16];
    }
}
