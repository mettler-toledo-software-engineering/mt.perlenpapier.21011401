﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.TerminalIND930.View
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
       // private readonly HomeScreenViewModel viewModel;
       private CompositionContainer _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen(CompositionContainer container)
        {
            _container = container;
            InitializeComponents();

        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }
        
        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
#if DEBUG
            get { return false; }
#else
            get { return false; }
#endif
        }
        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected  override async void OnFirstNavigation()
        {
            await Task.Delay(2000);
            _container.AddInstance<INavigationService>(NavigationService);
            NavigationService.NavigateTo(_container.Resolve<StartPage>());
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage"/>.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>

        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            return result;
        }
    }
}
