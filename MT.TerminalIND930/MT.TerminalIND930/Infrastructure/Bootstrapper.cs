﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.UserInteraction;
using MT.Singularity.Presentation.Utils;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation;
using MT.TerminalIND930.Barcode;
using MT.TerminalIND930.Config;

namespace MT.TerminalIND930.Infrastructure
{

    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();

        }

        /// <summary>
        /// Initializes the customer service.
        /// Put all customer services or components initialization code in this method.
        /// </summary>
        private async Task InitializeCustomerService()
        {
            try
            {
                LocalizationInfo.Export();
                LocalizationInfo.Load();
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var signalManager = CompositionContainer.Resolve<ISignalManager>();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IComponents>(customerComponent);

                var scaleService = CompositionContainer.Resolve<IScaleService>();
                var context = CompositionContainer.Resolve<Context>();
                await Scoreboard.CreateAsync(context, engine, scaleService);
                await Schenck.CreateAsync(context);
                context.SignalManager = signalManager;
                CompositionContainer.AddInstance(await DigitalIo.CreateAsync(engine));
#if !DEBUG
                var barcode = await BarcodeReader.CreateAsync(engine, signalManager);
                CompositionContainer.AddInstance<BarcodeReader>(barcode);
#else
                CompositionContainer.AddInstance<ZebraBarcodeReader>(await ZebraBarcodeReader.CreateAsync(CompositionContainer));
#endif
                Log4NetManager.ApplicationLogger.Info("Bootstrapper done");
            }

            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);

            }
        }
    }
}
