﻿using log4net;
using MT.Singularity.Composition;
using System.Linq;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Serialization;
using MT.TerminalIND930.Config;
using System.ComponentModel;

namespace MT.TerminalIND930.Infrastructure
{
    public class Scoreboard
    {
        private Thread _Sender;
        private ManualResetEvent _running;
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Scoreboard);
        private static StringSerializer stringSerializer;
        private static DelimiterSerializer delimiterSerializer;
        private Context _context;
        private WeightInformation lastWeight;
        private IScaleService _scaleService;
        private IPlatformEngine _engine;
        private bool _cOM5Enabled = false;
        private bool ValidSwitch;

        private Scoreboard(Context context, IPlatformEngine engine, IScaleService scale)
        {
            _context = context;
            _engine = engine;
            _scaleService = scale;
        }

        public static async Task<Scoreboard> CreateAsync(Context context, IPlatformEngine engine, IScaleService scale)
        {
            var newInstance = new Scoreboard(context, engine, scale);
            await newInstance.InitializeAsync();
            newInstance.Start();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            ValidSwitch = false;
            var interfaceService = await _engine.GetInterfaceServiceAsync();
            try
            {
                var allInterfaces = await interfaceService.GetAllInterfacesAsync();

                ISerialInterface serialInterface = allInterfaces.OfType<ISerialInterface>()
                    .FirstOrDefault(item => item.LogicalPort == 5);
                if (serialInterface == null)
                {
                    Logger.Error("Error: serial port 5 not installed");
                }
                else
                {
                    IConnectionChannel<DataSegment> actualConnectionChannel =
                        await serialInterface.CreateConnectionChannelAsync();
                    if (actualConnectionChannel != null)
                    {
                        delimiterSerializer = new DelimiterSerializer(actualConnectionChannel, CommonDataSegments.Newline);
                        stringSerializer = new StringSerializer(delimiterSerializer);
                        if (stringSerializer != null)
                        {
                            stringSerializer.CodePage = CodePage.CP850;
                            try
                            {
                                await stringSerializer.OpenAsync();
                                Logger.ErrorEx("Test port 5", SourceClass);
                                _cOM5Enabled = true;
                            }
                            catch (Exception ex)
                            {
                                Logger.ErrorEx("Error opening serial port 5", SourceClass, ex);
                                await stringSerializer.CloseAsync();
                                throw;
                            }

                            await stringSerializer.CloseAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx("Error CreateConnection RS232", SourceClass, ex);
            }
        }

        public void Start()
        {

            _Sender = new Thread(Worker);
            _Sender.Name = "ScoreboadSender";
            _Sender.IsBackground = true;
            _running = new ManualResetEvent(false);

            _Sender.Start();
        }


        private async void Worker()
        {

            string s;
            while (!_running.WaitOne(250, false))
            {
                try
                {
                    var weight = await _scaleService.SelectedScale.GetWeightAsync(UnitType.Display);
                    if (weight != null)
                    {
                        if (weight.IsValid)
                        {
                            if (!ValidSwitch)
                            {
                                Logger.InfoEx("Weight valid", SourceClass);
                                ValidSwitch = true;
                            }
                            _context._weightInformation = weight;
                            lastWeight = weight;
                        }
                        else
                        {
                            if (ValidSwitch)
                            {
                                Logger.InfoEx("Weight not valid", SourceClass);
                                ValidSwitch = false;
                            }
                            _context._weightInformation = lastWeight;
                        }
                        s = "         " + _context._weightInformation.NetWeightString;
                    }
                    else
                    {
                        s = "---------------";
                    }
                    s = s.Substring(s.Length - 9, 9);
                    string t = $"\x0002{s}\r\n";
                    if (_cOM5Enabled)
                    {
                        await stringSerializer.OpenAsync();
                        await stringSerializer.WriteAsync(t);
                        await stringSerializer.CloseAsync();
                    }
                }

                catch (Exception ex)
                {
                    Logger.ErrorEx($"Scoreboard Sender failed", SourceClass, ex);
                }

            }
        }
    }
}

