﻿using System;

using System.Collections.Generic;
using System.Reflection;
using System.Text;
using MT.Singularity.Platform;
using MT.Singularity.Composition;

namespace MT.TerminalIND930.Infrastructure
{ 
    [Export]
    public class Globals
    {
        private static readonly SingularityEnvironment Environment = new SingularityEnvironment("MT.TerminalIND930");
        public const string IND930USBDrive = "D:\\";

        public const string ProjectNumber = "P21090000";
        public const int ScreenWidth = 1280;
        public const int ScreenHeight = 800;
        public static string ProgVersion =  ProjectNumber + "\n" + ProgVersionStr(false);
        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
            }

            return vers;
        }

    
    }
}
