﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;

namespace MT.TerminalIND930.Infrastructure
{
    public class DigitalIo
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(DigitalIo);

        /// <summary>
        /// The digital 4IO
        /// </summary>
        private IDigitalIOInterface _digitalIo;

        /// <summary>
        /// The interfaces
        /// </summary>
        private IInterfaceService _interfaces;

        private CompositionContainer _composition;
        private IPlatformEngine _engine;

        private enum SignalColor
        {
            Red = 3,
            Green = 1,
            Hup = 2
        }

        public DigitalIo(IPlatformEngine engine)
        {
            _engine = engine;
        }

        public static async Task<DigitalIo> CreateAsync(IPlatformEngine engine)
        {
            var newInstance = new DigitalIo(engine);
            await newInstance.InitializeAsync();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            _interfaces = await _engine.GetInterfaceServiceAsync();

            for (int i = 1; i < 7; i++)
            {
                this._digitalIo = await _interfaces.GetDigitalIOInterfaceAsync(i);
                if (_digitalIo != null)
                {
                    Logger.InfoEx($"DigitalIO found on {i}", SourceClass);
                    break;
                }
            }
        }

        public void SetLightSignalRed()
        {
            WriteOutputByIndexDigital4IO((int)SignalColor.Red, true);
            WriteOutputByIndexDigital4IO((int)SignalColor.Green, false);
        }

        public void SetLightSignalGreen()
        {
            WriteOutputByIndexDigital4IO((int)SignalColor.Red, false);
            WriteOutputByIndexDigital4IO((int)SignalColor.Green, true);
        }

        public async void Hupe()
        {
            WriteOutputByIndexDigital4IO((int)SignalColor.Hup, true);
            await Task.Delay(800);
            WriteOutputByIndexDigital4IO((int)SignalColor.Hup, false);
        }

        /// <summary>
        /// Writes the output by index to digital 4IO.
        /// </summary>
        private async void WriteOutputByIndexDigital4IO(int which, bool what)
        {
            if (_digitalIo != null)
            {
                Logger.InfoEx($"Write DIO: {_digitalIo.FriendlyName} Channel {which}, Set {what}", SourceClass);
                await this._digitalIo.WriteOutputByIndexAsync(which, what);
            }
            else
            {
                Logger.InfoEx($"Attempt to write no existing DIO:  Channel {which}, Set {what}", SourceClass);
            }

        }
    
}

}
