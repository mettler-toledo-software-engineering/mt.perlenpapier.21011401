﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.TerminalIND930.View;
using MT.TerminalIND930.Data;

namespace MT.TerminalIND930.Infrastructure
{
    [InjectionBehavior(IsSingleton = true)]
    [Export(typeof(Context))]
    public class Context
    {
        public WeightInformation _weightInformation;
        public ScalePanel scalePanelView;
        public ISignalManager SignalManager;
        public TerminalIND930ResultSignOn TerminalIND930ResultSignOn;
        public TerminalIND930ResultMudData TerminalInd930ResultMudData;
        public LocalizationInfo Localization;
        public string OrderNr;
        public string FPLicense;
        public string SPLicense;
        public string MudNr;
        public string AktCarrier;
        public string AktPartner;
        public string AktMaterial;
        public int MtId;

    }

}
