﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace MT.TerminalIND930.Infrastructure
{
    public static class WeightExtension
    {
        public static bool SelectedScaleIsZeroAble(this WeightInformation weight)
        {
            bool result = false;
            if (weight.IsValid)
            {
                if (weight.GetGrossWeightIn(WellknownWeightUnit.Gram) < 10 &&
                    weight.GetGrossWeightIn(WellknownWeightUnit.Gram) > -10 && weight.IsValid)
                {
                    result = true;
                }
            }

            return result;
        }

        public static double GetGrossWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.GrossWeight, weight.Unit, unit);
        }
        public static double GetNetWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.NetWeight, weight.Unit, unit);
        }
        public static double GetTareWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.TareWeight, weight.Unit, unit);
        }

    }
}
