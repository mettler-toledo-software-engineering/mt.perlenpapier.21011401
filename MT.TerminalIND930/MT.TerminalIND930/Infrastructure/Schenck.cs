﻿using log4net;

using MT.Singularity.Composition;
using System.Linq;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using System;
using MT.Singularity.Components;
using System.Net;      //required
using System.Net.Sockets;
using System.Collections.Generic;
using System.Diagnostics;
using MT.Singularity.Logging;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MT.Singularity.Serialization;
using System.Net;

namespace MT.TerminalIND930.Infrastructure
{
    public class Schenck
    {
        private Thread _Sender;
        private ManualResetEvent _running;
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Scoreboard);
        private static StringSerializer stringSerializer;
        private static DelimiterSerializer delimiterSerializer;
        private CompositionContainer _composition;
        private Context _context;
        private Socket listener;
       
        private Schenck(Context context)
        {
            _context = context;
           }

        public static async Task<Schenck> CreateAsync(Context context)
        {
            var newInstance = new Schenck(context);
            await newInstance.InitializeAsync();
            newInstance.Start();
            return newInstance;
        }


        private async Task InitializeAsync()
        {
           IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 8000);
            // Create a TCP/IP socket.  
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            listener.Listen(10);
        }

        public void Start()
        {
            _Sender = new Thread(Worker);
            _Sender.Name = "ScoreboadSender";
            _Sender.IsBackground = true;
            _running = new ManualResetEvent(false);
            _Sender.Start();
        }


        private async void Worker()
        {
            string inputBuffer = "";
            string outputBuffer = "";
            char bcc;
            byte[] temp = new byte[100];

            Socket handler = null;
            while (!_running.WaitOne(250, false))
            {

                try
                {
                    if (handler == null)
                        handler = listener.Accept();
                    inputBuffer = "";

                    // handler.ReceiveTimeout = 500;
                    while (true)
                    {
                        int bytesRec = handler.Receive(temp);
                        inputBuffer += Encoding.ASCII.GetString(temp, 0, bytesRec);
                        if ((inputBuffer.IndexOf("\x0003") > -1) || bytesRec == 0)
                        {
                            if (bytesRec == 0)
                            {
                                inputBuffer = "";
                                handler = null;
                            }
                            break;
                        }
                    }

                    if (inputBuffer.Length > 0)
                    {
                        // Remove whitespace chars
                        inputBuffer = inputBuffer.Substring(0, inputBuffer.IndexOf("\x0003"));
                        inputBuffer = inputBuffer.TrimStart().TrimEnd().ToUpper();

                        if (inputBuffer.IndexOf("00#TG#") > -1)
                        // if (inputBuffer.IndexOf("00") > -1)
                        {
                            string s = _context._weightInformation.NetWeightString;
                            s = s.PadLeft(9);
                            s = s.Substring(s.Length - 7, 7);
                            outputBuffer = $"\x000200#TG#{s}";
                            outputBuffer = outputBuffer + "#      0#      0#80#\x0003";
                            temp = Encoding.ASCII.GetBytes(outputBuffer);
                            bcc = GetBCC(temp);
                            outputBuffer = string.Concat(outputBuffer, bcc);
                            temp = Encoding.ASCII.GetBytes(outputBuffer);
                            handler.Send(temp);


                        }
                    }
                }

                catch (Exception ex)
                {
                    Logger.ErrorEx($"Schenk Sender failed", SourceClass, ex);

                    handler = null;
                }

            }
        }


        public char GetBCC(byte[] inputStream)
        {
            byte bcc = 0;

            if (inputStream != null && inputStream.Length > 0)
            {
                // Exclude SOH during BCC calculation
                for (int i = 1; i < inputStream.Length; i++)
                {
                    bcc ^= inputStream[i];
                }
            }

            return Convert.ToChar(bcc);
        }



    }

}

