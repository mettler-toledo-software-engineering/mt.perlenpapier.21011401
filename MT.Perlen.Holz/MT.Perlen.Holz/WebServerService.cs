using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.Perlen.Holz.Infrastructure;
using MT.Perlen.Holz.Infrastructure.ScaleDriver;
using MT.Perlen.Holz.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MT.Perlen.Holz
{
    public class WebServerService : IService
    {
        private readonly ScaleProvider scaleProvider;
        private readonly PerlenConfiguration configuration;
      
        public WebServerService(ScaleProvider scaleProvider, PerlenConfiguration configuration)
        {
            this.scaleProvider = scaleProvider;
            this.configuration = configuration;
        }

        public string Name => "Web Server";

        public void Run(IServiceHost serviceHost)
        {
            var host = Host.CreateDefaultBuilder()
            .ConfigureWebHost(options =>
            {
#if DEBUG
                options.UseContentRoot(@"C:\MT-Projects\_PC-Apps\MT.Perlen.Holz\MT.Perlen.Holz\MT.Perlen.Holz");
#else
                    options.UseContentRoot(@"C:\Program Files\MT.DataServiceHolz\Plugins");
#endif
                // options.UseContentRoot(AppContext.BaseDirectory);
            })
            .ConfigureLogging(builder =>
            {
                builder.ClearProviders();
                builder.AddProvider(new LogSinkLoggerProvider(serviceHost.Log));
            })
            .ConfigureServices(services =>
            {
                services.AddSingleton(configuration);
                services.AddSingleton(scaleProvider);
                services.AddSingleton(serviceHost.Log);
                services.AddSingleton<SAPConnectorService>();
                services.AddSingleton<StarPrinter>();
                services.AddSingleton<ZebraPrinter>();
                services.AddDbContextFactory<HolzDbContext>(opt =>
               opt.UseSqlServer($"Server={configuration.ConnectionString}"));
            })

            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureKestrel(options =>
                {
                    options.Listen(new IPEndPoint(IPAddress.Any, configuration.ServerPort));
                });
                webBuilder.UseStartup(context => new Startup(context.Configuration, configuration));

            }).Build();



            //using (var scope = host.Services.CreateScope())
            //{
            //    var db = scope.ServiceProvider.GetRequiredService<HolzDbContext>();
            //    //    db.Database.EnsureCreated();

            //    //for (int i = 0; i < 10; i++)
            //    //{
            //    //    var tx = new Transaction() { SAPBelNr = $"Tx {i}", FirstWeight = i * 10, SecondWeight = i };
            //    //    db.Transactions.Add(tx);
            //    //}

            //    //db.SaveChanges();
            //}

            host.Start();
            serviceHost.Log.Action("Started server...");

            serviceHost.CancellationToken.WaitHandle.WaitOne();

            serviceHost.Log.Action("Stopped server...");
            host.Dispose();
        }

    }
}
