using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MT.Perlen.Holz.Model;
using MT.Perlen.Holz.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MT.Perlen.Holz.Hubs;

namespace MT.Perlen.Holz
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private static PerlenConfiguration perlenConfiguration;

        public Startup(IConfiguration configuration, PerlenConfiguration perlenConfiguration)
        {
            Configuration = configuration;
            perlenConfiguration = perlenConfiguration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            //services.AddSingleton<WeatherForecastService>();
            services.AddSignalR()
                .AddHubOptions<TerminalInterface930>(options =>
                {
                    options.EnableDetailedErrors = true;
                    
                });

            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
            services.AddDbContext<HolzDbContext>(options =>
            {
                options.UseSqlServer($"Server={perlenConfiguration.ConnectionString}");
                options.EnableSensitiveDataLogging();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapHub<TerminalInterface930>("/IND930hub");
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
