﻿using Microsoft.EntityFrameworkCore;
using MT.DataServices.Logging;
using MT.Perlen.Holz.Infrastructure.SAP;
using MT.Perlen.Holz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using MT.Perlen.Holz.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;

namespace MT.Perlen.Holz.Hubs
{
    public class TerminalInterface930 : Hub<ITerminalInterface930>
    {
        private readonly HolzDbContext holzDBContext;
        private readonly PerlenConfiguration configuration;
        private readonly ILogSink logSink;
        private readonly StarPrinter printer;
        private readonly SAPConnectorService sapConnectorService;
        private readonly ZebraPrinter zebraPrinterService;
        private static TerminalIND930ResultSignOn aktTerminalIND930ResultSignOn = null;
        private static TerminalIND930ResultMudData aktTerminalIND930ResultMudData = null;

        public TerminalInterface930(IServiceProvider sp, ILogSink logSink, SAPConnectorService sapConnectorService,
           ZebraPrinter zebraPrinterService, StarPrinter starPrinter, PerlenConfiguration configuration)

        {
            this.holzDBContext = sp.CreateScope().ServiceProvider.GetRequiredService<HolzDbContext>();
            this.configuration = configuration;
            this.logSink = logSink;
            this.printer = starPrinter;
            this.sapConnectorService = sapConnectorService;
            this.zebraPrinterService = zebraPrinterService;
        }

        public async Task<TerminalIND930ResultMudData> MudData()
        {

            logSink.Action($"MudData: Get Data");

            SchlammDataFromSAP result = sapConnectorService.SchlammDaten(true, true, true).Result;

            aktTerminalIND930ResultMudData = new TerminalIND930ResultMudData()
            {
                Deliveries = result.Deliveries,
                Materials = result.Materials,
                Partners = result.Partners
            };

            return aktTerminalIND930ResultMudData;
        }

        public async Task<TerminalIND930ResultWeight> SecondWeightIn(double id, double weight, string mode)
        {
            Transaction tr = holzDBContext.Transactions.Find(Convert.ToInt32(id));

            logSink.Action($"SecondWeight: MT-Id {id:F0} called");
            if (tr != null)
            {
                if (mode == "O")
                {
                    if (tr.SecondWeight == 0) // = Einwaage
                    {
                        tr.SecondWeight = weight;
                        tr.SecondWeightCreatedAt = DateTime.Now;
                        tr.DeltaWeight = Math.Abs(tr.FirstWeight - tr.SecondWeight);
                        holzDBContext.SaveChanges();

                        if (tr.Type == 0)
                        {
                            logSink.Action($"SecondWeight: MT-Id {id:F0} Weight {weight:F0} Type Wood ");

                            HolzTarameldungFromSAP result = sapConnectorService.HolzTarameldung(tr).Result;

                            if (result.BAPIRet.Message.Length == 0)
                            {
                                printer.PrintWoodTicket(tr);
                            }
                            else
                                logSink.Error($"SecondWeight: MT-Id {id:F0} SAP Message: {result.BAPIRet.Message}");
                            return new TerminalIND930ResultWeight() { Status = 1, Id = tr.Id };
                        }
                        else
                        {
                            logSink.Action($"SecondWeight: MT-Id {id:F0} Weight {weight:F0} Type Mud ");

                            SchlammwiegungFromSAP result1 = sapConnectorService.MudWeight(tr).Result;

                            if (result1.BAPIRet.Message.Length == 0)
                            {
                                //Tickets
                                printer.PrintMudTicket(tr);
                                await Task.Delay(100);
                                printer.PrintMudTicket(tr);
                            }
                            else
                                logSink.Error($"SecondWeight: MT-Id {id:F0} SAP Message: {result1.BAPIRet.Message}");
                            return new TerminalIND930ResultWeight() { Status = 1, Id = tr.Id };
                        }
                    }
                    else
                        logSink.Action($"SecondWeight: MT-Id {id:F0} already weighed");
                }
                if (mode == "R")   // = Reprint
                {
                    if (tr.Type == 0)
                    {
                        logSink.Action($"SecondWeightIn: MT-Id {tr.Id:F0} RePrint Type Wood");
                        if (tr.SecondWeight > 0)
                            printer.PrintWoodTicket(tr);
                        else
                            printer.PrintWeighingTicket(tr);
                    }
                    else
                    {
                        logSink.Action($"SecondWeightIn: MT-Id {tr.Id:F0} RePrint Type Mud");
                        if (tr.SecondWeight > 0)
                            printer.PrintMudTicket(tr);
                        else
                            printer.PrintWeighingTicket(tr);
                    }
                    return new TerminalIND930ResultWeight() { Status = 1, Id = tr.Id };
                }
            }
            else
                logSink.Error($"SecondWeightIn: MT-Id {id:F0} unknown!");

            return new TerminalIND930ResultWeight() { Status = 0, Id = Convert.ToInt32(id) };
        }

        public async Task<TerminalIND930ResultWeight> MudWeightIn(double id, double gross, string carrier, string partner, string material, string plate1, string plate2)
        {
            SchlammanmeldungFromSAP result;
            SchlammwiegungFromSAP result1;

            logSink.Action($"MudWeightIn called");

            Transaction tr = new Transaction()
            {
                Type = 1,
                OrderNr = "5555555555",
                LicPlateTruck = plate1.ToUpper(),
                LicPlateTrailer = plate2.ToUpper(),
                FirstWeight = gross,
                FirstWeightCreatedAt = DateTime.Now,
                CreatedAt = DateTime.Now
            };
            holzDBContext.Transactions.Add(tr);
            holzDBContext.SaveChanges();

            logSink.Action($"MudWeightIn: MTId {tr.Id} Carrier {carrier} Partner {partner} Material {material} Weight {gross:F0}");

            MaterialT aktMaterial = aktTerminalIND930ResultMudData.Materials.FirstOrDefault(d => d.Nr == material);
            PartnerT aktCarrier = aktTerminalIND930ResultMudData.Partners.FirstOrDefault(d => d.Nr == carrier);
            PartnerT aktPartner = aktTerminalIND930ResultMudData.Partners.FirstOrDefault(d => d.Nr == partner);
            if (aktMaterial == null)
            {
                logSink.Error($"MudWeightIn: MTId {tr.Id}  Wrong Material {material}");
                return new TerminalIND930ResultWeight() { Status = 0, Id = tr.Id };
            }

            if (aktCarrier == null)
            {
                logSink.Error($"MudWeightIn: MTId {tr.Id}  Wrong Carrier {carrier}");
                return new TerminalIND930ResultWeight() { Status = 0, Id = tr.Id };
            }
            if (aktPartner == null)
            {
                logSink.Error($"MudWeightIn: MTId {tr.Id}  Wrong Parter {partner}");
                return new TerminalIND930ResultWeight() { Status = 0, Id = tr.Id };
            }

            tr.CarrierId = CreatePartner(tr, aktCarrier, null);
            tr.PartnerId = CreatePartner(tr, aktPartner, null);
            tr.MatId = CreateMaterial(tr, aktMaterial.Nr, aktMaterial.MatName);

            holzDBContext.SaveChanges();

            result = sapConnectorService.MudSignOn(tr, carrier, partner, material).Result;

            if (result.BAPIRet.Message.Length == 0)
            {
                tr.SAPBelNr = result.Schlamm.SapBelNr;
                holzDBContext.SaveChanges();
                printer.PrintWeighingTicket(tr);
            }
            else
                logSink.Error($"MudWeightIn: MT-Id {id:F0} SAP Message: {result.BAPIRet.Message}");

            return new TerminalIND930ResultWeight() { Status = 1, Id = tr.Id };
        }

        public async Task<TerminalIND930ResultWeight> WoodWeightIn(double id, double gross, string carrierNr)
        {

            TerminalIND930ResultWeight result1 = new TerminalIND930ResultWeight();
            Transaction tr = holzDBContext.Transactions.Find(Convert.ToInt32(id));
            Material material = holzDBContext.Materials.Find(tr.MatId);

            logSink.Action($"WoodWeightIn called");

            if (tr != null)
            {
                logSink.Action($"WoodWeightIn: MT-Id {id:F0} Bestell-Nr {tr.OrderNr} Weight {gross:F0}");
                try
                {
                    result1.Id = tr.Id;
                    tr.FirstWeight = gross;
                    tr.FirstWeightCreatedAt = DateTime.Now;
                    if (tr.CarrierId == 0)
                    {
                        PartnerT aktCarrier = aktTerminalIND930ResultSignOn.Carrier.FirstOrDefault(d => d.Nr.Trim() == carrierNr.Trim());
                        if (aktCarrier != null)
                            tr.CarrierId = CreatePartner(tr, aktCarrier, null);
                    }
                    holzDBContext.SaveChanges();

                    result1.Status = 0;
                    HolzBruttomeldungFromSAP result = sapConnectorService.HolzBruttomeldung(tr).Result;

                    if (result.BAPIRet.Message.Length == 0)
                    {
                        printer.PrintWeighingTicket(tr);
                        zebraPrinterService.Print(tr, configuration.MatNrRundholz.Trim() == material.SAPNr.Trim() ? 2 : 1);
                        //zebraPrinterService.Print(tr, material.SAPNr.Trim() == "E0100330" ? 2 : 1);
                        result1.Status = 1;
                    }
                    else
                        logSink.Error($"WoodWeightIn: MT-Id {id:F0} SAP Message: {result.BAPIRet.Message}");

                    return result1;
                }
                catch (Exception ex)
                {
                    logSink.Error("WoodWeightIn: [Ex]:", ex);
                    result1.Status = 0;
                    return result1;
                }
            }
            else
            {
                logSink.Error($"WoodWeight: MT-Id  {id:F0} unknown!");
                result1.Status = -1;
                return result1;

            }
            result1.Status = 0;
            return result1;
        }

        public async Task<TerminalIND930ResultSignOn> SignOn(string barcode, string plate1, string force)
        {
            HolzAnmeldungFromSAP result;
            TerminalIND930ResultSignOn result1 = new TerminalIND930ResultSignOn();

            Debug.WriteLine("SignOn called");
            result1.Status = 1;
            result1.AktDateTime = DateTime.Now;

            if (barcode.Length == 0)
            {
                result1.Id = 0;
                result1.LiefName = "Keine Bestell-Nr";
                result1.Status = 0;
                logSink.Error($"Leere Bestellnummer");
                return (result1);
            }

            // Transaktion anlegen wenn nicht vorhanden oder erzwungen
            //Transaction tr = holzDBContext.Transactions.FirstOrDefault(d => d.OrderNr == barcode && d.SecondWeight == 0);
            Transaction tr = null;
            try
            {
                if ((tr == null) || (force == "X"))
                {
                    tr = new Transaction()
                    {
                        Type = 0,
                        OrderNr = barcode,
                        LicPlateTruck = plate1,
                        LicPlateTrailer = "",
                        CreatedAt = DateTime.Now
                    };

                    holzDBContext.Transactions.Add(tr);
                    holzDBContext.SaveChanges();
                    logSink.Action($"SignOn: {barcode} MT-Id H{tr.Id:0000000} Force {force}");

                    result = sapConnectorService.HolzAnmeldung(tr).Result;

                    if (result.BAPIRet.Message.Length == 0)
                    {
                        if (result.Holz.MoreCarrier.Length == 0)
                        {
                            result1.Carrier = new PartnerT[1]
                            {
                                new PartnerT()
                                {
                                    Nr = result.Holz.SpediNr,
                                    Name = result.Holz.SpediName,
                                    Ort = result.Holz.SpediOrt,
                                    Land = result.Holz.SpediLand
                                }
                            };
                        }
                        else
                            result1.Carrier = result.Carrier;

                        //Lieferant in ggf. DB erzeugen
                        tr.PartnerId = CreatePartner(tr, null, result.Holz);

                        //Material in ggf. DB erzeugen
                        tr.MatId = CreateMaterial(tr, result.Holz.MatNr.Trim(), result.Holz.MatName);

                        tr.SAPBelNr = result.Holz.SapBelNr;
                        holzDBContext.SaveChanges();

                        result1.LiefName = result.Holz.LiefName;
                        logSink.Action($"SignOn: Success {result1.LiefName} #Carrier: {result1.Carrier.Length}");

                        //aktTransactionId = tr.Id;
                        result1.Id = tr.Id;
                        result1.Status = 1;
                        aktTerminalIND930ResultSignOn = result1;  // zwischensperichern
                        return result1;
                    }
                    else
                    {
                        holzDBContext.Transactions.Remove(tr);
                        holzDBContext.SaveChanges();

                        result1.LiefName = result.BAPIRet.Message.Substring(0, Math.Min(result.BAPIRet.Message.Length, 40));
                        result1.Status = -1;
                        logSink.Error($"SignOn: Error Status {result.BAPIRet.Type}:  {result.BAPIRet.Message}");
                        return (result1);
                    }
                }

                else   // gleiche Ordernummer schon auf vorhanden
                {
                    string trimPlate1 = String.Concat(plate1.Where(c => !Char.IsWhiteSpace(c))).ToUpper();
                    string trimPlate2 = String.Concat(tr.LicPlateTruck.Where(c => !Char.IsWhiteSpace(c))).ToUpper();
                    if (trimPlate1 == trimPlate2)
                    {   // gleiches Nummernschild
                        result1.Status = -1;
                        logSink.Action($"SignOn: {barcode} Zweite Anmeldung, gleiches Schild");
                        result1.LiefName = "Bereits angemeldet...";
                        return (result1);
                    }
                    else
                    {
                        result1.Status = 0;
                        logSink.Action($"SignOn: {barcode} Zweite Anmeldung, neu");
                        result1.LiefName = "Weitere Lieferung anmelden ?";
                        return (result1);
                    }
                }
            }
            catch (Exception ex)
            {
                logSink.Error("SignOn [Ex]:", ex);
                result1.LiefName = ex.Message.Substring(0, Math.Min(ex.Message.Length, 20));
                result1.Status = 0;
                return (result1);
            }
        }

        private int CreatePartner(Transaction tr, PartnerT partnerT, ES_Holz holzLI)
        {
            Partner ptrPartner = null;

            if (partnerT != null)
            {
                ptrPartner = new Partner()
                {
                    TransId = tr.Id,
                    Type = "SP",
                    SAPNr = partnerT.Nr.Trim(),
                    SAPName1 = partnerT.Name,
                    SAPName2 = "",
                    SAPOrt = partnerT.Ort,
                    SAPPlz = "",
                    SAPCountry = partnerT.Land,
                    CreatedAt = DateTime.Now
                };
            }
            else if (holzLI != null)
            {
                ptrPartner = new Partner()
                {
                    TransId = tr.Id,
                    Type = "LI",
                    SAPNr = holzLI.LiefNr.Trim(),
                    SAPName1 = holzLI.LiefName,
                    SAPName2 = "",
                    SAPOrt = holzLI.LiefOrt,
                    SAPPlz = holzLI.LiefPLZ,
                    SAPCountry = holzLI.LiefLand,
                    CreatedAt = DateTime.Now
                };
            }

            if (ptrPartner != null)
            {
                Partner partner = holzDBContext.Partners.FirstOrDefault(d => d.SAPNr.Trim() == ptrPartner.SAPNr.Trim());
                if (partner == null)
                {
                    holzDBContext.Partners.Add(ptrPartner);
                }
                else
                {
                    ptrPartner.Id = partner.Id;
                    if (ptrPartner.SAPName1 != partner.SAPName1 || ptrPartner.SAPOrt != partner.SAPOrt ||
                            ptrPartner.SAPCountry != partner.SAPCountry || ptrPartner.SAPPlz != partner.SAPPlz)
                    {
                        partner.SAPName1 = ptrPartner.SAPName1; partner.SAPOrt = ptrPartner.SAPOrt; 
                        partner.SAPCountry = ptrPartner.SAPCountry; partner.SAPPlz = ptrPartner.SAPPlz;
                        holzDBContext.Partners.Update(partner);
                    }
                }
                holzDBContext.SaveChanges();
                return ptrPartner.Id;
            }
            else
                return 0;
        }

        private int CreateMaterial(Transaction tr, string nr, string name)
        {
            if (!string.IsNullOrEmpty(nr))
            {
                Material ptrMaterial = new Material()
                {
                    TransId = tr.Id,
                    SAPNr = nr,
                    SAPName = name,
                    CreatedAt = DateTime.Now
                };

                Material material = holzDBContext.Materials.FirstOrDefault(d => d.SAPNr.Trim() == nr.Trim());
                if (material == null)
                {
                    holzDBContext.Materials.Add(ptrMaterial);
                }
                else
                {
                    ptrMaterial.Id = material.Id;
                    if (ptrMaterial.SAPName != material.SAPName)
                    {
                        material.SAPName = ptrMaterial.SAPName;
                        holzDBContext.Materials.Update(material);
                    }
                }
                holzDBContext.SaveChanges();
                return ptrMaterial.Id;
            }
            return 0;
        }
    }

    public class TerminalIND930ResultSignOn
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public string LiefName { get; set; }
        public PartnerT[] Carrier { get; set; }
        public DateTime AktDateTime { get; set; }
    }

    public class TerminalIND930ResultWeight
    {
        public int Id { get; set; }
        public int Status { get; set; }

    }

    public class TerminalIND930ResultMudData
    {
        public PartnerT[] Partners { get; set; }
        public MaterialT[] Materials { get; set; }
        public DeliveryT[] Deliveries { get; set; }

    }
}




