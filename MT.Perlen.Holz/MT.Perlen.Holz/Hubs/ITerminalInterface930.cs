﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Hubs
{
     public interface ITerminalInterface930
    {
        Task<TerminalIND930ResultMudData> MudData();
        Task<TerminalIND930ResultWeight> SecondWeightIn(double id, double weight, string mode);
        Task<TerminalIND930ResultSignOn> SignOn(string barcode, string plate1, string force);
        Task<TerminalIND930ResultWeight> WoodWeightIn(double id, double gross, string carrierId, string mode);
        Task<TerminalIND930ResultWeight> MudWeightIn(double Id, double gross, string carrier, string partner, string material, string plate1, string plate2, string mode );
    }
}

   
