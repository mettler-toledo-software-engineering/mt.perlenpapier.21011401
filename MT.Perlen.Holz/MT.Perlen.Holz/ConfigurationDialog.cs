﻿using System;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using MT.DataServices.Plugins;

namespace MT.Perlen.Holz
{
    internal partial class ConfigurationDialog : UserControl, IPluginConfigurationPane
    {
        private readonly PerlenConfiguration configuration;

        internal ConfigurationDialog(PerlenConfiguration configuration)
        {
            InitializeComponent();

            this.configuration = configuration;
            txtIPIND780.Text = configuration.IPIND780.ToString();
            txtIPStar.Text = configuration.IPPrinterStar.ToString();
            txtIPScale.Text = configuration.IPTestScale.ToString();
            txtPortScale.Text = configuration.PortTestScale.ToString();
            txtIPZebra.Text = configuration.IPPrinterZD620.ToString();
            txtPortServer.Text = configuration.ServerPort.ToString();
            txtIPSAPServer.Text = configuration.SAPIP.ToString();
            txtSAPUser.Text = configuration.SAPUser;
            txtSAPPassword.Text = configuration.SAPPass;
            txtDBName.Text = configuration.DBName;
            txtDBServer.Text = configuration.DBServer;

            //configHost.FormClosing += OnConfiguationClosing;

        }

        Control IPluginConfigurationPane.Control
        {
            get { return this; }
        }

        string IPluginConfigurationPane.Title
        {
            get { return "Perlen Holz Configuration"; }
        }
               
        
        private void txtIPZebra_Leave(object sender, EventArgs e)
        {
            IPAddress ip;
            if (IPAddress.TryParse(txtIPZebra.Text, out ip))
                configuration.IPPrinterZD620 = txtIPZebra.Text.Trim();
            else
                MessageBox.Show("IP-Adresse Zebra-Drucker umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtIPIND780_Leave(object sender, EventArgs e)
        {
            IPAddress ip;
            if (IPAddress.TryParse(txtIPIND780.Text, out ip))
                configuration.IPIND780 = txtIPIND780.Text.Trim();
            else
                MessageBox.Show("IP-Adresse Terminal umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtIPStar_Leave(object sender, EventArgs e)
        {
            IPAddress ip;
            if (IPAddress.TryParse(txtIPStar.Text, out ip))
                configuration.IPPrinterStar = txtIPStar.Text.Trim();
            else
                MessageBox.Show("IP-Adresse STAR-Drucker umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtIPScale_Leave(object sender, EventArgs e)
        {
            IPAddress ip;
            if (IPAddress.TryParse(txtIPScale.Text, out ip))
                configuration.IPTestScale = txtIPScale.Text.Trim();
            else
                MessageBox.Show("IP-Adresse Test-Waage umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtPortScale_Leave(object sender, EventArgs e)
        {
            int p;
            if (int.TryParse(txtPortScale.Text, out p))
                configuration.PortTestScale = p;
            else
                MessageBox.Show("Port Test-Waage umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtIPSAPServer_Leave(object sender, EventArgs e)
        {
            IPAddress ip;
            if (IPAddress.TryParse(txtIPSAPServer.Text, out ip))
                configuration.SAPIP = txtIPSAPServer.Text.Trim();
            else
                MessageBox.Show("IP-Adresse SAP-Server umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);

            configuration.ConnectionStringSAP = $"AppServerHost={configuration.SAPIP}; SystemNumber={configuration.SAPSystem};Client={configuration.SAPClient};User ={configuration.SAPUser};Password={configuration.SAPPass};Language=DE;";
        }

        private void txtPortServer_Leave(object sender, EventArgs e)
        {
            int p;
            if (int.TryParse(txtPortServer.Text, out p))
                configuration.ServerPort = p;
            else
                MessageBox.Show("Port Server umgültig", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void txtDBServer_Leave(object sender, EventArgs e)
        {
            configuration.DBServer = txtDBServer.Text;
            configuration.ConnectionString = $"{configuration.DBServer}; Database = {configuration.DBName}; Integrated Security = SSPI; ";
        }

        private void txtDBName_Leave(object sender, EventArgs e)
        {
            configuration.DBName = txtDBName.Text;
            configuration.ConnectionString = $"{configuration.DBServer}; Database = {configuration.DBName}; Integrated Security = SSPI; ";
        }

        private void txtSAPUser_Leave(object sender, EventArgs e)
        {
            configuration.SAPUser = txtSAPUser.Text;
            configuration.ConnectionStringSAP = $"AppServerHost={configuration.SAPIP}; SystemNumber={configuration.SAPSystem};Client={configuration.SAPClient};User ={configuration.SAPUser};Password={configuration.SAPPass};Language=DE;";

        }

        private void txtSAPPassword_Leave(object sender, EventArgs e)
        {
            configuration.SAPPass = txtSAPPassword.Text;
            configuration.ConnectionStringSAP = $"AppServerHost={configuration.SAPIP}; SystemNumber={configuration.SAPSystem};Client={configuration.SAPClient};User ={configuration.SAPUser};Password={configuration.SAPPass};Language=DE;";
        }
    }
}
