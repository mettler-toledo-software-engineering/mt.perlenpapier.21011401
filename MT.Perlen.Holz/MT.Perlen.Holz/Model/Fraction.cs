﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class Fraction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int      Id { get; set; }
        public int      TransId { get; set; }
        public string   Name { get; set; }
        public double   Weight { get; set; }
        public DateTime WeightCreatedAt { get; set; }
               
    }
}
