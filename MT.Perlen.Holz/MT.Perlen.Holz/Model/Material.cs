﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class Material
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int TransId { get; set; }
        public string SAPNr { get; set; }
        public string SAPName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
