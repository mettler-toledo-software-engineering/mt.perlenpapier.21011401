﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class Partner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int    Id { get; set; }
        public int    TransId { get; set; }
        public string Type { get; set; }  //SP =Carrier   , WE ShipTo    LI = Supplier
        public string SAPNr { get; set; }
        public string SAPName1 { get; set; }
        public string SAPName2 { get; set; }
        public string SAPOrt { get; set; }
        public string SAPPlz { get; set; }
        public string SAPCountry { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
