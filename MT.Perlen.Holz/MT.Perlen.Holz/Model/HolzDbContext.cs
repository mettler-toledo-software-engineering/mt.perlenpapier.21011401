﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Model
{
    [Export]
    public class HolzDbContext : DbContext
    {
        public HolzDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<DeliveryData> DeliveryDatas { get; set; }
        public DbSet<Fraction> Fractions { get; set; }
        public DbSet<FracConst> FracConsts { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Partner> Partners { get; set; }

    }
}
