﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class DeliveryData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string SAPNrCarrier { get; set; }
        public string SAPNrShipTo { get; set; }
        public string SAPNrMat { get; set; }
    }
}
