﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class FracConst
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int      Id { get; set; }
        public int      FracNr { get; set; }
        public string   FracName { get; set; }
        public double   FracTara { get; set; }
                  
    }
}
