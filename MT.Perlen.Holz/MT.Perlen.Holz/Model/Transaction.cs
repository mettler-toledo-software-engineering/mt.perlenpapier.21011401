﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MT.Perlen.Holz.Model
{
    public class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int      Id { get; set; }
        public int      Type { get; set; }   // Holz oder Schlamm
        public string   SAPBelNr { get; set; }
        public string   OrderNr { get; set; }
        public string   LicPlateTruck { get; set; }
        public string   LicPlateTrailer { get; set; }
        public double   FirstWeight { get; set; }
        public DateTime FirstWeightCreatedAt { get; set; }
        public double   SecondWeight { get; set; }
        public DateTime SecondWeightCreatedAt { get; set; }
        public double   DeltaWeight { get; set; }

        public double   LutroWeight { get; set; }
        public double   ContainerWeight { get; set; }
        public DateTime LutroWeightCreatedAt { get; set; }
        public double   AtroWeight { get; set; }
        public DateTime AtroWeightCreatedAt { get; set; }
        public double   LossPercent { get; set; }

        public int      MatId { get; set; }
        public int      PartnerId { get; set; }
        public int      CarrierId { get; set; }

        public DateTime CreatedAt { get; set; }


    }
}
