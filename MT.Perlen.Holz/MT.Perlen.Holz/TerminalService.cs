﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.Perlen.Holz.Abstractions;
using MT.Perlen.Holz.Infrastructure;
using MT.Perlen.Holz.Model;
using MT.Recorder.SharedData;
using MT.TaskScript.Remoting;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using SapNwRfc;
using MT.Perlen.Holz.Infrastructure.SAP;

namespace MT.Perlen.Holz
{
    public class TerminalService : IService
    {
        private readonly PerlenConfiguration configuration;
        private readonly SAPConnectorService sapConnectorService;
        private readonly ZebraPrinter zebraPrinterService;
        private readonly StarPrinter starPrinterService;

        public string Name => "Terminal Service";

        public TerminalService(PerlenConfiguration configuration, SAPConnectorService sapConnectorService,
                            ZebraPrinter zebraPrinterService, StarPrinter starPrinterServcie)
        {
            this.configuration = configuration;
            this.sapConnectorService = sapConnectorService;
            this.zebraPrinterService = zebraPrinterService;
            this.starPrinterService = starPrinterServcie;
        }

        public void Run(IServiceHost serviceHost)
        {
            HolzDbContext holzDBContext = new HolzDbContext(configuration);

            ////TESTETST
            //var x = holzDBContext.Transactions.FirstOrDefault(d => d.Id == 11);
            //if (x != null)
            //    serviceHost.Log.Action($"Hurra, {x.SAPBelNr} ist da! ");

            //// https://weblog.west-wind.com/posts/2018/Feb/18/Accessing-Configuration-in-NET-Core-Test-Projects

            //var iConfig = GetIConfigurationRoot(Environment.CurrentDirectory);

            //// Transaktion anlegen  
            //Transaction tr = new Transaction()
            //{
            //    Id = 80,
            //    OrderNr = "4800047468",
            //    LicPlateTruck = "TEST ZH 564 778",
            //    LicPlateTrailer = "",
            //    FirstWeight = 28000,
            //    SecondWeight = 14000,
            //    LutroWeight = 300,
            //    AtroWeight = 200
            //};

            //List<Fraction> lf = new List<Fraction>();
            //lf.Add(new Fraction() { Name = "HE_FRAK1", Weight = 200, });
            //lf.Add(new Fraction() { Name = "HE_FRAK2", Weight = 100, });
            //lf.Add(new Fraction() { Name = "HE_FRAK3", Weight = 150, });
            //lf.Add(new Fraction() { Name = "HE_FRAK4", Weight = 250, });
            //lf.Add(new Fraction() { Name = "HE_FRAK5", Weight = 50, });
            //      var result = sapConnectorService.HolzFraktionmeldung(tr, lf).Result;
            //var result = sapConnectorService.SchlammDaten(true, true, true).Result;
            //printer.PrintWeighingTicket();
            //System.Xml.Serialization.XmlSerializer hugo = new System.Xml.Serialization.XmlSerializer(result.GetType());
            //var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//Result.xml";
            //System.IO.FileStream file = System.IO.File.Create(path);
            //hugo.Serialize(file, result);

            //var options = new System.Text.Json.JsonSerializerOptions { WriteIndented = true };
            //string fileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//Result.json";
            //string jsonString = System.Text.Json.JsonSerializer.Serialize(result, options);
            //System.IO.File.WriteAllText(fileName, jsonString);

            Ping sender = new Ping();
            if (sender.Send(configuration.IPIND780, 1000).Status == IPStatus.Success)
            {
                TerminalClient client = new TerminalClient(new IPEndPoint(IPAddress.Parse(configuration.IPIND780), 1701));
                client.RemotingEndPoints.Add(new ReflectionEndPoint<ITerminalInterface>(10, new TerminalInterface(configuration, serviceHost.Log, sapConnectorService, zebraPrinterService, starPrinterService)));

                serviceHost.Log.Action("Starting client...");
                client.Start();

                SharedVariable<DateTime> ak0105 = client.Client.GetVariable<DateTime>("AK0105");
                SharedVariableCallbackHandle y = ak0105.RequestCallback();
                ak0105.VariableChanged += (s, e) =>
                                {
                                    serviceHost.Log.Action($"Received : {ak0105.Value}");
                                };

                client.Client.ConnectionOpened += (s, e) => serviceHost.Log.Success("Connection to terminal opened.");
                client.Client.ConnectionClosed += (s, e) => serviceHost.Log.Success("Connection to terminal closed.");
                client.Client.ConnectionEstablished += (s, e) => serviceHost.Log.Success("Connection to terminal established.");
                client.Client.ConnectionBroken += (s, e) => serviceHost.Log.Warning("Connection to terminal broken.");
                client.Client.UnexpectedException += (s, e) => serviceHost.Log.Error("Commuication error", e.Exception);

                serviceHost.Log.Action("Started client...");
                serviceHost.CancellationToken.WaitHandle.WaitOne();

                serviceHost.Log.Action("Stopping client...");

                client.Stop();
            }
            else
                serviceHost.Log.Error(string.Format($"IND780 @{configuration.IPIND780} configured, but not reachable"));
        }

        public IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json", optional: false)
                .Build();
        }
    }
}
