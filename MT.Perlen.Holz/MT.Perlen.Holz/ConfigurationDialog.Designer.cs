﻿namespace MT.Perlen.Holz
{
    partial class ConfigurationDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIPIND780 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIPStar = new System.Windows.Forms.TextBox();
            this.txtIPZebra = new System.Windows.Forms.TextBox();
            this.txtIPScale = new System.Windows.Forms.TextBox();
            this.txtPortScale = new System.Windows.Forms.TextBox();
            this.txtPortServer = new System.Windows.Forms.TextBox();
            this.txtDBServer = new System.Windows.Forms.TextBox();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.txtIPSAPServer = new System.Windows.Forms.TextBox();
            this.txtSAPUser = new System.Windows.Forms.TextBox();
            this.txtSAPPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtIPIND780
            // 
            this.txtIPIND780.BackColor = System.Drawing.SystemColors.Control;
            this.txtIPIND780.Location = new System.Drawing.Point(281, 18);
            this.txtIPIND780.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtIPIND780.Name = "txtIPIND780";
            this.txtIPIND780.Size = new System.Drawing.Size(147, 23);
            this.txtIPIND780.TabIndex = 3;
            this.txtIPIND780.Leave += new System.EventHandler(this.txtIPIND780_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "IP-Adresse Wägeterminal:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "IP-Adresse STAR-Printer (Port 9100)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "IP-Adresse Zebra-Printer (Port 9100)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "IP-Adresse Probenwaage";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "IP-Port Probenwaage";
            // 
            // txtIPStar
            // 
            this.txtIPStar.BackColor = System.Drawing.SystemColors.Control;
            this.txtIPStar.Location = new System.Drawing.Point(281, 48);
            this.txtIPStar.Name = "txtIPStar";
            this.txtIPStar.Size = new System.Drawing.Size(147, 23);
            this.txtIPStar.TabIndex = 10;
            this.txtIPStar.Leave += new System.EventHandler(this.txtIPStar_Leave);
            // 
            // txtIPZebra
            // 
            this.txtIPZebra.BackColor = System.Drawing.SystemColors.Control;
            this.txtIPZebra.Location = new System.Drawing.Point(281, 78);
            this.txtIPZebra.Name = "txtIPZebra";
            this.txtIPZebra.Size = new System.Drawing.Size(147, 23);
            this.txtIPZebra.TabIndex = 11;
            this.txtIPZebra.Leave += new System.EventHandler(this.txtIPZebra_Leave);
            // 
            // txtIPScale
            // 
            this.txtIPScale.BackColor = System.Drawing.SystemColors.Control;
            this.txtIPScale.Location = new System.Drawing.Point(281, 108);
            this.txtIPScale.Name = "txtIPScale";
            this.txtIPScale.Size = new System.Drawing.Size(147, 23);
            this.txtIPScale.TabIndex = 12;
            this.txtIPScale.Leave += new System.EventHandler(this.txtIPScale_Leave);
            // 
            // txtPortScale
            // 
            this.txtPortScale.BackColor = System.Drawing.SystemColors.Control;
            this.txtPortScale.Location = new System.Drawing.Point(281, 138);
            this.txtPortScale.Name = "txtPortScale";
            this.txtPortScale.Size = new System.Drawing.Size(147, 23);
            this.txtPortScale.TabIndex = 13;
            this.txtPortScale.Leave += new System.EventHandler(this.txtPortScale_Leave);
            // 
            // txtPortServer
            // 
            this.txtPortServer.BackColor = System.Drawing.SystemColors.Control;
            this.txtPortServer.Location = new System.Drawing.Point(281, 185);
            this.txtPortServer.Name = "txtPortServer";
            this.txtPortServer.Size = new System.Drawing.Size(147, 23);
            this.txtPortServer.TabIndex = 14;
            this.txtPortServer.Leave += new System.EventHandler(this.txtPortServer_Leave);
            // 
            // txtDBServer
            // 
            this.txtDBServer.BackColor = System.Drawing.SystemColors.Control;
            this.txtDBServer.Location = new System.Drawing.Point(281, 233);
            this.txtDBServer.Name = "txtDBServer";
            this.txtDBServer.Size = new System.Drawing.Size(234, 23);
            this.txtDBServer.TabIndex = 15;
            this.txtDBServer.Leave += new System.EventHandler(this.txtDBServer_Leave);
            // 
            // txtDBName
            // 
            this.txtDBName.BackColor = System.Drawing.SystemColors.Control;
            this.txtDBName.Location = new System.Drawing.Point(281, 262);
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(152, 23);
            this.txtDBName.TabIndex = 16;
            this.txtDBName.Leave += new System.EventHandler(this.txtDBName_Leave);
            // 
            // txtIPSAPServer
            // 
            this.txtIPSAPServer.BackColor = System.Drawing.SystemColors.Control;
            this.txtIPSAPServer.Location = new System.Drawing.Point(281, 315);
            this.txtIPSAPServer.Name = "txtIPSAPServer";
            this.txtIPSAPServer.Size = new System.Drawing.Size(147, 23);
            this.txtIPSAPServer.TabIndex = 17;
            this.txtIPSAPServer.Leave += new System.EventHandler(this.txtIPSAPServer_Leave);
            // 
            // txtSAPUser
            // 
            this.txtSAPUser.BackColor = System.Drawing.SystemColors.Control;
            this.txtSAPUser.Location = new System.Drawing.Point(281, 344);
            this.txtSAPUser.Name = "txtSAPUser";
            this.txtSAPUser.Size = new System.Drawing.Size(147, 23);
            this.txtSAPUser.TabIndex = 18;
            this.txtSAPUser.Leave += new System.EventHandler(this.txtSAPUser_Leave);
            // 
            // txtSAPPassword
            // 
            this.txtSAPPassword.BackColor = System.Drawing.SystemColors.Control;
            this.txtSAPPassword.Location = new System.Drawing.Point(281, 373);
            this.txtSAPPassword.Name = "txtSAPPassword";
            this.txtSAPPassword.Size = new System.Drawing.Size(147, 23);
            this.txtSAPPassword.TabIndex = 19;
            this.txtSAPPassword.Leave += new System.EventHandler(this.txtSAPPassword_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "Server-Port";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 15);
            this.label7.TabIndex = 21;
            this.label7.Text = "DB-Server/Instanz";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 318);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 15);
            this.label8.TabIndex = 22;
            this.label8.Text = "IP-Adresse SAP";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(52, 347);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 15);
            this.label9.TabIndex = 23;
            this.label9.Text = "Username SAP";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(52, 377);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 15);
            this.label10.TabIndex = 24;
            this.label10.Text = "Passwort SAP";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 404);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(323, 15);
            this.label11.TabIndex = 25;
            this.label11.Text = "Falls nötig, Client- und System-Nr im XML-Datei anpassen.  ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(52, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 15);
            this.label12.TabIndex = 26;
            this.label12.Text = "DB-Name";
            // 
            // ConfigurationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSAPPassword);
            this.Controls.Add(this.txtSAPUser);
            this.Controls.Add(this.txtIPSAPServer);
            this.Controls.Add(this.txtDBName);
            this.Controls.Add(this.txtDBServer);
            this.Controls.Add(this.txtPortServer);
            this.Controls.Add(this.txtPortScale);
            this.Controls.Add(this.txtIPScale);
            this.Controls.Add(this.txtIPZebra);
            this.Controls.Add(this.txtIPStar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIPIND780);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ConfigurationDialog";
            this.Size = new System.Drawing.Size(537, 453);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtIPIND780;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIPStar;
        private System.Windows.Forms.TextBox txtIPZebra;
        private System.Windows.Forms.TextBox txtIPScale;
        private System.Windows.Forms.TextBox txtPortScale;
        private System.Windows.Forms.TextBox txtPortServer;
        private System.Windows.Forms.TextBox txtDBServer;
        private System.Windows.Forms.TextBox txtDBName;
        private System.Windows.Forms.TextBox txtIPSAPServer;
        private System.Windows.Forms.TextBox txtIPSAPUser;
        private System.Windows.Forms.TextBox txtSAPPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSAPUser;
    }
}
