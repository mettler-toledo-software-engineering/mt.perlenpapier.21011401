﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.Perlen.Holz.Infrastructure;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace MT.Perlen.Holz
{
    public class ScaleService : IService
    {
        private ScaleProvider scaleProvider;
        private readonly PerlenConfiguration configuration;
        private bool toggle = true;

        public ScaleService(ScaleProvider scaleProvider, PerlenConfiguration configuration)
        {
            this.scaleProvider = scaleProvider;
            this.configuration = configuration;
        }

        public string Name => "Scale Client";

        public void Run(IServiceHost serviceHost)
        {
            while (!serviceHost.CancellationToken.IsCancellationRequested)
            {
                try
                {
                    using var client = new TcpClient();
                    client.Connect("127.0.0.1", 8000);

                    var stream = client.GetStream();
                    stream.ReadTimeout = 5000;
                    var reader = new StreamReader(stream);
                    var writer = new StreamWriter(stream) { AutoFlush = true };
                    while (!serviceHost.CancellationToken.IsCancellationRequested)
                    {
                        writer.WriteLine(toggle ? "SI" : "TA");
                        var line = reader.ReadLine();

                        if (line.StartsWith("S") || line.StartsWith("TA"))
                        {
                            var weightAndUnit = line.Substring(4).Trim().Split(' ');
                            if (double.TryParse(weightAndUnit[0], out var weight) && weightAndUnit.Length > 1)
                            {
                                var myWeight = new Weight(weight, weightAndUnit[1]);
                                scaleProvider.SetWeightTest(myWeight);
                                //serviceHost.Log.Action(myWeight.ToString());
                            }
                        }
                        

                        serviceHost.CancellationToken.WaitHandle.WaitOne(500);
                    }
                }
                catch (IOException ex)
                {
                    serviceHost.Log.Error("Communication Exception", ex);
                }
            }
        }
    }

    public record Weight(double value, string unit);
    public record Tara(double value, string unit);
}
