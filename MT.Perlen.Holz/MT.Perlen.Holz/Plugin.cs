﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MT.DataServices.Configuration;
using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using MT.Perlen.Holz.Infrastructure;
using MT.Perlen.Holz.Infrastructure.ScaleDriver;
using MT.Perlen.Holz.Model;

namespace MT.Perlen.Holz
{
    /// <summary>
    /// Plugin for writing the log messages to disk and managing the log files.
    /// </summary>
    [Export(typeof(IPlugin))]
    public class Plugin : IPlugin
    {
        [Import]
        internal ILogSink LogSink { get; private set; }

        private IPluginHost host;
        private PerlenConfiguration configuration;
        public Plugin()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        /// <summary>
        /// Activates the plugin.
        /// </summary>
        /// <param name="host">The plugin host environment.</param>
        public void Activate(IPluginHost host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            this.host = host;
            LoadConfiguration();
            host.StartServices += OnStartServices;
        }

        private void OnStartServices(object sender, ServicesEventArgs e)
        {
            var scaleProvider = new ScaleProvider();
           
            e.ServiceManager.CreateServiceContainer(new WebServerService(scaleProvider, configuration), TimeSpan.FromSeconds(30)).Start();
            e.ServiceManager.CreateServiceContainer(new ScaleDriver(scaleProvider,configuration), TimeSpan.FromSeconds(30)).Start();
        }


        /// <summary>
        /// Creates a user control that provides the UI for configuring this plugin.
        /// </summary>
        /// <param name="configurationHost">The configuration dialog host object.</param>
        /// <returns>
        /// A usercontrol or <c>null</c> if this plugin does not support configuration.
        /// </returns>
        public IEnumerable<IPluginConfigurationPane> CreateConfigurationControl(IPluginConfigurationHost configurationHost)
        {
            configurationHost.FormClosed += OnConfigurationFormClosed;
            yield return new ConfigurationDialog(configuration);

        }

        /// <summary>
        /// Gets the name of this plugin.
        /// </summary>
        public string Name
        {
            get { return "Perlen Holz Server"; }
        }

        /// <summary>
        /// Gets the manufacturer of the plugin.
        /// </summary>
        public string Manufacturer
        {
            get { return "Mettler Toledo (Schweiz) GmbH"; }
        }

        /// <summary>
        /// Gets the version of the plugin.
        /// </summary>
        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        private void OnConfigurationFormClosed(object sender, PluginConfigurationHostClosingEventArgs e)
        {
            if (e.SaveChanges)
                SaveConfiguration();
            else
            {
                // If the user decides to cancel the changes, reload the old configuration.
                LoadConfiguration();
            }
        }
        internal void LoadConfiguration()
        {
            configuration = host.LoadConfigurationObject() as PerlenConfiguration ?? new PerlenConfiguration();
        }

        internal void SaveConfiguration()
        {
            host.SaveConfigurationObject(configuration);
        }

        public PerlenConfiguration Configuration
        {
            get { return configuration; }
        }
    }
}
