﻿using System.Xml.Serialization;

namespace MT.Perlen.Holz
{
    public class PerlenConfiguration
    {
        public PerlenConfiguration()
        {

            IPIND780 = "192.168.1.144";
            IPPrinterStar = "192.168.174.204";
            IPTestScale = "192.168.174.202";
            PortTestScale = 8000;
            IPPrinterZD620 = "192.168.174.203";
            ServerPort = 5000;
            ComPrinterCitizen = "COM3";

            SAPClient = "100";
            SAPIP = "10.41.55.151";
            SAPSystem = "00";
            SAPUser = "s_mtwaage";
            SAPPass = "s_mtw";

            DBName = "MT.Perlen.Holz";
            DBServer = "(local)\\SQLEXPRESS2014_";
            ConnectionStringSAP = $"AppServerHost={SAPIP}; SystemNumber={SAPSystem};Client={SAPClient};User ={SAPUser};Password={SAPPass};Language=DE;";
            ConnectionString = $"{DBServer}; Database = {DBName}; Integrated Security = SSPI; ";
            MatNrRundholz = "E0100330";
            TicketPath = @"C:\Program Files\MT.DataServiceHolz\Data";
        }

        public string IPIND780 { get; set; }
        public string IPPrinterStar { get; set; }
        public string IPPrinterZD620 { get; set; }
        public string IPTestScale { get; set; }
        public string ComPrinterCitizen { get; set; }

        public int PortTestScale { get; set; }

        public string DBName { get; set; }
        public string DBServer { get; set; }

        public int ServerPort { get; set; }
        [XmlIgnore]
        public string ConnectionString { get; set; }
        [XmlIgnore]
        public string ConnectionStringSAP { get; set; }

        public string MatNrRundholz { get; set; }
        public string TicketPath { get; set; }

        public string SAPIP { get; set; }
        public string SAPSystem { get; set; }
        public string SAPClient { get; set; }
        public string SAPUser { get; set; }
        public string SAPPass { get; set; }


    }
}
