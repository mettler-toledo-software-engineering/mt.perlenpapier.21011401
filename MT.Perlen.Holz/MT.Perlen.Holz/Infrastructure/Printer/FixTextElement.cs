﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public class FixTextElement : TextElementBase
    {
        public string Text { get; set; }

        internal override void Print(StarWriter writer)
        {
            int idx = writer.DefineStringTypeAndPosition(XStart, YStart, CharWidthFactor, CharHeightFactor, (int) Font, (int) charDirection, (int) stringDirection);
            writer.PlaceStringFix(idx, Text);
        }
    }
}
