﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MT.Perlen.Holz.Infrastructure
{
    internal class StarWriter
    {
        private readonly MemoryStream buffer = new MemoryStream();
        private int lineIdx;
        private int characterLineIdx;
        private int barcodeLineIdx;
        private readonly Dictionary<string, string> fieldParameters;

        public StarWriter(Dictionary<string, string> fieldParameters)
        {
            this.fieldParameters = fieldParameters;
        }

        private void WriteCommand(string cmd)
        {
            buffer.WriteByte(27);
            byte[] b = Encoding.GetEncoding(850).GetBytes(cmd);
            buffer.Write(b, 0, b.Length);
            buffer.WriteByte(10);
            buffer.WriteByte(0);
        }

        public void ClearFormat()
        {
            WriteCommand("C");
        }

        public void EnableCutter()
        {
            WriteCommand("B");
        }

        public void FeedLabel()
        {
            WriteCommand("I");
        }

        public void DrawLine(float xStart, float yStart, float xEnd, float yEnd, int thickness)
        {
            bool horizontal;
            horizontal = (yStart == yEnd);

            WriteCommand(string.Format("L{0:00};{1:0000},{2:0000},{3:0000},{4:0000},{5},{6}",
                lineIdx++, xStart.ToTenth(), yStart.ToTenth(), xEnd.ToTenth(), yEnd.ToTenth(), horizontal ? 0 : 1, thickness));
        }

        public int DefineStringTypeAndPosition(float xStart, float yStart, int charWidthFactor, int charHeightFactor, int charType, int charDirection, int stringDirection)
        {
            return DefineStringTypeAndPosition(xStart, yStart, charWidthFactor, charHeightFactor, charType, charDirection, stringDirection, null);
        }

        public int DefineStringTypeAndPosition(float xStart, float yStart, int charWidthFactor, int charHeightFactor, int charType, int charDirection, int stringDirection, int? charPitchUnits)
        {
            string charPitchPreformatted = (charPitchUnits.HasValue ? "," + charPitchUnits.Value.ToString("00") : "");

            WriteCommand(string.Format("PC{0:00};{1:0000},{2:0000},{3},{4},{5},{6:0}{7:0}{8}",
                characterLineIdx++, xStart.ToTenth(), yStart.ToTenth(), charWidthFactor, charHeightFactor,
                charType, charDirection, stringDirection, charPitchPreformatted));
            return characterLineIdx - 1;
        }

        public void PlaceStringFix(int FieldIdx, string Text)
        {
            WriteCommand(string.Format("RC{0:00};{1}", FieldIdx, Text));
        }

        public void PlaceStringVariable(int fieldIdx, string key, int minFieldWidth)
        {
            PlaceStringFix(fieldIdx, (fieldParameters[key] ?? "").PadLeft(minFieldWidth));
        }

        public int DefineBarcodeTypeAndPosition(float xStart, float yStart, int barCodeMode, int barCodeType, int printDirection, float barCodeHeight)
        {
            WriteCommand(string.Format("PB{0:00};{1:0000},{2:0000},{3},{4},{5},{6:0000}",
                barcodeLineIdx++, xStart.ToTenth(), yStart.ToTenth(), barCodeMode, barCodeType, printDirection, barCodeHeight.ToTenth()));
            return barcodeLineIdx - 1;
        }

        public void PlaceBarcodeFix(int FieldIdx, string Text)
        {
            WriteCommand(string.Format("RB{0:00};{1}", FieldIdx, Text));
        }

        public void PlaceBarcodeVariable(int FieldIdx, string key)
        {
            PlaceBarcodeFix(FieldIdx, fieldParameters[key]);
        }

        public void SetCharacterPitch(int pitches)
        {
            WriteCommand(string.Format("Y{00}", pitches));
        }

        public void SetPrintArea(float formatHeight)
        {
            WriteCommand(string.Format("D{0:0000}", formatHeight.ToTenth()));
        }

        public void WriteMemorySwitch(int swNo, int Bit, bool State)
        {
            string Mask = (1 << Bit).ToString("X4");
            WriteCommand(string.Format("\x001d#{0:X1}{1}{2}", (State) ? "+" : "-", swNo, Mask));
        }

        public void SaveMemorySwitch()
        {
            WriteCommand("\x001d#W00000");
        }

        public byte[] GetByteArray()
        {
            byte[] b = new byte[buffer.Length];
            Buffer.BlockCopy(buffer.GetBuffer(), 0, b, 0, (int) buffer.Length);
            return b;
        }
    }

    internal static class FloatExtension
    {
        public static int ToTenth(this float x)
        {
            return ((int) (10*x));
        }

        public static int ToDot(this float x)
        {
            return ((int) (8*x));
        }
    }
}
