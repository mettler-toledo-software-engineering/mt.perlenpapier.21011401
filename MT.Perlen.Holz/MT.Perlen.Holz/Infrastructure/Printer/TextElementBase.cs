﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public abstract class TextElementBase : TemplateElement
    {
        public float XStart { get; set; }
        public float YStart { get; set; }
        public int CharWidthFactor { get; set; }
        public int CharHeightFactor { get; set; }
        public FontType Font { get; set; }
        public PrintDirection charDirection { get; set; }
        public PrintDirection stringDirection { get; set; }
        public int PitchUnits { get; set; }
        public int MinFieldWidth { get; set; }
    }

    public enum FontType
    {
        Small8x16 = 1,
        Standard16x24 = 2,
        Chinese = 3,
        Bold24x32
    }

    public enum PrintDirection
    {
        Rotate0Degree = 0,
        Rotate90Degree = 1,
        Rotate180Degree = 2,
        Rotate270Degree = 3
    }
}
