﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public abstract class BarcodeElementBase : TemplateElement
    {
        public int XStart { get; set; }
        public int YStart { get; set; }
        public int BarcodeModeSelection { get; set; }
        public BCType BarcodeType { get; set; }
        public PrintDirection Direction { get; set; }
        public float BarcodeHeight { get; set; }
    }

    public enum BCType
    {
        Code39 = 1,
        Interleaved2of5 = 2,
        Code93 = 3,
        UPCA = 4,
        EAN8 = 5,
        EAN13 = 6,
        Code128 = 7,
        NW7 = 8,
        UPCE = 9
    }
}
