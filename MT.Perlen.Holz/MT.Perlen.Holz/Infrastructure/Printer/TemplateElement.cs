﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace MT.Perlen.Holz.Infrastructure
{
    [XmlInclude(typeof(TextElementBase))]
    [XmlInclude(typeof(VarTextElement))]
    [XmlInclude(typeof(FixTextElement))]
    [XmlInclude(typeof(BarcodeElementBase))]
    [XmlInclude(typeof(VarBarcodeElement))]
    [XmlInclude(typeof(FixBarcodeElement))]
    [XmlInclude(typeof(LineElement))]
    public abstract class TemplateElement
    {
        internal abstract void Print(StarWriter writer);
    }
}
