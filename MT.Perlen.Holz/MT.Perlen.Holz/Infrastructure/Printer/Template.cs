﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MT.Perlen.Holz.Infrastructure
{
    [Serializable]
    public class Template : TemplateElement
    {
        private static readonly XmlSerializer serializer = new XmlSerializer(typeof(Template));
        public float LabelLength { get; set; }
        public int TemplatelId { get; set; }
        public List<TemplateElement> Elements { get; set; }

        public Template()
        {
            Elements = new List<TemplateElement>();
        }

        internal override void Print(StarWriter writer)
        {
            writer.ClearFormat();
            writer.SetPrintArea(LabelLength);
            foreach(var element in Elements)
            {
                element.Print(writer);
            }
            writer.EnableCutter();
            writer.FeedLabel();
        }

        public void Save(string fileName)
        {
            using(FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                Save(fs);
            }
        }

        public void Save(Stream stream)
        {
            serializer.Serialize(stream, this);
        }

        public static Template Load(string fileName)
        {
            using(FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                return Load(fs);
            }
        }

        public static Template Load(Stream stream)
        {
            return (Template) serializer.Deserialize(stream);
        }
    }
}
