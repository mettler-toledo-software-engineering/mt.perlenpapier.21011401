﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public class VarBarcodeElement : BarcodeElementBase
    {
        public string Key { get; set; }

        internal override void Print(StarWriter writer)
        {
            int idx = writer.DefineBarcodeTypeAndPosition(XStart, YStart, BarcodeModeSelection, (int) BarcodeType, (int) Direction, BarcodeHeight);
            writer.PlaceBarcodeVariable(idx, Key);
        }
    }
}
