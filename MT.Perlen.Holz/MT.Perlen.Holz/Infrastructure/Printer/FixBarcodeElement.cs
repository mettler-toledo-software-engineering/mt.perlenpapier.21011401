﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public class FixBarcodeElement : BarcodeElementBase
    {
        public string Text { get; set; }

        internal override void Print(StarWriter writer)
        {
            int idx = writer.DefineBarcodeTypeAndPosition(XStart, YStart, BarcodeModeSelection, (int) BarcodeType, (int) Direction, BarcodeHeight);
            writer.PlaceBarcodeFix(idx, Text);
        }
    }
}
