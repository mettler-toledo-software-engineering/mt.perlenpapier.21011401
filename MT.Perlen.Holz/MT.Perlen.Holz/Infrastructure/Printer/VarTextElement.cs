﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public class VarTextElement : TextElementBase
    {
        public string Key { get; set; }

        internal override void Print(StarWriter writer)
        {
            int idx = writer.DefineStringTypeAndPosition(XStart, YStart, CharWidthFactor, CharHeightFactor, (int) Font, (int) charDirection, (int) stringDirection, PitchUnits);
            writer.PlaceStringVariable(idx, Key, MinFieldWidth);
        }
    }
}
