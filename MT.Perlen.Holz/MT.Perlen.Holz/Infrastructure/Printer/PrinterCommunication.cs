﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using MT.DataServices.Logging;

namespace MT.Perlen.Holz.Infrastructure.Printer
{
    public class PrinterCommunication
    {
        private byte[] iBuf;
        private int bufPtr = -1;

        public event PrtStatusEventHandler PrinterStatus;

        public delegate void PrtStatusEventHandler(PrinterFlags status, EventArgs e);

        private TcpClient client;
        private IPEndPoint iPEndPoint;
        private readonly ILogSink logSink;

        public PrinterCommunication(ILogSink logSink, IPAddress iPAdress)
        {

            this.iPEndPoint = new IPEndPoint(iPAdress, 9100);
            this.logSink = logSink;
        }

        public void ManualStatusCheck()
        {
            byte[] buf = new byte[100];
            int bytesReceived = 0;
            try
            {
                Ping sender = new Ping();
                if (sender.Send(iPEndPoint.Address, 250).Status == IPStatus.Success)
                {
                    using (client = new TcpClient())
                    {
                        client.Connect(iPEndPoint);
                        var stream = client.GetStream();
                        stream.ReadTimeout = 3000;
                        stream.Write(new byte[] { 0x1B, 0x06, 0x01 }, 0, 3);
                        stream.Flush();

                        while (bytesReceived < 9)
                        {
                            int byteread = stream.Read(buf, bytesReceived, 9 - bytesReceived);
                            if (byteread == 0)
                            {
                                throw new Exception("Connection Closed!");
                            }

                            bytesReceived += byteread;
                        }

                        InsertIntoIBuf(buf);
                        stream.Close();
                        client.Close();
                    }
                }
                else
                    logSink.Error(string.Format($"StarPrinter @{iPEndPoint} not reachable"));
            }
            catch (Exception ex)
            {
                logSink.Error("[ManualStatusCheck]: ", ex);
            }

        }


        private void InsertIntoIBuf(byte[] buf)
        {
            for (int i = 0; i < buf.Length; i++)
            {
                if (buf[i] == 0x23)
                {
                    iBuf = new byte[9];
                    bufPtr = 0;
                }

                if (bufPtr != -1)
                    iBuf[bufPtr++] = buf[i];

                if (bufPtr == 9)
                {
                    ParseAndValidate(iBuf);
                    bufPtr = -1;
                }
            }
        }

        private void ParseAndValidate(byte[] buf)
        {
            PrinterFlags flag = PrinterFlags.None;
            // Test
            if ((buf[2] & (byte)0x08) > 0) flag |= PrinterFlags.OffLine;
            if ((buf[2] & (byte)0x0020) > 0) flag |= PrinterFlags.CoverOpen;
            if ((buf[3] & (byte)0x0008) > 0) flag |= PrinterFlags.AutocutterErr;
            if ((buf[3] & (byte)0x0020) > 0) flag |= PrinterFlags.NonRecoverableErr;
            if ((buf[3] & (byte)0x0040) > 0) flag |= PrinterFlags.HeadTempToHigh;
            if ((buf[4] & (byte)0x0004) > 0) flag |= PrinterFlags.PaperJamAtPresenter;
            if ((buf[4] & (byte)0x0008) > 0) flag |= PrinterFlags.BlackmarkErr;
            if ((buf[5] & (byte)0x0004) > 0) flag |= PrinterFlags.PaperNearEnd;
            if ((buf[5] & (byte)0x0008) > 0) flag |= PrinterFlags.PaperEnd;

            if (PrinterStatus != null)
                PrinterStatus(flag, EventArgs.Empty);
        }

        public void WritePrinterSetupSmallTicket()
        {
            StarWriter sw = new StarWriter(null);
            sw.WriteMemorySwitch(1, 9, true); // Top Margin Setting 3mm
            sw.WriteMemorySwitch(1, 8, false); // Black Mark Function off
            sw.WriteMemorySwitch(1, 4, false); // Normal Zero representation

            sw.WriteMemorySwitch(1, 0, false); // International Characters
            sw.WriteMemorySwitch(1, 1, true); // Germany
            sw.WriteMemorySwitch(1, 2, false);
            sw.WriteMemorySwitch(1, 3, false);

            sw.WriteMemorySwitch(2, 4, false); // Low printing speed
            sw.WriteMemorySwitch(2, 5, true);

            sw.WriteMemorySwitch(4, 0, true); // Paper Width 80 mm
            sw.WriteMemorySwitch(4, 1, false);
            sw.WriteMemorySwitch(4, 2, false);

            sw.WriteMemorySwitch(7, 12, true); // Enable Sensors
            sw.WriteMemorySwitch(7, 8, true);
            sw.WriteMemorySwitch(6, 4, false);

            sw.SaveMemorySwitch();

            SendData(sw.GetByteArray());
        }

        public void SendTemplate(Template template, Dictionary<string, string> fieldParameters)
        {
            StarWriter writer = new StarWriter(fieldParameters);
            template.Print(writer);
            SendData(writer.GetByteArray());
        }

        private void SendData(byte[] pData)
        {
            try
            {
                Ping sender = new Ping();
                if (sender.Send(iPEndPoint.Address, 250).Status == IPStatus.Success)
                {
                    using (client = new TcpClient())
                    {
                        client.Connect(iPEndPoint);
                        var stream = client.GetStream();

                        stream.Write(pData);
                        stream.Flush();
                        stream.Close();
                        client.Close();
                    }
                }
                else
                    logSink.Error(string.Format($"StarPrinter @{iPEndPoint} not reachable"));
            }
            catch (Exception ex)
            {
                logSink.Error("[SendData]: ", ex);
            }

        }
    }

    [Flags]
    public enum PrinterFlags
    {
        None = 0,
        OffLine = 0x0001,
        CoverOpen = 0x0002,
        HeadTempToHigh = 0x0004,
        NonRecoverableErr = 0x0008,
        AutocutterErr = 0x0010,
        BlackmarkErr = 0x0020,
        PaperJamAtPresenter = 0x0040,
        PaperEnd = 0x0080,
        PaperNearEnd = 0x0100
    }
}
