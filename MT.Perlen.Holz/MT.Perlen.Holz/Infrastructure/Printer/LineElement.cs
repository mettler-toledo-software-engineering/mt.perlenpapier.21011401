﻿using System;
using System.Linq;

namespace MT.Perlen.Holz.Infrastructure
{
    public class LineElement : TemplateElement
    {
        public float XStart { get; set; }
        public float YStart { get; set; }
        public float XEnd { get; set; }
        public float YEnd { get; set; }
        public int Thickness { get; set; }

        internal override void Print(StarWriter writer)
        {
            writer.DrawLine(XStart, YStart, XEnd, YEnd, Thickness);
        }
    }
}
