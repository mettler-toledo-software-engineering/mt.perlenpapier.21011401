﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class HolzAtromeldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_LUTRO")]
        public double Lutro { get; set; }
        [SapName("IF_ATRO")]
        public double Atro { get; set; }
        [SapName("IF_GEWEI")]
        public string UOM { get; set; }  // 'g'
    }

    public class HolzAtromeldungFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }


}
