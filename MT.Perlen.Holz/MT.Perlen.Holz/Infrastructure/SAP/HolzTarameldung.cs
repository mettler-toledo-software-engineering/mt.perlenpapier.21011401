﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class HolzTarameldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_BRGEW")]
        public double Tara { get; set; }
        [SapName("IF_GEWEI")]
        public string UOM { get; set; }   // 'kg'
        [SapName("IF_KZ_EINAUS")]
        public string Type { get; set; }  // 'A'
        [SapName("IF_KZ_HANDEINGABE")]
        public string Manual { get; set; }  // ' '

    }

    public class HolzTarameldungFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }


}
