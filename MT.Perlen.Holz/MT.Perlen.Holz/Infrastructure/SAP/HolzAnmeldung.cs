﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class HolzAnmeldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_EBELN")]
        public string OrderNr { get; set; }
        [SapName("IF_CANCEL")]
        public string Cancel { get; set; }
    }

    public class HolzAnmeldungFromSAP
    {
        [SapName("ES_HOLZ")]
        public ES_Holz Holz { get; set; }
        [SapName("ET_ADR")]
        public PartnerT[] Carrier { get; set; }
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }

    public class ES_Holz
    {
        [SapName("LIFNR_LF")]
        public string LiefNr { get; set; }
        [SapName("NAME1_LF")]
        public string LiefName { get; set; }
        [SapName("PSTLZ_LF")]
        public string LiefPLZ { get; set; }
        [SapName("ORT01_LF")]
        public string LiefOrt { get; set; }
        [SapName("LAND1_LF")]
        public string LiefLand { get; set; }
        [SapName("LIFNR_SP")]
        public string SpediNr { get; set; }
        [SapName("NAME1_SP")]
        public string SpediName { get; set; }
        [SapName("PSTLZ_SP")]
        public string SpediPLZ { get; set; }
        [SapName("ORT01_SP")]
        public string SpediOrt { get; set; }
        [SapName("LAND1_SP")]
        public string SpediLand { get; set; }
        [SapName("MATNR")]
        public string MatNr { get; set; }
        [SapName("MAKTX")]
        public string MatName { get; set; }
        [SapName("EVERS")]
        public string Versand { get; set; }
        [SapName("BELNR")]
        public string SapBelNr { get; set; }
        [SapName("MULTI_SPED")]
        public string MoreCarrier { get; set; }
    }



}
