﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class SchlammwiegungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_BRGEW")]
        public double Brutto { get; set; }
        [SapName("IF_GEWEI")]
        public string UOM { get; set; }   // 'kg'
        [SapName("IF_KZ_EINAUS")]
        public string Type { get; set; }  // 'A'
    }

    public class SchlammwiegungFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }
}
