﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class SchlammDataToSAP
    {
        [SapName("IF_ADR")]
        public string Partner { get; set; }
        [SapName("IF_MAT")]
        public string Material { get; set; }
        [SapName("IF_DAT")]
        public string Delivery { get; set; }


    }

    public class SchlammDataFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
        [SapName("ET_ADR")]
        public PartnerT[] Partners { get; set; }
        [SapName("ET_MAT")]
        public MaterialT[] Materials { get; set; }
        [SapName("ET_DAT")]
        public DeliveryT[] Deliveries { get; set; }
    }

    public class PartnerT
    {

        [SapName("PARVW")]
        public string Role { get; set; }  // SP = Carrier  , WE ShipTo
        [SapName("KUNNR")]
        public string Nr { get; set; }
        [SapName("NAME1")]
        public string Name { get; set; }
        [SapName("NAME2")]
        public string Name2 { get; set; }
        [SapName("ORT01")]
        public string Ort { get; set; }
        [SapName("LAND1")]
        public string Land { get; set; }
    }

    public class MaterialT
    {
        [SapName("MATNR")]
        public string Nr { get; set; }
        [SapName("MAKTX")]
        public string MatName { get; set; }
    }


    public class DeliveryT
    {
        [SapName("SPDNR")]
        public string CarrierNr { get; set; }
        [SapName("KUNWE")]
        public string ShipToNr { get; set; }
        [SapName("MATNR")]
        public string MatNr { get; set; }
    }
}
