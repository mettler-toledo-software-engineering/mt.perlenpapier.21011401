﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class BAPIRet2
    {
        [SapName("TYPE")]
        public string Type { get; set; }
        [SapName("ID")]
        public string ID { get; set; }
        [SapName("NUMBER")]
        public string Number { get; set; }
        [SapName("MESSAGE")]
        public string Message { get; set; }
        [SapName("LOG_NO")]
        public string LogNr { get; set; }
        [SapName("LOG_MSG_NO")]
        public string LogMsgNr { get; set; }
        [SapName("MESSAGE_V1")]
        public string Message1 { get; set; }
        [SapName("MESSAGE_V2")]
        public string Message2 { get; set; }
        [SapName("MESSAGE_V3")]
        public string Message3 { get; set; }
        [SapName("MESSAGE_V4")]
        public string Message4 { get; set; }
        [SapName("PARAMETER")]
        public string Parameter { get; set; }
        [SapName("ROW")]
        public int Row { get; set; }
        [SapName("FIELD")]
        public string Field { get; set; }
        [SapName("SYSTEM")]
        public string System { get; set; }

    }
}
