﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{

    public class HolzBruttomeldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_BRGEW")]
        public double Brutto { get; set; }
        [SapName("IF_SIGNI")]
        public string LicPlateTruck { get; set; }
        [SapName("IF_SIGNI2")]
        public string LicPlateTrailer { get; set; }
        [SapName("IF_GEWEI")]
        public string UOM { get; set; }   // 'kg'
        [SapName("IF_KZ_EINAUS")]
        public string Type { get; set; }  // 'E'
        [SapName("IF_KZ_HANDEINGABE")]
        public string Manual { get; set; }  // ' '
#if VER2
        [SapName("IF_SPDNR")]
        public string CarrierNr { get; set; }
#endif
    }

    public class HolzBruttomeldungFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }


}
