﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class SchlammanmeldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IF_SPDNR")]
        public string CarrierId { get; set; }
        [SapName("IF_KUNWE")]
        public string ShipToId { get; set; }
        [SapName("IF_MATNR")]
        public string MatId { get; set; }
        [SapName("IF_BRGEW")]
        public double Tara { get; set; }
        [SapName("IF_SIGNI")]
        public string LicPlateTruck { get; set; }
        [SapName("IF_SIGNI2")]
        public string LicPlateTrailer { get; set; }
        [SapName("IF_GEWEI")]
        public string UOM { get; set; }   // 'kg'
        [SapName("IF_KZ_EINAUS")]
        public string Type { get; set; }  // 'E'

    }

    public class SchlammanmeldungFromSAP
    {
        [SapName("ES_SCHLAMM")]
        public ES_Schlamm Schlamm { get; set; }
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }

    public class ES_Schlamm
    {
        [SapName("KUNNR_WE")]
        public string LiefNr { get; set; }
        [SapName("NAME1_WE")]
        public string LiefName { get; set; }
        [SapName("PSTLZ_WE")]
        public string LiefPLZ { get; set; }
        [SapName("ORT01_WE")]
        public string LiefOrt { get; set; }
        [SapName("LAND1_WE")]
        public string LiefLand { get; set; }
        [SapName("LIFNR_SP")]
        public string SpediNr { get; set; }
        [SapName("NAME1_SP")]
        public string SpediName { get; set; }
        [SapName("PSTLZ_SP")]
        public string SpediPLZ { get; set; }
        [SapName("ORT01_SP")]
        public string SpediOrt { get; set; }
        [SapName("LAND1_SP")]
        public string SpediLand { get; set; }
        [SapName("MATNR")]
        public string MatNr { get; set; }
        [SapName("MAKTX")]
        public string MatName { get; set; }
        [SapName("BELNR")]
        public string SapBelNr { get; set; }
    }

}
