﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.SAP
{
    public class HolzFraktionmeldungToSAP
    {
        [SapName("IF_MT_ID")]
        public string MTID { get; set; }
        [SapName("IT_FRAKT")]
        public HolzFraktion[] Fraktionen { get; set; }
    }

    public class HolzFraktionmeldungFromSAP
    {
        [SapName("ES_BAPIRET2")]
        public BAPIRet2 BAPIRet { get; set; }
    }

    public class HolzFraktion
    {
        [SapName("FRAK")]
        public string Name { get; set; }
        [SapName("NTGEW")]
        public double Netto { get; set; }
        [SapName("GEWEI")]
        public string UOM { get; set; }   // 'G'
    }

}
