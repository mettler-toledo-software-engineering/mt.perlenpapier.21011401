﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Collections.Immutable;
using SapNwRfc;
using MT.Perlen.Holz.Infrastructure.SAP;
using MT.DataServices.Logging;
using MT.Perlen.Holz.Model;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace MT.Perlen.Holz.Infrastructure
{
    [Export(typeof(SAPConnectorService))]
    public class SAPConnectorService
    {
        private string _connectionString;
        private PerlenConfiguration configuration;
        private IDbContextFactory<HolzDbContext> dBFactory;

        public SAPConnectorService(ILogSink logSink, PerlenConfiguration configuration, IDbContextFactory<HolzDbContext> dBFactory)
        {
            this.dBFactory = dBFactory;
            this.configuration = configuration;
            _connectionString = this.configuration.ConnectionStringSAP;
        }

        #region HolzAnmeldung

        public Task<HolzAnmeldungFromSAP> HolzAnmeldung(Transaction mtId)
        {
            var tcs = new TaskCompletionSource<HolzAnmeldungFromSAP>();
            var backgroundThread = new Thread(() => HolzAnmeldungInternal(
                tcs,
                mtId)
            );

            backgroundThread.Name = "SAP Communication Thread Login";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void HolzAnmeldungInternal(TaskCompletionSource<HolzAnmeldungFromSAP> tcs, Transaction mtId)
        {
            HolzAnmeldungFromSAP result;
            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_HOLZ_ANMELDUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<HolzAnmeldungFromSAP>(new HolzAnmeldungToSAP
                        {
                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            OrderNr = $"{mtId.OrderNr}",
                            Cancel = " "
                        });
                    }
                    //string[] result = new string[messages.RowCount];
                    //for (int i = 0; i < messages.RowCount; i++)
                    //{
                    //    result[i] = messages[i].GetString(0);
                    //}

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region Bruttomeldung

        public Task<HolzBruttomeldungFromSAP> HolzBruttomeldung(Transaction mtId)
        {
            var tcs = new TaskCompletionSource<HolzBruttomeldungFromSAP>();
            HolzDbContext holzDBContext = dBFactory.CreateDbContext();
            //HolzDbContext holzDBContext = new HolzDbContext(configuration);
            Partner aktCarrier = holzDBContext.Partners.Find(mtId.CarrierId);

            var backgroundThread = new Thread(() => HolzBruttomeldungInternal(
                tcs,
                mtId, aktCarrier)
            );

            backgroundThread.Name = "SAP Communication Thread Brutto";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void HolzBruttomeldungInternal(TaskCompletionSource<HolzBruttomeldungFromSAP> tcs, Transaction mtId, Partner aktCarrier)
        {
            HolzBruttomeldungFromSAP result;

            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_HOLZ_WIEGUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<HolzBruttomeldungFromSAP>(new HolzBruttomeldungToSAP
                        {

                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Brutto = mtId.FirstWeight,
                            LicPlateTrailer = mtId.LicPlateTrailer,
                            LicPlateTruck = mtId.LicPlateTruck,
                            UOM = "KG",
                            Type = "E",
                            Manual = " ",
                            CarrierNr = aktCarrier.SAPNr
                        }); ;
                    }
                    //string[] result = new string[messages.RowCount];
                    //for (int i = 0; i < messages.RowCount; i++)
                    //{
                    //    result[i] = messages[i].GetString(0);
                    //}

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region Tarameldung  (eigentlich Bruttomeldung mit Kennzeichen A)

        public Task<HolzTarameldungFromSAP> HolzTarameldung(Transaction mtId)
        {
            var tcs = new TaskCompletionSource<HolzTarameldungFromSAP>();
            var backgroundThread = new Thread(() => HolzTarameldungInternal(
                tcs,
                mtId)
            );

            backgroundThread.Name = "SAP Communication Thread Tara";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void HolzTarameldungInternal(TaskCompletionSource<HolzTarameldungFromSAP> tcs, Transaction mtId)
        {
            HolzTarameldungFromSAP result;
            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_HOLZ_WIEGUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<HolzTarameldungFromSAP>(new HolzTarameldungToSAP
                        {

                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Tara = mtId.SecondWeight,
                            UOM = "KG",
                            Type = "A",
                            Manual = " "
                        });
                    }

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region Atromeldung

        public Task<HolzAtromeldungFromSAP> HolzAtromeldung(Transaction mtId)
        {
            var tcs = new TaskCompletionSource<HolzAtromeldungFromSAP>();
            var backgroundThread = new Thread(() => HolzAtromeldungInternal(
                tcs,
                mtId)
            );

            backgroundThread.Name = "SAP Communication Thread";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void HolzAtromeldungInternal(TaskCompletionSource<HolzAtromeldungFromSAP> tcs, Transaction mtId)
        {
            HolzAtromeldungFromSAP result;
            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_HOLZ_TROCKNUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<HolzAtromeldungFromSAP>(new HolzAtromeldungToSAP
                        {

                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Lutro = mtId.LutroWeight,
                            Atro = mtId.AtroWeight,
                            UOM = "G",
                        });
                    }
                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region Fraktionmeldung

        public Task<HolzFraktionmeldungFromSAP> HolzFraktionmeldung(Transaction mtId, List<Fraction> fraction)
        {
            var tcs = new TaskCompletionSource<HolzFraktionmeldungFromSAP>();
            var backgroundThread = new Thread(() => HolzFraktionmeldungInternal(
                tcs,
                mtId, fraction)
            );

            backgroundThread.Name = "SAP Communication Thread Fraktion";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void HolzFraktionmeldungInternal(TaskCompletionSource<HolzFraktionmeldungFromSAP> tcs, Transaction mtId, List<Fraction> fraction)
        {
            HolzFraktionmeldungFromSAP result;
            try
            {
                HolzFraktion[] holzfraktion = new HolzFraktion[fraction.Count];

                for (int i = 0; i < fraction.Count; i++)
                {
                    holzfraktion[i] = new HolzFraktion();
                    holzfraktion[i].Name = fraction[i].Name;
                    holzfraktion[i].Netto = fraction[i].Weight;
                    holzfraktion[i].UOM = "G";
                }

                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_HOLZ_FRAKTION")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<HolzFraktionmeldungFromSAP>(new HolzFraktionmeldungToSAP
                        {
                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Fraktionen = holzfraktion,
                        });
                    }

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region Schlammdata

        public Task<SchlammDataFromSAP> SchlammDaten(bool deliveryFlag, bool materialFlag, bool partnerFlag)
        {
            var tcs = new TaskCompletionSource<SchlammDataFromSAP>();
            var backgroundThread = new Thread(() => SchlammDatenInternal(
                tcs,
                deliveryFlag,
                materialFlag,
                partnerFlag
            ));

            backgroundThread.Name = "SAP Communication Thread Fraktion";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void SchlammDatenInternal(TaskCompletionSource<SchlammDataFromSAP> tcs, bool deliveryFlag, bool materialFlag, bool partnerFlag)
        {
            SchlammDataFromSAP result;
            try
            {


                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_SCHLAMM_MD")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<SchlammDataFromSAP>(new SchlammDataToSAP
                        {
                            Delivery = deliveryFlag ? "X" : "",
                            Material = materialFlag ? "X" : "",
                            Partner = partnerFlag ? "X" : ""
                        }); ;
                    }

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region SchlammTarameldung 

        public Task<SchlammanmeldungFromSAP> MudSignOn(Transaction mtId, string carrier, string partner, string material)
        {
            var tcs = new TaskCompletionSource<SchlammanmeldungFromSAP>();

            var backgroundThread = new Thread(() => MudSignOnInternal(
                tcs,
                mtId, carrier, partner, material)
            );

            backgroundThread.Name = "SAP Communication Thread Brutto";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void MudSignOnInternal(TaskCompletionSource<SchlammanmeldungFromSAP> tcs, Transaction mtId, string carrier, string partner, string material)
        {
            SchlammanmeldungFromSAP result;

            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_SCHLAMM_ANMELDUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<SchlammanmeldungFromSAP>(new SchlammanmeldungToSAP
                        {

                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Tara = mtId.FirstWeight,
                            LicPlateTrailer = mtId.LicPlateTrailer,
                            LicPlateTruck = mtId.LicPlateTruck,
                            UOM = "KG",
                            Type = "E",
                            CarrierId = carrier,
                            ShipToId = partner,
                            MatId = material
                        }); ;
                    }

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

        #region SchlammBruttomeldung 

        public Task<SchlammwiegungFromSAP> MudWeight(Transaction mtId)
        {
            var tcs = new TaskCompletionSource<SchlammwiegungFromSAP>();

            var backgroundThread = new Thread(() => MudWeightInternal(
                tcs,
                mtId)
            );

            backgroundThread.Name = "SAP Communication Thread Brutto";
            backgroundThread.Start();

            return tcs.Task;
        }

        private void MudWeightInternal(TaskCompletionSource<SchlammwiegungFromSAP> tcs, Transaction mtId)
        {
            SchlammwiegungFromSAP result;

            try
            {
                using (SapConnection connection = new SapConnection(_connectionString))
                {
                    connection.Connect();

                    using (ISapFunction customerBapi = connection.CreateFunction("Z_SNMTTC_SCHLAMM_WIEGUNG")) // ist durch TCON festgelegt worden
                    {
                        result = customerBapi.Invoke<SchlammwiegungFromSAP>(new SchlammwiegungToSAP
                        {

                            MTID = string.Format("H{0:0000000}", mtId.Id),
                            Brutto = mtId.SecondWeight,
                            UOM = "KG",
                            Type = "A",
                        }); ;
                    }

                    tcs.SetResult(result);
                };
            }

            catch (Exception ex)
            {
                //  Logger.ErrorEx($"Exception SAP", SourceClass, ex);
                tcs.SetException(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        #endregion

    }


    public class SAPResult
    {
        public SAPResult(ImmutableArray<string> errors)
        {
            Errors = errors;
        }

        public ImmutableArray<string> Errors { get; private set; }
    }


}

