﻿using MT.DataServices.Logging;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;


namespace MT.Perlen.Holz.Infrastructure
{
    public class NetworkPrinter
    {
        private IPAddress ipAddress;
        private string _printerIp;
        private int port;
        private TcpClient client;
        private readonly ILogSink logSink;
        // private Configuration _configuration;


        public NetworkPrinter(ILogSink logSink, PerlenConfiguration configuration)
        {
            port = 9100;
            ipAddress = IPAddress.Parse(configuration.IPPrinterZD620);
            this.logSink = logSink;
        }

        public PrintState CheckIfPrinterIsOnline()
        {
            PrintState result = PrintState.PrinterOffline;
            try
            {
                var ping = new Ping();
                var reply = ping.Send(ipAddress, 500);
                result = reply?.Status == IPStatus.Success ? PrintState.PrinterOnline : PrintState.PrinterOffline;
            }
            catch (Exception ex)
            {
                logSink.Error("Zebra ping printer failed", ex);
                return PrintState.PrinterOffline;
            }

            return result;
        }

        public PrintState Print(string template)
        {
            try
            {

                using (client = new TcpClient())
                {
                    client.Connect(new IPEndPoint(ipAddress, port));
                    var message = Encoding.UTF8.GetBytes(template);
                    var stream = client.GetStream();
                    stream.Write(message, 0, message.Length);
                    stream.Close();
                    client.Close();
                    return PrintState.PrintDone;
                }
            }
            catch (Exception ex)
            {
                logSink.Error("Zebra printer could not be reached", ex);
                return PrintState.PrintFailed;
            }
        }
    }
    public enum PrintState
    {
        PrintDone,
        PrintFailed,
        PrintFailedTemplate,
        PrinterOffline,
        PrinterOnline,
        PrinterNoData
    }

}
