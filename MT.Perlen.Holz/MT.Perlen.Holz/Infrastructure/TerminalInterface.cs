﻿using Microsoft.EntityFrameworkCore;
using MT.DataServices.Logging;
using MT.Perlen.Holz.Abstractions;
using MT.Perlen.Holz.Infrastructure.SAP;
using MT.Perlen.Holz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure
{
    public class TerminalInterface : ITerminalInterface
    {
        private readonly PerlenConfiguration configuration;
        private readonly ILogSink logSink;
        private readonly StarPrinter printer;
        private readonly SAPConnectorService sapConnectorService;
        private readonly ZebraPrinter zebraPrinterService;

        private PartnerT aktCarrier = new PartnerT();
        private HolzAnmeldungFromSAP aktHolzAnmeldungFromSAP = null;
        private int aktTransactionId;
        private int carrierIndex;

        public TerminalInterface(PerlenConfiguration configuration, ILogSink logSink, SAPConnectorService sapConnectorService,
            ZebraPrinter zebraPrinterService, StarPrinter starPrinter)
        {
            this.configuration = configuration;
            this.logSink = logSink;
            this.printer = starPrinter;
            this.sapConnectorService = sapConnectorService;
            this.zebraPrinterService = zebraPrinterService;
        }

        public double MudWeigh(string barcode, double gross)
        {
            return Convert.ToDouble(0);
        }

        public double WoodWeight(double id, double gross, string carrierNr, string mode)
        {
            HolzDbContext holzDBContext = new HolzDbContext(configuration);
            Transaction tr = holzDBContext.Transactions.Find(Convert.ToInt32(id));
            Material material = holzDBContext.Materials.Find(tr.MatId);
            if (tr != null)
            {
                logSink.Action($"WoodWeight: {tr.OrderNr} MT-Id {id:N0}, Mode {mode} Weight {gross:F0}");
                try
                {
                    if (mode == "I")   // = Einwaage
                    {
                        tr.FirstWeight = gross;
                        tr.FirstWeightCreatedAt = DateTime.Now;
                        tr.SecondWeight = 0;
                        tr.DeltaWeight = 0;
                        if (tr.CarrierId == 0)
                        {
                            PartnerT aktCarrier = aktHolzAnmeldungFromSAP.Carrier.FirstOrDefault(d => d.Nr.Trim() == carrierNr.Trim());
                            if (aktCarrier != null)
                                tr.CarrierId = CreatePartner(tr, aktCarrier, null);
                        }

                        holzDBContext.SaveChanges();
                        HolzBruttomeldungFromSAP result = sapConnectorService.HolzBruttomeldung(tr).Result;

                        if (result.BAPIRet.Message.Length == 0)
                        {
                            printer.PrintWeighingTicket(tr);
                            zebraPrinterService.Print(tr, configuration.MatNrRundholz.Trim() == material.SAPNr.Trim() ? 2 : 1);
                            return Convert.ToDouble(1);
                        }
                        else
                            return Convert.ToDouble(0);

                    }


                    if (mode == "O")   // = Einwaage
                    {
                        tr.SecondWeight = gross;
                        tr.SecondWeightCreatedAt = DateTime.Now;
                        tr.DeltaWeight = tr.FirstWeight - tr.SecondWeight;
                        holzDBContext.SaveChanges();
                        HolzTarameldungFromSAP result = sapConnectorService.HolzTarameldung(tr).Result;
                        if (result.BAPIRet.Message.Length == 0)
                        {
                            printer.PrintWoodTicket(tr);
                            return Convert.ToDouble(1);
                        }
                        else
                            return Convert.ToDouble(0); // SAP Problem
                    }

                    if (mode == "R")   // = Reprint
                    {
                        if (tr.DeltaWeight > 0)
                            printer.PrintWoodTicket(tr);
                        else
                            printer.PrintWeighingTicket(tr);
                        return Convert.ToDouble(1);
                    }
                }
                catch (Exception ex)
                {
                    logSink.Error("WoodWeight: [Ex]:", ex);
                    return Convert.ToDouble(0);
                }
            }
            else
            {
                logSink.Error($"WoodWeight: MT-Id  {id:N0} unknown!");
                return (-1);

            }
            return Convert.ToDouble(0);
        }

        public double SignOn(string barcode, string plate1, string force, out string name, out int status)
        {
            HolzAnmeldungFromSAP result;
            status = 1;
            aktTransactionId = 0;
            if (barcode.Length == 0)
            {
                status = 0;
                name = "Keine Bestell-Nr";
                logSink.Error($"Leere Bestellnummer");
                return (-1);
            }

            // Transaktion anlegen wenn nicht vorhanden oder erzwungen
            HolzDbContext holzDBContext = new HolzDbContext(configuration);
            Transaction tr = holzDBContext.Transactions.FirstOrDefault(d => d.OrderNr == barcode && d.DeltaWeight == 0);
            try
            {
                if ((tr == null) || (force == "X"))
                {
                    tr = new Transaction()
                    {
                        OrderNr = barcode,
                        LicPlateTruck = plate1.ToUpper(),
                        LicPlateTrailer = ""
                    };

                    holzDBContext.Transactions.Add(tr);
                    holzDBContext.SaveChanges();
                    logSink.Action($"SignOn: {barcode} MT-Id H{tr.Id:0000000}");

                    result = sapConnectorService.HolzAnmeldung(tr).Result;

                    if (result.BAPIRet.Message.Length == 0)
                    {
                        carrierIndex = 0;
                        if (result.Holz.MoreCarrier.Length == 0)
                        {

                            aktCarrier.Nr = result.Holz.SpediNr;
                            aktCarrier.Name = result.Holz.SpediName;
                            aktCarrier.Ort = result.Holz.SpediOrt;
                            aktCarrier.Land = result.Holz.SpediLand;
                            tr.CarrierId = CreatePartner(tr, aktCarrier, null);
                            status = 1;
                        }
                        else
                        {
                            tr.CarrierId = 0;
                            status = result.Carrier.Length;
                            aktCarrier = result.Carrier[carrierIndex];
                        }

                        tr.PartnerId = CreatePartner(tr, null, result.Holz);

                        Material material = new Material()
                        {
                            TransId = tr.Id,
                            SAPName = result.Holz.MatName,
                            SAPNr = result.Holz.MatNr.Trim()
                        };
                        holzDBContext.Materials.Add(material);
                        holzDBContext.SaveChanges();

                        tr.MatId = material.Id;

                        tr.SAPBelNr = result.Holz.SapBelNr;
                        holzDBContext.SaveChanges();

                        name = result.Holz.LiefName;
                        logSink.Action($"SignOn: Success {name} #Carrier: {status}");

                        aktTransactionId = tr.Id;
                        aktHolzAnmeldungFromSAP = result;
                        return Convert.ToDouble(tr.Id);
                    }
                    else
                    {
                        holzDBContext.Transactions.Remove(tr);
                        holzDBContext.SaveChanges();

                        name = result.BAPIRet.Message.Substring(0, Math.Min(result.BAPIRet.Message.Length, 40));
                        logSink.Error($"SignOn: Error Status {result.BAPIRet.Type}:  {result.BAPIRet.Message}");
                        return (-1);
                    }
                }

                else   // gleiche Ordernummer schon auf vorhanden
                {
                    string trimPlate1 = String.Concat(plate1.Where(c => !Char.IsWhiteSpace(c))).ToUpper();
                    string trimPlate2 = String.Concat(tr.LicPlateTruck.Where(c => !Char.IsWhiteSpace(c))).ToUpper();
                    if (trimPlate1 == trimPlate2)
                    {   // gleiches Nummernschild
                        status = 0;
                        name = "Bereits angemeldet...";
                        return (-1);
                    }
                    else
                    {
                        status = 1;
                        name = "Weitere Lieferung anmelden ?";
                        return (-1);
                    }
                }
            }
            catch (Exception ex)
            {
                logSink.Error("SignOn [Ex]:", ex);
                name = ex.Message.Substring(0, Math.Min(ex.Message.Length, 20));
                return (0);
            }
        }

        public double GetNextCarrier(double id, out string name, out string carrierNr)
        {
            name = aktCarrier.Name;
            carrierNr = aktCarrier.Nr;
            if (aktHolzAnmeldungFromSAP.Carrier.Length > 0)
            {
                carrierIndex = carrierIndex++ % aktHolzAnmeldungFromSAP.Carrier.Length;
                aktCarrier = aktHolzAnmeldungFromSAP.Carrier[carrierIndex];
            }
            return (1);
        }

        private int CreatePartner(Transaction tr, PartnerT partner, ES_Holz holzLI)
        {
            HolzDbContext holzDBContext = new HolzDbContext(configuration);

            if (partner != null)
            {
                Partner ptrCarrier = new Partner()
                {
                    TransId = tr.Id,
                    Type = "SP",
                    SAPNr = partner.Nr.Trim(),
                    SAPName1 = partner.Name,
                    SAPName2 = "",
                    SAPOrt = partner.Ort,
                    SAPPlz = "",
                    SAPCountry = partner.Land
                };
                holzDBContext.Partners.Add(ptrCarrier);
                holzDBContext.SaveChanges();
                return ptrCarrier.Id;
            }

            if (holzLI != null)
            {

                Partner ptrSupplier = new Partner()
                {
                    TransId = tr.Id,
                    Type = "LI",
                    SAPNr = holzLI.LiefNr.Trim(),
                    SAPName1 = holzLI.LiefName,
                    SAPName2 = "",
                    SAPOrt = holzLI.LiefOrt,
                    SAPPlz = holzLI.LiefPLZ,
                    SAPCountry = holzLI.LiefLand
                };
                holzDBContext.Partners.Add(ptrSupplier);
                holzDBContext.SaveChanges();
                return ptrSupplier.Id;
            }
            return 0;
        }
    }



}




