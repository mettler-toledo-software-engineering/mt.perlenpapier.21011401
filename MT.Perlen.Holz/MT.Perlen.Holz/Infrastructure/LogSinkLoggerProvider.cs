﻿using Microsoft.Extensions.Logging;
using MT.DataServices.Logging;
using System;
using AspNetLogLevel = Microsoft.Extensions.Logging.LogLevel;
using LogLevel = MT.DataServices.Logging.LogLevel;

namespace MT.Perlen.Holz.Infrastructure
{
    public class LogSinkLoggerProvider : ILoggerProvider
    {
        public LogSinkLoggerProvider(ILogSink logSink)
        {
            LogSink = logSink;
        }

        public ILogSink LogSink { get; }

        public ILogger CreateLogger(string categoryName)
        {
            return new LogSinkLogger(LogSink, categoryName);
        }

        public void Dispose()
        {
        }

        private sealed class LogSinkLogger : ILogger
        {
            private readonly ILogSink logSink;
            private readonly string category;

            public LogSinkLogger(ILogSink logSink, string category)
            {
                this.logSink = logSink;
                this.category = category;
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return new Scope();
            }

            public bool IsEnabled(AspNetLogLevel logLevel)
            {
                return true;
            }

            public void Log<TState>(AspNetLogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                var translatedLogLevel = logLevel switch
                {
                    AspNetLogLevel.Trace => LogLevel.Action,
                    AspNetLogLevel.Debug => LogLevel.Action,
                    AspNetLogLevel.Information => LogLevel.Hint,
                    AspNetLogLevel.Warning => LogLevel.Warning,
                    AspNetLogLevel.Error => LogLevel.Error,
                    AspNetLogLevel.Critical => LogLevel.Fatal,
                    _ => LogLevel.Warning,
                };

                logSink.Log(new LogMessage(translatedLogLevel, $"[{category}] {formatter(state, exception)}"));
            }

            private sealed class Scope : IDisposable
            {
                public void Dispose()
                {
                }
            }
        }
    }
}
