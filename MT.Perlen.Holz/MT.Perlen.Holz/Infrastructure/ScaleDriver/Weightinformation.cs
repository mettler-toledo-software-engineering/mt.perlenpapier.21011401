﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.ScaleDriver
{
    public class WeightInformation
    {

        public bool isValid { get; set; }
        public bool isOverload { get; set; }
        public bool isUnderLoad { get; set; }
        public bool isStable { get; set; }
        public string NetDisplayed { get; set; }
        public string GrossDisplayed { get; set; }
        public string TareDisplayed { get; set; }
        public string Unit { get; set; }
        public string WeightFormat { get; set; }
        public Protocol ScaleProtocol { get; set; }
        public TimeSpan LastUpdate { get; set; }

        public enum Protocol
        {
            MMR,
            SICS
        }
        private double gross;
        public double Gross
        {
            get
            {
                return gross;
            }
            set
            {
                gross = value;
                GrossDisplayed = gross.ToString(WeightFormat);
            }
        }
        private double net;
        public double Net
        {
            get
            {
                return net;
            }
            set
            {
                net = value;
                NetDisplayed = net.ToString(WeightFormat);
            }
        }

        private double tare;
        public double Tare
        {
            get
            {
                return tare;
            }
            set
            {
                tare = value;
                TareDisplayed = tare.ToString(WeightFormat);
            }
        }
    }
}
