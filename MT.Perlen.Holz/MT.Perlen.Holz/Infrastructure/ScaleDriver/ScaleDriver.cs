﻿using MT.DataServices.Logging;
using MT.DataServices.Plugins;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.ScaleDriver
{
    public class ScaleDriver : IService
    {
        private readonly ScaleProvider scaleProvider;
        private readonly ConcurrentQueue<string> Queue = new ConcurrentQueue<string>();
        IPAddress hostIP;
        int port;
        bool running = true;
        WeightInformation wi = new WeightInformation();
        public ErrorStates errorState { get; set; }

        public string Name => "Scale Client";
        public int timeout;
        private string inputBuffer = "";
        private byte[] bytes = new byte[1024];


        /// <summary>
        /// Constructor TcpIp client
        /// </summary>

        // Constructer for TcpIp-Sics client
        public ScaleDriver(ScaleProvider scaleProvider, PerlenConfiguration configuration)
        {
            hostIP = IPAddress.Parse(configuration.IPTestScale);
            port = configuration.PortTestScale;
            timeout = 2000;
            this.scaleProvider = scaleProvider;
        }

        public void TareAsynch()
        {
            Queue.Enqueue("T");
        }
        public void ClearTareAsynch()
        {
            Queue.Enqueue("TAC");
        }
        public void ZeroAsynch()
        {
            Queue.Enqueue("Z");
        }


        /// <summary>
        /// TCP client thread
        /// </summary>
        /// 
        public void Run(IServiceHost serviceHost)
        {
            int i = 0;
            Socket sender = null;
            errorState = ErrorStates.ConnectionClosed;

            string response = "";
            while (!serviceHost.CancellationToken.IsCancellationRequested && running)
            {
                if (sender == null)
                {
                    IPEndPoint remoteEP = new IPEndPoint(hostIP, port);
                    // Create a TCP/IP  socket.  
                    sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sender.ReceiveTimeout = 1500;

                    try
                    {
                        IAsyncResult result = sender.BeginConnect(hostIP, port, null, null);
                        bool success = result.AsyncWaitHandle.WaitOne(timeout, true);
                        if (sender.Connected)
                        {
                            sender.EndConnect(result);
                            inputBuffer = "";
                            timeout = 100;
                            // serviceHost.Log.Success($"Sucessful connection to {hostIP.ToString()}:{port.ToString()}");
                            errorState = ErrorStates.Connected;
                        }
                        else
                        {
                            sender.Close();
                            serviceHost.Log.Error($"Connection to {hostIP.ToString()}:{port.ToString()} failed..");
                            errorState = ErrorStates.ConnectionFailed;
                            scaleProvider.SetWeight(0, 0, "", TimeSpan.MaxValue);
                            timeout = 5000;
                            sender = null;
                            // running = false;
                        }

                    }
                    catch (Exception e)
                    {
                        serviceHost.Log.Success($"Nein {hostIP.ToString()}:{port.ToString()}");
                        serviceHost.Log.Error(e.Message);
                        errorState = ErrorStates.failed;
                        sender = null;
                        //     running = false;
                    }
                }

                if (sender != null && sender.Connected == false)   // Socket von unten geschlossen
                {
                    sender.Close();
                    serviceHost.Log.Error($"Connection to {hostIP.ToString()}:{port.ToString()} lost.");
                    errorState = ErrorStates.ConnectionFailed;
                    scaleProvider.SetWeight(0, 0, "", TimeSpan.MaxValue);
                    timeout = 5000;
                    sender = null;
                }

                if (sender != null && sender.Connected && errorState == ErrorStates.Connected)
                {
                    try
                    {
                        DateTime dt = DateTime.Now;
                        Thread.Sleep(500);
                        string cmd = "";
                        if (Queue.TryDequeue(out cmd))
                        {
                            byte[] msg = Encoding.ASCII.GetBytes(cmd + "\r\n");
                            sender.Send(msg);
                            var r = ReceiveSICSResponse(sender);
                        }
                        else
                        {

                            byte[] msg = Encoding.ASCII.GetBytes("SI\r\n");
                            sender.Send(msg);
                            // Receive the response from the remote device.  
                            response = ReceiveSICSResponse(sender);
                            if (!response.StartsWith("!"))
                            {
                                // do SI Handling
                                if (SICSParser(response, wi))
                                {
                                    msg = Encoding.ASCII.GetBytes("TA\r\n");
                                    sender.Send(msg);
                                    // Receive the response from the remote device.  
                                    response = ReceiveSICSResponse(sender);
                                    if (!response.StartsWith("!"))
                                    {
                                        SICSParser(response, wi);
                                        wi.Gross = wi.Net + wi.Tare;
                                        TimeSpan diff = DateTime.Now - dt;
                                        scaleProvider.SetWeight(wi.Net, wi.Tare, wi.Unit, diff);
                                        if (i++ % 60 == 0)
                                            serviceHost.Log.Action($"Weight received: {wi.Net.ToString("N1")}, {diff.TotalSeconds.ToString()}");
                                        
                                    }
                                    else
                                        scaleProvider.SetWeight(0, 0, "", TimeSpan.MaxValue);

                                    sender.Disconnect(false);
                                    sender.Close();
                                    errorState = ErrorStates.ConnectionClosed;
                                    timeout = 5000;
                                    sender = null;
                                }
                            }
                            else
                            {  
                                scaleProvider.SetWeight(0, 0, "", TimeSpan.MaxValue);
                                serviceHost.Log.Error($"Response result :{response}");
                                sender.Disconnect(false);
                                sender.Close();
                                errorState = ErrorStates.ConnectionClosed;
                                timeout = 5000;
                                sender = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        // running = false;
                        if ((sender != null) && (sender.Connected == true))
                        {
                            serviceHost.Log.Error($"Connection to {hostIP.ToString()}:{port.ToString()} lost.: {ex.Message}");
                            sender.Disconnect(false);
                            sender.Close();
                            sender.Dispose();
                            sender = null;
                        }
                    }

                }
            }

            if ((sender != null) && (sender.Connected == true))
            {
                sender.Disconnect(false);
                sender.Close();
                sender.Dispose();
                sender = null;
            }


        }

        private string ReceiveSICSResponse(Socket sender)
        {
            int bytesRec = 0;
            string response = "!Empty";
            do
            {
                try
                {
                    bytesRec = sender.Receive(bytes);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    response = $"!Receive timout after 1.5s\r\n{ex.Message}";
                    break;
                }
                inputBuffer += Encoding.ASCII.GetString(bytes, 0, bytesRec);
            }
            while (!inputBuffer.Contains("\r\n") && bytesRec > 0);
            if (inputBuffer.Length >= 2)
            {
                response = inputBuffer.Substring(0, inputBuffer.IndexOf("\r\n"));
                if (response.Length < 3)
                    response = "!To small";
              
                // inputBuffer = inputBuffer.Substring(inputBuffer.IndexOf("\r\n") + 2);
                inputBuffer = String.Empty;
            }

            return response;
        }


        bool SICSParser(string SICSresponse, WeightInformation wi)
        {
            mmrMode = false;
            if (SICSresponse.StartsWith("TA A"))
            {
                // Tara response
                if (!ParseWeight("TA A", SICSresponse, wi))
                    return false;
            }
            else if (SICSresponse.StartsWith("S S"))
            {
                // Weight stable response
                wi.ScaleProtocol = WeightInformation.Protocol.SICS;
                if (!ParseWeight("S S", SICSresponse, wi))
                    return false;
                wi.isValid = true;
                wi.isStable = true;
                wi.isUnderLoad = false;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("S D"))
            {
                // Weight dynamic response
                wi.ScaleProtocol = WeightInformation.Protocol.SICS;
                if (!ParseWeight("S D", SICSresponse, wi))
                    return false;
                wi.isValid = true;
                wi.isStable = false;
                wi.isUnderLoad = false;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("S "))
            {
                // Weight stable response
                wi.ScaleProtocol = WeightInformation.Protocol.MMR;
                mmrMode = true;
                if (!ParseWeight("S ", SICSresponse, wi))
                    return false;
                wi.isValid = true;
                wi.isStable = true;
                wi.isUnderLoad = false;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("SD"))
            {
                // Weight dynamic response
                wi.ScaleProtocol = WeightInformation.Protocol.MMR;
                mmrMode = true;
                if (!ParseWeight("SD", SICSresponse, wi))
                    return false;
                wi.isValid = true;
                wi.isStable = false;
                wi.isUnderLoad = false;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("TAC A"))
            {
                // TAC response
                wi.isValid = false;
            }
            else if (SICSresponse.StartsWith("T S"))
            {
                // Tare response
                wi.isValid = false;
            }
            else if (SICSresponse.StartsWith("Z A"))
            {
                // Zero response
                wi.isValid = false;
            }
            else if (SICSresponse.StartsWith("S -"))
            {
                // underload

                wi.isValid = false;
                wi.isUnderLoad = true;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("S +"))
            {
                // overload
                wi.isValid = false;
                wi.isUnderLoad = false;
                wi.isOverload = true;
            }
            else if (SICSresponse.StartsWith("SI-"))
            {
                // underload
                mmrMode = true;
                wi.isValid = false;
                wi.isUnderLoad = true;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("SI+"))
            {
                // overload
                mmrMode = true;
                wi.isValid = false;
                wi.isUnderLoad = false;
                wi.isOverload = true;
            }
            else if (SICSresponse.StartsWith("T -"))
            {
                // underload
                wi.isValid = false;
                wi.isUnderLoad = true;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("T +"))
            {
                // overload
                wi.isValid = false;
                wi.isUnderLoad = false;
                wi.isOverload = true;
            }
            else if (SICSresponse.StartsWith("Z -"))
            {
                // underload
                wi.isValid = false;
                wi.isUnderLoad = true;
                wi.isOverload = false;
            }
            else if (SICSresponse.StartsWith("Z +"))
            {
                // overload
                wi.isValid = false;
                wi.isUnderLoad = false;
                wi.isOverload = true;
            }
            else if (SICSresponse.StartsWith("E"))
            {
                // invalid
                wi.isValid = false;
            }
            else
                if (!mmrMode)
                wi.isValid = false;
            else
                wi.isValid = true;
            return true;
        }

        bool mmrMode;
        bool ParseWeight(string command, string response, WeightInformation wi)
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("de-CH");
            try
            {
                bool quit = false;
                response = response.Substring(command.Length).Trim();
                do
                {
                    response = response.Replace("  ", " ");
                    quit = response.IndexOf("  ") < 0;
                }
                while (!quit);
                string[] s = response.Split(' ');
                string format = "";
                for (int i = 0; i < s[0].Length; i++)
                {
                    if (s[0].Substring(i, 1) == ".")
                        format += ".";
                    else if (s[0].Substring(i, 1) != "-")
                        format += "0";
                }
                format += " ";
                format += s[1];
                wi.WeightFormat = format;
                wi.Unit = s[1];
                switch (command)
                {
                    case "S D":
                        mmrMode = false;
                        wi.Net = double.Parse(s[0], culture);
                        break;
                    case "S S":
                        mmrMode = false;
                        wi.Net = double.Parse(s[0], culture);
                        break;
                    case "TA A":
                        mmrMode = false;
                        wi.Tare = double.Parse(s[0], culture);
                        break;
                    case "SD":
                        mmrMode = true;
                        wi.Net = double.Parse(s[0], culture);
                        break;
                    case "S ":
                        mmrMode = true;
                        wi.Net = double.Parse(s[0], culture);
                        break;
                    default:
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}