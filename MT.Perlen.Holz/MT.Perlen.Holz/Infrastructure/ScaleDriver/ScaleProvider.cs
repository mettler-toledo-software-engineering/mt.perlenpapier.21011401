﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.ScaleDriver
{
    public class ScaleProvider
    {
        public ScaleProvider()
        {
            LastWeightTest = new Weight(0, 0, "g", TimeSpan.MinValue);
        }

        public record Weight(double net, double tare, string unit, TimeSpan diff);
       

        public Weight LastWeightTest { get; private set; }
        public Weight LastWeightIND780 { get; private set; }

        public event EventHandler WeightChanged;
        public event EventHandler WeightChangedIND780;

        public void SetWeight(double net, double tare,string unit, TimeSpan diff)
        {
            Weight newWeight = new Weight(net, tare, unit, diff);
            var previousWeight = LastWeightTest;
            LastWeightTest = newWeight;
            if (previousWeight != newWeight)
            {
                WeightChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public void SetWeightIND780(Weight weight)
        {
            var previousWeight = LastWeightIND780;
            LastWeightIND780 = weight;
            if (previousWeight != weight)
            {
                WeightChangedIND780?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
