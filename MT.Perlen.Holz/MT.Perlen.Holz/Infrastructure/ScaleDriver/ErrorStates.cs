﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Infrastructure.ScaleDriver
{
    public enum ErrorStates
    {
        ConnectionTimeout,
        ConnectionClosed,
        Connected,
        ConnectionFailed,
        ReceiveTimoeut,
        PortNotAvailable,
        succeeded,
        failed
    }
}
