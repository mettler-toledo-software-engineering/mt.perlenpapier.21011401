﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MT.DataServices.Logging;
using MT.Perlen.Holz.Model;

namespace MT.Perlen.Holz.Infrastructure
{
    [Export(typeof(ZebraPrinter))]
    public class ZebraPrinter
    {
        private readonly ILogSink logSink;

        private NetworkPrinter printer;
        private PerlenConfiguration configuration;
        private readonly IDbContextFactory<HolzDbContext> dBFactory;

        public ZebraPrinter(ILogSink logSink, PerlenConfiguration configuration, IDbContextFactory<HolzDbContext> dBFactory)
        {
            this.logSink = logSink;
            this.configuration = configuration;
            this.dBFactory = dBFactory;
            printer = new NetworkPrinter(this.logSink, this.configuration);
            this.logSink = logSink;
        }


        public PrintState Print(Transaction data, int count = 1)
        {
            string templateData = "";
            string fileName = string.Empty;

            var isOnline = printer.CheckIfPrinterIsOnline();
            if (isOnline == PrintState.PrinterOnline)
            {
                HolzDbContext holzDBContext = dBFactory.CreateDbContext();
                Partner supplier = holzDBContext.Partners.Find(data.PartnerId);
                Material material = holzDBContext.Materials.Find(data.MatId);
                try
                {

#if DEBUG
                    fileName = @"C:\MT-Projects\_PC-Apps\MT.Perlen.Holz\MT.Perlen.Holz\MT.Perlen.Holz\Data\AtroLayout.txt";
#else
                    fileName = @"C:\Program Files\MT.DataServiceHolz\Data\AtroLayout.txt";
                      //          fileName = Template.Load(Path.Combine(configuration.TicketPath,"AtroLayout.xml"));
#endif
                    if (File.Exists(fileName))
                    {
                        templateData = File.ReadAllText(fileName, Encoding.GetEncoding(1252));
                    }
                    else
                    {
                        logSink.Error($"No template at {fileName}");
                        return (PrintState.PrintFailedTemplate);
                    }
                    logSink.Action($"Printing Zebra-Ticket {data.Id}...");
                    templateData = templateData.Replace("{SUPNAME}", supplier.SAPName1.Substring(0, Math.Min(supplier.SAPName1.Length, 15)));
                    templateData = templateData.Replace("{SAPNR}", data.SAPBelNr);
                    templateData = templateData.Replace("{MTID}", string.Format("H{0:0000000}", data.Id));
                    templateData = templateData.Replace("{MATNAME}", material.SAPName.Substring(0, Math.Min(material.SAPName.Length, 12)));
                    templateData = templateData.Replace("{DATE}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                    templateData = templateData.Replace("{LICPLATE}", data.LicPlateTruck.Substring(0, Math.Min(data.LicPlateTruck.Length, 12)));
                    templateData = templateData.Replace("{COUNT}", string.Format("{0:0}", count));
                }
                catch (Exception ex)
                {
                    logSink.Error($"Zebra-Ticket: Print of template {fileName} failed !", ex);
                }

                return printer.Print(string.Concat(Environment.NewLine, templateData));

            }
            else
                logSink.Error($"Printer not reachable, Zebra-Ticket {data.Id} not printed !");
            return (PrintState.PrinterOffline);

        }

        /// <summary>
        /// Occures when the event is raised
        /// </summary>
        public event EventHandler<PrintResultEventArgs> PrintResult;

        /// <summary>
        /// Raises the <see cref="PrintResult"/> event
        /// </summary>
        /// <param name="e">An <see cref="PrintResultEventArgs"/> that contains the event data.</param>
        protected virtual void OnPrintResult(PrintResultEventArgs e)
        {
            if (PrintResult != null)
                PrintResult(this, e);
        }

        /// <summary>
        /// Contains all information related to a PrintResult event
        /// </summary>
        public class PrintResultEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PrintResult"/> class
            /// </summary>
            /// 
            public PrintStatus PrintStatus { get; set; }
            public string Message { get; set; }
            public PrintResultEventArgs(PrintStatus PrintStatus, string Message)
            {
                this.PrintStatus = PrintStatus;
                this.Message = Message;
            }
        }

        public enum PrintStatus
        {
            printTimeout,
            printFailed,
            errorTemplate,
            printDone,
            nothingToPrint
        }

    }


}
