﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using MT.DataServices.Logging;
using System.Net;
using MT.Perlen.Holz.Model;
using System.ComponentModel.Composition;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace MT.Perlen.Holz.Infrastructure
{
    /// <summary>
    /// Manages the connection to the label printer
    /// </summary>
    [Export(typeof(StarPrinter))]
    public class StarPrinter
    {
        private PrinterCommunication printer;

        private readonly ILogSink logSink;
        private readonly PerlenConfiguration configuration;
        private readonly IDbContextFactory<HolzDbContext> dBFactory;


        /// <summary>
        /// Opens the connection to the label printer
        /// </summary>

        public StarPrinter(ILogSink logSink, PerlenConfiguration configuration, IDbContextFactory<HolzDbContext> dBFactory)
        {

            this.logSink = logSink;
            this.configuration = configuration;
            this.dBFactory = dBFactory;
            printer = new PrinterCommunication(logSink, IPAddress.Parse(configuration.IPPrinterStar));
        }

        /// <summary>
        /// Gets the wood ticket template
        /// </summary>
        private Template WoodTicketTemplate
        {
            get
            {
                if (woodTicketTemplate == null)
                {
                    try
                    {
#if DEBUG
                        woodTicketTemplate = Template.Load(@"C:\MT-Projects\_PC-Apps\MT.Perlen.Holz\MT.Perlen.Holz\MT.Perlen.Holz\Data\TicketLayoutWood.xml");
#else
                        woodTicketTemplate = Template.Load(@"C:\Program Files\MT.DataServiceHolz\Data\TicketLayoutWood.xml");
                        //          woodTicketTemplate = Template.Load(Path.Combine(configuration.TicketPath,"TicketLayoutWood.xml"));
#endif
                    }
                    catch (Exception ex)
                    {
                        logSink.Error("[WoodTicketTemplate]: ", ex);
                    }
                }

                return woodTicketTemplate;
            }
        }
        private Template woodTicketTemplate;

        /// <summary>
        /// Gets the mud ticket template
        /// </summary>
        private Template MudTicketTemplate
        {
            get
            {
                if (mudTicketTemplate == null)
                {
                    try
                    {
#if DEBUG
                        mudTicketTemplate = Template.Load(@"C:\MT-Projects\_PC-Apps\MT.Perlen.Holz\MT.Perlen.Holz\MT.Perlen.Holz\Data\TicketLayoutMud.xml");
#else
                        mudTicketTemplate = Template.Load(@"C:\Program Files\MT.DataServiceHolz\Data\TicketLayoutMud.xml");
              //          mudTicketTemplate = Template.Load(Path.Combine(configuration.TicketPath,"TicketLayoutMud.xml"));
#endif
                    }
                    catch (Exception ex)
                    {
                        logSink.Error("[MudTicketTemplate]: ", ex);
                    }
                }

                return mudTicketTemplate;
            }
        }
        private Template mudTicketTemplate;

        /// <summary>
        /// Gets the ticket template
        /// </summary>
        private Template TicketTemplate
        {
            get
            {
                if (ticketTemplate == null)
                {
                    try
                    {
#if DEBUG
                        ticketTemplate = Template.Load(@"C:\MT-Projects\_PC-Apps\MT.Perlen.Holz\MT.Perlen.Holz\MT.Perlen.Holz\Data\TicketLayoutWeighing.xml");
#else
                        ticketTemplate = Template.Load(@"C:\Program Files\MT.DataServiceHolz\Data\TicketLayoutWeighing.xml");
                        //          ticketTemplate = Template.Load(Path.Combine(configuration.TicketPath,"TicketLayoutWeighing.xml"));
#endif
                    }
                    catch (Exception ex)
                    {
                        logSink.Error("[TicketTemplate]: ", ex);
                    }
                }

                return ticketTemplate;
            }
        }
        private Template ticketTemplate;

        /// <summary>
        /// Prints a label from the given template with the specified parameters
        /// </summary>
        /// <param name="template">The label template</param>
        /// <param name="parameters">The dictionary of parameter values</param>
        private void PrintTemplate(Template template, Dictionary<string, string> parameters)
        {
            if (printer == null)
                return;

            try
            {
                printer.SendTemplate(template, parameters);
            }
            catch (Exception ex)
            {
                logSink.Error("[PrintTemplate]: ", ex);
            }
        }

        /// <summary>
        /// Prints an wood ticket
        /// </summary>
        public void PrintWoodTicket(Transaction data)
        {
            try
            {
                Template template = WoodTicketTemplate;
                if (template != null && data != null)
                {
                    HolzDbContext holzDBContext = dBFactory.CreateDbContext();
                    Material material = holzDBContext.Materials.Find(data.MatId);
                    Partner carrier = holzDBContext.Partners.Find(data.CarrierId);
                    Partner supplier = holzDBContext.Partners.Find(data.PartnerId);
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    logSink.Action($"Printing Star-Label Wood-Ticket {data.Id}...");
                    parameters.Add("Artikel", $"{material.SAPNr} {material.SAPName}");
                    parameters.Add("TotalNetto", Math.Abs(data.DeltaWeight).ToString("F0"));
                    parameters.Add("Netto", Math.Abs(data.DeltaWeight).ToString("F0"));
                    parameters.Add("GewichtAusgang", data.SecondWeight.ToString("F0"));
                    parameters.Add("GewichtEingang", data.FirstWeight.ToString("F0"));
                    parameters.Add("WZAusgang", data.SecondWeightCreatedAt.ToString("HH:mm"));
                    parameters.Add("WZEingang", data.FirstWeightCreatedAt.ToString("HH:mm"));
                    parameters.Add("DatumZeit", DateTime.Now.ToString("dd.MM.yyyy"));
                    parameters.Add("TrailerNr", data.LicPlateTrailer);
                    parameters.Add("FahrzeugNr", data.LicPlateTruck);
                    parameters.Add("Lieferant", supplier.SAPNr);
                    parameters.Add("WLZeile1", supplier.SAPName1);
                    parameters.Add("WLZeile2", "");
                    parameters.Add("WLZeile3", $"{supplier.SAPPlz} {supplier.SAPOrt}");
                    parameters.Add("SAPNr", data.SAPBelNr);
                    parameters.Add("Spedition", $"{carrier.SAPNr} {carrier.SAPName1 }");
                    parameters.Add("MTNr", string.Format("H{0:0000000}", data.Id));
                    parameters.Add("BelNr", data.OrderNr);
                    PrintTemplate(template, parameters);

                }
            }
            catch (Exception ex)
            {
                logSink.Error($"Star-Label Wood Ex", ex);
            }
        }

        /// <summary>
        /// Prints an mud ticket
        /// </summary>
        public void PrintMudTicket(Transaction data)
        {
            try
            {
                Template template = MudTicketTemplate;
                if (template != null && data != null)
                {
                    HolzDbContext holzDBContext = dBFactory.CreateDbContext();
                    Material material = holzDBContext.Materials.Find(data.MatId);
                    Partner carrier = holzDBContext.Partners.Find(data.CarrierId);
                    Partner supplier = holzDBContext.Partners.Find(data.PartnerId);
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    logSink.Action($"Printing Star-Label Mud-Ticket {data.Id}...");
                    parameters.Add("Artikel", $"{material.SAPNr} {material.SAPName}");
                    parameters.Add("TotalNetto", Math.Abs(data.DeltaWeight).ToString("F0"));
                    parameters.Add("Netto", Math.Abs(data.DeltaWeight).ToString("F0"));
                    parameters.Add("GewichtAusgang", data.SecondWeight.ToString("F0"));
                    parameters.Add("GewichtEingang", data.FirstWeight.ToString("F0"));
                    parameters.Add("WZAusgang", data.SecondWeightCreatedAt.ToString("HH:mm"));
                    parameters.Add("WZEingang", data.FirstWeightCreatedAt.ToString("HH:mm"));
                    parameters.Add("DatumZeit", DateTime.Now.ToString("dd.MM.yyyy"));
                    parameters.Add("TrailerNr", data.LicPlateTrailer);
                    parameters.Add("FahrzeugNr", data.LicPlateTruck);
                    parameters.Add("Lieferant", supplier.SAPNr);
                    parameters.Add("WLZeile1", supplier.SAPName1);
                    parameters.Add("WLZeile2", "");
                    parameters.Add("WLZeile3", $"{supplier.SAPCountry} {supplier.SAPPlz} {supplier.SAPOrt}");
                    parameters.Add("SAPNr", data.SAPBelNr.Substring(data.SAPBelNr.Length - 4, 4));
                    parameters.Add("Spedition", $"{carrier.SAPName1}".Substring(0, Math.Min(25, carrier.SAPName1.Length)));
                    parameters.Add("MTNr", string.Format("H{0:0000000}", data.Id));
                    parameters.Add("BelNr", data.SAPBelNr);
                    PrintTemplate(template, parameters);

                }
            }
            catch (Exception ex)
            {
                logSink.Error($"Star-Label Mud Ex", ex);
            }
        }

        /// <summary>
        /// Prints a weighing ticket
        /// </summary>
     //  public void PrintWeighingTicket()
        public void PrintWeighingTicket(Transaction data)
        {
            try
            {
                Template template = TicketTemplate;

                if (template != null)
                {
                    HolzDbContext holzDBContext = dBFactory.CreateDbContext();
                    Partner supplier = holzDBContext.Partners.Find(data.PartnerId);
                    var parameters = new Dictionary<string, string>();
                    logSink.Action($"Printing Star-Label SignOn-Ticket  {data.Id}...");
                    parameters.Add("Gewicht", $"{data.FirstWeight:F0} kg");
                    parameters.Add("Zeit", data.FirstWeightCreatedAt.ToString("HH:mm:ss"));
                    parameters.Add("Datum", data.FirstWeightCreatedAt.ToString("dd.MM.yyyy"));
                    parameters.Add("Lieferant", supplier.SAPName1);
                    parameters.Add("MTId", string.Format("H{0:0000000}", data.Id));
                    PrintTemplate(template, parameters);
                }
            }
            catch (Exception ex)
            {
                logSink.Error($"Star-Label Ticket Ex", ex);
            }

        }

        /// <summary>
        /// Updates the server printer status
        /// </summary>
        void printer_PrinterStatus(PrinterFlags status, EventArgs e)
        {
            logSink.Action($"[PrinterStatus]: {status.ToString()}");
        }

    }
}
