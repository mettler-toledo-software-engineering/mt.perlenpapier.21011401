﻿using MT.TaskScript.Remoting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MT.Perlen.Holz.Abstractions
{
    [TaskScriptService]
    public interface ITerminalInterface
    {
        double SignOn(string barcode, string plate1, string force, out string name, out int status);
        double GetNextCarrier(double id, out string name, out string carrierId);
        double WoodWeight(double id, double gross, string carrierId, string mode);
        double MudWeigh(string barcode, double gross);
    }
}

   
