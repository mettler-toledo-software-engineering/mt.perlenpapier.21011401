﻿namespace MT.Perlen.Holz.Terminal.Logic
{
    public class Enums
    {
        public enum WaitOn
        {
            LogInInfo,
            InWeighInfo,
            GetNextCarrier,
            OutWeighInfo,
            MudWeighInfo,
            Reprint
        }

        public enum Outputs
        {
            Red = 1,
            Green = 2,
        }
    }
}