﻿using System;
using System.Terminal;

namespace MT.Perlen.Holz.Terminal.Logic
{
    public static class ReadLanguage
    {
        public static void ReadScaleMessage()
        {
            string line;
            int lineNr = 0;
            File scaleline = new File(1);
            scaleline.Open("\\storage card\\TaskExpert\\Data\\ScaleText.txt", FileAccess.Input);
            while (lineNr < 10) Program.ScaleText[lineNr++] = "";
            lineNr = 0;
            while (!scaleline.AtEndOfStream && lineNr < 10)
            {
                scaleline.ReadLine(out line);
                Program.ScaleText[lineNr] = line;
                lineNr++;

            }
            scaleline.Close();
        }
        public static void ReadScanMessage()
        {
            string line;
            int lineNr = 0;
            File scanline = new File(1);
            scanline.Open("\\storage card\\TaskExpert\\Data\\ScanText.txt", FileAccess.Input);
            while (lineNr < 10) Program.ScanText[lineNr++] = "";
            lineNr = 0;
            while (!scanline.AtEndOfStream && lineNr < 10)
            {
                scanline.ReadLine(out line);
                Program.ScanText[lineNr] = line;
                lineNr++;

            }
            scanline.Close();
        }

        public static void ReadFirstPlateMessage()
        {
            // string line;
            int lineNr = 0;
            //  File plateline = new File(1);
            //  plateline.Open("\\storage card\\TaskExpert\\Data\\FPlateText.txt", FileAccess.Input);
            while (lineNr < 10) Program.FPlateText[lineNr++] = "Kennzeichen Zugfahrzeug";
            //lineNr = 0;
            // while (!plateline.AtEndOfStream && lineNr < 10)
            // {
            //     plateline.ReadLine(out line);
            //     Program.FirstPlateText[lineNr] = line;
            //     lineNr++;

            // }
            // plateline.Close();
        }
        public static void ReadSecondPlateMessage()
        {
            // string line;
            int lineNr = 0;
            //  File plateline = new File(1);
            //  plateline.Open("\\storage card\\TaskExpert\\Data\\SPlateText.txt", FileAccess.Input);
            while (lineNr < 10) Program.SPlateText[lineNr++] = "Kennzeichen Anh" + String.Chr(228) + "nger";
            //lineNr = 0;
            // while (!plateline.AtEndOfStream && lineNr < 10)
            // {
            //     plateline.ReadLine(out line);
            //     Program.FirstPlateText[lineNr] = line;
            //     lineNr++;

            // }
            // plateline.Close();
        }
        public static void ReadLeaveMessage()
        {
            string line;
            int lineNr = 0;
            File plateline = new File(1);
            plateline.Open("\\storage card\\TaskExpert\\Data\\LeaveText.txt", FileAccess.Input);
            while (lineNr < 10) Program.LeaveText[lineNr++] = "";
            lineNr = 0;
            while (!plateline.AtEndOfStream && lineNr < 10)
            {
                plateline.ReadLine(out line);
                Program.LeaveText[lineNr] = line;
                lineNr++;

            }
            plateline.Close();
        }
    }
}
