﻿using System;
using System.Terminal;

namespace MT.Perlen.Holz.Terminal.Logic
{


    public static class Barcode
    {
        private static int turnScannerOnSequence = 0x12;
        private static int turnScannerOffSequence = 0x14;
        private const int SerialPortHandle = 0;
        private static string barcodeData;
        private static bool scanning;


        private static readonly SerialPort barcode = new SerialPort(SerialPortHandle, 1, AccessType.Write, 50, '\r', 3000, SerialPortFlags.FireEvent | SerialPortFlags.AutoFlush);

        public static void InitBarcode()
        {
            scanning = false;
            barcode.ClearInputBuffer();
            barcode.DataArrival += new DataArrivalEventHandler(barcode_DataArrival);
            barcode.Read(out barcodeData);
        }

        static void barcode_DataArrival()
        {
            if ((barcodeData.Right(1) == "\n") || (barcodeData.Right(1) == "\r"))
            {
                barcodeData = barcodeData.Substring(1, barcodeData.Length - 1);
            }
            if ((barcodeData.Right(1) == "\n") || (barcodeData.Right(1) == "\r"))
            {
                barcodeData = barcodeData.Substring(1, barcodeData.Length - 1);
            }
            if (barcodeData.Length == 11)
                Program.BarcodeData = barcodeData.Substring(2, 10);
            if (barcodeData.Length == 9)
                Program.BarcodeData = barcodeData.Substring(2, 8);

            barcode.ClearInputBuffer();
            barcode.Read(out barcodeData);
            scanning = false;
        }

        public static void StartScanning()
        {
            string start = String.Chr(22) + "T" + String.Chr(0x0d);
            if (!scanning)
            {
                barcode.Write(start);
                scanning = true;
            }
        }
        public static void StopScanning()
        {
            string stop = String.Chr(22) + "U" + String.Chr(0x0d);
            if (scanning)
            {
                barcode.Write(stop);
                scanning = false;
            }
        }
    }
}
