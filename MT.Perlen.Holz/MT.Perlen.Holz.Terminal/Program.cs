﻿using System;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;
using MT.Perlen.Holz.Terminal.Pages;

namespace MT.Perlen.Holz.Terminal
{
    static class Program
    {
        public static readonly TerminalInterfaceClient Server = new TerminalInterfaceClient(10);

        public const string programTitle = "MT.Perlen.Holz.Terminal";
        public const string version = "V1.0";

        /// <summary>
        /// Main entry point of your application
        /// </summary>
        [Share(SD.Application.DynamicString, 1)]
        public static string Message;
        [Share(SD.Application.DynamicString, 2)]
        public static string Message2;
        [Share(SD.Application.DynamicString, 3)]
        public static string Message3;
        [Share(SD.Application.DynamicString, 4)]
        public static string Message4;
        [Share(SD.Application.DynamicString, 5)]
        public static string HeartBeat;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 14)] // KC0114 Route Scale Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteScaleKeys;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 30)] // KC0130 Route Scale Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int ConsoleMode;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 10)] // KC0110 RouteKeypad Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteKeypadKeys;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 11)] // KC0111 Route AlphaNum Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteAlphaNumKeys;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 12)] // KC0112 Route Enter Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteEnterKey;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 13)] // KC0113 Route Nav Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteNavKeys;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 15)] // KC0115 Route Clear Keys (1=Control Panel, 3=Disabled, 4=TaskExpert)
        public static int RouteClearKeys;
        [Share(SD.DisplayAndKeyboard.KeyboardRoutingCommands, 19)] // KC0119 Softkey off
        public static int SoftkeyOff;
        [Share(SD.System.State, 52)] // XD0152 LastKey
        public static int LastKey;
        [Share(SD.System.State, 04)] 
        public static string TimeofDay;

       
       

        [Share(SD.Application.SetupString, 1)]
        public static string FirstRun;
        [Share(SD.Application.SetupString, 2)]
        public static string SupplierName;
        [Share(SD.Application.SetupString, 3)]
        public static string FPlate;
        [Share(SD.Application.SetupString, 4)]
        public static string SPlate;
        [Share(SD.Application.SetupString, 5)]
        public static string Force;
        [Share(SD.Application.SetupString, 6)]
        public static string CarrierName;
        [Share(SD.Application.SetupString, 7)]
        public static string CarrierNr;

        [Share(SD.Application.SetupInteger, 1)]
        public static int Minimalgewicht;
        [Share(SD.Application.SetupInteger, 2)]
        public static int WaitMode;
        [Share(SD.Application.SetupInteger, 3)]
        public static int Language;

        [Share(SD.Application.SetupDecimal, 1)]
        public static double Id;
        [Share(SD.Application.SetupDecimal, 2)]
        public static double AktWeight;

        [Share(SD.Application.ProcessString, 1, FireEvents = true)]
        public static string BarcodeData;


        public static string[] ScaleText = new string[15];
        public static string[] ScanText = new string[15];
        public static string[] FPlateText = new string[15];
        public static string[] SPlateText = new string[15];
        public static string[] LeaveText = new string[15];

        public static KeyCodes Key;
        public delegate bool Action();
        public static Action ActionPointer;


        static void Main()
        {

            if (FirstRun != version)
            {
                Minimalgewicht = 100;
                FirstRun = programTitle;
            }
            Input.InitKeyEvent();
            Input.KeyPress += new Input.KeyEventDelegate(Input_KeyPress);

            RuntimeScreen.Initialize();
            RouteScaleKeys = 4;
            ConsoleMode = 0;
            SplashScreen.ShowDialog(programTitle, version);
            ReadLanguage.ReadScaleMessage();
            ReadLanguage.ReadScanMessage();
            ReadLanguage.ReadFirstPlateMessage();
            ReadLanguage.ReadSecondPlateMessage();
            ReadLanguage.ReadLeaveMessage();

            BarcodeData = "";
            Barcode.InitBarcode();
            InOut.SetRed();

            ActionPointer = new Action(MainScreen.ShowDialog);
            while (!ActionPointer()) ;
        }

        static void Input_KeyPress()
        {
            Key = (KeyCodes)Input.InKey();
            Display.Popup(String.Chr((int) Key));
        }
    }
}
