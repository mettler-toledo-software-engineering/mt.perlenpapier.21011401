﻿using System;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Infrastructure
{
    public static class InOut
    {
        [Inline]
        private static void SetOut(Enums.Outputs o, bool state)
        {
           IO.Set(6,(int)o, state);
        }

        public static void SetRed()
        {
            SetOut(Enums.Outputs.Green, false);
            SetOut(Enums.Outputs.Red, true);
        }

        public static void SetGreen()
        {
            SetOut(Enums.Outputs.Red, false);
            SetOut(Enums.Outputs.Green, true);
        }
       
    }
}
