﻿using System;
using System.Terminal;

namespace MT.Perlen.Holz.Terminal.Infrastructure
{
    public static class Scale
    {
        [Share(SD.Scale.Commands, 4)]
        public static int SetZero;
        [Share(SD.Scale.Commands, 1)]
        public static int TareScale;
        [Share(SD.Scale.Commands, 2)]
        public static int clrTareScale;
        [Share(SD.Scale.Commands, 3)] // print command
        public static int PrintRequest;

        [Share(SD.Scale.ProcessData, 2)]
        private static double tareWeight;

        [Share(SD.Scale.Status, 1)]
        public static int TareStatus;
        [Share(SD.Scale.Status, 3)] // print command
        public static int PrintStatus;
        [Share(SD.Scale.Status, 4)]
        public static int ZeroStatus;
        [Share(SD.Scale.Status, 31)]
        public static bool InMotion;
        [Share(SD.Scale.Status, 32)]
        public static int CenterOfZero;
        [Share(SD.Scale.Status, 33)]
        public static int OverCapacity;
        [Share(SD.Scale.Status, 34)]
        public static int UnderZero;
        [Share(SD.Scale.Status, 35)]
        public static int NetMode;
        [Share(SD.Scale.Status, 38)]
        public static int WeightValid;
        [Share(SD.Scale.Calibration, 5)]
        public static double IncrementSize;

        [Share(SD.Scale.DynamicWeight, 17)]
        public static double GrossWeight;
        [Share(SD.Scale.DynamicWeight, 18)]
        public static double NetWeight;
        [Share(SD.Scale.DynamicWeight, 1)] // wt0101
        public static string WgtToSave;
        [Share(SD.Scale.ProcessData, 13)]
        public static double LastDemandGrossWeight;

        public static void ClearTare()
        {

            if (tareWeight > 0)
                clrTareScale = 1;
        }
    }
}
