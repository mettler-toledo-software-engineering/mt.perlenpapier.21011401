﻿using System;
using System.IO.Pipes;
using System.Linq;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Pages
{
    public static class WaitScreen
    {

        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 8, "AK0101", "!60", Fonts.Normal16, Colors.Black);
        /// <summary>
        /// Stores the sharedDataLabel2 control
        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 30, "AK0102", "!60", Fonts.Normal16, Colors.Black);

        private static readonly SharedDataLabel<string> sharedDataLabel3 = new SharedDataLabel<string>(3, 0, 52, "AK0103", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel4 = new SharedDataLabel<string>(4, 0, 74, "AK0104", "!60", Fonts.Normal16, Colors.Black);

        static int key;

        //bool running = true;
        static string t = "";
        static int n = 0;
        static double endTime = 0;
        private static string lastBarcode;

        public static bool ShowDialog()
        {
            string param1 = "";
            int param2 = 0;
            double id = 0;

            InitializeComponents();
            MainScreen.WriteMessage("Bitte warten.....", "");
            lastBarcode = Program.BarcodeData;
            switch (Program.WaitMode)
            {
                case (int)Enums.WaitOn.LogInInfo:
                    //   Display.Popup("Grüezi", Program.Server.LastCustomErrorMessage, Program.BarcodeData);
                    id = Program.Server.SignOn(lastBarcode, Program.FPlate, Program.Force, out param1, out param2, Wait);
                    break;
                case (int)Enums.WaitOn.GetNextCarrier:
                    id = Program.Server.GetNextCarrier(Program.Id, out Program.CarrierName, out Program.CarrierNr, Wait);
                    Program.BarcodeData = "";
                    break;
                case (int)Enums.WaitOn.InWeighInfo:
                    //Display.Popup("In", Program.Server.LastCustomErrorMessage, lastBarcode);
                    id = Program.Server.WoodWeight(Program.Id, Program.AktWeight, Program.CarrierNr, "I", Wait);
                    Program.BarcodeData = "";
                    break;
                case (int)Enums.WaitOn.OutWeighInfo:
                    //Display.Popup("Out", Program.Server.LastCustomErrorMessage, lastBarcode);
                    id = Program.Server.WoodWeight(Program.Id, Program.AktWeight, Program.CarrierNr, "O", Wait);
                    Program.BarcodeData = "";
                    break;
                case (int)Enums.WaitOn.Reprint:
                    id = Program.Server.WoodWeight(Program.Id, 0, Program.CarrierNr, "R", Wait);
                    break;
                case (int)Enums.WaitOn.MudWeighInfo:
                    id = Program.Server.MudWeigh(Program.BarcodeData.Substring(2), Program.AktWeight, Wait);
                    break;
            }

            if (Program.Server.LastError == -1)
            {
                Display.Popup("Server Error", Program.Server.LastCustomErrorMessage, "TimeOut, bitte Mitarbeiter rufen!");
                Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                Program.BarcodeData = "";
            }
            else
            {
                if (id > 0)
                {
                    switch (Program.WaitMode)
                    {
                        case (int)Enums.WaitOn.LogInInfo:
                            Program.Id = id;
                            Program.Language = 0;
                            Program.SupplierName = param1;
                            Program.WaitMode = (int)Enums.WaitOn.GetNextCarrier;
                            break;
                        case (int)Enums.WaitOn.GetNextCarrier:
                                Program.WaitMode = (int)Enums.WaitOn.InWeighInfo;
                            break;
                        case (int)Enums.WaitOn.InWeighInfo:
                            InOut.SetGreen();
                            Program.ActionPointer = new Program.Action(LeaveScreen.ShowDialog);
                            break;
                        case (int)Enums.WaitOn.OutWeighInfo:
                            InOut.SetGreen();
                            Program.ActionPointer = new Program.Action(LeaveScreen.ShowDialog);
                            break;
                        case (int)Enums.WaitOn.Reprint:
                            InOut.SetGreen();
                            Program.ActionPointer = new Program.Action(LeaveScreen.ShowDialog);
                            break;
                        case (int)Enums.WaitOn.MudWeighInfo:
                            InOut.SetGreen();
                            Program.ActionPointer = new Program.Action(LeaveScreen.ShowDialog);
                            break;
                    }
                }
                else if (id == -1)  // Fehler
                {
                    switch (Program.WaitMode)
                    {
                        case (int)Enums.WaitOn.LogInInfo:
                            if (param2 == 0)  // Fehler
                            {
                                Display.Popup("Problem:", param1);
                                Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                                Program.BarcodeData = "";
                            }
                            if (param2 == 1)  // gleiche Bestellnummer
                            {
                                // Display.Popup("Information:", param1);
                                MainScreen.WriteMessageLine1(param1);
                                Program.ActionPointer = new Program.Action(EnterYN.ShowDialog);
                            }
                            break;
                        case (int)Enums.WaitOn.InWeighInfo:
                            Display.Popup("Problem", "Vorgang wurde nicht erfasst. Bitte Mitarbeiter rufen!");
                            Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                            Program.BarcodeData = "";
                            break;
                        case (int)Enums.WaitOn.OutWeighInfo:
                            Display.Popup("Problem", "Vorgang wurde nicht erfasst. Bitte Mitarbeiter rufen!");
                            Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                            Program.BarcodeData = "";
                            break;
                    }
                }
                else
                {
                    Display.Popup("SAP-Fehler", "Bitte Mitarbeiter rufen!", param1);
                    Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                    Program.BarcodeData = "";
                }
            }

            return false;
        }

        public static bool Wait()
        {
            key = RuntimeScreen.ReadKey();
            if ((key == 100) || (key == 37))
            {
                Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
            }

            if (endTime < DeviceTimer.GetTicks())
            {
                n = n % 10 + 1;
                if (n == 1) t = "";
                t = t + "=> ";
                MainScreen.WriteMessage("Bitte warten.....", t);
                endTime = 0.5 + DeviceTimer.GetTicks();
            }

            return true;
        }


        private static void InitializeComponents()
        {

            Display.Clear();
            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            sharedDataLabel3.Draw();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);

            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 100, "\\exit.bmp");
            SoftKeys.Replace();
        }


    }
}
