﻿using System;
using System.Management.Instrumentation;
using System.Reflection;
using System.Security.Cryptography;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Pages
{
    public static class EnterFPlateWood
    {


        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 8, "AK0101", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 30, "AK0102", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel3 = new SharedDataLabel<string>(3, 0, 52, "AK0103", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel4 = new SharedDataLabel<string>(4, 0, 74, "AK0104", "!60", Fonts.Normal16, Colors.Black);


        public static bool ShowDialog()
        {
            bool running = true;
            string inText;
            bool x;
            int key;
            double endTime;

            endTime = 0;
            x = true;
            InitializeComponents();
            inText = "";
            MainScreen.WriteMessageLine1("Perlen Papier AG");
            MainScreen.WriteMessageLine4(inText);
            Program.RouteAlphaNumKeys = 4;
            Program.RouteKeypadKeys = 4; // zu TE
            Program.RouteEnterKey = 4;
            Program.RouteNavKeys = 4;
            Program.RouteClearKeys = 4;
            InOut.SetRed();
            do
            {
                key = RuntimeScreen.ReadKey();

                if ((key == 123) || (key == 37))    // F1 oder <-
                {
                    Program.BarcodeData = "";
                    Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                    running = false;
                }

                if ((key == 127) || (key == 13))     // F5 oder CR
                {
                    if (inText.Length < 3)
                        Display.Popup("Fehler", "Kennzeichen fehlt");
                    else
                    {

                        while (Scale.InMotion) ;
                        Program.AktWeight = Double.Parse(Scale.WgtToSave);
                        Program.FPlate = inText;
                        Program.SPlate = "";
                        Program.Force = "";
                        Program.WaitMode = (int)Enums.WaitOn.LogInInfo;
                        Program.ActionPointer = new Program.Action(WaitScreen.ShowDialog);
                        running = false;
                    }
                }
                if (((key > 0x2F) && (key < 0x7b)) || (key == 0x20))// 0-9 a-z A-Z ' '
                {
                    inText += String.Chr(key);
                    MainScreen.WriteMessageLine4(inText);
                }

                if ((key == 8) && (inText.Length > 0))
                {
                    inText = inText.Substring(1, inText.Length - 1);
                    MainScreen.WriteMessageLine4(inText);
                }


                if (endTime < DeviceTimer.GetTicks())
                {
                    x = !x;
                    if (x)
                        MainScreen.WriteMessage("Kennzeichen Zugfahrzeug", "mit <ENTER> abschliessen");
                    else
                        MainScreen.WriteMessage("Kennzeichen Zugfahrzeug", "mit <= abbrechen");

                    endTime = 0.6 + DeviceTimer.GetTicks();
                }

            } while (running);
            Barcode.StopScanning();
            return false;
        }

        private static void InitializeComponents()
        {

            Display.Clear();
            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            sharedDataLabel3.Draw();
            sharedDataLabel4.Draw();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);

            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 123, "\\exit.bmp");
            SoftKeys.Define(Keys.SoftKey5, 127, "\\ok.bmp");
            SoftKeys.Replace();
        }
    }
}
