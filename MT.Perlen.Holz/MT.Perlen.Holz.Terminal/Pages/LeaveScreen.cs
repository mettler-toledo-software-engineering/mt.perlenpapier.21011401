﻿using System;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Pages
{
    public static class LeaveScreen
    {

        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 10, "AK0101", "!60", Fonts.Normal16, Colors.Black);
        /// <summary>
        /// Stores the sharedDataLabel2 control
        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 35, "AK0102", "!60", Fonts.Normal16, Colors.Black);



        public static bool ShowDialog()
        {
            bool running = true;
            int key;
            int textCycle = -1;
            double endTime;

            endTime = 0;
            InitializeComponents();
            MainScreen.WriteMessageLine1( "Perlen Papier AG");
            do
            {
                key = RuntimeScreen.ReadKey();

                if (key == 100)
                {
                    Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                    InOut.SetRed();
                    running = false;
                }

                if (key == 102)
                {
                    // reprint
                    Program.WaitMode = (int)Enums.WaitOn.Reprint;
                    Program.ActionPointer = new Program.Action(WaitScreen.ShowDialog);
                    running = false;
                }

                if (Scale.GrossWeight < (0.5 * Program.AktWeight))
                {
                    Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                    InOut.SetRed();
                    running = false;
                }

                if (endTime < DeviceTimer.GetTicks())
                {
                    textCycle++;
                    if (Program.LeaveText[textCycle].Length == 0)
                        textCycle = 0;
                    MainScreen.WriteMessage(Program.LeaveText[textCycle],"");
                    endTime = 0.6 + DeviceTimer.GetTicks();
                }

            } while (running);

            return false;
        }

        private static void InitializeComponents()
        {

            Display.Clear();
            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);

            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 100, "\\Exit.bmp");
            SoftKeys.Define(Keys.SoftKey3, 102, "\\report.bmp");
            SoftKeys.Replace();
        }

    }
}
