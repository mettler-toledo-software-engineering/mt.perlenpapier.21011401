﻿using System;
using System.Management.Instrumentation;
using System.Reflection;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Pages
{
    public static class MainScreen
    {

        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 8, "AK0101", "!60", Fonts.Normal16, Colors.Black);
        /// <summary>
        /// Stores the sharedDataLabel2 control
        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 30, "AK0102", "!60", Fonts.Normal16, Colors.Black);

        private static readonly SharedDataLabel<string> sharedDataLabel3 = new SharedDataLabel<string>(3, 0, 52, "AK0103", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel4 = new SharedDataLabel<string>(4, 0, 74, "AK0104", "!60", Fonts.Normal16, Colors.Black);




        public static bool ShowDialog()
        {
            bool running = true;
            int key;
            int textCycle = -1;
            double endTime;
            double nextBeat;

            endTime = 0;
            nextBeat = 0;
            InitializeComponents();
            Program.RouteAlphaNumKeys = 4; // zu TE
            Program.RouteKeypadKeys = 4; // zu TE
            Program.RouteEnterKey = 4;
            Program.RouteNavKeys = 4;
            Program.RouteClearKeys = 4;
            MainScreen.WriteMessageLine1("Perlen Papier AG");
            MainScreen.WriteMessageLine4("");
            //  Program.SoftkeyOff = 1;
            InOut.SetRed();
            do
            {
                key = RuntimeScreen.ReadKey();

                if ((key == 100) || (key == 13))// F1
                {
                    Program.ActionPointer = new Program.Action(ManEntry.ShowDialog);
                    running = false;
                }

                if (Program.RouteAlphaNumKeys == 1) // Pfeil und ENTER Taste
                {
                    Program.RouteAlphaNumKeys = 4;
                    if (Program.LastKey == 13) // ENTER
                    {
                        Program.ActionPointer = new Program.Action(ManEntry.ShowDialog);
                        running = false;
                    }
                }

                if (Program.BarcodeData.Length > 0)
                {
                    if (Program.BarcodeData.IndexOf("?") == 0)
                    {
                        if (Program.BarcodeData == "248163264") // F1
                        {
                            Environment.Exit();
                        }

                        if (Program.BarcodeData.Length == 10) // Barcode stammt von LS
                        {
                            Program.ActionPointer = new Program.Action(EnterFPlateWood.ShowDialog);
                            running = false;
                        }
                        else if (Program.BarcodeData.Length == 9) // Barcode Schlamm
                        {
                            //Program.WaitMode = (int)Enums.WaitOn.LogInInfo;
                            //Program.ActionPointer = new Program.Action(WaitScreen.ShowDialog);
                            //Program.ActionPointer = new Program.Action(EnterFPlate.ShowDialog);
                            //running = false;
                        }
                        else if (Program.BarcodeData.Length == 8) // Barcode stammt von MT
                        {
                            // Rückwaage
                            while (Scale.InMotion) ;
                            Program.AktWeight = Double.Parse(Scale.WgtToSave);
                            Program.Id = double.Parse(Program.BarcodeData.Substring(2, 7));
                            Program.WaitMode = (int)Enums.WaitOn.OutWeighInfo;
                            Program.ActionPointer = new Program.Action(WaitScreen.ShowDialog);
                            running = false;
                        }
                        else
                        {
                            Display.Popup("Fehler", "Barcode unbekannt!", Program.BarcodeData);
                            Program.BarcodeData = "";
                        }
                    }
                    else
                        Program.BarcodeData = "";
                }

                if (Scale.GrossWeight > 79 && running)
                    Barcode.StartScanning();
                else
                    Barcode.StopScanning();


                if (endTime < DeviceTimer.GetTicks())
                {
                    textCycle++;
                    if (Program.ScaleText[textCycle].Length == 0)
                        textCycle = 0;
                    if (Scale.GrossWeight < 100)
                        WriteMessage(Program.ScaleText[textCycle], " ");
                    else
                        WriteMessage(Program.ScanText[textCycle], "<ENTER> = Handeingabe");
                    endTime = 0.6 + DeviceTimer.GetTicks();
                   
                    if (nextBeat < DeviceTimer.GetTicks())
                    {
                        nextBeat = 30 + DeviceTimer.GetTicks();
                        Program.HeartBeat = Program.TimeofDay;
                    }
                }

            } while (running);

            Barcode.StopScanning();
            return false;
        }

        private static void InitializeComponents()
        {

            Display.Clear();
            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            sharedDataLabel3.Draw();
            sharedDataLabel4.Draw();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare,
                WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);

            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 100, "\\edit.bmp");
            SoftKeys.Replace();
            //     Display.SetFocusList(sharedDataLabel1.Id, sharedDataLabel2.Id);
        }

        public static void WriteMessage(string line2, string line3)
        {
            string a = line2.TrimStart().TrimEnd();
            string b = line3.TrimStart().TrimEnd();

            int i = a.Length / 2;
            Program.Message2 = String.Space(30 - i) + a;
            i = b.Length / 2;
            Program.Message3 = String.Space(30 - i) + b;

        }

        public static void WriteMessageLine4(string line4)
        {
            string a = line4.TrimStart().TrimEnd();
            int i = a.Length / 2;
            Program.Message4 = String.Space(30 - i) + a;

        }
        public static void WriteMessageLine1(string line1)
        {
            string a = line1.TrimStart().TrimEnd();
            int i = a.Length / 2;
            Program.Message = String.Space(30 - i) + a;

        }
    }
}
