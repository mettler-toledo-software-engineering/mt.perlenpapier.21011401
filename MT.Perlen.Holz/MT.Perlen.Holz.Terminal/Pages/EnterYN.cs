﻿using System;
using System.Management.Instrumentation;
using System.Reflection;
using System.Security.Cryptography;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Infrastructure;
using MT.Perlen.Holz.Terminal.Logic;

namespace MT.Perlen.Holz.Terminal.Pages
{
    public static class EnterYN
    {

        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 8, "AK0101", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 30, "AK0102", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel3 = new SharedDataLabel<string>(3, 0, 52, "AK0103", "!60", Fonts.Normal16, Colors.Black);
        private static readonly SharedDataLabel<string> sharedDataLabel4 = new SharedDataLabel<string>(4, 0, 74, "AK0104", "!60", Fonts.Normal16, Colors.Black);


        public static bool ShowDialog()
        {
            bool running = true;
            bool x = true;
            int key;
            double endTime;

            endTime = 0;
            x = true;
            InitializeComponents();

            MainScreen.WriteMessageLine4("");
            Program.RouteAlphaNumKeys = 4; // zu TE
            Program.RouteEnterKey = 4;
            Program.RouteNavKeys = 4;
            Program.RouteClearKeys = 4;
            do
            {
                key = RuntimeScreen.ReadKey();

                if ((key == 123) || (key == 13) || (key == 'n') || (key == 'N'))    // F1
                {
                    Program.BarcodeData = "";
                    Program.ActionPointer = new Program.Action(MainScreen.ShowDialog);
                    running = false;
                }

                if ((key == 127) || (key == 'Y') || (key == 'y') || (key == 'O') || (key == 'o')
                    || (key == 'S') || (key == 's') || (key == 'J') || (key == 'j'))     // F5 oder CR oder Ja
                {
                    Program.Force = "X";
                    Program.WaitMode = (int)Enums.WaitOn.LogInInfo;
                    Program.ActionPointer = new Program.Action(WaitScreen.ShowDialog);
                    running = false;
                }

                if (endTime < DeviceTimer.GetTicks())
                {
                    x = !x;
                    if (x)
                        MainScreen.WriteMessage("Nein / Non / No / No", "<N> / <ENTER>");
                    else
                        MainScreen.WriteMessage("Ja / Oui / Si  / Yes", "<J> / <O> / <S> / <Y>");

                    endTime = 0.6 + DeviceTimer.GetTicks();
                }

            } while (running);

            return false;
        }

        private static void InitializeComponents()
        {

            Display.Clear();
            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            sharedDataLabel3.Draw();
            sharedDataLabel4.Draw();
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);

            // Update softkeys
            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey1, 123, "\\exit.bmp");
            SoftKeys.Define(Keys.SoftKey5, 127, "\\ok.bmp");
            SoftKeys.Replace();
        }
    }
}
