﻿using System;
using System.Terminal;
using MT.Perlen.Holz.Terminal.Pages;

namespace MT.Perlen.Holz.Terminal
{
    public static class SplashScreen
    {

        private static readonly SharedDataLabel<string> sharedDataLabel1 = new SharedDataLabel<string>(1, 0, 10, "AK0101", "!40", Fonts.Normal16, Colors.Black);
        /// <summary>
        /// Stores the sharedDataLabel2 control
        /// </summary>					
        private static readonly SharedDataLabel<string> sharedDataLabel2 = new SharedDataLabel<string>(2, 0, 35, "AK0102", "!40", Fonts.Normal16, Colors.Black);

        public static void ShowDialog(string productName, string version)
        {
            double timeOut;
            Display.SetupWeightDisplay(WeightDisplayVisibility.On, WeightDisplayScale.Active, WeightDisplayTare.Tare, WeightDisplayCompress.Uncompress, WeightDisplayScaleSize.Large, WeightDisplayScaleSize.Small);
            Display.Clear();

            sharedDataLabel1.Draw();
            sharedDataLabel2.Draw();
            MainScreen.WriteMessage(productName + " " + version, "Mettler Toledo ©2021 ");
           

            SoftKeys.Clear();
            SoftKeys.Define(Keys.SoftKey5, "\\AppExit.bmp");
            SoftKeys.Replace();

            timeOut = 2.0 + DeviceTimer.GetTicks();
            do
            {
                if (Program.Key == KeyCodes.Softkey05)
                {
                    Environment.Exit();
                }
            } while (timeOut > DeviceTimer.GetTicks());
            MainScreen.WriteMessage( " " , " ");


        }
    }
}
